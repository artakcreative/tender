$(document).ready(function () {
    let mounts = [
        'Հունվար',
        'Փետրվար',
        'Մարտ',
        'Ապրիլ',
        'Մայիս',
        'Հունիս',
        'Հուլիս',
        'Օգոստոս',
        'Սեպտեմբեր',
        'Հոկտեմբեր',
        'Նոյեմբեր',
        'Դեկտեմբեր',
    ];

    let weeks = [
        '1-Հունվար', '2-Հունվար', '3-Հունվար', '4-Հունվար',
        '1-Փետրվար','2-Փետրվար','3-Փետրվար','4-Փետրվար',
        '1-Մարտ','2-Մարտ','3-Մարտ','4-Մարտ',
        '1-Ապրիլ','2-Ապրիլ','3-Ապրիլ','4-Ապրիլ',
        '1-Մայիս','2-Մայիս','3-Մայիս','4-Մայիս',
        '1-Հունիս','2-Հունիս','3-Հունիս','4-Հունիս',
        '1-Հուլիս','2-Հուլիս','3-Հուլիս','4-Հուլիս',
        '1-Օգոստոս','2-Օգոստոս','3-Օգոստոս','4-Օգոստոս',
        '1-Սեպտեմբեր','2-Սեպտեմբեր','3-Սեպտեմբեր','4-Սեպտեմբեր',
        '1-Հոկտեմբեր','2-Հոկտեմբեր','3-Հոկտեմբեր','4-Հոկտեմբեր',
        '1-Նոյեմբեր',  '2-Նոյեմբեր',  '3-Նոյեմբեր',  '4-Նոյեմբեր',
        '1-Դեկտեմբեր','2-Դեկտեմբեր','3-Դեկտեմբեր','4-Դեկտեմբեր', '4-Դեկտեմբեր',
    ];

    let quarters = [
        'Առաջին եռամսյակ',
        'Երկրորդ եոամսյակ',
        'Երրորդ եոամսյակ',
        'Չորորդ եոամսյակ'
    ];



    var label = [];
    var infoUser = [];

    //default
    mounts.forEach(function (item) {
        label.push(item,)
    });
    infoUser.push({
        label: 'Ակտիվություն',
        backgroundColor: 'rgb(255,126,12)',
        borderColor: 'rgb(255,126,12)',
        data: [2,3,5,6,7,8,9,3,1,2,6,2],
    });


    // Users

    $('.start-user_date').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                maxDate: $('.end-user_date').val()? $('.end-user_date').val():false
            })
        },
        timepicker:false
    });
    $('.end-user_date').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                minDate: $('.start-user_date').val() ? $('.start-user_date').val():false
            })
        },
        timepicker:false
    });

    $('.periodicity').prepend('<option selected></option>').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել ժամանակահատված",
        "language": {
            "noResults": function () {
                return "Չի գտնվել";
            },
            searching: function () {
                return "Փնտրում...";
            },
            errorLoading: function () {
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
    });


    $('.selectUser').prepend('<option selected></option>').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել օգտատիրոջը",
        "language": {
            "noResults": function(){
                return "Օգտատեր չի գտնվել";
            },
            searching: function() {
                return "Փնտրում...";
            },
            errorLoading:function(){
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
        ajax: {
            url: '/admin/statistics/users/data',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    search: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        let info;
                        if(item.name != null || item.surname != null ) {
                            info = item.full_name ;
                        } else {
                            info = item.email
                        }
                        return {
                            text: info,
                            id: item.id
                        }
                    })
                };
            }
        }

    });


    $('.filter_user-static').on('click', function () {
        $('.error-range_user').css('display', 'none');
        if(!$("#selectUserStatistic").val() || !$('.end-user_date').val() || !$('.start-user_date').val() ||  !$(".periodicity").val()) {
            $('.error-range_user').css('display', 'block');
        }else{
            $.ajax({
                type: 'GET',
                url: '/admin/statistics/users/statistic',
                data: {
                    id: $("#selectUserStatistic").val(),
                    start: $('.start-user_date').val(),
                    end: $('.end-user_date').val(),
                    type: $(".periodicity").val()
                },
                success: function (res) {
                    infoUser.pop();
                    label.splice(0, label.length)
                    eval(res[1]).forEach(function (item) {
                        label.push(item,)
                    });
                    infoUser.push({
                        label: 'Ակտիվություն',
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)',
                        data: res[0],
                    });
                    chartUsers.update();

                },
                errors: function (res) {
                    console.log(res)
                }
            });
        }
    });

        const data = {
            labels: label,
            datasets: infoUser
        };

        const config = {
            type: 'bar',
            data: data,
            options: {}
        };

        let chartUsers = new Chart(
            $("#userStatistic"),
            config
        );





});
