$(document).ready(function () {
    let mounts = [
        'Հունվար',
        'Փետրվար',
        'Մարտ',
        'Ապրիլ',
        'Մայիս',
        'Հունիս',
        'Հուլիս',
        'Օգոստոս',
        'Սեպտեմբեր',
        'Հոկտեմբեր',
        'Նոյեմբեր',
        'Դեկտեմբեր',
    ];

    let weeks = [
        '1-Հունվար', '2-Հունվար', '3-Հունվար', '4-Հունվար',
        '1-Փետրվար','2-Փետրվար','3-Փետրվար','4-Փետրվար',
        '1-Մարտ','2-Մարտ','3-Մարտ','4-Մարտ',
        '1-Ապրիլ','2-Ապրիլ','3-Ապրիլ','4-Ապրիլ',
        '1-Մայիս','2-Մայիս','3-Մայիս','4-Մայիս',
        '1-Հունիս','2-Հունիս','3-Հունիս','4-Հունիս',
        '1-Հուլիս','2-Հուլիս','3-Հուլիս','4-Հուլիս',
        '1-Օգոստոս','2-Օգոստոս','3-Օգոստոս','4-Օգոստոս',
        '1-Սեպտեմբեր','2-Սեպտեմբեր','3-Սեպտեմբեր','4-Սեպտեմբեր',
        '1-Հոկտեմբեր','2-Հոկտեմբեր','3-Հոկտեմբեր','4-Հոկտեմբեր',
        '1-Նոյեմբեր',  '2-Նոյեմբեր',  '3-Նոյեմբեր',  '4-Նոյեմբեր',
        '1-Դեկտեմբեր','2-Դեկտեմբեր','3-Դեկտեմբեր','4-Դեկտեմբեր', '4-Դեկտեմբեր',
    ];

    let quarters = [
        'Առաջին եռամսյակ',
        'Երկրորդ եոամսյակ',
        'Երրորդ եոամսյակ',
        'Չորորդ եոամսյակ'
    ];


    var label = [];
    var infoAnnouncement = [];


    //default
    mounts.forEach(function (item) {
        label.push(item,)
    });
    infoAnnouncement.push({
        type: 'bar',
        label: 'Օրինակ-1',
        backgroundColor: 'rgb(34,43,255)',
        borderColor: 'rgb(36,60,255)',
        data: [9,3,5,6,2,2,9,3,1,2,9,2],
    }, {
        type: 'bar',
        label: 'Օրինակ-2',
        backgroundColor: 'rgb(34,132,25)',
        borderColor: 'rgb(31,120,24)',
        data: [2,1,3,3,9,2,6,3,1,3,0,6],
    });

    $('.start-announcement_date').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                maxDate: $('.end-announcement_date').val()? $('.end-announcement_date').val():false
            })
        },
        timepicker:false
    });
    $('.end-announcement_date').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                minDate: $('.start-announcement_date').val() ? $('.start-announcement_date').val():false
            })
        },
        timepicker:false
    });


    $('.periodicity').prepend('<option selected></option>').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել ժամանակահատված",
        "language": {
            "noResults": function () {
                return "Չի գտնվել";
            },
            searching: function () {
                return "Փնտրում...";
            },
            errorLoading: function () {
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
    });

    $('.selectAnnouncement').prepend('<option selected></option>').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել հայտարարությունը",
        "language": {
            "noResults": function(){
                return "Հայտարարություն չի գտնվել";
            },
            searching: function() {
                return "Փնտրում...";
            },
            errorLoading:function(){
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
    });


    $('.filter_announcement-static').on('click', function () {
        $('.error-range_announcement').css('display', 'none');
        if(!$("#selectAnnouncementStatistic").val() || !$('.end-announcement_date').val() || !$('.start-announcement_date').val() || !$('.periodicity').val()) {
            $('.error-range_announcement').css('display', 'block');
        }else{
            $.ajax({
                type: 'GET',
                url: '/admin/statistics/announcements/statistic',
                data: {
                    name: $("#selectAnnouncementStatistic").val(),
                    start: $('.start-announcement_date').val(),
                    end: $('.end-announcement_date').val(),
                    type: $('.periodicity').val(),
                },
                success: function (res) {
                    infoAnnouncement.splice(0, infoAnnouncement.length);
                    label.splice(0, label.length)
                    eval(res[1]).forEach(function (item) {
                        label.push(item,)
                    });
                    if(res[0]) {
                        $.each( res[0], function( key, value ) {
                            let r = Math. round (Math. random () * 255);
                            let g = Math. round (Math. random () * 255);
                            let b = Math. round (Math. random () * 255);
                            infoAnnouncement.push({
                                type: 'bar',
                                label:  value.label,
                                data:  value.data,
                                borderColor:  "rgb(" + r+ ", " + g + ", " + b + ")",
                                backgroundColor: "rgb(" + r+ ", " + g + ", " + b + ")",
                            },);
                        });
                    }
                    chartAnnouncement.update();
                },
                errors: function (res) {
                    console.log(res)
                }
            });
        }
    });

    const dataAnnouncement = {
        labels: label,
        datasets: infoAnnouncement
    };


    const configAnnouncement = {
        type: 'line',
        data: dataAnnouncement,
        options: {}
    };

    let chartAnnouncement = new Chart(
        $("#аnnouncementStatistic"),
        configAnnouncement
    );

})
