//Քարտեզ
$(document).ready(function () {
    $('.place_of_delivery').on('click', function(){
        $('#map').removeClass('d-none');
    })

    // map initialization
    ymaps.ready(init);
    function init() {
            let map = new ymaps.Map("map", {
                center: [40.177628, 44.512555],
                zoom: 10,
                controls: ['zoomControl'],
                behaviors: ['drag']
            });

    // map initialization

        // adding place-mark
            let placemark = new ymaps.Placemark([40.177628, 44.512555], {
            })
            map.geoObjects.add(placemark)
        // adding place-mark

        $( "#place_of_delivery").on('keyup', function(e){
            myMap();
        });
        //area search
        function myMap() {
            var searchControl = new ymaps.control.SearchControl({
                options: {
                    provider: 'yandex#search',
                    noPopup: false,
                    controls:[]
                }
            });
            map.controls.add(searchControl);
            searchControl.search($('.place_of_delivery').val());
            $('.ymaps-2-1-79-controls__control_toolbar').addClass('d-none');

            // get latitude and longitude and put it into inputs
            var myGeocoder = ymaps.geocode($('.place_of_delivery').val());
            myGeocoder.then(
                function (res) {
                    var latitude = res.geoObjects.get(0).geometry.getCoordinates()[0];
                    var longitude = res.geoObjects.get(0).geometry.getCoordinates()[1];

                    $('.latitude').val(latitude);
                    $('.longitude').val(longitude);
                },
                function (err) {
                    return false
                }
            );
        }


        // get latitude and longitude and put it into inputs

        // search tooltips
            var suggestView1 = new ymaps.SuggestView('place_of_delivery');
        // search tooltips
    }
})
//Քարտեզ ավարտ
