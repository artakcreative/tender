$(document).ready(function () {
    let mounts = [
        'Հունվար',
        'Փետրվար',
        'Մարտ',
        'Ապրիլ',
        'Մայիս',
        'Հունիս',
        'Հուլիս',
        'Օգոստոս',
        'Սեպտեմբեր',
        'Հոկտեմբեր',
        'Նոյեմբեր',
        'Դեկտեմբեր',
    ];

    let weeks = [
        '1-Հունվար', '2-Հունվար', '3-Հունվար', '4-Հունվար',
        '1-Փետրվար','2-Փետրվար','3-Փետրվար','4-Փետրվար',
        '1-Մարտ','2-Մարտ','3-Մարտ','4-Մարտ',
        '1-Ապրիլ','2-Ապրիլ','3-Ապրիլ','4-Ապրիլ',
        '1-Մայիս','2-Մայիս','3-Մայիս','4-Մայիս',
        '1-Հունիս','2-Հունիս','3-Հունիս','4-Հունիս',
        '1-Հուլիս','2-Հուլիս','3-Հուլիս','4-Հուլիս',
        '1-Օգոստոս','2-Օգոստոս','3-Օգոստոս','4-Օգոստոս',
        '1-Սեպտեմբեր','2-Սեպտեմբեր','3-Սեպտեմբեր','4-Սեպտեմբեր',
        '1-Հոկտեմբեր','2-Հոկտեմբեր','3-Հոկտեմբեր','4-Հոկտեմբեր',
        '1-Նոյեմբեր',  '2-Նոյեմբեր',  '3-Նոյեմբեր',  '4-Նոյեմբեր',
        '1-Դեկտեմբեր','2-Դեկտեմբեր','3-Դեկտեմբեր','4-Դեկտեմբեր', '4-Դեկտեմբեր',
    ];

    let quarters = [
        'Առաջին եռամսյակ',
        'Երկրորդ եոամսյակ',
        'Երրորդ եոամսյակ',
        'Չորորդ եոամսյակ'
    ];


    var label = [];
    var infoManager = [];

    //default
    mounts.forEach(function (item) {
        label.push(item,)
    });
    infoManager.push({
        type: 'bar',
        label: 'Օրինակ-1',
        backgroundColor: 'rgb(255, 25, 132)',
        borderColor: 'rgb(255, 25, 120)',
        data: [2,3,5,6,7,8,9,3,1,2,6,2],
    }, {
        type: 'bar',
        label: 'Օրինակ-2',
        backgroundColor: 'rgb(34,132,25)',
        borderColor: 'rgb(31,120,24)',
        data: [0,1,3,3,9,2,6,3,1,3,0,4],
    });

    // Managers

    $('.start-manager_date').datetimepicker({
        format: 'Y/m/d',
        scrollMonth: false,
        scrollInput: false,
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('.end-manager_date').val() ? $('.end-manager_date').val() : false
            })
        },
        timepicker: false
    });
    $('.end-manager_date').datetimepicker({
        format: 'Y/m/d',
        scrollMonth: false,
        scrollInput: false,
        onShow: function (ct) {
            this.setOptions({
                minDate: $('.start-manager_date').val() ? $('.start-manager_date').val() : false
            })
        },
        timepicker: false
    });

    $('.periodicity').prepend('<option selected></option>').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել ժամանակահատված",
        "language": {
            "noResults": function () {
                return "Չի գտնվել";
            },
            searching: function () {
                return "Փնտրում...";
            },
            errorLoading: function () {
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
    });


    $('.selectManager').prepend('<option selected></option>').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել մենեջերին",
        "language": {
            "noResults": function () {
                return "Մենեջեր չի գտնվել";
            },
            searching: function () {
                return "Փնտրում...";
            },
            errorLoading: function () {
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
        ajax: {
            url: '/admin/statistics/managers/data',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    search: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        let info;
                        if (item.name != null || item.surname != null) {
                            info = item.full_name;
                        } else {
                            info = item.email
                        }
                        return {
                            text: info,
                            id: item.id
                        }
                    })
                };
            }
        }
    });




    $('.filter_manager-static').on('click', function () {
        $('.error-range_manager').css('display', 'none');
        if (!$("#selectManagerStatistic").val() || !$('.end-manager_date').val() || !$('.start-manager_date').val() || !$('.periodicity').val()) {
            $('.error-range_manager').css('display', 'block');
        } else {
            $.ajax({
                type: 'GET',
                url: '/admin/statistics/managers/statistic',
                data: {
                    id: $("#selectManagerStatistic").val(),
                    start: $('.start-manager_date').val(),
                    end: $('.end-manager_date').val(),
                    type: $('.periodicity').val(),
                },
                success: function (res) {
                    infoManager.splice(0, infoManager.length);
                    label.splice(0, label.length)
                    eval(res[1]).forEach(function (item) {
                        label.push(item,)
                    });
                    if (res[0]) {
                        $.each(res[0], function (key, value) {
                            let r = Math.round(Math.random() * 255);
                            let g = Math.round(Math.random() * 255);
                            let b = Math.round(Math.random() * 255);
                            infoManager.push({
                                type: 'bar',
                                label: value.label,
                                data: value.data,
                                borderColor: "rgb(" + r + ", " + g + ", " + b + ")",
                                backgroundColor: "rgb(" + r + ", " + g + ", " + b + ")",
                            },);
                        });
                    }
                    chartManager.update();
                },
                errors: function (res) {
                    console.log(res)
                }
            });
        }
    });


    const dataManager = {
        labels: label,
        datasets: infoManager
    };


    const configManager = {
        type: 'line',
        data: dataManager,
        options: {}
    };

    let chartManager = new Chart(
        $("#managerStatistic"),
        configManager
    );

})
