$(document).ready(function () {

    function animationCount() {
        if ($('span').is('.count')) {
            $(".count").each(function () {
                $(this).prop("Counter", 0)
                    .animate({
                            Counter: $(this).text(),
                        },
                        {
                            duration: $(this).data('duration') * 100,
                            easing: "swing",
                            step: function (now) {
                                now = Number(Math.ceil(now)).toLocaleString('en');
                                $(this).text(now);
                            },
                        });
            });
        }
    }

    animationCount()

    let id = [];
    $('.new-select-item .input-checkbox .home-sphere-input_checkbox').on('change', function () {
        let $this = $(this);
        let allActive = $this.parents('.charts-inner').find('.active_count').data('type_active');
        let allArchived = $this.parents('.charts-inner').find('.archived_count').data('type_archived');
        if ($this.is(":checked")) {
            id.push(($this.data('id')));
        } else {
            let idx = id.indexOf($this.data('id'));
            id.splice(idx, 1);
        }

        $.ajax({
            type: 'POST',
            url: '/select-announcement-by-sphere',
            data: {
                id: id
            },
            success: function (res) {
                $this.parents('.charts-inner').find('.active_count').attr('data-duration', res.activeAnnouncements).text(res.activeAnnouncements);
                $this.parents('.charts-inner').find('.archived_count').attr('data-duration', res.archivedAnnouncements).text(res.archivedAnnouncements);
                animationCount()
            },

            error: function (res) {
                $this.parents('.charts-inner').find('.active_count').attr('data-duration', allActive).text(allActive);
                $this.parents('.charts-inner').find('.archived_count').attr('data-duration', allArchived).text(allArchived);
            }
        })

    })
})
