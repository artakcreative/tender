$(document).ready(function () {
    let proSphereCount = 0;
    let basicSphereCount = 0;

    $('.new-select-item .input-checkbox .pro_input-checkbox').on('change', function () {
        let $this = $(this);
        if ($this.is(":checked")){
            proSphereCount++;
        }else{
            proSphereCount--;
        }
        let price = 0;
        if (proSphereCount <= 1){
           price = 50000;
        }else{
            if(proSphereCount == 2) {
                price = 50000 + (50000 - (50000 * 10/100));
            }else {
                price = 50000 + (50000 - (50000 * 10 /100)) + (proSphereCount - 2) * (50000 - (50000 * 20 /100));
            }
        }
        $('.pro_price').html(price + '֏');
    })


    $('.new-select-item .input-checkbox .basic_input-checkbox').on('change', function () {
        let $this = $(this);
        if ($this.is(":checked")){
            basicSphereCount++;
        }else{
            basicSphereCount--;
        }
        let price = 0;
        if (basicSphereCount <= 1){
            price  = 30000;
        }else{
            if(basicSphereCount == 2) {
                price = 30000 + (30000 - (30000 * 10/100));
            }else {
                price =30000 + (30000 - (30000 * 10 /100)) + (basicSphereCount - 2) * (30000- (30000 * 20 /100));
            }
        }
        $('.basic_price').html(price + '֏');
    })

})
