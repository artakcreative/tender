$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    $('#btn-registration').on('click', function () {
        $('#registration .input-group').removeClass('error')
        let email = $('input[name=email]');
        let phone = $('input[name=phone]');
        let password = $('input[name=password]');
        let password_confirmation = $('input[name=password_confirmation]');
        let checkbox = $('input[name=checkbox]');
        $('.error-valid').empty();
        $.ajax({
            type: 'POST',
            url: '/register',
            data: {
                email: email.val(),
                phone: phone.val(),
                password: password.val(),
                password_confirmation: password_confirmation.val(),
                checkbox: checkbox.prop('checked')
            },
            success: function (res) {
                $(".modal").fadeOut(function () {
                    $("body").css("overflow", "auto");
                    $('header').css('visibility', 'unset')
                });
                $("#verification").fadeIn();
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                Object.entries(errors).forEach(([key, value]) => {
                    if (email.attr('name') === key) {
                        email.parent().addClass('error');
                        email.parent().append(`<small class="error-valid">${value}</small>`)

                    } else if (phone.attr('name') === key) {
                        phone.parent().addClass('error');
                        phone.parent().append(`<small class="error-valid">${value}</small>`)

                    } else if (password.attr('name') === key) {
                        password.parent().addClass('error');
                        password.parent().append(`<small class="error-valid">${value}</small>`);

                    } else if (password_confirmation.attr('name') === key) {
                        password_confirmation.parent().addClass('error');
                        password_confirmation.parent().append(`<small class="error-valid">${value}</small>`)

                    } else if (checkbox.attr('name') === key) {
                        $('#reg-checkbox').append(`<div><small class="error-valid mt-4">
                                                        Համաձայնություն դաժշտը պարտադիր է:</small></div>`)
                    }
                });
            }
        });
    });
    $(".modal-verification .close").on("click", function (e) {
        e.preventDefault();
        $(".modal-verification").fadeOut(function () {
            $("body").css("overflow", "auto");
            $('header').css('visibility', 'unset')
        });
    });

    $('#btn-login').on('click', function () {
        $('#login .input-group').removeClass('error')
        let emailLogin = $('#email');
        let passwordLogin = $('#password');
        let checkboxLogin = $('#checkbox');
        $('.error-valid').empty();
        $.ajax({
            type: 'POST',
            url: '/login',
            data: {
                email: emailLogin.val(),
                password: passwordLogin.val(),
                remember_me: checkboxLogin.prop('checked')
            },
            success: function (res) {
                if(res[0] === 'error') {
                    $('#login-email').append(`<small class="error-valid">${res[1]}</small>`)
                } else {
                    $(".modal").fadeOut(function () {
                        $("body").css("overflow", "auto");
                        $('header').css('visibility', 'unset')
                    });
                    location.reload();
                }
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                Object.entries(errors).forEach(([key, value]) => {
                    if (emailLogin.attr('name') === key) {
                        emailLogin.parent().addClass('error');
                        emailLogin.parent().append(`<small class="error-valid">${value}</small>`)
                    }  else if (passwordLogin.attr('name') === key) {
                        passwordLogin.parent().addClass('error');
                        passwordLogin.parent().append(`<small class="error-valid">${value}</small>`);
                    }
                });
            }
        });
    });

    $('#btn-forgot').on('click', function () {
        $('#modal_remove .input-group').removeClass('error')
        let emailForgot = $('#email-forgot');
        $('.error-valid').empty();
        $.ajax({
            type: 'POST',
            url: '/forgot-password',
            data: {
                email: emailForgot.val(),
            },
            success: function (res) {
                if(res.error) {
                    emailForgot.css("border-color", "red");
                    $('#forgot-email').append(`<small class="error-valid">${res.error}</small>`)
                }else{
                    $(".modal").fadeOut(function () {
                        $("body").css("overflow", "auto");
                        $('header').css('visibility', 'unset')
                    });
                }
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                Object.entries(errors).forEach(([key, value]) => {
                    if (emailForgot.attr('name') === key) {
                        emailForgot.parent().addClass('error');
                        emailForgot.parent().append(`<small class="error-valid">${value}</small>`)
                    }
                });
            }
        });
    });
    let sessionToken = $('#new_password').data('token');
    let sessionEmail = $('#new_password').data('email');
    if(sessionToken && sessionEmail) {
        $('#new_password').fadeIn();
        $('#btn-reset').on('click', function () {
            $('#new_password .input-group').removeClass('error')
            let passwordForgot = $('#password-reset');
            let passwordConfForgot = $('#password-conf-reset');
            $('.error-valid').empty();
            passwordForgot.css("border-color", "#53B7E8");
            passwordConfForgot.css("border-color", "#53B7E8");
            $.ajax({
                type: 'POST',
                url: '/reset-password',
                data: {
                    token: sessionToken,
                    email: sessionEmail,
                    password: passwordForgot.val(),
                    password_confirmation: passwordConfForgot.val(),
                },
                success: function (res) {
                    $("#new_password").fadeOut(function () {
                        $("body").css("overflow", "auto");
                        $('header').css('visibility', 'unset')
                    });
                    $('.append-text-reset').text(res.error ? res.error: res.success);
                    $(".modal-reset").fadeIn();

                },
                error: function (res) {
                    let errors = res.responseJSON.errors;
                    Object.entries(errors).forEach(([key, value]) => {
                        if (passwordForgot.attr('name') === key) {
                            passwordForgot.parent().addClass('error')
                            passwordForgot.parent().append(`<small class="error-valid">${value}</small>`)
                        } else  if (passwordConfForgot.attr('name') === key) {
                            passwordConfForgot.parent().addClass('error');
                            passwordConfForgot.parent().append(`<small class="error-valid">${value}</small>`)
                        }
                    });
                }
            });
        });
    }
});
