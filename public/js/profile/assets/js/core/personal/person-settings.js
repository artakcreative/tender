$(document).ready(function () {

    $('.person-password-save').on('click', function () {
        let oldPassword = $('input[name=old_password]');
        let password = $('input[name=password]');
        let passwordConfirmation = $('input[name=password_confirmation]');
        $('.error-valid').empty();
        $.ajax({
            type: 'POST',
            url: '/person/update/password',
            data: {
                old_password: oldPassword.val(),
                password: password.val(),
                password_confirmation: passwordConfirmation.val(),
            },
            success: function (res) {
                if (res[0] === 'error') {
                    oldPassword.css("border-color", "red");
                    $('.old-pass-body').append(`<small class="error-valid">${res[1]}</small>`)
                }
                if (res[0] === 'success') {
                    $('.text-success-pass').text(res[1]);
                    $(".password-success-modal").fadeIn();
                }
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                Object.entries(errors).forEach(([key, value]) => {
                    if (oldPassword.attr('name') === key) {
                        oldPassword.css("border-color", "red");
                        $('.old-pass-body').append(`<small class="error-valid">${value}</small>`)

                    } else if (password.attr('name') === key) {
                        password.css("border-color", "red");
                        $('.pass-body').append(`<small class="error-valid">${value}</small>`);

                    } else if (passwordConfirmation.attr('name') === key) {
                        passwordConfirmation.css("border-color", "red");
                        $('.pass-conf-body').append(`<small class="error-valid">${value}</small>`)
                    }
                });
            }
        });
    });

    jQuery.datetimepicker.setLocale('hy');
    $('.datepicker').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        scrollMonth: false,
        scrollInput: false
    })

    $(".input-radio-inner input").on('change', function() {
        if($(this).val() == 'legal-person_radio') {
            $('input[name=legal_success]').val('true-legal');
            $('input[name=physical_success]').val('');
            $('#physical-person_block').css("display", "none");
            $('#legal-person_block').css("display", "block");
        }else {
            $('input[name=legal_success]').val('');
            $('input[name=physical_success]').val('true-physical');
            $('#legal-person_block').css("display", "none");
            $('#physical-person_block').css("display", "block");
        }
    });

    let formData = new FormData();

    $('#upload').bind("change", function () {
        $.each($('#upload')[0].files, function (key, input) {
            formData.append('file[]', input);
        });

    });
    $('#upload_contract').bind("change", function () {
        $.each($('#upload_contract')[0].files, function (key, input) {
            formData.append('contract_files[]', input);
        });

    });

    $('.person-information-save').on('click', function () {
        let id = $('input[name=id]');
        let name = $('input[name=name]');
        let surname = $('input[name=surname]');
        let email = $('input[name=email]');
        let phone = $('input[name=phone]');
        let license_plate = $('input[name=public_service_license_plate]');
        let passport_number = $('input[name=passport_number]');
        let passport_take_date = $('input[name=passport_take_date]');
        let passport_take_from = $('input[name=passport_take_from]');
        let tin = $('input[name=tin]');
        let legal_address = $('input[name=legal_address]');
        let bank = $('input[name=bank]');
        let bank_account = $('input[name=bank_account]');
        let physical_success = $('input[name=physical_success]');
        let legal_success = $('input[name=legal_success]');
        formData.append('id', id.val());
        formData.append('name', name.val());
        formData.append('surname', surname.val());
        formData.append('email', email.val());
        formData.append('phone', phone.val());
        if(physical_success.val() == 'true-physical') {
            formData.append('public_service_license_plate', license_plate.val());
            formData.append('passport_number', passport_number.val());
            formData.append('passport_take_date', passport_take_date.val());
            formData.append('passport_take_from', passport_take_from.val());
            formData.append('physical_success', physical_success.val());
        }else {
            formData.append('tin', tin.val());
            formData.append('legal_address', legal_address.val());
            formData.append('bank', bank.val());
            formData.append('bank_account', bank_account.val());
            formData.append('legal_success', legal_success.val());
        }
        $.ajax({
            type: 'POST',
            url: '/person/update/profile',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function (res) {
                if (res[0] === 'success') {
                    location.reload();
                }
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                Object.entries(errors).forEach(([key, value]) => {
                    let str = value[0].split("երբ");
                    str = str[0] + ':';
                    if (name.attr('name') === key) {
                        name.parent().addClass('error');
                        name.parent().append(`<small class="error-valid">${value}</small>`)

                    } else if (surname.attr('name') === key) {
                        surname.parent().addClass('error');
                        surname.parent().append(`<small class="error-valid">${value}</small>`)

                    } else if (email.attr('name') === key) {
                        email.parent().addClass('error');
                        email.parent().append(`<small class="error-valid">${value}</small>`);

                    } else if (phone.attr('name') === key) {
                        phone.parent().addClass('error');
                        phone.parent().append(`<small class="error-valid">${value}</small>`);
                    } else if (license_plate.attr('name') === key) {
                        license_plate.parent().addClass('error');
                        license_plate.parent().append(`<small class="error-valid">${str}</small>`)

                    } else if (passport_number.attr('name') === key) {
                        passport_number.parent().addClass('error');
                        passport_number.parent().append(`<small class="error-valid">${str}</small>`);

                    } else if (passport_take_date.attr('name') === key) {
                        passport_take_date.parent().addClass('error');
                        passport_take_date.parent().append(`<small class="error-valid">${str}</small>`);

                    }else if (passport_take_from.attr('name') === key) {
                        passport_take_from.parent().addClass('error');
                        passport_take_from.parent().append(`<small class="error-valid">${str}</small>`)

                    } else if (tin.attr('name') === key) {
                        tin.parent().addClass('error');
                        tin.parent().append(`<small class="error-valid">${str}</small>`);

                    } else if (legal_address.attr('name') === key) {
                        legal_address.parent().addClass('error');
                        legal_address.parent().append(`<small class="error-valid">${str}</small>`);

                    }else if (bank.attr('name') === key) {
                        bank.parent().addClass('error');
                        bank.parent().append(`<small class="error-valid">${str}</small>`)

                    } else if (bank_account.attr('name') === key) {
                        bank_account.parent().addClass('error');
                        bank_account.parent().append(`<small class="error-valid">${str}</small>`);

                    }
                });
            }
        });
    });

    $('.delete_file').on('click', function () {
        let $this = $(this);
        let id = $this.data('id');
        let url = $this.data('url');
        $.ajax({
            type: 'delete',
            url: `${url}/${+id}`,
            success: function (res) {
                if (res.success === true) {
                    $this.parent('.file-item').remove()
                }
            }
        });

        // Swal.fire({
        //     title: 'Դուք համոզված ե՞ք,',
        //     text: 'որ ցանկանում եք ջնջել',
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: 'Այո, Ջնջել!',
        //     cancelButtonText: 'Չեղարկել'
        // }).then((result) => {
        //     if (result.value) {
        //         $.ajax({
        //             type: 'delete',
        //             url: `${url}/${+id}`,
        //             success: function (res) {
        //                 if (res.success === true){
        //                     $this.parent('.file-item').remove()
        //                 }
        //             },
        //             errors: function (res) {
        //                 console.log(res)
        //             }
        //         });
        //     }
        // })
    });

});
