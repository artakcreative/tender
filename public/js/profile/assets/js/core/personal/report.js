$( document ).ready(function() {

    let mounts = [
        'Հունվար',
        'Փետրվար',
        'Մարտ',
        'Ապրիլ',
        'Մայիս',
        'Հունիս',
        'Հուլիս',
        'Օգոստոս',
        'Սեպտեմբեր',
        'Հոկտեմբեր',
        'Նոյեմբեր',
        'Դեկտեմբեր',
    ];

    let weeks = [
        '1-Հունվար', '2-Հունվար', '3-Հունվար', '4-Հունվար',
        '1-Փետրվար','2-Փետրվար','3-Փետրվար','4-Փետրվար',
        '1-Մարտ','2-Մարտ','3-Մարտ','4-Մարտ',
        '1-Ապրիլ','2-Ապրիլ','3-Ապրիլ','4-Ապրիլ',
        '1-Մայիս','2-Մայիս','3-Մայիս','4-Մայիս',
        '1-Հունիս','2-Հունիս','3-Հունիս','4-Հունիս',
        '1-Հուլիս','2-Հուլիս','3-Հուլիս','4-Հուլիս',
        '1-Օգոստոս','2-Օգոստոս','3-Օգոստոս','4-Օգոստոս',
        '1-Սեպտեմբեր','2-Սեպտեմբեր','3-Սեպտեմբեր','4-Սեպտեմբեր',
        '1-Հոկտեմբեր','2-Հոկտեմբեր','3-Հոկտեմբեր','4-Հոկտեմբեր',
        '1-Նոյեմբեր',  '2-Նոյեմբեր',  '3-Նոյեմբեր',  '4-Նոյեմբեր',
        '1-Դեկտեմբեր','2-Դեկտեմբեր','3-Դեկտեմբեր','4-Դեկտեմբեր', '4-Դեկտեմբեր',
    ];

    let quarters = [
        'Առաջին եռամսյակ',
        'Երկրորդ եոամսյակ',
        'Երրորդ եոամսյակ',
        'Չորորդ եոամսյակ'
    ];

    if($(".general-dynamics-link").hasClass('active')){
        getDefault();
    }


    $('.report-sectoral-link').on('click', function () {
        $('.periodicity').prepend('<option selected></option>');
        $('.datepicker-report').val("");
        $('.error-range_sectoral').css('display', 'none')
        $('.filter-date-report').css('display', 'none');
        getDefault();
        $(".general-dynamics-link").removeClass('active');
        $(".report-structure-link").removeClass('active');
        $(this).addClass('active');
        $('.blog-sectoral').css('display', 'block');
        $('.blog-general-dynamics').css('display', 'none');
        $('.blog-structure').css('display', 'none');
    });

    $('.general-dynamics-link').on('click', function () {
        $('.periodicity').prepend('<option selected></option>');
        $('.datepicker-report').val("");
        $('.error-range-text').css('display', 'none')
        $('.filter-date-report').css('display', 'none');
        getDefault();
        $(".report-sectoral-link").removeClass('active');
        $(".report-structure-link").removeClass('active');
        $(this).addClass('active');
        $('.blog-general-dynamics').css('display', 'block');
        $('.blog-sectoral').css('display', 'none');
        $('.blog-structure').css('display', 'none');
    });

    $('.report-structure-link').on('click', function () {
        $('.periodicity').prepend('<option selected></option>');
        $('.datepicker-report').val("");
        $('.error-range_structure').css('display', 'none')
        $('.filter-date-report').css('display', 'none');
        getDefault();
        $(".report-sectoral-link").removeClass('active');
        $(".general-dynamics-link").removeClass('active');
        $(this).addClass('active');
        $('.blog-structure').css('display', 'block');
        $('.blog-general-dynamics').css('display', 'none');
        $('.blog-sectoral').css('display', 'none');
    });


    function getDefault() {
        $.ajax({
            type: 'GET',
            url: '/report/statistic',
            success: function (res) {
                general(mounts,res);
                sectoral(mounts,res);
                structure(res);
            },
            errors: function (res) {
                console.log(res)
            }
        });
    }

    function getStaticsFilter(type, start, end) {
        $.ajax({
            type: 'GET',
            url: '/report/statistic',
            data: {
                start: start,
                end: end,
                type: type
            },
            success: function (res) {
                general(eval(res[1]), res[0]);
                sectoral(eval(res[1]),res[0]);
                structure(res[0]);
            },
            errors: function (res) {
                console.log(res)
            }
        });
    }

    $('.timepicker_start').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                maxDate: $('.timepicker_end').val()? $('.timepicker_end').val():false
            })
        },
        timepicker:false
    });
    $('.timepicker_end').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                minDate: $('.timepicker_start').val() ? $('.timepicker_start').val():false
            })
        },
        timepicker:false
    });


    $('.timepicker_start-sectoral').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                maxDate: $('.timepicker_end-sectoral').val()? $('.timepicker_end-sectoral').val():false
            })
        },
        timepicker:false
    });
    $('.timepicker_end-sectoral').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                minDate: $('.timepicker_start-sectoral').val() ? $('.timepicker_start-sectoral').val():false
            })
        },
        timepicker:false
    });


    $('.timepicker_start-structure').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                maxDate: $('.timepicker_end-structure').val()? $('.timepicker_end-structure').val():false
            })
        },
        timepicker:false
    });
    $('.timepicker_end-structure').datetimepicker({
        format:'Y/m/d',
        scrollMonth : false,
        scrollInput : false,
        onShow:function( ct ){
            this.setOptions({
                minDate: $('.timepicker_start-structure').val() ? $('.timepicker_start-structure').val():false
            })
        },
        timepicker:false
    });

    $('.periodicity').prepend('<option selected></option>').select2({
        containerCssClass: "select2-periodicity",
        placeholder: "Ընտրել",
        "language": {
            "noResults": function () {
                return "Արդյունք չի գտնվել";
            },
            searching: function () {
                return "Փնտրում...";
            },
            errorLoading: function () {
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
    });



    $('body').on('change', '.periodicity-general', function () {
        let type = $(this).val();
        $('.error-range-text').css('display', 'none');
        if(!$('.timepicker_start').val() || !$('.timepicker_end').val()) {
            $('.error-range-text').css('display', 'block');
        }else {
            getStaticsFilter(type, $('.timepicker_start').val(), $('.timepicker_end').val());
        }
    });

    $('body').on('change','.periodicity-sectoral', function () {
        console.log($('.timepicker_start-sectoral').val(), $('.timepicker_end-sectoral').val())
        let type = $(this).val();
        $('.error-text_sectoral').css('display', 'none');
        if(!$('.timepicker_start-sectoral').val() || !$('.timepicker_end-sectoral').val()) {
            $('.error-range_sectoral').css('display', 'block');
        }else {
            getStaticsFilter(type, $('.timepicker_start-sectoral').val(), $('.timepicker_end-sectoral').val());
        }
    });

    $('body').on('change', '.periodicity-structure', function () {
        let type = $(this).val();
        $('.error-range_structure').css('display', 'none');
        if(!$('.timepicker_start-structure').val() || !$('.timepicker_end-structure').val()) {
            $('.error-range_structure').css('display', 'block');
        }else {
            getStaticsFilter(type, $('.timepicker_start-structure').val(),$('.timepicker_end-structure').val() );
        }
    });


    $('body').on('click','.filter_btn-report', function () {
        $('.filter-date-report').toggle();
    });



       function general(structure,data = null) {
           let info = [];
           if(data) {
               $.each( data, function( key, value ) {
                   info.push(value);
               });
           }
           Highcharts.chart('chart-general-dynamics', {
               chart: {
                   type: 'column'
               },
               title: {
                   text: 'Ընդհանուր դինամիկա'
               },
               lang:{
                   downloadCSV: 'Ներբեռնել CSV',
                   downloadJPEG: 'Ներբեռնել JPEG',
                   downloadPDF: 'Ներբեռնել PDF',
                   downloadPNG: 'Ներբեռնել PNG',
                   downloadXLS: 'Ներբեռնել XLS',
                   viewFullscreen: 'Դիտել ամբողջ Էկրանով',
                   exitFullscreen: 'Փոքրացնել',
                   printChart: 'Տպել'
               },

               exporting: {
                   buttons: {
                       contextButton: {
                           menuItems: [
                               'viewFullscreen',
                               'exitFullscreen',
                               'downloadCSV',
                               'downloadJPEG',
                               'downloadPDF',
                               'downloadPNG',
                               'downloadXLS',
                               'printChart'
                           ],
                       },
                   },
               },
               xAxis: {
                   categories: structure,
                   crosshair: true
               },
               yAxis: {
                   min: 0,
                   title: {
                       text: 'Քանակ'
                   }
               },
               tooltip: {
                   headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                   pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                       '<td style="padding:0"><b>{point.y:.1f} քանակ</b></td></tr>',
                   footerFormat: '</table>',
                   shared: true,
                   useHTML: true
               },
               plotOptions: {
                   column: {
                       pointPadding: 0.2,
                       borderWidth: 0
                   }
               },
               series:info,
               credits: false
           });

        }


    function sectoral(structure, data = null) {
        let info = [];
        if(data) {
            $.each( data, function( key, value ) {
                info.push(value);
            });
        }
        Highcharts.chart('chart-sectoral', {

            chart: {
                type: 'area'
            },
            title: {
                text: 'Ոլորտային դինամիկա'
            },
            lang:{
                downloadCSV: 'Ներբեռնել CSV',
                downloadJPEG: 'Ներբեռնել JPEG',
                downloadPDF: 'Ներբեռնել PDF',
                downloadPNG: 'Ներբեռնել PNG',
                downloadXLS: 'Ներբեռնել XLS',
                viewFullscreen: 'Դիտել ամբողջ Էկրանով',
                exitFullscreen: 'Փոքրացնել',
                printChart: 'Տպել'
            },

            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: [
                            'viewFullscreen',
                            'exitFullscreen',
                            'downloadCSV',
                            'downloadJPEG',
                            'downloadPDF',
                            'downloadPNG',
                            'downloadXLS',
                            'printChart'
                        ],
                    },
                },
            },
            xAxis: {
                categories: structure,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Քանակ'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} քանակ</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: info,
            credits: false
        });

    }


    function structure(data = null) {
        let info = [];
        if(data) {
            let allSum = 0;
            $.each( data, function( key, value ) {
                let sum = 0;
                value.data.forEach(function(item){
                    sum+=item
                });
                allSum+=sum;
            });

            $.each( data, function( key, value ) {
                let sum = 0;
                value.data.forEach(function(item){
                    sum+=item
                });
                info.push( { name: value.name, y: allSum / sum * 100  },)
            });

        }
        Highcharts.chart('chart-structure', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Կառուցվածքային դինամիկա'
            },
            lang:{
                downloadCSV: 'Ներբեռնել CSV',
                downloadJPEG: 'Ներբեռնել JPEG',
                downloadPDF: 'Ներբեռնել PDF',
                downloadPNG: 'Ներբեռնել PNG',
                downloadXLS: 'Ներբեռնել XLS',
                viewFullscreen: 'Դիտել ամբողջ Էկրանով',
                exitFullscreen: 'Փոքրացնել',
                printChart: 'Տպել'
            },
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: [
                            'viewFullscreen',
                            'exitFullscreen',
                            'downloadCSV',
                            'downloadJPEG',
                            'downloadPDF',
                            'downloadPNG',
                            'downloadXLS',
                            'printChart'
                        ],
                    },
                },
            },
            tooltip: {
                pointFormat: '<b>քանակ: {point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: '',
                data: info
            }],
            credits: false,
        });
    }







});
