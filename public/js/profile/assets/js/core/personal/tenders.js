$(document).ready(function () {
    $('.archive-but').on('click', function () {
        $(".active-but").removeClass('active');
        $('.archive-but').addClass('active');
        $('.blog-active').css('display', 'none');
        $('.blog-archive').css('display', 'block');

    });
    $('.active-but').on('click', function () {
        $(".archive-but").removeClass('active');
        $('.active-but').addClass('active');
        $('.blog-active').css('display', 'block');
        $('.blog-archive').css('display', 'none');
    });

    $("#search-sub_spheres").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".all-spheres .filter-area__header").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#search-organization").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".all-organizations .input-checkbox").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('.my-filter').on('click', function () {
        $('.filter-search').addClass('btn-border');
        $(this).removeClass('btn-border');
    });

    $('.reject-filter').on('click', function () {
        $('.filter-search').addClass('btn-border');
        $(this).removeClass('btn-border');
    });

    if ($('.filter-success').val() == 1) {
        $('.filter-search').addClass('btn-border');
        $('.my-filter').removeClass('btn-border');
    }

    if(sessionStorage.getItem( 'first_user_sphere' ) != null) {
        $(".active-but").removeClass('active');
        $('.archive-but').addClass('active');
        $('.blog-active').css('display', 'none');
        $('.blog-archive').css('display', 'block');
        sessionStorage.removeItem( 'first_user_sphere' );
    }



    // let sub_spheres = $('.blog-active').find('.sub-field');
    // $.each(sub_spheres, function (idx, val){
    //     let str = val.innerHTML;
    //     console.log(str.slice(0, -1));
    // })
});
