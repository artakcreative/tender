$(document).ready(function () {

    $('.favorite_announcement').on('click', function () {
        $(this).toggleClass('active');
        let $this = $(this),
            announcement_id = $this.data('announcement_id'),
            user_id = $this.data('user_id');
        $.ajax({
            type: 'Post',
            url: 'tenders/add-delete-favorite',
            cache: false,
            data:{
                announcement_id : announcement_id,
                user_id : user_id
            },
            success: function (res){
                if (res.detached.length){
                    $(".delete_blog_item" + res.detached[0]).remove();
                    if ($(".saved_blog_list").html() !== undefined){
                        if($(".saved_blog_list").html().trim() === ''){
                            $(".saved_blog_list").append('<h2 class="sub-title">Նախընտրած հայտարարուրյուններ չկան</h2>');
                        }
                    }
                }
            },
            error: function (res) {
                console.log(res)
            }

        })

        if ($('.favorite_announcement').hasClass('active')){
            $('.nav_saved').addClass('active');
        }else{
            $('.nav_saved').removeClass('active');
        }
    })


    function announcementTimer(value, idx){
        let deadline = new Date(value.innerHTML).getTime();
        let timer = setInterval(function() {
            let now = new Date().getTime();
            let diff = deadline - now;
            let days = Math.floor(diff / (1000 * 60 * 60 * 24));
            let hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((diff % (1000 * 60)) / 1000);

            document.querySelectorAll('.deadline_timer_show_date')[idx].innerHTML =
                "<b>" + days + "</b>" + " օր " +  "<b>" + hours  + "</b>" + " ժ. "
                + "<b>" + minutes + "</b>" + " ր. " + "<b>" + seconds + "</b>" + " վ-ից ";
            if (diff < 0) {
                clearInterval(timer);
                document.querySelectorAll('.deadline_timer_show')[idx].innerHTML = "<b>Տենդերն ավարտված է</b>";
            }
        }, 1000);
    }

    let date = $('.deadline_timer');
    $.each(date, function (idx,value){
        announcementTimer(value,idx);
    })

})
