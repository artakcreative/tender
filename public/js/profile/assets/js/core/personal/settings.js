$(document).ready(function () {
    const month = 30,
        blocked = 0,
        TARIFF = $('.tariff'),
        AMOUNT_TARIFF = $('.amountTariff');
    window.sum = 0;
    var tariffPrice = 0,
        block = false,
        selectedNotPayed = [],
        notPayedData = [],
        selected = [],
        payed = [],
        allData = [],
        allCount = [],
        subs = [],
        spheres = [],
        spheres_change = [],
        spheresNotPayed = [],
        last_id = null,
        last_text = null,
        basic = null,
        pro = null,
        tariff_id = null,
        stateCheckbox = $('.state'),
        privateCheckbox = $('#private');

    $.ajax({
        type: 'GET',
        url: '/settings/tariff',
        success: function (res) {
            if (res.error) {
                basic = res.error.id;
                tariff_id = res.error.id;
                tariffPrice = res.error.price;
                TARIFF.text(res.error.name);
                AMOUNT_TARIFF.text(XFormatPrice(res.error.price));
                AMOUNT_TARIFF.data('price', res.error.price);
                $('.activeDate').text('ակտիվացրած ոլորտներ չկան։');
                if (res.error.name === privateCheckbox.data('name')) {
                    privateCheckbox.attr('checked', true);

                }
            } else {
                pro = res.id;
                tariff_id = res.id;
                tariffPrice = res.price;
                $('.activeDate').text(res.active);
                TARIFF.text(res.name);
                AMOUNT_TARIFF.data('price', res.price);
                AMOUNT_TARIFF.text(XFormatPrice(res.price));
                if (res.name === privateCheckbox.data('name')) {
                    privateCheckbox.attr('checked', true);
                }
            }
        }
    });

    privateCheckbox.on('click', function () {
        if ($(this).prop("checked") === true) {
            tariff_id = privateCheckbox.data('id');
            $('.tariff').text(privateCheckbox.data('name'));
            AMOUNT_TARIFF.data('price', privateCheckbox.data('price'));
            AMOUNT_TARIFF.text(XFormatPrice(privateCheckbox.data('price')));
        } else if ($(this).prop("checked") === false) {
            tariff_id = stateCheckbox.data('id');
            $('.tariff').text(stateCheckbox.data('name'));
            AMOUNT_TARIFF.data('price', stateCheckbox.data('price'));
            AMOUNT_TARIFF.text(XFormatPrice(stateCheckbox.data('price')));
        }
        readyCount();
    });

    $.ajax({
        type: 'GET',
        url: '/settings/spheres/selected/payed',
        success: function (res) {
            if (res.length > 0) {
                for (const [key, value] of Object.entries(res)) {
                    if (value[0].sphere) {
                        selected.push({id: value[0].sphere.id, text: value[0].sphere.name});
                        payed.push({
                            id: value[0].sphere.id,
                            active: value[0].user_spheres[0].active,
                            price: value[0].user_spheres[0].tariff.price
                        });
                        allData.push(value);
                    }
                }
            }
            $.ajax({
                url: '/settings/spheres/selected',
                type: "GET",
                success: function (result) {
                    let days = 0,
                        counts = 0,
                        sumsPayed = 0;
                    if (result.length > 0) {
                        for (const [key, value] of Object.entries(result)) {
                            if (value[0].sphere) {
                                payed.push({
                                    id: value[0].sphere.id,
                                    active: value[0].user_spheres[0].active,
                                    price: value[0].user_spheres[0].tariff.price
                                });
                                selectedNotPayed.push({id: value[0].sphere.id, text: value[0].sphere.name});
                                notPayedData.push(value);
                            }
                        }
                    }
                    $.each(payed, function (index, item) {
                        if (item.active !== null) {
                            let given = moment(item.active, "YYYY-MM-DD"),
                                current = moment().startOf('day');
                            days = moment.duration(given.diff(current)).asDays() - 1;
                            counts++;
                        }
                    });
                    if (days > 0) {
                        if (counts > 1) {
                            if (counts === 2) {
                                sumsPayed = tariffPrice + (tariffPrice - (tariffPrice * 10 / 100));
                            } else {
                                sumsPayed = tariffPrice + (tariffPrice - (tariffPrice * 10 / 100)) + (counts - 2) * (tariffPrice - (tariffPrice * 20 / 100));
                            }
                        } else {
                            sumsPayed = tariffPrice;
                        }
                        window.sum = sumsPayed / month * days;
                    }
                    $('.all-price').text(XFormatPrice(sumsPayed));
                    $('.month-price').text(XFormatPrice(sumsPayed - window.sum));

                    $.ajax({
                        type: 'GET',
                        url: '/settings/spheres',
                        success: function (res) {
                                if (typeof res == "object") {
                                    var array = selected.concat(selectedNotPayed);
                                    var props = ['id', 'text'];
                                    window.result = res.filter(function (o1) {
                                        return !array.some(function (o2) {
                                            return o1.id === o2.id;
                                        });
                                    }).map(function (o) {
                                        return props.reduce(function (newo, text) {
                                            newo[text] = o[text];
                                            return newo;
                                        }, {});
                                    });
                                }

                                block = Number($("[name=actived]").val()) == blocked;
                                if (selected.length === 0) {
                                    $('.selectAppends').append(`<span class="green">*Դուք չունեք վճարված ոլորտ</span>`)
                                } else {
                                    if(privateCheckbox.is(':checked')) {
                                        privateCheckbox.attr('disabled', true);
                                    }
                                    selected.forEach(function (item, index, arr) {
                                        $('.selectAppends').append(`<div class="selectAppend"><div class="form-group body-select-settings">
                        <select class="selectCategoryUser appends${index} js-states form-control"></select>
                    </div><div class="checkbox-body-append input-checkbox-childes"><div class="sub-title-2">Ենթաոլորտներ</div></div></div>`);
                                        $(`.appends${index}`).append(`<option  class="opt-selected${item.id} opt" value="${item.id}" selected>${item.text}</option>`);
                                        window.result.forEach(function (value, i, arr) {
                                            $(`.appends${index}`).append(`<option class="opt-selected${value.id} opt" value="${value.id}">${value.text}</option>`);
                                        });
                                    });
                                }


                                allData.forEach(function (items, i, arr) {
                                    items.forEach(function (item1, index1) {
                                        if (item1.id) {
                                            $('.checkbox-body-append').eq(i).append(`<div class="input-checkbox">
                            <input class="checkbox global-checkbox" type="checkbox" id="${item1.id + item1.name}" data-id="${item1.id}" name="checkboxes"  checked ${block ? 'disabled' : ''}  >
                            <label for="${item1.id + item1.name}">${item1.name}</label>`)
                                        } else {
                                            $('.checkbox-body-append').eq(i).append(`<div class="input-checkbox"> Ենթաոլորտ չկա `)
                                            spheres_change.push(item1.sphere.id);
                                            spheres.push(item1.sphere.id);
                                        }
                                    })
                                });

                                $('.selectCategoryUser').select2({
                                    disabled: block
                                });

                                if (selectedNotPayed.length === 0) {
                                    $('.selectNotPayed').append(`<span class="green">*Դուք չունեք ավելացրած ոլորտ</span>`)
                                } else {
                                    selectedNotPayed.forEach(function (item, index) {
                                        $('.selectNotPayed').append(`<div class="selectAppend"><div class="form-group body-select-settings">
                        <select class="select-category-user-not-payed appendsNotPayed${index} js-states form-control"></select>
                        <input type="button" class="btn btn-action delete-not_payed" value="Հեռացնել">
                    </div><div class="input-checkbox-childes checkbox-not-payed"><div class="sub-title-2">Ենթաոլորտներ</div></div></div>`);
                                        $(`.appendsNotPayed${index}`).append(`<option  class="opt-selected${item.id}" value="${item.id}" selected>${item.text}</option>`);
                                        window.result.forEach(function (value, i, arr) {
                                            $(`.appendsNotPayed${index}`).append(`<option class="opt-selected${value.id}" value="${value.id}">${value.text}</option>`);
                                        });
                                    });
                                }

                                notPayedData.forEach(function (items, i) {
                                    items.forEach(function (item1) {
                                        if (item1.id) {
                                            $('.checkbox-not-payed').eq(i).append(`<div class="input-checkbox">
                            <input class="checkbox global-checkbox" type="checkbox" id="${item1.id + item1.name}" data-id="${item1.id}" name="checkboxes" disabled checked>
                            <label for="${item1.id + item1.name}">${item1.name}</label>`)
                                        } else {
                                            spheresNotPayed.push(item1.sphere.id);
                                            spheres.push(item1.sphere.id);
                                        }
                                    })
                                });

                                $('.select-category-user-not-payed').select2({
                                    disabled: true
                                });
                            readyCount();
                        },
                    });


                },
                error: function (err) {

                }
            });
        }
    });


    const BODY = $("body")

    BODY.on("select2:selecting", '.selectCategoryUser', function () {
        last_id = $(this).find('option:selected').val();
        last_text = $(this).find('option:selected').text()
    }).on('change', '.selectCategoryUser', function () {
        spheres = spheres.filter(function (value) {
            return value !== last_id
        });
        spheres_change = spheres_change.filter(function (value) {
            return value !== last_id
        });
        window.result.push({id: parseInt(last_id), text: last_text});
        let $this = $(this);
        let info = [];
        let id = $this.find('option:selected').val();
        let text = $this.find('option:selected').text();
        $.ajax({
            type: 'GET',
            url: '/subcategories',
            data: {
                id: id
            },
            success: function (res) {
                let propsRepeater = ['id', 'text'];
                info.push({id: parseInt(id), text: text});
                window.result = window.result.filter(function (o1) {
                    return !info.some(function (o2) {
                        return o1.id === o2.id;
                    });
                }).map(function (o) {
                    return propsRepeater.reduce(function (newo, text) {
                        newo[text] = o[text];
                        return newo;
                    }, {});
                });

                $this.parents('.selectAppend').find('.checkbox-body-append').remove();
                if (typeof res == 'string') {
                    $this.parents('.selectAppend').append(`<div class="checkbox-body-append input-checkbox-childes payedCheckbox">${res}</div>`);
                    let checkboxs = $this.parents('.selectAppend').find('.input-checkbox-childes .checkbox');
                    checkboxs.each(function () {
                        $(this).prop('checked', true);
                    });

                } else {
                    spheres_change.push(parseInt(res[1]));
                    spheres.push(parseInt(res[1]));
                }
                removeAddCategory();
            }

        });
    });

    BODY.on('click', '.global-checkbox', function () {
        var $this = $(this),
            checkbox = $this.parents('.input-checkbox-childes').find('.input-checkbox .checkbox:checked');
        if (checkbox.length === 0) $this.prop('checked', true)
    }).on('change', '.global-checkbox', function () {
        let $this = $(this),
            ids = $this.data('id');
        if ($this.is(':checked')) {
            subs.push(ids);
        } else {
            for (let i = 0; i < subs.length; i++) {
                if (subs[i] === ids) {
                    subs.splice(i, 1)
                }
            }
        }
    });

    BODY.on('click', '.addSelect', function () {
        $(".addSelect").addClass('remove');
        $('.selectNotPayed .green').remove()
    });

    BODY.on('change', '.selectCategoryUserChildes', function () {
        let info = [],
            $this = $(this);
        $(this).attr("disabled", true);
        allCount.push({id: $this.val()});
        readyCount();
        let id = $this.find('option:selected').val();
        let text = $this.find('option:selected').text();
        $.ajax({
            type: 'GET',
            url: '/subcategories',
            data: {
                id: id
            },
            success: function (res) {
                var propsRepeater = ['id', 'text'];
                info.push({id: parseInt(id), text: text});
                window.result = window.result.filter(function (o1) {
                    return !info.some(function (o2) {
                        return o1.id === o2.id;
                    });
                }).map(function (o) {
                    return propsRepeater.reduce(function (newo, text) {
                        newo[text] = o[text];
                        return newo;
                    }, {});
                });

                removeAddCategory();

                if (window.result.length === 0) $(".addSelect").addClass('remove');
                else $(".addSelect").removeClass('remove');

                if (typeof res === 'string') {
                    $this.parents('.parent-item').find('.input-checkbox-childes').html('<div class="sub-title-2"><p style="font-size: 15px">Ընտրեք անհրաժեշտ ենթաոլորտները ծանուցումներ ստանալու համար</p></div>' + res);
                    let checkboxs = $this.parents('.parent-item').find('.input-checkbox-childes .checkbox');
                    checkboxs.each(function () {
                        $(this).prop('checked', true)
                    });
                } else {
                    spheres.push(parseInt(res[1]))
                }
            }
        });
    });

    let deleteIds = [];

    let deleteNotPayedIds = []


    BODY.on('click', '.delete-not_payed', function () {
        let $this = $(this),
            sphere = $this.parents('.selectAppend').find('.select-category-user-not-payed').find(":selected"),
            subSpheres = $this.parents('.selectAppend').find('.input-checkbox');
        subSpheres.each(function () {
            deleteNotPayedIds.push($(this).find('.checkbox').data('id'));
        });
        payed = payed.filter(object => {
            return object.id !== parseInt(sphere.val());
        });
        window.result.unshift({id: parseInt(sphere.val()), text: sphere.text()});
        readyCount();
        removeNotPayed(parseInt(sphere.val()), deleteNotPayedIds);
        $(this).parents('.selectAppend').remove();
    });

    BODY.on('click', '.delete-select', function () {
        allCount.shift();
        let $this = $(this),
            check = $this.parents('.parent-item').find('.input-checkbox');
        check.each(function () {
            deleteIds.push($(this).find('.checkbox').data('id'));
        });
        subs = deleteIds.concat(subs).filter((item) => subs.indexOf(item) < 0 || deleteIds.indexOf(item) < 0);
        readyCount();
        if (window.result.length !== 0) {
            $(".addSelect").removeClass('remove')
        }
    });


    $('.repeater').repeater({
        show: function () {
            $(this).slideDown();
            $('.selectCategoryUserChildes').select2({
                theme: "classic",
                containerCssClass: "user-select2-container-class",
                placeholder: "Ընտրել ոլորտ",
                "language": {
                    "noResults": function () {
                        return "Ոլորտ չի գտնվել";
                    },
                    searching: function () {
                        return "Փնտրում...";
                    },
                    errorLoading: function () {
                        return "Արդյունքները չհաջողվեց բեռնել"
                    }
                },
                data: window.result
            });

        },
        initEmpty: true,
        hide: function (deleteElement) {
            let infos = $(this).find('.selectCategoryUserChildes'),
                remove_id = infos.find('option:selected').val(),
                remove_text = infos.find('option:selected').text(),
                index = spheres.indexOf(parseInt(remove_id));
            if (index > -1) {
                spheres.splice(index, 1);
            }
            window.result.push({id: parseInt(remove_id), text: remove_text});
            removeAddCategory();
            $(this).slideUp(deleteElement);
        }
    });

    function removeAddCategory() {
        let append = $('.selectAppends').find('.selectAppend');
        append.each(function () {
            let $this = $(this);
            $this.find('option').not(':selected').remove();
            window.result.forEach(function (value) {
                $this.find('.selectCategoryUser').append(`<option value="${value.id}" class="opt">${value.text}</option>`);
            });
        });
    }


    function readyCount() {
        setTimeout(function () {
            let payedLength = payed.concat(allCount).length,
                infoPrice = parseInt(AMOUNT_TARIFF.data('price')),
                sumAll = 0;
            if (payedLength > 1) {
                if(payedLength == 2) {
                    sumAll =  infoPrice  + (infoPrice - (infoPrice * 10 /100));
                }else {
                    sumAll = infoPrice + (infoPrice - (infoPrice * 10 /100)) + (payedLength  - 2) * (infoPrice - (infoPrice * 20 /100));
                }
            }else {
                sumAll = parseInt(AMOUNT_TARIFF.data('price'));
            }

            $('.all-price').text(XFormatPrice(sumAll));
            $('.month-price').text(XFormatPrice(sumAll - window.sum));
        }, 1500);
    }

    function removeNotPayed(sphere, deleteNotPayedIds) {
        setTimeout(function (){
            $.ajax({
                type: 'POST',
                url: '/settings/delete/not-payed',
                data: {
                    user_id: window.authUserId,
                    sphere_id: sphere,
                    sub_spheres_ids: deleteNotPayedIds,
                },
                success: function (res) {
                }
            });
        },500)
    }

    /* map*/
    ymaps.ready(init);

    function init() {
        let map = new ymaps.Map("map-2", {
            center: [40.177628, 44.512555],
            zoom: 10,
            controls: ['zoomControl'],
            behaviors: ['drag']
        });

        let placemark = new ymaps.Placemark([40.177628, 44.512555], {});
        map.geoObjects.add(placemark)

        $("#place_of_delivery").on('keyup', function () {
            myMap();
            $('#map-2').removeClass('remove');
        });

        function myMap() {
            var searchControl = new ymaps.control.SearchControl({
                options: {
                    provider: 'yandex#search',
                    noPopup: false,
                    controls: []
                }
            });
            map.controls.add(searchControl);
            searchControl.search($('.place_of_delivery').val());
            $('.ymaps-2-1-79-controls__control_toolbar').css('display', 'none');

            var myGeocoder = ymaps.geocode($('.place_of_delivery').val());
            myGeocoder.then(
                function (res) {
                    var latitude = res.geoObjects.get(0).geometry.getCoordinates()[0],
                        longitude = res.geoObjects.get(0).geometry.getCoordinates()[1];

                    $('.latitude').val(latitude);
                    $('.longitude').val(longitude);
                }
            );
        }

        // search tooltips
        var suggestView1 = new ymaps.SuggestView('place_of_delivery');
        // search tooltips
    }

    $('#saved-spheres').on('click', function () {
        let userSettingsItem = $('.user-settings-item'),
            data1 = userSettingsItem.find('.input-checkbox-childes .input-checkbox'),
            data2 = userSettingsItem.find('.checkbox-body-append .input-checkbox'),
            data3 = userSettingsItem.find('.checkbox-not-payed .input-checkbox');
        var sub_spheres = [],
            sub_spheres_change = [],
            subSpheresNotPayed = [];

        data1.each(function () {
            if ($(this).find('.global-checkbox').prop("checked") === true) {
                sub_spheres.push($(this).find('.global-checkbox').data('id'));
            }
        });

        data2.each(function () {
            if ($(this).find('.global-checkbox').prop("checked") === true) {
                sub_spheres_change.push($(this).find('.global-checkbox').data('id'));
            }
        });

        data3.each(function () {
            if ($(this).find('.global-checkbox').prop("checked") === true) {
                subSpheresNotPayed.push($(this).find('.global-checkbox').data('id'));
            }
        });

        spheres = spheres.filter(x => !spheres_change.includes(x));
        spheres = spheres.filter(x => !spheresNotPayed.includes(x));
        sub_spheres = sub_spheres.filter(x => !sub_spheres_change.includes(x));
        sub_spheres = sub_spheres.filter(x => !subSpheresNotPayed.includes(x));


        $.ajax({
            type: 'POST',
            url: '/settings/save',
            data: {
                user_id: window.authUserId,
                spheres: spheres,
                sub_spheres: sub_spheres,
                spheres_change: spheres_change,
                sub_spheres_change: sub_spheres_change,
                tariff_id: tariff_id
            },
            success: function (res) {
                success(res, true);
            }
        });
    });

    if ($('.update-success').val().length > 0) {
        $('.setting-title').removeClass('active');
        $('.filter-title').addClass('active');
        $('.page-setting').css("display", "none");
        $('.page-filter').css("display", "block");
        success($('.update-success').val(), false);
    }

    if ($('.add-mup').val()) {
        $('.setting-title').removeClass('active');
        $('.filter-title').addClass('active');
        $('.page-setting').css("display", "none");
        $('.page-filter').css("display", "block");
    }

    function success(text, action) {
        Swal.fire({
            title: `<h2 class="sub-title">${text}</h2>`,
            icon: "success",
            showCloseButton: true,
            confirmButtonColor: '#53B7E8',
            confirmButtonText: "Անձնական տվյալներ",
        }).then((result) => {
            if (result.isConfirmed) {
                window.open(window.personSettings);
            }
        })
    }

    function XFormatPrice(_number) {
        var decimal = 0,
            separator = '.',
            decpoint = '.',
            format_string = '#',
            r = parseFloat(_number),
            exp10 = Math.pow(10, decimal);// приводим к правильному множителю
        r = Math.floor(r * exp10) / exp10;// округляем до необходимого числа знаков после запятой
        rr = Number(r).toFixed(decimal).toString().split('.');
        b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g, "\$1" + separator);
        r = (rr[1] ? b + decpoint + rr[1] : b);
        return format_string.replace('#', r);
    }
});
