$(document).ready(function () {
    $('.eye').on('click', function () {
        $(this).toggleClass('show')
        let input = $(this).parent().find('input')
        let inputType = input.attr('type')
        input.attr('type', inputType === 'password' ? 'text' : 'password')
    })

    $('.news-slider').slick({
        infinite: true,
        slidesToShow: 2,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

    $('.about-us__slider').slick({
        infinite: true,
        slidesToShow: 3,
        autoplay: true,
        autoplaySpeed: 2000,
        asNavFor: '.about-us-parent__slider',
    });

    $('.about-us-parent__slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.about-us__slider'
    });

    $('.menu__icons').on('click', function () {
        $(this).toggleClass('menu__icons-active')
        $('.nav').toggleClass('active')
    });

    $('.select').styler();

    const DIV = $('div')

    if (DIV.is('#chart')) {
        var options = {
            // animationEnabled: true,
            data: [{
                type: "pie",
                startAngle: 270,
                legendText: "{label}",
                indexLabelFontSize: 20,
                indexLabelColor: "#00A775",
                indexLabel: "{label}",
                dataPoints: [
                    {y: 67, label: "Արխիվ (317)", color: "#014732"},
                    {y: 33, label: "Ակտիվ (102)", color: "#00A775"},

                ]
            }]
        };
        $("#chart").CanvasJSChart(options);
    }

    if (DIV.is('#map')) {
        ymaps.ready(init_map_center);
    }

    function init_map_center() {
        myMap = new ymaps.Map("map", {
            center: [40.189506, 44.524799],
            zoom: 16,
            controls: []
        });
        var myPlacemark = new ymaps.Placemark([40.189506, 44.524799], {});
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom')
        myMap.events.add('click', function () {
            myMap.behaviors.enable('scrollZoom');
        })
    }

    $('.new-select-title ').on('click', function () {
        $('.new-select').removeClass('new-select-active')
        $(this).parent('.new-select').toggleClass('new-select-active')
    })

    $('.new-select-bg').on('click', function () {
        $(this).parent('.new-select').removeClass('new-select-active')
    })


    $('.new-select-item .input-checkbox input').on('change', function () {
        var $this = $(this)
        var label = $this.parent().children('label').text()
        if ($this.is(":checked")) {
            let html = `<div class="price__item-check">${label}</div>`
            $(this).parents('.price__inner').children('.price__item-checks').append(html)
        } else {
            let priceItem = $(this).parents('.price__inner').children('.price__item-checks').children()
            for (let i = 0; i < priceItem.length; i++) {
                if (label === priceItem.eq(i).text().trim()) {
                    priceItem.eq(i).remove()
                }
            }
        }

    });

    $(".modal .modal__container").on("click", function (e) {
        e.stopPropagation();
    });

    $(".modal .close, .modal").on("click", function (e) {
        e.preventDefault();
        $(".modal").fadeOut(function () {
            $("body").css("overflow", "auto");
            $('header').css('visibility', 'unset')
        });
    });

    $(".open__modal").on("click", function (e) {
        e.preventDefault();
        var $this = $(this);
        var modal = $this.attr('data-modal');
        $("body").css("overflow", "hidden");
        $('.modal').fadeOut()
        $(`${modal}`).fadeIn();
    });

    $('.user-settings-header__item').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.parent().find('.user-settings-header__item').removeClass('active')
            $this.addClass('active')
            $('.user-settings-item').removeClass('active').eq($this.data('id')).addClass('active')
        }
    })

    $('.input-group input').on('focus', function () {
        var $this = $(this);
        $this.parent().addClass('input-group-active')
        $this.parent().addClass('input-group-now')
    }).on('blur', function () {
        var $this = $(this);
        $this.parent().removeClass('input-group-now')

        if ($this.val() === '') {
            $this.parent().removeClass('input-group-active')
        }
    });

    if ($('input').is('.phone-mask')) {
        $('.phone-mask').inputmask({"mask": '+374 (99) 99-99-99'});
    }

    $('.filter-area__header .filter-area__button').on('click', function () {
        $(this).parents('.filter-area__item').toggleClass('active')
    })

    $('.filter-area__header .input-checkbox input').on('change', function () {
        let inputCheckbox = $(this).parents('.filter-area__item').children('.filter-area__body').children('.input-checkbox')

        for (let i = 0; i < inputCheckbox.length; i++) {
            inputCheckbox.eq(i).children('input').prop("checked", $(this).is(':checked'))
        }
    })

    $('.filter-open').on('click', function () {
        $('.filter-block').toggleClass('active')
    })

    $('.filter-close').on('click', function () {
        $('.filter-block').toggleClass('active')
    })


    var kmg_fix = 45, // В каком положении полосы прокрутки прятать верхнюю панель
        speed = 350, // Скорость прокрутки
        tempScrollTop, currentScrollTop = 0;
    $(window).scroll(function () {
        currentScrollTop = jQuery(window).scrollTop();
        if (tempScrollTop < currentScrollTop) {
            if ($(this).scrollTop() > kmg_fix) {
                $('header').addClass('kmg-fix');
            } else {
                $('header').removeClass('kmg-fix');
            }
        } else if (tempScrollTop > currentScrollTop) {
            $('header').removeClass('kmg-fix');
        } else if (tempScrollTop > currentScrollTop) {
            $('header').removeClass('kmg-fix');
        }
        tempScrollTop = currentScrollTop;
    });

    if (window.authUserId !== undefined) {
        $.ajax({
            type: 'GET',
            url: '/check/spheres',
            data: {
                user_id: window.authUserId,
            },
            success: function (res) {
                if (res[0] === 'error') {
                    return saveCompetition(res[1]);
                }
            }
        });
    }

    var sub = [],
        sphereId = null;

    function saveCompetition(data) {
        $('.selectCategory').prepend('<option selected></option>').select2({
            theme: "classic",
            containerCssClass: "user-select2-container-class",
            placeholder: "Ընտրել ոլորտ",
            "language": {
                "noResults": function () {
                    return "Ոլորտ չի գտնվել";
                },
                searching: function () {
                    return "Փնտրում...";
                },
                errorLoading: function () {
                    return "Արդյունքները չհաջողվեց բեռնել"
                }
            },
            data: data
        });

        $('.selectSubCategory').prepend('<option selected></option>').select2({
            theme: "classic",
            containerCssClass: "user-select2-container-class",
            placeholder: "Ընտրել ենթաոլորտ",
            "language": {
                "noResults": function () {
                    return "Ենթաոլորտ չի գտնվել";
                },
                searching: function () {
                    return "Փնտրում...";
                },
                errorLoading: function () {
                    return "Արդյունքները չհաջողվեց բեռնել"
                }
            },
        });

        $(".selectCategory").on('change', function () {
            $('.error-valid').empty();
            var id = $(".selectCategory option:selected").val(),
                appends = $('.input-checkbox-appends'),
                $this = $(this);
            $.ajax({
                type: 'GET',
                url: '/subcategories',
                data: {
                    id: id
                },
                success: function (res) {
                    sub = [];
                    if (res[0] === 'sphere_id') {
                        sphereId = res[1];
                        appends.css('display', 'none');
                    } else {
                        appends.css('display', 'block');
                        appends.html(res);
                        let check = $this.parents('.body-select').find('.input-checkbox-appends .checkbox');
                        check.each(function () {
                            $(this).prop('checked', true);
                            sub.push($(this).data('id'));
                        });
                    }
                }
            });
        });

        $(".competition-modal").fadeIn();
    }

    $('body').on('change', '.checkbox', function () {
        // var $this = $(this),
        //     checkbox = $this.parents('.input-checkbox-appends').find('.input-checkbox .checkbox:checked');
        // if (checkbox.length === 0) $this.prop('checked', true)
        sphereId = null;
        let ids = $(this).data('id');
        if ($(this).is(':checked')) {
            sub.push(ids);
        } else {
            if (sub != 'undefined') {
                for (let i = 0; i < sub.length; i++) {
                    if (sub[i] === ids) {
                        sub.splice(i, 1)
                    }
                }
            }

        }
    });

    $(".save-category-user").on('click', function () {
        $('.error-valid').empty();
        $.ajax({
            type: 'POST',
            url: '/save/sphere',
            data: {
                user_id: window.authUserId,
                sub_spheres: sub,
                sphere_id: sphereId
            },
            success: function () {
                sessionStorage.setItem( 'first_user_sphere', 1 );
                window.location.href = '/tenders'
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                Object.entries(errors).forEach(([key, value]) => {
                    if (key === 'sphere_id') {
                        $(".body-select").append(`<small class="error-valid">${value[0]}</small>`)
                    }
                });
            }
        });
    });

    const anchors = document.querySelectorAll('a[href*="#"]')

    for (let anchor of anchors) {
        anchor.addEventListener('click', function (e) {
            e.preventDefault()
            const blockID = anchor.getAttribute('href').substr(1)
            document.querySelector('.nav').classList.remove('active')

            document.getElementById(blockID).scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            })
        })
    }
    $('input[type="file"]').on('change', function (e) {

        if (e.target.files.length != 0) {
            $(this).parent().children('.file-text').html('');
            $.each(e.target.files, (idx, val) => {
                $(this).parent().find('.file-text').append(`<span class="file-text-block">${val.name} <span style="padding: 10px; color: red" class="file-delete" title="Ջնջել ֆայլը">x</span></span>`)
            })
        }
        $('.file-delete').on('click', function (e) {
            e.preventDefault();
            $(this).parent().remove();
            if($('div.user-settings-file>div>label>span.file-text').html() == ''){
                $('div.user-settings-file>div>label>span.file-text').append(`<span class="file-text">Կցել ֆայլ</span>`);
            };
            })
    })

    jQuery.datetimepicker.setLocale('hy');
    $('.datepicker').datetimepicker({
        timepicker: false,
        format: 'd.m.Y',
        scrollMonth: false,
        scrollInput: false,
    })


    function addCheckedAttribute(data, elementCount){
        for(let i = 0; i< elementCount; i++){
            data[i].setAttribute( "checked" , 'checked');
        }
    }

    // addCheckedAttribute($('.home-sphere-input_checkbox'), 2);
    // addCheckedAttribute($('.pro_input-checkbox'), 2);
    // addCheckedAttribute($('.basic_input-checkbox'), 2);


});
