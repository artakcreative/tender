<?php

namespace Database\Seeders;



use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Permission::create(['name' => 'edit']);
        Permission::create(['name' => 'delete']);
        Permission::create(['name' => 'create']);
        Permission::create(['name' => 'update']);
        Permission::create(['name' => 'Edit approved statement', 'display_name'=>'Խմբագրել հաստատված հայտարարությունը']);
        Permission::create(['name' => 'Set a deadline for paid user service', 'display_name'=>'Սահմանել օգտատիրոջ վճարովի ծառայության վերջնաժամկետը']);
        Permission::create(['name' => 'Adding a new customer and industry', 'display_name'=>'Նոր պատվիրատուի եւ ոլորտի ավելացում']);
        Permission::create(['name' => 'Attach user manager', 'display_name'=>'Կցել օգտատիրոջը մենեջեր']);

        $role = Role::create(['name' => 'admin'])
            ->givePermissionTo(['create', 'delete','update','edit']);
        Role::create(['name'=>'manager'])->givePermissionTo(['Edit approved statement', 'Set a deadline for paid user service', 'Adding a new customer and industry', 'Attach user manager']);
        Role::create(['name'=>'user']);
        $data = [
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('12345678')
        ];

        $admin = Admin::create($data);

        $admin->assignRole('admin');
    }
}
