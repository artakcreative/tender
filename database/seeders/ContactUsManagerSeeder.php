<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactUsManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contact_us')->truncate();
        $data = [
            [
                'name' => 'John',
                'surname' => 'Doe',
                'phone' => '+37445785966',
                'email' => 'test@test.ru',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
        ];

        DB::table('contact_us')->insert($data);
    }
}
