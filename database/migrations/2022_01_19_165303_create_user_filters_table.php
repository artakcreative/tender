<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_filters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('estimated_price_start')->nullable();
            $table->integer('estimated_price_end')->nullable();
            $table->json('business_address')->nullable();
            $table->integer('place_of_delivery')->nullable();
            $table->boolean('availability_of_financial_means')->nullable();
            $table->date('date_deadline')->nullable();
            $table->integer('prepayment_available')->nullable();
            $table->boolean('proof')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_filters');
    }
}
