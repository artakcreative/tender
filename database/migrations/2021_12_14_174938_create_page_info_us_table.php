<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageInfoUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_info_us', function (Blueprint $table) {
            $table->id();
            $table->text('description');
            $table->string('email');
            $table->string('phone');
            $table->string('more_phone');
            $table->string('youtube');
            $table->string('facebook');
            $table->string('linkedin');
            $table->string('full_address');
            $table->string('footer_info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_info_us');
    }
}
