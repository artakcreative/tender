<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->id();
            $table->string('announcement_name')->nullable();
            $table->string('announcement_password')->nullable();
            $table->unsignedBigInteger('competition_type_id')->nullable();
            $table->unsignedBigInteger('how_to_apply_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('purchase_item')->nullable();
            $table->string('estimated_price')->nullable();
            $table->string('application_deadline')->nullable();
            $table->string('return_auction_day')->nullable();
            $table->string('application_opening_day')->nullable();
            $table->date('deadline_for_supply')->nullable();
            $table->json('place_of_delivery')->nullable();
            $table->boolean('availability_of_financial_resources')->nullable();
            $table->json('payment_schedule')->nullable();
            $table->json('prepayment')->nullable();
            $table->json('provides')->nullable();
            $table->text('other_conditions')->nullable();
            $table->string('file_path')->nullable();
            $table->string('create_date')->nullable();
            $table->tinyInteger('status')->default(\App\Models\Announcement::SAVED);
            $table->tinyInteger('archive')->nullable();
            $table->unsignedBigInteger('manager_id')->nullable();
            $table->timestamps();

            $table->foreign('manager_id')->references('id')->on('admins')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('competition_type_id')->references('id')->on('competition_types')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('how_to_apply_id')->references('id')->on('competition_application_forms')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('competition_customers')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
