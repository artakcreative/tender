<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAnnouncementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->longText('payment_schedule_for_pdf')->nullable()->after('payment_schedule');
            $table->longText('results_of_monitoring')->nullable()->after('purchase_item');
            $table->longText('doses')->nullable()->after('other_conditions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->dropColumn('payment_schedule_for_pdf');
            $table->dropColumn('results_of_monitoring');
            $table->dropColumn('doses');
        });
    }
}
