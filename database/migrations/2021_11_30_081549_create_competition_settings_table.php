<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_settings', function (Blueprint $table) {
            $table->id();
            $table->json('competition_type')->nullable();
            $table->json('how_to_apply')->nullable();
            $table->json('customer_affiliation')->nullable();
            $table->json('customers')->nullable();
            $table->json('sphere')->nullable();
            $table->integer('template_id')->nullable();
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->timestamps();

            $table->foreign('admin_id')->references('id')->on('admins')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_settings');
    }
}
