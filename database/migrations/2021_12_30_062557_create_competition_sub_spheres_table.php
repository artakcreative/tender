<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionSubSpheresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_sub_spheres', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('competition_sphere_id');
            $table->timestamps();

            $table->foreign('competition_sphere_id')->references('id')->on('competition_spheres')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_sub_spheres');
    }
}
