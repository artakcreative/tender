@extends('profile.personal')
@section('personal')
    <div class="main-content">
        <div class="user-settings">
            <div class="user-settings-header">
                <div class="user-settings-header__item active" data-id="0">Անձնական տվյալներ</div>
                <div class="user-settings-header__item" data-id="1">Փոխել գաղտնաբառը</div>
            </div>
            <div class="user-settings-item active">
                <div class="input-radio-group">
                    <div class="input-radio-inner">
                        <div class="input-radio">
                            <input type="radio" id="fz" name="group" value="physical-person_radio"
                                   @if(($user->physicalPerson == null and  $user->legalPerson == null) or $user->physicalPerson !== null)checked @endif
                                   @if($user->legalPerson !== null) disabled @endif>
                            <label for="fz">Ֆիզիկական անձ</label>
                        </div>
                        <div class="input-radio">
                            <input type="radio" id="noFz" name="group" value="legal-person_radio"
                                   @if( $user->legalPerson !== null)checked @endif
                                   @if($user->physicalPerson !== null) disabled @endif>
                            <label for="noFz">Իրավաբանական անձ (ԱՁ, ՍՊԸ)</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id" value="{{ $user->id }}">
                <div class="input-group {{ $user->name !== null ? 'input-group-active' : '' }}">
                    <input type="text" id="name" name="name" value="{{ $user->name }}"
                           @if($user->data_blocked == true) disabled @endif>
                    <label for="name">Անուն <span class="text-important">*</span></label>
                </div>
                <div class="input-group {{ $user->surname !== null ? 'input-group-active' : '' }}">
                    <input type="text" id="surname" name="surname" value="{{ $user->surname }}"
                           @if($user->data_blocked == true) disabled @endif>
                    <label for="surname">Ազգանուն <span class="text-important">*</span></label>
                </div>
                @if($user->legalPerson == null)
                    <div id="physical-person_block">
                        <input type="hidden" name="physical_success" value="true-physical">
                        <div class="input-group {{ ($user->physicalPerson  and  $user->physicalPerson['public_service_license_plate'] !== null) ? 'input-group-active' : '' }}">
                            <input type="text" id="public-service_license_plate" name="public_service_license_plate"
                                   @if($user->data_blocked == true) disabled @endif
                                   value="{{ ($user->physicalPerson  and  $user->physicalPerson['public_service_license_plate'] !== null) ? $user->physicalPerson['public_service_license_plate'] : null }}">
                            <label for="public-service_license_plate">ՀԾՀ <span class="text-important">*</span></label>
                        </div>
                        <div class="input-group {{($user->physicalPerson  and  $user->physicalPerson['passport_number'] !== null) ? 'input-group-active' : '' }}">
                            <input type="text" id="passport_number" name="passport_number" @if($user->data_blocked == true) disabled @endif
                            value="{{($user->physicalPerson  and  $user->physicalPerson['passport_number'] !== null) ? $user->physicalPerson['passport_number'] : null }}">
                            <label for="passport_number">Անձնագրի համար <span class="text-important">*</span></label>
                        </div>
                        <div class="input-group {{ ($user->physicalPerson  and  $user->physicalPerson['passport_take_date'] !== null) ? 'input-group-active' : '' }} block-datepicker">
                            <input type="text" id="passport_take_date" name="passport_take_date" class="datepicker"
                                   autocomplete="off" @if($user->data_blocked == true) disabled @endif
                                   value="{{ ($user->physicalPerson  and  $user->physicalPerson['passport_take_date'] !== null) ? $user->physicalPerson['passport_take_date']  : null }}">
                            <label for="passport_take_date">Տրված <span class="text-important">*</span></label>
                        </div>
                        <div class="input-group {{ ($user->physicalPerson  and  $user->physicalPerson['passport-take_from'] !== null) ? 'input-group-active' : '' }}">
                            <input type="text" id="passport-take_from" name="passport_take_from" @if($user->data_blocked == true) disabled @endif
                            value="{{ ($user->physicalPerson  and  $user->physicalPerson['passport-take_from'] !== null) ? $user->physicalPerson['passport-take_from'] : null }}">
                            <label for="passport-take_from">Ում կողմից <span class="text-important">*</span></label>
                        </div>
                    </div>
                @endif

                @if($user->physicalPerson == null)
                    <div id="legal-person_block">
                        @include('profile.pages.sub_pages.person_legal_entity', ['user' => $user])
                    </div>
                @endif



                <div class="input-group {{ $user->email !== null ? 'input-group-active' : '' }}">
                    <input type="text" id="email" name="email"
                           @if($user->data_blocked == true) disabled @endif value="{{ $user->email }}">
                    <label for="email">Էլ-հասցե <span class="text-important">*</span></label>
                </div>
                <div class="input-group {{ $user->phone !== null ? 'input-group-active' : '' }}">
                    <input type="text" id="phone" class="phone-mask" name="phone"
                           @if($user->data_blocked == true) disabled @endif value="{{ $user->phone }}">
                    <label for="phone">Հեռախոսահամար <span class="text-important">*</span></label>
                </div>
                <div class="user-settings-file">
                    <div>
                        <label class="download-file">
                            <input type="file" name="files" id="upload" multiple>
                            <span class="file-text">Կցել ֆայլ</span>
                        </label>
                    </div>
                    <div>
                        @foreach($user->getMedia() as $media)
                            @if($media->mime_type == "image/png")
                                <div class="file-list d-flex">
                                    <div class="file-item">
                                        <img src="{{ $media->getUrl() }}" alt="" style="width: 150px">
                                    </div>
                                </div>
                            @else
                                @php
                                    $ext = explode('.', $media->file_name);
                                @endphp
                                <div class="file-list" style="margin: 0">
                                    <div class="file-item">
                                        <label class="download-contract">
                                            <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name}}.{{$ext[1]}}</a>
                                        </label>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>

                    <div>
                        <label class="download-file">
                            <input type="file" name="contract_files" id="upload_contract" multiple>
                            <span class="file-text">Կցել պայմանագրի Ձեր օրինակը</span>
                        </label>
                    </div>
                </div>

                @if($user->getMedia('contract'))
                    @foreach($user->getMedia('contract') as $media)
                        @php
                            $ext = explode('.', $media->file_name);
                        @endphp
                        <div class="file-list" style="margin: 0">
                            <div class="file-item">
                                <label class="download-contract">
                                    <a href="{{$media->getUrl()}}" class="download_contract" target="_blank">{{$media->name}}.{{$ext[1]}}</a>
                                </label>
                            </div>
                        </div>
                    @endforeach
                @endif

                @if($admin->getMedia('contract-global'))
                    @foreach($admin->getMedia('contract-global') as $media)
                        <div class="file-list" style="margin: 0">
                            <div class="file-item">
                                <label class="download-file">
                                    <a href="{{$media->getUrl()}}" class="download_contract" target="_blank">Բեռնել պայմանագրի
                                        օրինակը</a>
                                </label>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="buttons-group">
                    <button class="btn person-information-save">Պահպանել</button>
                </div>
            </div>
            <div class="user-settings-item">
                <div class="input-radio-group">
                    <div class="input-group old-pass-body">
                        <input type="password" id="pass" name="old_password" placeholder="Գրեք հին գաղտնաբառը">
                        <label for="pass">Հին գաղտնաբառ </label>
                        <span class="eye"></span>
                    </div>
                    <div class="input-group pass-body">
                        <input type="password" id="pass1" name="password" placeholder="Գրեք նոր գաղտնաբառը">
                        <label for="pass1">Նոր գաղտնաբառ </label>
                        <span class="eye"></span>
                    </div>
                    <div class="input-group pass-conf-body">
                        <input type="password" id="pass2" name="password_confirmation" placeholder="Կրկնեք գաղտնաբառը">
                        <label for="pass2">Կրկնեք գաղտնաբառը </label>
                        <span class="eye"></span>
                    </div>
                    <div class="buttons-group">
                        <button type="button" class="btn person-password-save">Պահպանել</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal password-success-modal">
        <div class="modal__container">
            <div class="close"></div>
            <p class="text-success-pass"></p>
        </div>
    </div>
@endsection

