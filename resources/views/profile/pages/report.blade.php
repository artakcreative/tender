@extends('profile.personal')
@section('personal')
    <div class="main-content">
        <div class="d-flex">
            <div class="main-content__tab general-dynamics-link active">Ընդհանուր դինամիկա</div>
            <div class="main-content__tab report-sectoral-link">Ոլորտային դինամիկա</div>
            <div class="main-content__tab report-structure-link">Կառուցվածքային դինամիկա</div>
        </div>
        <div class="blog-general-dynamics">
            <div>
                <button class="btn-filter filter_btn-report">Ֆիլտրեր</button>
            </div>
            <div style="display: none" class="d-flex filter-date-report">
                <div>
                    <p class="filter-title">Սկիզբ</p>
                    <div class="filter-input block-datepicker">
                        <input type="text" name="date_start" class="timepicker_start datepicker-report" autocomplete="off">
                    </div>
                </div>
                <div class="div-end-range">
                    <p class="filter-title">Վերջ</p>
                    <div class="filter-input block-datepicker">
                        <input type="text" name="date_end" class="timepicker_end datepicker-report" autocomplete="off">
                    </div>
                </div>
                <div class="div-periodicity">
                    <p class="filter-title">Պարբերականություն</p>
                    <div class="filter-input">
                        <select class="periodicity periodicity-general">
                            <option value="1">շաբաթական</option>
                            <option value="2">ամսական</option>
                            <option value="3">եռամսյակային</option>
                        </select>
                    </div>
                </div>
            </div>
            <p class="error-range-text">Ժամանակահատվածը նշելը պարտադիր է։</p>
            <br>
            <div>
                <div id="chart-general-dynamics"  style="width: 100%; height: 500px"></div>
            </div>
        </div>
        @include('profile.pages.sub_pages.report_sectoral')
        @include('profile.pages.sub_pages.report_structure')
    </div>
@endsection
