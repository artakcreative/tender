@extends('profile.personal')
@section('personal')
    <input type="hidden" name="success" class="filter-success" value="{{ $success }}">
    <input type="hidden" name="first-sphere" class="first-sphere" value="{{ session()->get('first_user_sphere') }}">
    <div class="main-content">
        <div class="main-content-filter">
            <div class="main-content-filter__item input-radio-group">
                <div class="input-radio-title @if($tariff_success == false) d-none @endif ">Փաթեթ ` {{ $tariff['name'] }}</div>
                <div class="input-radio-title"></div>
                <div class="input-radio-inner">
                </div>
            </div>
            <div class="main-content-filter__item ">
                <button class="btn-filter filter-open">Ֆիլտրեր</button>
            </div>
        </div>
        <br>
        <div class="d-flex">
            <div class="main-content__tabs">
                <div class="main-content__tab active-but active">Ակտիվ</div>
                <div class="main-content__tab archive-but">Արխիվ</div>
            </div>
        </div>
        <div class="blog-active">
            <div class="blog-list">
                @if(count($active) > 0)
                    @foreach($active as $value)
                        <div class="blog-item">
                            <div class="blog-left">
                                <div class="type">
                                    <b>{{$value['announcement_name']}}</b>
                                </div>
                                <div class="type">
                                    Մրցույթի տիպ՝
                                    <b>{{$value->competition_types ? $value->competition_types['name'] : null }}</b>
                                </div>
                                <div class="type">
                                    Մրցույթի կոդ՝
                                    <b>{{ $value->id }}</b>
                                </div>
                                <div class="date">
                                    Հայտի ներկայացման վերջնաժամկետ՝
                                    <b>{{ Carbon\Carbon::parse($value['application_deadline'])->format('d.m.Y') }}</b>
                                </div>
                                <div class="field">
                                    @if(isset($value['sub_spheres']) || isset($value['spheres']))
                                        @php
                                            $announcement_sub_spheres_spheres = [];
                                            $announcement_spheres = [];
                                            foreach ($value['sub_spheres'] as $sub_spheres){
                                            $announcement_sub_spheres_spheres[] = $sub_spheres->sphere['name'];
                                            }
                                            if (isset($value['spheres'])){
                                                foreach ($value['spheres'] as $sphere){
                                                    $announcement_spheres[] = $sphere['name'];
                                                }
                                                $value_spheres = array_merge($announcement_sub_spheres_spheres,$announcement_spheres);

                                            }

                                        $sphere_text = "";

                                        foreach (array_unique($value_spheres) as $value_sphere){
                                            $sphere_text .= $value_sphere . ", ";
                                        }
                                        $sphere_text = substr($sphere_text, 0, -2)
                                        @endphp

                                        Ոլորտ՝
                                            <b> {{  $sphere_text  }}</b>
                                    @endif

                                </div>
                                <div class="sub-field">
                                    Ենթաոլորտ՝
                                    @if(isset($value['sub_spheres']) && count($value['sub_spheres']))
                                        @php
                                            $sub_sphere_name = "";
                                            foreach ( $value['sub_spheres'] as $item){
                                                $sub_sphere_name .= $item['name'] . ', ';
                                            }
                                            $sub_sphere_name = substr($sub_sphere_name, 0, -2);

                                        @endphp
                                        {{$sub_sphere_name}}
                                    @else
                                        Տվյալ հայտարարությունը ենթաոլորտ չունի
                                    @endif

                                </div>
                            </div>
                            <div class="blog-right">
                                @forelse($user_announcements as $user_announcement)

                                    @if(auth_user() && isset($user_announcement) && $value['id'] == $user_announcement['id'] )
                                        <div class="saved favorite_announcement active"
                                             data-announcement_id="{{$value['id']}}"
                                             data-user_id="{{auth_user()->id}}"></div>
                                        @break
                                    @else
                                        <div class="saved favorite_announcement" data-announcement_id="{{$value['id']}}"
                                             data-user_id="{{auth_user()->id}}"></div>
                                    @endif
                                @empty
                                    <div class="saved favorite_announcement" data-announcement_id="{{$value['id']}}"
                                         data-user_id="{{auth_user()->id}}"></div>
                                @endforelse
                                <div class="text">Նախահաշվային գին՝
                                    <b>{{  number_format( $value['estimated_price'], 0 , '.', ' ')  }} դրամ</b></div>
                                <div class="d-flex align-items-center">
                                    @if(count($value->getMedia('generate_pdf'))> 0)
                                        <div><a href="{{ $value->getMedia('generate_pdf')[0]->getUrl() }}"
                                                target="_blank">
                                                <p class="pdf"></p></a>
                                        </div>
                                    @endif
                                    <form method="post" action="{{ route('user.tenders.details') }}"
                                          class="form-horizontal">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $value['id'] }}">
                                        <button type="submit" class="btn btn-info btn-round">Մանրամասները</button>
                                    </form>
                                    <input type="hidden" class="unread_announcement_id" value="{{$value['id']}}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    {{--            {{dd(auth_user()->userSpheres->all()[0]->payed)}}--}}
                    @if(!auth_user()->userSpheres->all()[0]->payed)
                        <p style="margin: 50px 0 20px;"> Ակտիվ տենդերները տեսնելու համար ձեռք բերեք մեր ծառայությունների
                            փաթեթներից մեկը:</p>
                        <a href="{{route('user.settings')}}" class="btn btn-primary">Անցնել վճարման</a>
                    @else
                        <p style="margin: 50px 0 20px;"> Ֆիլտրով որոնելու արդյունքում տվյալներ չկան</p>
                    @endif
                @endif
            </div>
            {{ $active->links('vendor.pagination.simple-tailwind') }}
        </div>
        @include('profile.pages.sub_pages.archive', ['archive' => $archive])
    </div>
    @include('profile.pages.sub_pages.filter', ['spheres' => $spheres,
                                                'customer' => $customer,
                                                'mapSuccess' => $mapSuccess,
                                                'userTemporaryFilter' => $userTemporaryFilter])
@endsection
