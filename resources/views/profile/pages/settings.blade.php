@extends('profile.personal')
@section('personal')
    <input type="hidden" name="success" class="update-success" value="{{ session()->get('success')}}">
    <input type="hidden" name="map" class="add-mup" value="{{ $map}}">
    <div class="main-content">
        <div class="user-settings">
            <div class="user-settings-header">
                <div class="user-settings-header__item setting-title active" data-id="0">Կարգավորումներ</div>
                <div class="user-settings-header__item filter-title" data-id="1">Իմ ֆիլտր</div>
            </div>
        </div>
        <div class="user-settings-item active page-setting">
            <div class="setting">
                <div class="setting-item">
                    <div class="setting-header">
                        <div class="sub-title-2">Փաթեթ՝ <b class="tariff"></b></div>
                        <div class="text-right">Ակտիվ է մինչ՝ <b class="activeDate"></b></div>
                    </div>
                    <div class="input-checkbox">
                        <input type="checkbox" id="checkbox" name="checkbox" checked disabled  class="state"
                               data-id="{{ $tariffs[0]['id']}}"  data-price="{{ $tariffs[0]['price'] }}"
                               data-name="{{ $tariffs[0]['name']}}" >
                        <label for="checkbox">Պետական գնումներ</label>
                    </div>
                    <div class="input-checkbox">
                        <input type="checkbox" id="private" name="private"
                               data-id="{{ $tariffs[1]['id']}}"  data-price="{{ $tariffs[1]['price'] }}"
                               data-name="{{ $tariffs[1]['name']}}">
                        <label for="private">Մասնավոր տեղական և մասնավոր միջազգային գնումներ</label>
                    </div>
                </div>
                <div class="setting-item">
                    <div>
                        <span class="green">*Յուրաքանչուր ոլորտի ընտրության արժեքը՝ <span class="green amountTariff" data-price="0"></span> ֏ </span>
                        <p style="margin-top:20px"><span class="green">*2-րդ ոլորտի համար 10% զեղչ</span></p>
                        <p style="margin-top:20px"><span class="green">*3֊րդ և հաջորդ ոլորտների համար՝ 20% զեղչ</span></p>
                    </div><br>
                    <div class="sub-title-2">Վճարված ոլորտներ</div>
                    <span class="green">*Ձեր ընտրած ոլորտը կարող եք փոփոխել 15 օր հետո</span>
                    <div class="selectAppends" style="margin-top:20px"></div>
                </div>
                <div class="setting-item-not-payed">
                    <div class="sub-title-2">Չվճարված ոլորտներ</div>
                    <div class="selectNotPayed"></div>
                </div>
                <input type="hidden" name="actived" value="{{ $actived }}">

                <div class="sub-title-2">Այլ ոլորտներ</div>
                <form class="repeater">
                    <div data-repeater-list="category-group">
                        <div data-repeater-item class="parent-item">
                            <div class="form-group body-select-settings">
                                <select class="selectCategoryUserChildes js-states form-control">
                                    <option value=""></option>
                                </select>
                                <input data-repeater-delete type="button" class="btn btn-action delete-select" value="Հեռացնել"/>
                            </div>
                            <div class="input-checkbox-childes"></div>
                        </div>
                    </div>
                    <input data-repeater-create type="button" class="addSelect btn btn-action" value="Ավելացնել"/>
                </form>
            </div>
            <div class="sub-title-2">Վճարման ենթակա արժեք՝ <b class="month-price"></b><b>֏</b></div>
            <div class="sub-title-2">Ընդանուր վճարը՝ <b class="all-price"></b><b>֏</b></div>
            <div class="d-flex">
                <button class="btn" id="saved-spheres">Անցնել վճարման</button>
            </div>
        </div>
        @include('profile.pages.sub_pages.person_filter', ['filter' => $filter])
    </div>
@endsection

<script>
    @auth()
      window.personSettings = `{{ route("user.person.settings") }}`;
    @endauth
</script>
@push('script')
    <script type="text/javascript" src="{{ asset('js/profile/assets/js/core/personal/settings.js?v='). time() }}"></script>
@endpush
