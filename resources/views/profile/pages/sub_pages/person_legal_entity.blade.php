<div class="input-group {{ ($user->legalPerson !== null and $user->legalPerson['tin'] !== null) ? 'input-group-active' : '' }}">
    <input type="text" id="tin" name="tin" @if($user->data_blocked == true) disabled @endif
           value="{{ ($user->legalPerson !== null and $user->legalPerson['tin'] !== null) ?  $user->legalPerson['tin'] : null }}">
    <label for="tin">ՀՎՀՀ <span class="text-important">*</span></label>
</div>
<div class="input-group {{ ($user->legalPerson !== null and $user->legalPerson['legal_address'] !== null) ? 'input-group-active' : '' }}">
    <input type="text" id="legal_address" name="legal_address" @if($user->data_blocked == true) disabled @endif
           value="{{ ($user->legalPerson !== null and $user->legalPerson['legal_address'] !== null) ? $user->legalPerson['legal_address'] : null }}">
    <label for="legal_address">Իրավաբանական հասցե <span class="text-important">*</span></label>
</div>
<div class="input-group {{ ($user->legalPerson !== null and $user->legalPerson['bank'] !== null) ? 'input-group-active' : '' }}">
    <input type="text" id="bank" name="bank" @if($user->data_blocked == true) disabled @endif
           value="{{ ($user->legalPerson !== null and $user->legalPerson['bank'] !== null) ? $user->legalPerson['bank'] : null }}">
    <label for="bank">Բանկ</label>
</div>
<div class="input-group {{ ($user->legalPerson !== null and $user->legalPerson['bank_account'] !== null) ? 'input-group-active' : '' }}">
    <input type="text" id="bank_account" name="bank_account" @if($user->data_blocked == true) disabled @endif
           value=" {{ ($user->legalPerson !== null and $user->legalPerson['bank_account'] !== null) ? $user->legalPerson['bank_account'] : null }}">
    <label for="bank_account">Բանկային հաշվեհամար</label>
</div>
<input type="hidden" name="legal_success">
