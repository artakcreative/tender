<div class="filter-block">
    <div class="filter-block__header">
        <div class="sub-title">Ֆիլտր</div>
        <div class="close filter-open"></div>
    </div>
    <form method="post" action="{{route('user.tenders')}}">
        @csrf
        <div class="filter-item">
{{--            <div class="search">--}}
{{--                <input type="search" id="search-sub_spheres" placeholder="Որոնեք ոլորտի անվանումը"/>--}}
{{--                <button class="icon-search"></button>--}}
{{--            </div>--}}
            <br>
            <div class="filter-area">
                <div class="filter-area__list">

                    @foreach($spheres as $key=>$value)
                        <div class="filter-area__item all-spheres">
                            <div class="filter-area__header">
                                <div class="filter-title">
                                    <div class="input-checkbox">
                                        <input type="checkbox" name="sphere[]" value="{{  $value['id'] }}" id="checkbox{{ $value['name']}}"
                                        @if(count($userTemporaryFilter)>0 and count($userTemporaryFilter['spheres'])>0 and in_array($value['id'], $userTemporaryFilter['spheres']) ) checked  @endif>
                                        <label for="checkbox{{ $value['name'] }}">
                                            <span class="filter-title">{{ $value['name'] }}</span>
                                        </label>
                                    </div>
                                </div>
{{--                                @if(count($value['sub_spheres']) > 0)--}}
{{--                                    <div class="filter-area__button"></div>--}}
{{--                                @endif--}}
                            </div>
                            <div class="filter-area__body">
                                @if(count($value['sub_spheres']) > 0)
                                    @foreach($value['sub_spheres'] as $item)
                                        <div class="input-checkbox">
                                            <input type="checkbox" name="sub_sphere[]" value="{{ $item['id'] }}" id="checkbox{{ $item['name'] }}">
                                            <label for="checkbox{{ $item['name'] }}">{{ $item['name'] }}</label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="filter-item">
            <div class="filter-title">Նախահաշվային գին</div>

            <div class="extra-controls form-inline">
                <div class="filter-flex">
                    <div class="filter-flex__item filter-input">
                        <input type="number" name="start" class="js-input-from form-control"
                               @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['estimated_price_start'] !== null) value="{{ $userTemporaryFilter['estimated_price_start'] }}" @else value="0" @endif >
                    </div>
                    <div class="filter-flex__item filter-input">
                        <input type="number" name="end" class="js-input-to form-control"
                               @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['estimated_price_end'] !== null) value="{{ $userTemporaryFilter['estimated_price_end'] }}" @else value="50000000" @endif >
                    </div>
                </div>
            </div>
            <div class="range-slider">
                <input type="text" class="js-range-slider" value=""/>
            </div>
        </div>

{{--        <div class="filter-item">--}}
{{--            <div class="filter-area__item">--}}
{{--                <div class="filter-area__header">--}}
{{--                    <div class="filter-title">Պատվիրատու</div>--}}
{{--                    <div class="filter-area__button"></div>--}}
{{--                </div>--}}
{{--                <div class="filter-area__body">--}}
{{--                    <div class="search">--}}
{{--                        <input type="search" id="search-organization" placeholder="Որոնեք կազմակերպության անվանումը"/>--}}
{{--                        <button class="icon-search"></button>--}}
{{--                    </div>--}}
{{--                    <div class="all-organizations">--}}
{{--                        @if(count($customer) > 0)--}}
{{--                            @foreach($customer as $item)--}}
{{--                                <div class="input-checkbox">--}}
{{--                                    <input type="checkbox" name="organization[]" value="{{ $item['id'] }}" id="checkbox{{ $item['name'] }}">--}}
{{--                                    <label--}}
{{--                                        for="checkbox{{ $item['name'] }}">{{ $item['name'].' '.'('.$item['abbreviation'] . ')' }}</label>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        @if($mapSuccess == true)

            <div>
                <div class="filter-title">Մատակարարման վայրը ըստ շառավիղի</div>
                <div class="filter-input">
                    <input type="number" name="delivery" min="0" placeholder="0 կմ․"
                           @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['place_of_delivery'] !== null) value="{{ $userTemporaryFilter['place_of_delivery'] }}" @endif>
                </div>
            </div>
            @else
            <div class="filter-item">
                <div class="filter-title">Դուք չունենք նշված գործունեության հասցե</div>

                <div class="filter-input">
                    <button class="btn btn-border"><a href="{{ route('user.settings', ['map' => 1]) }}">Ավելացնել</a></button>
                </div>
            </div>
        @endif
        <div class="filter-item">
            <div class="filter-title">Ֆինանսական միջոցի առկայություն</div>
            <div class="input-radio-inner">
                <div class="input-radio">
                    <input type="radio" id="yes" value="1" name="means"
                           @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['availability_of_financial_means'] == 1) checked  @endif>
                    <label for="yes">Այո</label>
                </div>
                <div class="input-radio">
                    <input type="radio" id="no" value="2" name="means"
                           @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['availability_of_financial_means'] == 2) checked  @endif>
                    <label for="no">Ոչ </label>
                </div>
                <div class="input-radio">
                    <input type="radio" id="all" value="0" name="means"
                           @if((count($userTemporaryFilter)>0 and $userTemporaryFilter['availability_of_financial_means'] == 0)
                            or count($userTemporaryFilter) == 0 ) checked @endif>
                    <label for="all">Բոլորը </label>
                </div>
            </div>
        </div>

        <div class="filter-item">
            <div class="filter-title">Հայտի վերջնաժամկետ</div>
            <div class="filter-input block-datepicker">
                <input type="text" name="date" class="datepicker" autocomplete="off"
                       @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['date_deadline'] !== null) value="{{ $userTemporaryFilter['date_deadline'] }}" @endif>
            </div>
        </div>
        <div class="filter-item">
            <div class="filter-title">Կանխավճարի առկայություն</div>
            <div class="filter-input">
                <input type="number" placeholder="Դրամ կամ տոկոս․" name="prepayment" min="0"
                       @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['prepayment_available'] !== null) value="{{ $userTemporaryFilter['prepayment_available'] }}" @else value="" @endif >
            </div>
        </div>

        <div class="filter-item">
            <div class="filter-title">Հայտի ապավովում</div>

            <div class="input-radio-inner">
                <div class="input-radio">
                    <input type="radio" id="yes1" name="proof" value="1"
                           @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['proof'] == 1) checked  @endif>
                    <label for="yes1">Այո</label>
                </div>
                <div class="input-radio">
                    <input type="radio" id="no1" name="proof" value="2"
                           @if(count($userTemporaryFilter)>0 and $userTemporaryFilter['proof'] == 2) checked  @endif>
                    <label for="no1">Ոչ </label>
                </div>
                <div class="input-radio">
                    <input type="radio" id="all1" name="proof" value="0"
                           @if((count($userTemporaryFilter)>0 and $userTemporaryFilter['proof'] == 0)
                           or count($userTemporaryFilter) == 0) checked @endif>
                    <label for="all1">Բոլորը </label>
                </div>
            </div>
        </div>

        <div class="filter-buttons">
            <button type="submit" class="btn filter-search">Հաստատել</button>
        </div>
    </form>
    <div>
        <form method="get" action="{{ route('user.settings', ['map' => 1]) }}">
            @csrf
            <input type="hidden" name="my_filter" value="1">
            <button class="btn btn-border my-filter">Իմ Ֆիլտրերը</button>
        </form>
    </div>
    <div>
        <form method="post" action="{{route('user.tenders')}}">
            @csrf
            <input type="hidden" name="reject" value="1">
            <button type="submit" class="btn btn-border reject-filter">Չեղարկել</button>
        </form>
    </div>

</div>
