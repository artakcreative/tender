<div class="blog-structure" style="display: none">
    <div>
        <button class="btn-filter filter_btn-report">Ֆիլտրեր</button>
    </div>
    <div style="display: none" class="d-flex filter-date-report">
        <div>
            <p class="filter-title">Սկիզբ</p>
            <div class="filter-input block-datepicker">
                <input type="text" name="date_start" class="timepicker_start-structure datepicker-report" autocomplete="off">
            </div>
        </div>
        <div class="div-end-range">
            <p class="filter-title">Վերջ</p>
            <div class="filter-input block-datepicker">
                <input type="text" name="date_end" class="timepicker_end-structure datepicker-report" autocomplete="off">
            </div>
        </div>
        <div class="div-periodicity">
            <p class="filter-title">Պարբերականություն</p>
            <div class="filter-input">
                <select class="periodicity periodicity-structure">
                    <option value="1">շաբաթական</option>
                    <option value="2">ամսական</option>
                    <option value="3">եռամսյակային</option>
                </select>
            </div>
        </div>
    </div>
    <p class="error-range_structure">Ժամանակահատվածը նշելը պարտադիր է։</p>
    <br>
    <div>
        <div id="chart-structure" style="width: 100%; height: 500px"></div>
    </div>
</div>
