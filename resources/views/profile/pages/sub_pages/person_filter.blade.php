<div class="user-settings-item page-filter">
    <form method="post" action="{{route('user.settings.filter.save')}}" class="form-horizontal">
        @csrf
        @method('POST')
        <input type="hidden" name="id" value="{{ auth()->guard('web')->user()->id }}">
        <div class="filter-block">
            <div class="filter-item">
                <div class="filter-title">Նախահաշվային գին</div>
                <div class="display: flex;align-items: center;flex-wrap: wrap;">
                    <div class="extra-controls form-inline">
                        <div class="filter-flex">
                            <div class="filter-flex__item filter-input">
                                <input type="number" class="js-input-from form-control" name="start"
                                       value="{{isset( $filter['estimated_price_start']) ? $filter['estimated_price_start'] : 0  }}">
                            </div>
                            <div class="filter-flex__item filter-input input-plus">
                                <input type="number" class="js-input-to form-control" name="end"
                                       value="{{isset( $filter['estimated_price_end']) ?  $filter['estimated_price_end'] : 50000000 }}">
                                <span class="plus"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="range-slider">
                    <input type="text" class="js-range-slider" value=""/>
                </div>
            </div>

            <div class="setting-item filter-item">
                <div class="filter-title">Գործունեության հասցե</div>
                <div class="filter-input input-group-active delivery_data d-flex">
                    <input type="text" class="form-control place_of_delivery" id="place_of_delivery"
                           name="business_address[area][]" placeholder="հասցե"
                           value="{{isset($filter['business_address']['area']) ? $filter['business_address']['area'][0] : null}}">
                    <input type="text" class="latitude" name="business_address[latitude][]"
                           value="{{isset($filter['business_address']['area']) ? $filter['business_address']['latitude'][0] : null}}">
                    <input type="text" class="longitude" name="business_address[longitude][]"
                           value="{{ isset($filter['business_address']['area']) ? $filter['business_address']['longitude'][0] : null}}">
                </div>
            </div>
            <div class="row">
                <label class="col-md-3 col-form-label"></label>
                <div class="col-md-9">
                    <div id="map-2" class="remove"></div>
                </div>
            </div>
            <div class="filter-item">
                <div class="filter-title">Մատակարարման վայրը ըստ շառավիղի</div>
                <div class="filter-input">
                    <input type="number" id="coordinate1" name="delivery" min="0" placeholder="0 կմ"
                           value="{{ isset( $filter['place_of_delivery']) ? $filter['place_of_delivery'] : null }}">
                </div>
            </div>

            <div class="filter-item">
                <div class="filter-title d-flex">Ֆինանսական միջոցի առկայություն <span title="
Մինչև ֆինանսական միջոցներ նախատեսվելը Գնումների մասին ՀՀ օրենքով  սահմանված կարգով կարող է կնքվել պայմանագիր` պայմանով, որ դրա շրջանակներում գնում կարող է կատարվել անհրաժեշտ ֆինանսական միջոցներ նախատեսվելու դեպքում: Սույն մասի համաձայն կնքված պայմանագիրը լուծվում է, եթե այն կնքելու օրվան հաջորդող վեց ամսվա ընթացքում պայմանագրի կատարման համար ֆինանսական միջոցներ չեն նախատեսվել: Սույն մասը կարող է կիրառվել, եթե`
1) պատվիրատուն չի կարողանում նախապես կանխատեսել (հաշվարկել) գնումների համար անհրաժեշտ ֆինանսական միջոցների չափը.
2) ապրանքի մատակարարումը, աշխատանքի կատարումը կամ ծառայության մատուցումը պետք է սկսվի տվյալ գնման համար ֆինանսական միջոցներ նախատեսվելու պահից հաշված այնպիսի ժամկետում, որի ընթացքում գնման մրցակցային որևէ ձևի կիրառումը ժամկետի առումով անհնար է:
">
                        <img src="/img/icon.png" style="cursor: pointer" width="17" height="17" alt="icon">
                    </span></div>
                <div class="input-radio-inner">
                    <div class="input-radio">
                        <input type="radio" id="yes" value="1" name="means"
                               @if (isset($filter['availability_of_financial_means']) && $filter['availability_of_financial_means'] == 1 ) checked @endif >
                        <label for="yes">Այո</label>
                    </div>
                    <div class="input-radio">
                        <input type="radio" id="no" value="2" name="means"
                               @if (isset($filter['availability_of_financial_means']) && $filter['availability_of_financial_means'] == 2))
                               checked @endif>
                        <label for="no">Ոչ </label>
                    </div>
                    <div class="input-radio">
                        <input type="radio" id="all" value="0" name="means"
                               @if (isset($filter['availability_of_financial_means']) && $filter['availability_of_financial_means'] == 0  || !isset($filter['availability_of_financial_means'])) checked @endif>
                        <label for="all">Բոլորը</label>
                    </div>
                </div>
            </div>

            <div class="filter-item">
                <div class="filter-title">Հայտի վերջնաժամկետ</div>
                <div class="filter-input block-datepicker">
                    <input type="text" name="date" class="datepicker"
                           value="{{ isset( $filter['date_deadline']) ? $filter['date_deadline'] : null }}">
                </div>
            </div>

            <div class="filter-item">
                <div class="filter-title">Կանխավճարի առկայություն</div>
                <div class="filter-input">
                    <input type="number" name="prepayment" placeholder="Դրամ կամ տոկոս․" min="0"
                           value="{{ isset( $filter['prepayment_available']) ? $filter['prepayment_available'] : null }}">
                </div>
            </div>

            <div class="filter-item">
                <div class="filter-title">Հայտի ապահովում</div>
                <div class="input-radio-inner">
                    <div class="input-radio">
                        <input type="radio" id="yes1" name="proof" value="1"
                               @if (isset($filter['proof']) && $filter['proof'] == 1 ) checked @endif>
                        <label for="yes1">Այո</label>
                    </div>
                    <div class="input-radio">
                        <input type="radio" id="no1" name="proof" value="2"
                               @if (isset($filter['proof']) && $filter['proof'] == 2 )checked @endif>
                        <label for="no1">Ոչ </label>
                    </div>
                    <div class="input-radio">
                        <input type="radio" id="all1" name="proof" value="0"
                               @if (isset($filter['proof']) && $filter['proof'] == 0  || !isset($filter['proof'])) checked @endif >
                        <label for="all1">Բոլորը</label>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn" id="save-filter">Հաստատել</button>
        </div>
    </form>
</div>
