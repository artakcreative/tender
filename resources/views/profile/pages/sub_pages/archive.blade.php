<div class="blog-archive" style="display: none">
    <div class="blog-list">
        @if(count($archive) > 0)
            @foreach($archive as $value)
                <div class="blog-item">
                    <div class="blog-left">
                        <div class="type">
                            Մրցույթի անվանում՝
                            <b>{{$value['announcement_name']}}</b>
                        </div>
                        <div class="type">
                            Մրցույթի կոդ՝
                            <b>{{ $value->id }}</b>
                        </div>
                        <div class="type">
                            Մրցույթի տիպ՝
                            <b>{{$value->competition_types ? $value->competition_types['name'] : null }}</b>
                        </div>
                        <div class="date">
                            Հայտի ներկայացման վերջնաժամկետ՝
                            <b>{{ Carbon\Carbon::parse($value['application_deadline'])->format('d.m.Y') }}</b>
                        </div>
                        <div class="field">
                            Ոլորտ՝
                            <b> @if(isset($value['sub_spheres']) || isset($value['spheres']))
                                    @php
                                        $announcement_sub_spheres_spheres = [];
                                        $announcement_spheres = [];
                                        foreach ($value['sub_spheres'] as $sub_spheres){
                                        $announcement_sub_spheres_spheres[] = $sub_spheres->sphere['name'];
                                        }
                                        if (isset($value['spheres'])){
                                            foreach ($value['spheres'] as $sphere){
                                                $announcement_spheres[] = $sphere['name'];
                                            }
                                            $value_spheres = array_merge($announcement_sub_spheres_spheres,$announcement_spheres);

                                        }

                                    $sphere_text = "";

                                    foreach (array_unique($value_spheres) as $value_sphere){
                                        $sphere_text .= $value_sphere . ", ";
                                    }
                                    $sphere_text = substr($sphere_text, 0, -2)
                                    @endphp
                                    <b> {{  $sphere_text  }}</b>
                                 @endif</b>
                        </div>
                        <div class="sub-field">
                            Ենթաոլորտ՝
                            <b>@if(isset($value['sub_spheres']) && count($value['sub_spheres']))
                                    @php
                                        $sub_sphere_name = "";
                                        foreach ( $value['sub_spheres'] as $item){
                                            $sub_sphere_name .= $item['name'] . ', ';
                                        }
                                        $sub_sphere_name = substr($sub_sphere_name, 0, -2);
                                    @endphp
                                    {{$sub_sphere_name}}
                                @else
                                    Տվյալ հայտարարությունը ենթավոլորտ չունի
                                @endif
                            </b>
                        </div>
                    </div>
                    <div class="blog-right">
                        @forelse($user_announcements as $user_announcement)

                            @if(auth_user() && isset($user_announcement) && $value['id'] == $user_announcement['id'] )
                                <div class="saved favorite_announcement active" data-announcement_id="{{$value['id']}}" data-user_id="{{auth_user()->id}}"></div>
                                @break
                            @else
                                <div class="saved favorite_announcement" data-announcement_id="{{$value['id']}}" data-user_id="{{auth_user()->id}}"></div>
                            @endif
                        @empty
                            <div class="saved favorite_announcement" data-announcement_id="{{$value['id']}}" data-user_id="{{auth_user()->id}}"></div>
                        @endforelse
                        <div class="text">Նախահաշվային գին՝ <b>{{ number_format( $value['estimated_price'], 0 , '.', ' ')  }} դրամ</b></div>
                        <div class="d-flex align-items-center">
                            @if(count($value->getMedia('generate_pdf')) > 0 )
                                <div><a href="{{ $value->getMedia('generate_pdf')[0]->getUrl() }}" target="_blank">
                                        <p class="pdf"></p></a>
                                </div>
                            @endif
                            <form method="post" action="{{ route('user.tenders.details') }}" class="form-horizontal">
                                @csrf
                                <input type="hidden" name="id" value="{{ $value['id'] }}">
                                <button type="submit" class="btn btn-info btn-round">Մանրամասները</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    {{ $archive->links('vendor.pagination.simple-tailwind') }}
</div>
