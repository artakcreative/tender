@extends('profile.personal')
@section('personal')
{{--    {{dd($details)}}--}}
        <div class="wrapper">
                    <div class="navigation">
                        <div class="navigation__item">Ընտրված տենդերներ</div>
                        <div class="navigation__item active">տենդեր {{ $details['announcement_name'] }}</div>
                    </div>

                    <div class="blog-details">
                        <div class="blog-details__information">
                            <div class="blog-details__information-left">
                                <ul>
                                    <li>Կոդ</li>
                                    <li>Մրցույթի ծածկագիր</li>
                                    <li>Մրցույթի տիպ</li>
                                    <li>Հայտի ներկայացման եղանակ</li>
                                    <li>Պատվիրատու</li>
                                </ul>
                            </div>
                            <div class="blog-details__information-right">
                                <ul>
                                    <li><b>{{ $details['id'] }}</b></li>
                                    <li><b>{{ $details['announcement_password'] }}</b></li>
                                    <li><b>{{ $details['competition_types']['name'] }}</b></li>
                                    <li><b>{{Str::ucfirst($details['application_forms']['name']) }}</b></li>
                                    <li><b>{{ $details['customers']['name'] }}</b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="blog-details__information-1">
                            <div class="blog-details__information-1-left">
                                <ul>
                                    <li class="mb-5">Ոլորտ</li>
                                    <li>Ենթաոլորտ</li>
                                </ul>
                            </div>
                            <div class="blog-details__information-1-right">
                                <ul>
                                    <li>
{{--                                            @if(isset($details['spheres']) && count($details['spheres']))--}}
{{--                                                {{ $details['spheres'][0]->name }}--}}
{{--                                            @else{{ $details['sub_spheres'][0]->sphere['name'] }}--}}
{{--                                            @endif--}}

                                            @if(isset($details['sub_spheres']) || isset($details['spheres']))
                                                @php
                                                    $announcement_sub_spheres_spheres = [];
                                                    $announcement_spheres = [];
                                                    foreach ($details['sub_spheres'] as $sub_spheres){
                                                    $announcement_sub_spheres_spheres[] = $sub_spheres->sphere['name'];
                                                    }
                                                    if (isset($details['spheres'])){
                                                        foreach ($details['spheres'] as $sphere){
                                                            $announcement_spheres[] = $sphere['name'];
                                                        }
                                                        $value_spheres = array_merge($announcement_sub_spheres_spheres,$announcement_spheres);

                                                    }
                                                @endphp
                                                @forelse(array_unique($value_spheres) as $value_sphere)
                                                    <div class="file-block"><b>{{$value_sphere}}</b></div>
                                                @empty
                                                @endforelse
                                            @endif

                                    </li>
                                    <li>@if(isset($details['sub_spheres']) && count($details['sub_spheres']))
                                                @foreach($details['sub_spheres'] as $item)
                                                    <div class="file-block"><span>{{ $item['name'] }}</span></div>
                                                @endforeach
                                            @else
                                                Տվյալ հայտարարությունը ենթաոլորտ չունի
                                            @endif

                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="blog-details__information-list">
                            <div class="blog-details__information-item w-100">
                                Նախահաշվային գին
                                <span title="Ապրանքը, աշխատանքը, ծառայությունը ձեռք բերելու համար Պատվիրատուի կողմից սահմանված առավելագույն գինը։ ">
                                    <img src="/img/icon.png" style="cursor: pointer" width="17" height="17" alt="icon">
                                </span>
                                `<b> {{ number_format($details['estimated_price'], 0 , '.', ' ') }} ֏</b>
                            </div>
                            <div class="blog-details__information-item w-100">
                                Հայտի ներկայացման վերջնաժամկետ՝ <b> {{ Carbon\Carbon::parse($details['application_deadline'])->format('d.m.Y') }}</b>
                            </div>
                            @if($details['return_auction_day'] != null and Str::ucfirst($details['application_forms']['name']) == 'Էլէկտրոնային աճուրդ')
                            <div class="blog-details__information-item w-100">
                                Հետադարձ աճուրդի օր
                                <span title="էլեկտրոնային միջոցների օգտագործմամբ` նվազեցված գները ներկայացնելու գործընթացի իրականացման օր։">
                                    <img src="/img/icon.png" style="cursor: pointer" width="17" height="17" alt="icon">
                                </span>
                                `<b> @if($details['return_auction_day'] != null) {{ Carbon\Carbon::parse($details['return_auction_day'])->format('d.m.Y') }}@endif</b>
                            </div>
                            @endif
                            <div class="blog-details__information-item w-100">
                                Մատակարարման վերջնաժամկետ՝ <b> @if($details['deadline_for_supply'] != null) {{ Carbon\Carbon::parse($details['deadline_for_supply'])->format('d.m.Y') }}@endif</b>
                            </div>
                            <div class="blog-details__information-item w-100">
                                Հայտի բացման նիստի օր
                                <span title="Հայտերը բացվում են հրավերում նշված օրը, ժամին և վայրում` հայտերի բացման նիստում: Հայտերի բացման օրը և ժամը սովորաբար համընկնում են դրանք ներկայացնելու վերջնաժամկետին։">
                                    <img src="/img/icon.png" style="cursor: pointer" width="17" height="17" alt="icon">
                                </span>
                                `<b> @if($details['application_opening_day'] != null) {{ Carbon\Carbon::parse($details['application_opening_day'])->format('d.m.Y') }}@endif</b>
                            </div>
                            <div class="blog-details__information-item w-100">
                                Մատակարարման վայր՝ <b> {{ $details['place_of_delivery']['area']}}</b>
                            </div>
                            <div class="blog-details__information-item w-100 d-flex">
                                Ֆինանսական միջոցի առկայություն
                                <span title="Մինչև ֆինանսական միջոցներ նախատեսվելը Գնումների մասին ՀՀ օրենքով  սահմանված կարգով կարող է կնքվել պայմանագիր` պայմանով, որ դրա շրջանակներում գնում կարող է կատարվել անհրաժեշտ ֆինանսական միջոցներ նախատեսվելու դեպքում: Սույն մասի համաձայն կնքված պայմանագիրը լուծվում է, եթե այն կնքելու օրվան հաջորդող վեց ամսվա ընթացքում պայմանագրի կատարման համար ֆինանսական միջոցներ չեն նախատեսվել: Սույն մասը կարող է կիրառվել, եթե`
1) պատվիրատուն չի կարողանում նախապես կանխատեսել (հաշվարկել) գնումների համար անհրաժեշտ ֆինանսական միջոցների չափը.
2) ապրանքի մատակարարումը, աշխատանքի կատարումը կամ ծառայության մատուցումը պետք է սկսվի տվյալ գնման համար ֆինանսական միջոցներ նախատեսվելու պահից հաշված այնպիսի ժամկետում, որի ընթացքում գնման մրցակցային որևէ ձևի կիրառումը ժամկետի առումով անհնար է:">
                                    <img src="/img/icon.png" style="cursor: pointer" width="17" height="17" alt="icon">
                                </span>
                                `<b>  @if($details['availability_of_financial_resources'] != null || $details['availability_of_financial_resources']  == 1 )
                                        Կա @else Չկա @endif</b>
                            </div>
                        </div>
                        <div class="blog-details__time-title">
                            Վճարման ժամանակացույց
                        </div>
                        <div class="blog-details__time-list">
                            @if($details['payment_schedule']['date'][0]  !== null)
                                @foreach($details['payment_schedule']['date'] as $key => $value)
                                    <div class="blog-details__time-item">
                                        <div class="month">{{Carbon\Carbon::parse($value)->monthName}} </div>
                                        <div class="percent">{{ $details['payment_schedule']['percent'][$key] }}%</div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="blog-details__information-list">
                            @if($details['prepayment'] != null)
                            <div class="blog-details__information-item w-100">
                                Կանխավճար՝ <b>{{ $details['prepayment']['dram_or_percent'] }}  @if($details['prepayment']['dram_or_percent'] == 100) % @else ֏ @endif</b>
                            </div>
                            @endif
                            <div class="blog-details__information-item w-100">
                                Տրամադրած ժամկետ՝ <b> @if($details['prepayment'] != null){{ Carbon\Carbon::parse($details['prepayment']['time_of_provision'])->format('d.m.Y') }}  @endif</b>
                            </div>
                            <div class="blog-details__information-item w-100">
                                Ապահովում՝ <b>@if($details['provides'] != null)Կա @else Չկա @endif</b>
                                    <div class="table">
                                        @if($details['provides'] != null)
                                            @if(isset($details['provides']['contract_provide']))
                                                <div class="table-item">
                                                    <div class="text">Հայտի ապահովում</div>
                                                    <div class="percent">
                                                        {{ $details['provides']['contract_provide'] }}
                                                    </div>
                                                </div>
                                            @endif

                                            @if(isset($details['provides']['prepayment_provide']))
                                                <div class="table-item">
                                                    <div class="text">Պայմանագրի ապահովում</div>
                                                    <div class="percent">
                                                        {{ $details['provides']['prepayment_provide'] }}
                                                    </div>
                                                </div>
                                            @endif

                                            @if(isset( $details['provides']['application_provide']))
                                                <div class="table-item">
                                                    <div class="text">Որակավորման ապահովում</div>
                                                    <div class="percent">
                                                        {{ $details['provides']['application_provide'] }}
                                                    </div>
                                                </div>
                                            @endif

                                            @if(isset( $details['provides']['qualification_provide']))
                                                <div class="table-item">
                                                    <div class="text">Կանխավճարի ապահովում</div>
                                                    <div class="percent">
                                                        {{ $details['provides']['qualification_provide'] }}
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                            </div>
                                @if($details['other_conditions'] != null)
                                    <div class="blog-details__information-item w-100">
                                        <b class="text-no-wrap pr-10">Այլ պայմաններ </b>
                                        <p> {!! $details['other_conditions'] !!}</p>
                                    </div>
                                @endif
                            <div class="blog-details__information-item w-100 m15-0">
                                <b class="text-no-wrap pr-10">Կցված ֆայլեր</b>
                                @php
                                    $generate_pdf = $details->getMedia('generate_pdf');
                                    $default = $details->getMedia();
                                    $files = $generate_pdf->merge($default);
                                @endphp
                                <p>
                                    @foreach($files as $value)
                                        <a href="{{ $value->getUrl() }}" target="_blank">
                                             <span class="download-pdf">{{ $value->name }}</span>
                                         </a>
                                    @endforeach
                                </p>
                            </div>
                        </div>
                        <button data-modal="#phone-call"  class="btn open__modal">Դիմել</button>
            </div>
        </div>

@endsection
