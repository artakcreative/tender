
@foreach($value as $key => $item)
    <div class="input-checkbox">
        <input class="checkbox global-checkbox" type="checkbox" id="{{ $item['text'] .'-'. $item['id']}}" name="checkbox" data-id="{{ $item['id'] }}" >
        <label for="{{ $item['text'] .'-'. $item['id'] }}">{{ $item['text'] }}</label>
    </div>
@endforeach
