<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/jquery-3.6.0.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/price-slider.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/jquery.canvasjs.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/jquery.formstyler.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/jquery.inputmask.js') }}"></script>
<script src="https://api-maps.yandex.ru/2.1/?apikey=00c6dcc3-8872-47d9-a19b-d8e220789b91&lang=ru_RU" type="text/javascript">
</script>

@stack('script')

<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/sweetalert2.all.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/chartjs.min.js') }}"></script>--}}

{{--amcharts--}}
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/amcharts/index.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/amcharts/xy.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/amcharts/Animated.js') }}"></script>


{{--highcharts--}}
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/highcharts/highcharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/highcharts/exporting.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/highcharts/export-data.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/highcharts/accessibility.js') }}"></script>


<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/repeater.js') }}"></script>
{{--<script type="text/javascript" src="{{ asset('js/profile/assets/js/plugins/countfect.min.js') }}"></script>--}}

<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/personal/report.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/profile.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/tariff_price_count.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/active-archived-announcements-by-sphere.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/registration.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/personal/person-settings.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/personal/tenders.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/profile/assets/js/core/personal/favorite_announcements.js') }}"></script>
<script src="{{ mix('js/app-user.js') }}"></script>

