
<header class="header">
    <div class="header-top">
        <div class="wrapper">
            <div class="header-top__inner">
                <div class="top-left">
                    <ul>
                        <li><img src="{{ asset('img/profile/icons/clock.svg') }}" alt="">
                            <a href="javascript:void(0);">
                                Երկ. - Ուրբ. 09:00-18:00 &nbsp;| &nbsp; Շբթ. 09:00-14:00
                            </a>
                        </li>
                        <li><img src="{{ asset('img/profile/icons/email.svg') }}" alt=""> <a href="mailto:info@kmg.am">info@kmg.am</a>
                        </li>
                        <li><img src="{{ asset('img/profile/icons/phone.svg') }}" alt="Հեռախոսահամր">
                            <a href="tel:+37410581958">+374 (10) 58 19 58</a> &nbsp; | &nbsp;
                            <a href="tel:+37499130099">+374 (99) 13 00 99</a>
                        </li>
                    </ul>
                </div>
                <div class="top-right">
                    <ul>
                        <li><img src="{{ asset('img/profile/icons/map.svg') }}" alt=""> <a href="javascript:void(0);">ք.
                                Երևան,
                                Աբովյան փողոց, շենք 41, թիվ 5</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="wrapper">
        <div class="header-inner">
            <a href="{{ route('home') }}" class="header-logo logo">
                <img src="{{ asset('img/profile/logo.png') }}" alt="logo">
            </a>
            <nav class="nav">
                <ul class="nav-list">
                    @auth('web')
                        @if(Illuminate\Support\Facades\Route::currentRouteName() != 'home')
                            <li class="nav-item ">
                                <a href="{{ route('user.tenders') }}" class="nav-link nav-login ">
                                    {{ Illuminate\Support\Facades\Auth::guard('web')->user()->full_name }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('user.saved') }}" class="saved nav_saved {{Illuminate\Support\Facades\Route::currentRouteName() == 'user.saved' || count(auth_user()->announcements) > 0    ? 'active' : ''}}"></a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('user.notification') }}">
                                    <span class="notifications {{Illuminate\Support\Facades\Route::currentRouteName() == 'user.notification'  ? 'active' : ''}}"></span>
                                </a>
                            </li>
                        @endif
                    @endauth
                    @if(Illuminate\Support\Facades\Route::currentRouteName() == 'home')
                            <li class="nav-item">
                                <a href="#charts" class="nav-link">Տենդերներ</a>
                            </li>
                            <li class="nav-item">
                                <a href="#price-block" class="nav-link">Ծառայության փաթեթներ</a>
                            </li>
                            <li class="nav-item">
                                <a href="#about-us" class="nav-link">Մեր մասին</a>
                            </li>
                            <li class="nav-item">
                                <a href="#news" class="nav-link">Նյութեր</a>
                            </li>
                            @guest()
                                <li class="nav-item">
                                    <a href="#" data-modal="#login" class="nav-link nav-login open__modal">Մուտք</a>
                                </li>
                            @endguest
                            @auth('web')
                                <li class="nav-item">
                                    <a href="javascript:void" onclick="$('#logout-form').submit();" class=" ">
                                    <div class="icon"></div>
                                    Դուրս գալ
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                                    @csrf
                                </form>
                                </li>
                                <li>
                                    <a href="{{ route('user.person.settings') }}" class="nav-link nav-login ">
                                        {{ auth_user()->full_name }}
                                    </a>
                                </li>
                            @endauth
                     @endif

                </ul>
            </nav>
            <div class="menu__icons">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</header>


<div class="phone-call">
    <button data-modal="#phone-call" class="phone-call-btn open__modal">
        <span></span>
    </button>
</div>
{{--{{ dd(auth_user()->manager) }}--}}
<div id="phone-call" class="modal">
    <div class="modal__container">
        <div class="close"></div>
        <h2 class="sub-title">Ձեզ սպասարկող մենեջերն է՝</h2>
        <div class="sub-title"><b>{{ auth_user()->manager->name ??  $contactUs->name }} {{ auth_user()->manager->surname ??  $contactUs->surname  }}ը</b></div>
        <div class="phone-call__list">
            <div class="phone-call__item">
                <a class="email" href="mailto:hovanisyan@kmg.am" target="_blank">{{ auth_user()->manager->email ?? $contactUs->email  }}</a>
            </div>
            <div class="phone-call__item">
                <a class="phone" href="tel:+374 (10) 58 19 58" target="_blank">{{ auth_user()->manager->phone ??  $contactUs->phone  }}</a>
            </div>
        </div>
    </div>
</div>

<script>
    @auth()
      window.authUserId  = `{{ auth_user()->id}}`
    @endauth
</script>
