<footer class="footer">
    <div class="wrapper">
        @if(isset($pageInfo))
            <div class="footer__inner">
                <div class="footer__list">
                    <div class="footer__item">
                        <div class="footer__title">Կապ մեզ հետ</div>
                        <ul>
                            <li>
                                <a class="email" target="_blank" href="mailto:{{ $pageInfo->email }}">{{ $pageInfo->email }}</a>
                            </li>
                            <li>
                                <a class="phone" target="_blank" href="tel:{{ $pageInfo->more_phone }}">{{ $pageInfo->phone }}</a>
                            </li>
                            <li>
                                <a class="phone" target="_blank" href="tel:{{ $pageInfo->more_phone }}">{{ $pageInfo->more_phone }}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__item">
                        <div class="footer__title">Հետևիր մեզ</div>
                        <ul>
                            <li>
                                <a class="youtube" target="_blank" href="{{ $pageInfo->youtube }}">Youtube</a>
                            </li>
                            <li>
                                <a class="facebook" target="_blank" href="{{ $pageInfo->facebook }}">Facebook</a>
                            </li>
                            <li>
                                <a class="linkedin" target="_blank" href="{{ $pageInfo->linkedin }}">Linkedin</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__item">
                        <div class="footer__title">Հասցե</div>
                        <ul>
                            <li class="map">
                                {{ $pageInfo->full_address }}
                            </li>
                        </ul>
                        <div id="map"></div>
                        <div class="creative-web-logo">
                            Created by
                            <a  href="https://creativeweb.am/" target="_blank">
                                <img src="{{ asset('img/logo-creativeweb.png') }}" alt="creative-web">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="footer__info"> {{ $pageInfo->footer_info}}</div>
            </div>
        @endif
    </div>
</footer>

