<div id="verification" class="modal competition-modal">
    <div class="modal__container">
        <div class="close"></div>
        <h1>@lang('message.not_competition_text')</h1>
        <p class="info">@lang('message.not_competition_text_1')</p>
        <div class="body-select">
            <div class="form-group select-category-body">
                <select class="selectCategory  js-states form-control"></select>
            </div>
            <div class="input-checkbox-appends"></div>
        </div>
        <div class="btn-body-competition">
            <button type="button" class="btn btn-success save-category-user">Պահպանել</button>
        </div>
    </div>
</div>
