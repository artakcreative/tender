<div id="registration" class="modal">
    <div class="modal__container">
        <div class="close"></div>
        <h2 class="sub-title">Գրանցվել</h2>
            <div class="input-group" id="reg-email">
                <input type="text" class="is-invalid " id="email1" name="email"
                       placeholder="Գրեք էլ-հասցեն">
                <label for="email1">Էլ-հասցե</label>
            </div>
            <div class="input-group" id="reg-phone">
                <input type="text" id="phone" class="phone-mask" name="phone" placeholder="+374 __ __ __ __">
                <label for="phone">Հեռախոսահամար </label>
            </div>
            <div class="input-group" id="reg-password">
                <input type="password" id="password1" name="password" placeholder="Գրեք գաղտնաբառը">
                <label for="password1">Գաղտնաբառ </label>
                <span class="eye"></span>

        </div>
        <div class="input-group" id="reg-password-confirmation">
            <input type="password" id="password2" name="password_confirmation" placeholder="Գրեք գաղտնաբառը">
            <label for="password2">Կրկնեք գաղտնաբառը </label>
            <span class="eye"></span>
        </div>
        <div class="modal-form__more">
            <div class="input-checkbox input-register " id="reg-checkbox">
              <div class="input-checkbox-form ">
                  <input type="checkbox" id="checkbox-register-confirm" name="checkbox" checked>
                  <label for="checkbox-register-confirm"> </label>
                  <div class="label">Ես համաձայն եմ օգտատիրոջ <a href="{{route('data-processing-policy')}}" target="_blank" class="data-processing-policy">տվյալների մշակման քաղաքականության</a> հետ:</div>

              </div>
                 </div>
        </div>
        <button class="btn" id="btn-registration">Գրանցվել</button>
    </div>
</div>

<div id="verification" class="modal modal-verification">
    <div class="modal__container">
        <div class="close"></div>
        Դուք հաջողությամբ գրանցվել եք, խնդրում ենք ստուգել Ձեր էլկտրոնային փոստը՝ նույնականացում անցնելու համար։
    </div>
</div>

