@extends('profile.layouts.app')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('css/user/user.css') }}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
    <main class="main">
        <div class="wrapper">
            <div class="main-content main-textarea  main-full">
                {!! isset($dataProcessingPolicy[0]['content']) ? $dataProcessingPolicy[0]['content'] : '' !!}
            </div>
        </div>

    </main>

@endsection
