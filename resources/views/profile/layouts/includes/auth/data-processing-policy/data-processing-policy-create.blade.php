@extends('admin.layouts.app')

@section('content')
    <div class="card-body">
        <div class="card">
            <div class="card-header" style="margin-left: 30px">
                <h4 class="card-title">Ավելացնել տվյալների մշակման քաղաքականության</h4>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-12">
                            <form method="post" enctype="multipart/form-data"
                                  action="{{route('admin.settings.data_processing_policy.store')}}" class="form-horizontal">
                                @csrf

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control textarea_style" id="data-processing-policy" name="content" value=""></textarea>
                                        </div>
                                    </div>

                                <input type="hidden" value="1" name="template_id">
                                <div class="row">

                                    <div class="col-md-10" style="margin-left: 10px">
                                        <button type="submit" class="btn btn-primary">Պահպանել</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('js/admin/assets/ckeditor/ckeditor.js')}}"></script>
    <script>
        let toolbar = {
            toolbar: [{
                name: 'document',
                items: ['Print']
            },
                {
                    name: 'clipboard',
                    items: ['Undo', 'Redo']
                },
                {
                    name: 'styles',
                    items: ['Styles','Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
                {
                    name: 'align',
                    items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                },
                '/',
                {
                    name: 'basicstyles',
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink']
                },
                {
                    name: 'paragraph',
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-']
                },
                {
                    name: 'insert',
                    items: ['Table']
                },
                {
                    name: 'tools',
                    items: ['Maximize']
                },
                {
                    name: 'editing',
                    items: ['Scayt']
                },

            ]};
        CKEDITOR.replace( 'data-processing-policy', toolbar)
    </script>

@endpush
