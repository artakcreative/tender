<div id="modal_remove" class="modal">
    <div class="modal__container">
        <div class="close"></div>
        <h2 class="sub-title">Մուտք անձնական  էջ</h2>
        <form action="#" class="modal-form">
            <div class="description">
                @lang('auth.forgot_password_text')
            </div>
            <div class="input-group" id="forgot-email">
                <input type="text" id="email-forgot" name="email" placeholder="Գրեք էլ-հասցեն">
                <label for="email-forgot">Էլ-հասցե</label>
            </div>
            <button  type="button" class="btn" id="btn-forgot">Վերականգնել</button>
        </form>
    </div>
</div>


<div id="new_password" class="modal" data-token="{{ session()->get( 'token' ) }}" data-email="{{ session()->get( 'email' ) }}">
    <div class="modal__container">
        <div class="close"></div>
        <h2 class="sub-title">Նոր գաղտնաբառ</h2>
            <div class="input-group input-group-active input-group-now" id="reset-password">
                <input type="password" id="password-reset" name="password" placeholder="Գրեք գաղտնաբառը">
                <label for="password3">Գաղտնաբառ </label>
                <span class="eye"></span>

            </div>
            <div class="input-group input-group-active input-group-now" id="reset-conf-password">
                <input type="password" id="password-conf-reset" name="password_confirmation" placeholder="Գրեք գաղտնաբառը">
                <label for="password4">Կրկնեք գաղտնաբառը </label>
                <span class="eye"></span>

            </div>
            <button class="btn" id="btn-reset">Պահպանել</button>
    </div>
</div>

<div id="verification" class="modal modal-verification modal-reset">
    <div class="modal__container">
        <div class="close"></div>
        <p class="append-text-reset"></p>
    </div>
</div>
