<div id="login" class="modal">
    <div class="modal__container">
        <div class="close"></div>
        <h2 class="sub-title">Մուտք անձնական էջ</h2>
            <div class="input-group" id="login-email">
                <input type="text" id="email" name="email" placeholder="Գրեք էլ-հասցեն">
                <label for="email">Էլ-հասցե</label>
            </div>
            <div class="input-group" id="login-password">
                <input type="password" id="password" name="password" placeholder="Գրեք գաղտնաբառը">
                <label for="password">Գաղտնաբառ </label>
                <span class="eye"></span>
            </div>
            <div class="modal-form__more">
                <div class="input-checkbox">
                    <input type="checkbox" id="checkbox" name="remember" checked>
                    <label for="checkbox"> Հիշել ինձ </label>
                </div>
                <a href="#" class="remove open__modal" data-modal="#modal_remove">Մոռացե՞լ եք գաղտնաբառը</a>
            </div>
            <button class="btn" id="btn-login">Մուտք</button>
            <button class="btn btn-border open__modal" data-modal="#registration">Գրանցվել</button>
    </div>
</div>
