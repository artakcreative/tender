@include('profile.layouts.includes.header')
{{--@prepend('style')--}}
    {{--<link href="{{ asset('css/user/user.css') }}" rel="stylesheet" type="text/css"/>--}}
{{--@endprepend--}}
<body>
@include('profile.layouts.includes.nav')

    @yield('content')
    <!-- Navbar -->

<!-- End Navbar -->
    {{--<div class="content">--}}
        {{--@yield('content')--}}
    {{--</div>--}}

    @include('profile.layouts.includes.auth.registration')
    @include('profile.layouts.includes.auth.login')
    @include('profile.layouts.includes.auth.forgot')
    @include('profile.layouts.includes.auth.competiton')
    @include('profile.layouts.includes.footer')

@include('profile.layouts.includes.scripts')


</body>
</html>
