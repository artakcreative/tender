@extends('profile.layouts.app')

@push('style')
    <!-- CSS Select2 -->
    <link href="{{ asset('css/profile/profile.css') }}" rel="stylesheet"/>
@endpush


@section('content')
    <section class="one-section">
        <div class="wrapper">
            <div class="one-section__inner">
                @if(isset($pageHeader))
                    <div class="one-section__left">
                        {!!  $pageHeader->description !!}
                        @guest('web')
                            <button class="btn open__modal" data-modal="#registration">Գրանցվել</button>
                        @endguest
                    </div>
                    <div class="one-section__right">
                        @foreach($pageHeader->getMedia('pageHeader') as $media)
                            <img src="{{ $media->getFullUrl() }}" alt="one-section-image">
                        @endforeach
                @else
                    @guest('web')
                        <div class="one-section__left">
                            <button class="btn open__modal" data-modal="#registration">Գրանցվել</button>
                        </div>
                    @endguest
                @endif

            </div>
        </div>
    </section>

    <section class="our-works">
        <div class="wrapper">
            <h2 class="title">Ի՞նչու աշխատել մեզ հետ</h2>
            <div class="our-works__list">
                @if(count($pageAdvantages))
                    @foreach($pageAdvantages as $value)
                        <div class="our-works__item">
                            <div class="our-works__icon">
                                @if(isset($value['url_image']))
                                    <img src="{{ $value['url_image'] }}" alt="works-1">
                                @endif
                            </div>
                            <h2 class="sub-title"  style="color: #fff">{{ $value['title'] }}</h2>
                            <div class="our-works__description">
                                {{ $value['description'] }}
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

    <section class="charts" id="charts">
        <div class="wrapper">
            <h2 class="title">Տենդերների վիճակագրություն </h2>
            <div class="charts-inner">
                <div class="charts-left">
                    <label> Ընտրել ոլորտը </label>
                    <div class="new-select">
                        <div class="new-select-bg"></div>
                        <div class="new-select-title">Ընտրեք ոլորտները</div>
                        <div class="new-select-list">
                            <div class="new-select-item" >
                                @forelse($spheres as $sphere)
                                    <div class="input-checkbox">
                                        <input type="checkbox" id="checkbox{{$sphere->id}}" class="home-sphere-input_checkbox" data-id="{{$sphere->id}}">
                                        <label for="checkbox{{$sphere->id}}">{{$sphere->name}}</label>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                    @if(auth_user())
                        <a class="btn see_more-link" href="{{ route('user.tenders')}}">տեսնել ավելին</a>
                    @else
                        <button class="btn open__modal" data-modal="#login">տեսնել ավելին</button>
                    @endif
                </div>
                <div class="charts-right">
                    <div class="charts-active">
                        <div class="charts-title">Ակտիվ</div>
                        <div class="charts-round">
                            <span class="number count active_count" data-duration="{{$activeAnnouncementsCount}}" data-type_active="{{$activeAnnouncementsCount}}" >{{$activeAnnouncementsCount}}</span>
                        </div>
                    </div>
                    <div class="charts-archive">
                        <div class="charts-title">Արխիվ</div>
                        <div class="charts-round">
                            <span class="number count archived_count" data-duration="{{$archivedAnnouncementsCount}}" data-type_archived="{{$archivedAnnouncementsCount}}">{{$archivedAnnouncementsCount}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="join-us">
        <div class="wrapper">
            <h2 class="title">Միացիր մեզ</h2>
            <div class="join-us__list">
                @if(count($pageConnects))
                    @foreach($pageConnects as $key => $value)
                        <div class="join-us__item join-us__item-{{ $key }}">
                            <div class="join-us__info">
                                @if(isset($value['url_image']))
                                    <img src="{{ $value['url_image'] }}" alt="join-us">
                                @endif
                            </div>
                            <div class="join-us__description">
                                {!! $value['subject'] !!}
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <section class="price-block" id="price-block">
        <div class="wrapper">
            <h2 class="title">Ծառայության փաթեթներ</h2>
            <div class="price__list">
                <div class="price__item">
                    <div class="price__item-header">{{$tariffs[0]->name}}</div>
                    <div class="price__inner">
                        <div class="price__item-title">Միայն պետական տենդերներ</div>
                        <div class="new-select">
                            <div class="new-select-bg"></div>
                            <div class="new-select-title">Ընտրեք ոլորտները</div>
                            <div class="new-select-list">
                                @forelse($spheres as $sphere)
                                    <div class="new-select-item">
                                        <div class="input-checkbox">
                                            <input type="checkbox" id="basic_checkbox{{$sphere->id}}" class="basic_input-checkbox">
                                                <label for="basic_checkbox{{$sphere->id}}">{{@$sphere->name}}</label>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <span>*2-րդ ոլորտի համար 10% զեղ</span>
                        <br>
                        <span>*3֊րդ և հաջորդ ոլորտների համար՝ 20% զեղչ</span>
                        <div class="price__item-checks">
                        </div>
                        <div class="price__item-info">
                            Ամսվա վճարը
                            <div class="price basic_price">{{$tariffs[0]->price}}֏</div>
                        </div>
                    </div>

                    @if(auth_user())
                        <button class="price__item-footer" ><a href="{{route('user.settings')}}">Ընտրել</a></button>
                    @else
                        <button class="price__item-footer open__modal" data-modal="#login">Ընտրել</button>
                    @endif
                </div>
                <div class="price__item premium">
                    <div class="price__item-header">{{$tariffs[1]->name}}</div>
                    <div class="price__inner">
                        <div class="price__item-title">Պետական և մասնավոր տենդերներ</div>

                        <div class="new-select">
                            <div class="new-select-bg"></div>
                            <div class="new-select-title">Ընտրեք ոլորտները</div>
                            <div class="new-select-list">
                                @forelse($spheres as $sphere)
                                    <div class="new-select-item">
                                        <div class="input-checkbox">
                                            <input type="checkbox" id="pro_checkbox{{$sphere->id}}" class="pro_input-checkbox" value="{{$sphere->name}}">
                                            <label for="pro_checkbox{{$sphere->id}}">{{$sphere->name}}</label>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        </div>

                        <span>*2-րդ ոլորտի համար 10% զեղ</span>
                        <br>
                        <span>*3֊րդ և հաջորդ ոլորտների համար՝ 20% զեղչ</span>
                        <div class="price__item-checks">

                        </div>
                        <div class="price__item-info">
                            Ամսվա վճարը
                            <div class="price pro_price">{{$tariffs[1]->price}}֏</div>
                        </div>
                    </div>

                    @if(auth_user())
                        <button class="price__item-footer" ><a href="{{route('user.settings')}}">Ընտրել</a></button>
                    @else
                        <button class="price__item-footer open__modal" data-modal="#login">Ընտրել</button>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <section class="about-us" id="about-us">
        <div class="wrapper">
            <div class="about-us__inner">
                <div class="about-us__left">
                    <h2 class="title">Մեր մասին</h2>
                    @if(isset($pageInfo))
                        <p class="description"> {{ $pageInfo->description }}</p>
                    @endif
                    <div class="about-us__slider slider">
                        @if(count($pageAbouts))
                            @foreach($pageAbouts as $value)
                                @if(isset($value['url_image']))
                                    <div class="about-us__slider-item">
                                        <img src="{{ $value['url_image'] }}" alt="">
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="about-us__right">
                    <div class="about-us-parent__slider slider">
                        @if(count($pageAbouts))
                            @foreach($pageAbouts as $value)
                                <div class="about-us-parent__slider-item">
                                    @if(isset($value['url_image']))
                                        <div class="about-us__slider-item">
                                            <img src="{{ $value['url_image'] }}" alt="">
                                        </div>
                                    @endif
                                    <div class="info">
                                        <div class="name">{{ $value['full_name'] }}</div>
                                        <div class="position">{{ $value['position'] }}</div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="news" id="news">
        <div class="wrapper">
            <h2 class="title">Հետաքրքիր նյութեր</h2>
            <div class="news-slider slider">
                @if(count($pageMaterials))
                    @foreach($pageMaterials as $value)
                        <div class="news-slider__item">
                            @if(isset($value['url_image']))
                                <div class="news-slider__img">
                                    <img src="{{ $value['url_image'] }}" alt="">
                                </div>
                            @endif
                            <div class="news-slider__information">
                                <div class="news__header">
                                    <div
                                        class="date">{{ \Carbon\Carbon::parse($value['created_at'] )->format('d.m.Y') }}</div>
                                    <div class="tender">Տենդերներ</div>
                                </div>
                                <div class="news__title">{{ $value['title'] }}</div>
                                <p class="news__description">{{ $value['description'] }}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

@endsection

@push('content')
@endpush
