<div class="blog blog-single blog-archive" style="display: none">
    <div class="blog-list saved_blog_list">
        @forelse($user_announcements as $announcement)
            @if($announcement->application_deadline < \Carbon\Carbon::now()->format('Y-m-d H:i:s'))
                <div class="blog-item delete_blog_item{{$announcement->id}} ">
                    <div class="blog-left">
                        <div class="type">
                            Մրցույթի անվանում՝
                            <b>{{$announcement->announcement_name}}</b>
                        </div>
                        <div class="type">
                            Մրցույթի տիպ՝
                            <b>{{$announcement->competition_types['name']}}</b>
                        </div>
                        <div class="type">
                            Մրցույթի կոդ՝
                            <b>{{ $announcement->id }}</b>
                        </div>
                        <div class="date">
                            Հայտի ներկայացման վերջնաժամկետ՝
                            <b>{{Carbon\Carbon::parse($announcement->application_deadline)->format('d.m.Y')}}</b>
                            <p class="deadline_timer" style="display: none">{{$announcement->application_deadline}}</p>
                        </div>
                        <div class="field">
                            @if(isset($announcement->sub_spheres) || isset($announcement->spheres))
                                @php
                                    $announcement_sub_spheres_spheres = [];
                                    $announcement_spheres = [];
                                    foreach ($announcement->sub_spheres as $sub_spheres){
                                    $announcement_sub_spheres_spheres[] = $sub_spheres->sphere['name'];
                                    }
                                    if (isset($announcement->spheres)){
                                        foreach ($announcement->spheres as $sphere){
                                            $announcement_spheres[] = $sphere['name'];
                                        }
                                        $spheres_name = array_merge($announcement_sub_spheres_spheres,$announcement_spheres);

                                    }
                                    $sphere_text = "";

                                    foreach (array_unique($spheres_name) as $sphere_name){
                                        $sphere_text .= $sphere_name . ", ";
                                    }
                                    $sphere_text = substr($sphere_text, 0, -2)
                                @endphp

                                Ոլորտ՝
                                <b> {{$sphere_text}}</b>
                            @endif
                        </div>
                        <div class="sub-field">
                            Ենթաոլորտ՝
                            @if(isset($announcement->sub_spheres) && count($announcement->sub_spheres))
                                @php
                                    $sub_sphere_name = "";
                                    foreach ( $announcement->sub_spheres as $item){
                                        $sub_sphere_name .= $item['name'] . ', ';
                                    }
                                    $sub_sphere_name = substr($sub_sphere_name, 0, -2);

                                @endphp
                                {{$sub_sphere_name}}
                            @else
                                Տվյալ հայտարարությունը ենթաոլորտ չունի
                            @endif

                        </div>
                        <div class="deadline_timer_show">
                            <b>Տենդերն ավարտված է</b>
                        </div>
                    </div>
                    <div class="blog-right">
                        @forelse($user_announcements as $user_announcement)
                            @if(auth_user() && isset($user_announcement) && $announcement->id == $user_announcement['id'] )
                                <div class="saved favorite_announcement active"
                                     data-announcement_id="{{$announcement->id}}"
                                     data-user_id="{{auth_user()->id}}"></div>
                                @break
                            @else
                                <div class="saved favorite_announcement "
                                     data-announcement_id="{{$announcement->id}}"
                                     data-user_id="{{auth_user()->id}}"></div>

                            @endif
                        @empty
                            <div class="saved favorite_announcement " data-announcement_id="{{$announcement->id}}"
                                 data-user_id="{{auth_user()->id}}"></div>
                        @endforelse
                        <div class="text">Նախահաշվային գին՝ <b>{{number_format($announcement->estimated_price, 0 , '.', ' ') }} դրամ</b></div>
                        <div class="d-flex align-items-center">
                            @if(count($announcement->getMedia('generate_pdf')) > 0 )
                                <div><a href="{{ $announcement->getMedia('generate_pdf')[0]->getUrl() }}"
                                        target="_blank">
                                        <p class="pdf"></p></a>
                                </div>
                            @endif
                            <form method="post" action="{{ route('user.tenders.details') }}"
                                  class="form-horizontal">
                                @csrf
                                <input type="hidden" name="id" value="{{ $announcement['id'] }}">
                                <button type="submit" class="btn btn-info btn-round">Մանրամասները</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        @empty
            <h2 class=" sub-title" style="margin-top: 20px;">Նախընտրած հայտարարուրյուններ չկան</h2>
        @endforelse
    </div>
</div>
