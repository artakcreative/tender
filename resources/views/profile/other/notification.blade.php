@extends('profile.personal')
@section('personal')
    <div class="main-content">
        <div class="sub-title-2">Ծանուցումներ</div>
        <div class="notification-list">
            @if(count($notification) > 0)
                @foreach($notification as $value)
                    <div class="notification-item">
                        <div class="notification-header">
                            <div class="notification-title">{{ json_decode($value->data)->subject }}</div>
                        </div>
                        <div class="notification-body">
                            <div class="description">
                                @foreach((array)json_decode($value->data, true)['description'] as $item)
                                    <p>{{ $item }}</p><br>
                                @endforeach
                                <p> {{ Carbon\Carbon::parseFromLocale(json_decode($value->data)->time)->addHours(4)->format('Y-m-d H:i:s')}}</p>
                                @if(json_decode($value->data)->linkId)
                                    <br>
                                    <p>
                                    <form method="post" action="{{ route('user.tenders.details')}}"
                                          class="form-horizontal">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ json_decode($value->data)->linkId }}">
                                        <button type="submit" class="btn btn-info btn-round">Մանրամասները</button>
                                    </form>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="close remove-notify" data-id="{{ json_decode($value->data)->uuid }}" data-user_id="{{ json_decode($value->data)->user_id }}"></div>
                    </div>
                @endforeach
            @endif
        </div>
        <form method="post" class="append-input form-horizontal" action="{{ route('user.tenders.details') }}">
            @csrf
            <input type="hidden" class="append-input" name="id" value="">
        </form>
        {{$notification->links('vendor.pagination.simple-tailwind')}}
    </div>

@endsection
