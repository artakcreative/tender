@extends('profile.layouts.app')

@push('style')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="{{ asset('css/user/user.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <main class="main">
        <div class="wrapper">
            <div class="main-inner">
                <div class="sidebar">
                    <div class="user-name sub-title">Անձնական էջ</div>
                    <div class="sidebar__list nav-personal">
                        <a href="{{ route('user.tenders') }}"
                           class="sidebar__item sidebar__item-1 @if(Route::currentRouteName() == 'user.tenders') active @endif">
                            <div class="icon"></div>
                            Գնման մրցույթներ
                        </a>
                        <a href="{{ route('user.report') }}"
                           class="sidebar__item sidebar__item-2 @if(Route::currentRouteName() == 'user.report') active @endif">
                            <div class="icon"></div>
                            Հաշվետվություն
                        </a>
                        <a href="{{ route('user.settings') }}"
                           class="sidebar__item sidebar__item-3 @if(Route::currentRouteName() == 'user.settings') active @endif">
                            <div class="icon"></div>
                            Կարգավորումներ
                        </a>
                        <a href="{{ route('user.person.settings') }}"
                           class="sidebar__item sidebar__item-4 @if(Route::currentRouteName() == 'user.person.settings') active @endif">
                            <div class="icon"></div>
                            Անձնական տվյալներ
                        </a>
                        <a href="javascript:void;" onclick="$('#logout-form').submit();" class="sidebar__item sidebar__item-5">
                            <div class="icon"></div>
                            Դուրս գալ
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                        </form>
                    </div>
                </div>
               @yield('personal')
            </div>
        </div>
    </main>
@endsection

<script>
  window.authUserId = `{{ auth()->guard('web')->user()->id}}`;
</script>


