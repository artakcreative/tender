@extends('errors::layout')

@section('title', __('Not Found'))

@section('code', '404')

@section('message')
    <div class="container error-page">
        <div class="error-page__inner">
            <h2>404</h2>
            <h5>Էջը չի գտնվել</h5>
            <p>Ստուգեք URL- ի ճշտությունը: Եթե վստահ եք ձեր գործողություններում, դիմեք մեզ՝<b> info@kmg.am</b></p>
            <a href="{{route('home')}}" class="btn">
                Վերադառնալ գլխավոր էջ
            </a>
        </div>
    </div>
@endsection
