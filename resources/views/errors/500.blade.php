@extends('errors::layout')

@section('title', __('Not Found'))
@section('code', '500')
@section('message')
    <div class="container error-page">
        <div class="error-page__inner">
            <h2>500</h2>
            <h5>Սերվերի խափանում</h5>
            <p>Ստուգեք URL- ի ճշտությունը: Եթե վստահ եք ձեր գործողություններում, դիմեք մեզ՝ <b> info@kmg.am</b></p>
        </div>
    </div>
@endsection
