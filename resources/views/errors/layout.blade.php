<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .btn {
            max-width: 400px;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            margin: 0 auto;
        }

        .error-page {
            max-width: 550px;
            width: 100%;
            margin: 30px auto;
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
            padding: 0 15px;
        }

        .error-page h2 {
            font-size: 180px;
            line-height: 1;
            margin-bottom: 0;
        }

        .error-page h5 {
            font-size: 30px;
            opacity: 1;
            font-weight: 500;
            line-height: 48px;
        }

        .error-page p {
            font-size: 16px;
            line-height: 1.6;
            margin: 10px 0 30px;
        }

        .error-main {
            display: flex;
            align-items: center;
            justify-content: center;
            padding-top: 88px;
            box-sizing: border-box;
        }

        @media screen and (max-width: 450px) {
            .error-page h2 {
                font-size: 150px;
            }

            .error-page h5 {
                font-size: 24px;
                line-height: 36px;
            }

            .error-page p {
                font-size: 14px;
            }

            .error-page .btn{
                font-size: 14px;
            }
        }
    </style>
    <link href="{{ asset('css/user/user.css') }}" rel="stylesheet" type="text/css"/>
</head>
<body>

<header class="header">
    <div class="header-top">
        <div class="wrapper">
            <div class="header-top__inner">
                <div class="top-left">
                    <ul>
                        <li><img src="{{ asset('img/profile/icons/clock.svg') }}" alt="">
                            <a href="javascript:void(0);">
                                Երկ. - Ուրբ. 09:00-18:00 &nbsp;| &nbsp; Շբթ. 09:00-14:00
                            </a>
                        </li>
                        <li><img src="{{ asset('img/profile/icons/email.svg') }}" alt=""> <a href="mailto:info@kmg.am">info@kmg.am</a>
                        </li>
                        <li><img src="{{ asset('img/profile/icons/phone.svg') }}" alt="Հեռախոսահամր">
                            <a href="tel:+37410581958">+374 (10) 58 19 58</a> &nbsp; | &nbsp;
                            <a href="tel:+37499130099">+374 (99) 13 00 99</a>
                        </li>
                    </ul>
                </div>
                <div class="top-right">
                    <ul>
                        <li><img src="{{ asset('img/profile/icons/map.svg') }}" alt=""> <a href="javascript:void(0);">ք.
                                Երևան,
                                Աբովյան փողոց, շենք 41, թիվ 5</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="header-inner">
            <a href="{{ route('home') }}" class="header-logo logo">
                <img src="{{ asset('img/profile/logo.png') }}" alt="logo">
            </a>
            <nav class="nav">
            </nav>
        </div>
    </div>
</header>


<div class="phone-call">
    <button data-modal="#phone-call" class="phone-call-btn open__modal">
        <span></span>
    </button>
</div>
{{--{{ dd(auth_user()->manager) }}--}}
<div id="phone-call" class="modal">
    <div class="modal__container">
        <div class="close"></div>
        <h2 class="sub-title">Ձեզ սպասարկող մենեջերն է՝</h2>
        <div class="sub-title"><b>{{ auth_user()->manager->name ??  $contactUs->name }} {{ auth_user()->manager->surname ??  $contactUs->surname  }}ը</b></div>
        <div class="phone-call__list">
            <div class="phone-call__item">
                <a class="email" href="mailto:hovanisyan@kmg.am" target="_blank">{{ auth_user()->manager->email ?? $contactUs->email  }}</a>
            </div>
            <div class="phone-call__item">
                <a class="phone" href="tel:+374 (10) 58 19 58" target="_blank">{{ auth_user()->manager->phone ??  $contactUs->phone  }}</a>
            </div>
        </div>
    </div>
</div>

<script>
    @auth()
        window.authUserId  = `{{ auth_user()->id}}`
    @endauth
</script>


<body>

<main class="error-main">
    @yield('message')
</main>


@include('profile.layouts.includes.footer')



@include('profile.layouts.includes.scripts')


</body>
</html>
