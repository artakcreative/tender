@extends('errors::layout')

@section('title', __('Page Expired'))
@section('code', '419')

@section('message')
        <div class="container error-page">
            <div class="error-page__inner">
                <h2>419</h2>
                <h5>էջը ժամկետանց է</h5>
                <p>Խնդրեւմ ենք թարմացնել էջը: Եթե վստահ եք ձեր գործողություններում, դիմեք մեզ՝<b> info@kmg.am</b></p>
                <a href="{{route('home')}}" class="btn">
                    Վերադառնալ գլխավոր էջ
                </a>
            </div>
        </div>
@endsection
