<tr>
    <td class="header">
        <a href="{{ $url }}" style="display: inline-block;">
            @if (trim($slot) === env('APP_NAME'))
                <img src="{{ asset('img/email_logo.png') }}" class="logo"
                     style="max-width: 100%;width: 100%;min-height: 64px;height: 64px; max-height:64px;" alt="KMG Logo">
            @else
                {{ $slot }}
            @endif
        </a>
    </td>
</tr>
