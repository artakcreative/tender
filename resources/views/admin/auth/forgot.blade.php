<div class="card card-forgot">
    <div class="close-forgot"></div>
    <div class="card-header ">
        <div class="card-header ">
            <h4 class="header text-center">Մուտք անձնական  էջ</h4>
        </div>
    </div>
    <div class="card-body">
        <div class="input-group">
            @lang('auth.forgot_password_text')
        </div>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="nc-icon nc-key-25"></i>
                </span>
            </div>
            <input type="email" placeholder="Էլ-հասցե" class="form-control" id="email-forgot" name="email">
        </div>
        <div id="forgot-email"></div><br>
        <button type="button" class="btn btn-warning btn-round btn-block mb-3" id="btn-forgots">Վերականգնել</button>
    </div>
</div>



<div class="card card-password" data-token="{{ session()->get( 'admin-token' ) }}" data-email="{{ session()->get( 'admin-email' )}}">
    <div class="card-header">
        <div class="card-header">
            <h4 class="header text-center">Նոր գաղտնաբառ</h4>
        </div>
    </div>
    <div class="card-body ">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="nc-icon nc-key-25"></i>
                </span>
            </div>
            <input type="password" placeholder="Գաղտնաբառ" class="form-control password-reset" name="password">
        </div>
        <div id="reset-password"></div>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="nc-icon nc-key-25"></i>
                </span>
            </div>
            <input type="password" placeholder="Կրկնեք Գաղտնաբառը" class="form-control password-conf-reset" name="password_confirmation">
        </div>
        <div id="reset-conf-password"></div><br>
        <button type="button" class="btn btn-warning btn-round btn-block mb-3" id="btn-reset-admin">Պահպանել</button>
    </div>
</div>


<div class="card card-verification">
    <div class="close-text"></div>
    <div class="card-body mt-2">
        <p class="text-card-verification"></p>
    </div>
</div>

