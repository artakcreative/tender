@include('admin.layouts.includes.header')

<body class="login-page">
<!-- Extra details for Live View on GitHub Pages -->
<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
<div class="wrapper wrapper-full-page">
    <div class="full-page section-image" filter-color="black">
        <div class="content">
            <div class="container">
                <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                    <form method="POST" action="{{ route('admin.admin-login') }}">
                        @csrf
                        @if ($errors->any())
                            <div>
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card card-login card-login-admin">
                            <div class="card-header ">
                                <div class="card-header ">
                                    <h3 class="header text-center">Մուտք</h3>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                         <span class="input-group-text">
                                             <i class="nc-icon nc-single-02"></i>
                                         </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Ձեր էլ․ փոստի հասցեն"
                                           name="email" value="{{old('email')}}">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="nc-icon nc-key-25"></i>
                                        </span>
                                    </div>
                                    <input type="password" placeholder="Գաղտնաբառ" class="form-control" name="password">
                                </div>
                                <br/>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label for="remember_me" class="form-check-label">
                                            <input id="remember_me" type="checkbox" class="form-check-input"
                                                   name="remember">
                                            <span class="form-check-sign">Հիշել ինձ</span>
                                        </label>
                                        <a href="#" class="remove" data-modal="#modal_remove" id="forgot">
                                            Մոռացե՞լ եք գաղտնաբառը
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer ">
                                <button type="submit" class="btn btn-warning btn-round btn-block mb-3">Մուտք</button>
                            </div>
                        </div>
                    </form>
                    @include('admin.auth.forgot')
                </div>
            </div>
        </div>

        <footer class="footer footer-black  footer-white ">
            <div class="container-fluid">
                <div class="row">

                    <div class="credits ml-auto">
              <span class="copyright">
                © <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim
              </span>
                    </div>
                </div>
            </div>
        </footer>
        <div class="full-page-background"
             style="background-image: url('{{asset('img/admin/business-people.jpg')}}') "></div>

    </div>
</div>
<!--   Core JS Files   -->
@include('admin.layouts.includes.scripts')
</body>


<!-- Mirrored from demos.creative-tim.com/paper-dashboard-2-pro/examples/pages/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Nov 2021 08:17:29 GMT -->
</html>
