@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="d-flex flex-wrap">
                <div class="col-md-2">
                    <select class="selectManager  js-states form-control" id="selectManagerStatistic"></select>
                </div>
                <div class="col-md-2">
                    <select class="periodicity">
                        <option value="1">շաբաթական</option>
                        <option value="2">ամսական</option>
                        <option value="3">եռամսյակային</option>
                    </select>
                </div>
                <div class="col-1"></div>
                <div class="col-md-2 form-group block-datepicker">
                    <input type="text" name="end" class="form-control start-manager_date" autocomplete="off">
                </div>

                <div class="col-md-2 form-group block-datepicker">
                    <input type="text" name="start" class="form-control end-manager_date" autocomplete="off">
                </div>
                <div class="col-md-2 form-group">
                    <button class="btn btn-info filter_manager-static">Ֆիլտրել</button>
                </div>
            </div>
            <div class="col-2">
                <p class="error-range_manager" style="display: none;">Դաշտերը լրացնելը պարտադիր է։</p>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <canvas id="managerStatistic"></canvas>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{asset('js/admin/assets/js/core/manager-statistic.js')}}"></script>
@endpush
