@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="d-flex flex-wrap">
                <div class="col-md-2">
                    <select class="selectUser js-states form-control" id="selectUserStatistic"></select>
                </div>
                <div class="col-md-2">
                    <select class="periodicity">
                        <option value="1">շաբաթական</option>
                        <option value="2">ամսական</option>
                        <option value="3">եռամսյակային</option>
                    </select>
                </div>
                <div class="col-1"></div>
                <div class="col-md-2 form-group block-datepicker body_start-user_date">
                    <input type="text" name="end" class="form-control start-user_date" autocomplete="off">
                </div>
                <div class="col-md-2 form-group block-datepicker">
                    <input type="text" name="start" class="form-control end-user_date" autocomplete="off">
                </div>
                <div class="col-md-2 form-group">
                    <button class="btn btn-info filter_user-static">Ֆիլտրել</button>
                </div>
            </div>
            <div class="col-2">
                <p class="error-range_user" style="display: none;">Դաշտերը լրացնելը պարտադիր է։</p>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <canvas id="userStatistic"></canvas>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/admin/assets/js/core/user-statistic.js')}}"></script>
@endpush
