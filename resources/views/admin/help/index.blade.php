@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Օգնություն</h4>
        </div>
        <div class="card-body">

            @if(isset($help) and ! auth()->guard('admin')->user()->hasRole('admin'))
                <div>{!! $help->description  !!}</div>
                @elseif(auth()->guard('admin')->user()->hasRole('admin'))
                <form method="post" enctype="multipart/form-data" class="form-horizontal" action="{{route('admin.help.create')}}">
                    @csrf
                    @if(isset($help))
                        <div class="form-group">
                            <label for="description_help">Նկարագրություն</label>
                            <textarea class="form-control @error('description') is-invalid @enderror"
                                      id="description_help"
                                      name="description" rows="3">{{ $help->description }}</textarea>
                        </div>
                        <div>
                            @error('description')
                            <div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Փոփոխել</button>
                    @else
                        <div class="form-group">
                            <label for="description_help">Նկարագրություն</label>
                            <textarea class="form-control @error('description') is-invalid @enderror"
                                      id="description_help" name="description"
                                      rows="3"></textarea>
                        </div>
                        <div>
                            @error('description')
                            <div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Պահպանել</button>
                    @endif
                </form>
                @if(isset($help))
                    <form method="post" enctype="multipart/form-data"
                          class="form-horizontal" action="{{route('admin.help.delete')}}">
                        @csrf
                        <input type="hidden" name="id" value="{{ $help->id }}">
                        <button type="button" class="btn btn-danger delete-description" data-type="page-header">Հեռացնել</button>
                    </form>
                @endif
            @endif
        </div>
    </div>

@endsection
@push('scripts')
    <script src="{{asset('js/admin/assets/ckeditor/ckeditor.js')}}"></script>
    <script>
      CKEDITOR.replace( 'description_help');
    </script>
@endpush
