@extends('admin.layouts.app')

@section('content')
    <div class="card ">
        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success') }}
            </div>
        @endif
        <div class="card-header ">
            <h4 class="card-title">Անձնական տվյալներ</h4>
        </div>
        <div class="card-body ">
            <form method="post" enctype="multipart/form-data" action="{{route('admin.managers.update', ['id'=> $manager->id])}}" class="form-horizontal">
                @csrf
                @method('PUT')

                @include('admin.managers.form',['mode' => 'edit'])

                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">Պահպանել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

