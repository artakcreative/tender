<div class="row">
    <label class="col-md-3 col-form-label">Անուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Անուն" name="name" value="{{ isset($manager->name) ? $manager->name : old('name') }}">
        </div>
        <div>
            @error('name')<div class="alert alert-danger">{{ $errors->first('name') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Ազգանուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('surname') is-invalid @enderror" placeholder="Ազգանուն" name="surname" value="{{ isset($manager->surname) ? $manager->surname : old('surname') }}">
        </div>
        <div>
            @error('surname')<div class="alert alert-danger">{{ $errors->first('surname') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Էլեկտրոնային հասցե</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Էլեկտրոնային հասցե" name="email" value="{{ isset($manager->email) ? $manager->email : old('email') }}">
        </div>
        <div>
            @error('email')<div class="alert alert-danger">{{ $errors->first('email') }}</div>@enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">Հեռախոսահամար</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control  @error('phone') is-invalid @enderror" placeholder="Հեռախոսահամար" name="phone" value="{{ isset($manager->phone) ? $manager->phone :  old('phone') }}">
        </div>
        <div>
            @error('phone')<div class="alert alert-danger">{{ $errors->first('phone') }}</div>@enderror
        </div>
    </div>
</div>

@if($mode === 'create')
<div class="row">
    <label class="col-md-3 col-form-label">Գաղտնաբառ</label>
    <div class="col-md-9">
        <div class="form-group input-absolute">
            <i class="fa fa-magic icon generate_password"></i>
            <input type="text" class="form-control @error('password') is-invalid @enderror" placeholder="Գաղտնաբառ" name="password" id="password" value="{{ old('password') }}">
        </div>
        <div>
            @error('password')<div class="alert alert-danger">{{ $errors->first('password') }}</div>@enderror
        </div>
    </div>
</div>
@endif

{{--@if($mode == 'edit')--}}
{{--    {{dd($manager)}}--}}
{{--@endif--}}
@if($mode === 'create' ||  auth()->guard('admin')->user()->hasRole('admin'))
    <legend>Տալ թույլտվություն</legend>
@endif
    <div class="row permission">
        @if($mode == 'create')
        @foreach ($roles as $role)
            @foreach($role->permissions as $permission)
                <label class="col-md-3 col-form-label">{{$permission->display_name}}</label>
                <input  class="bootstrap-switch" type="checkbox" name="permissions[]"
                        value="{{ $permission->name}}" data-toggle="switch" data-on-label="<i class='nc-icon nc-check-2'></i>"
                        data-off-label="<i class='nc-icon nc-simple-remove'></i>" data-on-color="success" data-off-color="success" />
            @endforeach
        @endforeach
        @else
            @if(auth()->guard('admin')->user()->hasRole('admin'))
            @foreach ($roles as $role)
                @foreach($role->permissions as $permission)
                    <label class="col-md-3 col-form-label">{{$permission->display_name}}</label>
                    <input class="bootstrap-switch permission-checkbox" type="checkbox" name="permissions[]" value="{{$permission->name}}" data-toggle="switch"
                           {{$manager->hasDirectPermission($permission->name) ? "checked" : ''}}
                           data-on-label="<i class='nc-icon nc-check-2'></i>"
                           data-off-label="<i class='nc-icon nc-simple-remove'></i>"
                           data-on-color="success" data-off-color="success" />
                @endforeach
            @endforeach
                @endif
        @endif
    </div>


