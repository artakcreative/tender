@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    <div class="card">
        @if(auth()->guard('admin')->user()->hasRole('admin'))
            <div class="card-header">
                <a href="{{route('admin.managers.create')}}" type="button" class="btn btn-primary float-right">Ստեղծել</a>
                <h4 class="card-title">Մենեջերներ</h4>
            </div>
        @endif
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Անուն
                        </th>
                        <th>
                            Ազգանուն
                        </th>
                        <th>
                            Հեռախոսահամար
                        </th>
                        <th>
                            Էլեկտրոնային հասցե
                        </th>
                        <th>
                            Կարգավիճակ
                        </th>
                        @if(auth()->guard('admin')->user()->hasRole('admin'))
                            <th class="text-right">
                                Գործողություններ
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($managers as $manager)
                        <tr class="{{$manager->status == \App\Models\Admin::BLOCK ? 'blocked_manager' : ''}}">
                            <td>
                                {{ $manager->name }}
                            </td>
                            <td>
                                {{ $manager->surname }}
                            </td>
                            <td>
                                {{ $manager->phone }}
                            </td>
                            <td>
                                {{ $manager->email }}
                            </td>
                            <td>
                                @if($manager->status == \App\Models\Admin::ACTIVE)
                                    Ակտիվ է
                                @else
                                    Բլոկավորված է
                                @endif
                            </td>
                            <td class="text-right">
                                @if(auth()->guard('admin')->user()->hasRole('admin'))
                                <a type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm "
                                   href="{{route('admin.managers.edit', ['id'=>$manager->id])}}"
                                   data-original-title="Փոխել մենեջերի անձնական տվյալները">
                                    <i class="fa fa-edit"></i>
                                </a>
                                    @if($manager->status == \App\Models\Admin::BLOCK)
                                        <form method="post"
                                              action="{{route('admin.managers.unlock', ['id'=>$manager->id])}}"
                                              style="display: inline-block;">
                                            @csrf
                                            @method('PUT')
                                            <button type="button" rel="tooltip"
                                                    class="btn btn-default btn-icon btn-sm unlock_manager"
                                                    data-original-title="Ապաբլոկավորել մենեջերին" data-type="unlock_manager"
                                                    title="">
                                                <i class="fa fa-unlock"></i>
                                            </button>
                                        </form>
                                    @else
                                        <form method="post"
                                              action="{{route('admin.managers.block', ['id'=>$manager->id])}}"
                                              style="display: inline-block;">
                                            @csrf
                                            @method('PUT')
                                            <button type="button" rel="tooltip"
                                                    class="btn btn-danger btn-icon btn-sm block_manager"
                                                    data-original-title="Բլոկավորել մենեջերին" data-type="block_manager"
                                                    title="">
                                                <i class="fa fa-lock"></i>
                                            </button>
                                        </form>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
