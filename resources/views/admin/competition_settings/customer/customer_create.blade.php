@extends('admin.layouts.app')
@section('content')
    <div class="card ">
        <div class="card-header ">
            <h4 class="card-title">Պատվիրատու</h4>
        </div>
        <div class="card-body ">
            <form method="post" action="{{route('admin.customer.store')}}" class="form-horizontal">
                @csrf
                <div class="row">
                    <label class="col-md-3 col-form-label">Պատվիրատու</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control @error('customer') is-invalid @enderror" placeholder="Պատվիրատու" name="customer" value="{{ old('customer') }}">
                        </div>
                        <div>
                            @error('customer')<div class="alert alert-danger">{{ $errors->first('customer') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 col-form-label">Հապավում</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control @error('abbreviation') is-invalid @enderror" placeholder="Հապավում" name="abbreviation" value="{{ old('customer') }}">
                        </div>
                        <div>
                            @error('abbreviation')<div class="alert alert-danger">{{ $errors->first('abbreviation') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 col-form-label">Պատվիրատուների պատկանելիություն</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <select type="text" class="form-control @error('customer_affiliation_id') is-invalid @enderror"  name="affiliation_id">
                                <option class="bs-title-option" value="" disabled hidden selected>Պատվիրատուների պատկանելիություն</option>
                                @foreach($customerAffiliations as $customerAffiliation)
                                    <option class="" value="{{$customerAffiliation->id}}">{{$customerAffiliation->customer_affiliation}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            @error('affiliation_id')<div class="alert alert-danger">{{ $errors->first('affiliation_id') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">Ստեղծել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
