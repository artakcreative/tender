@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    <div class="card {{auth()->guard('admin')->user()->hasDirectPermission('Adding a new customer and industry') ? 'd-none' : '' }}">
        <div class="card-header">
{{--            <a href="{{ route('admin.customer_affiliation.create') }}" type="button" class="btn btn-primary float-right">Ստեղծել</a>--}}
            <h4 class="card-title">Պատվիրատուների պատկանելիություն</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Պատվիրատուների պատկանելիություն
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($customerAffiliations as $customerAffiliation)
                        <tr>
                            <td>
                                {{$customerAffiliation->customer_affiliation}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <a href="{{route('admin.customer.create')}}" type="button" class="btn btn-primary float-right">Ստեղծել Պատվիրատու</a>
            <a href="{{route('admin.customer.history')}}" type="button" class="btn btn-default float-right">Նայել պատմությունը</a>
            <h4 class="card-title">Պատվիրատուներ</h4>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Պատվիրատուներ
                        </th>
                        <th>
                            Հապավում
                        </th>
                        <th>
                            Պատվիրատուի պատկանելիություն
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($customers))
                        @foreach($customers as $customer)
                            <tr>
                                <td>
                                    {{$customer->name}}
                                </td>
                                <td>
                                    {{$customer->abbreviation}}
                                </td>
                                <td>
                                    @if($customer->affiliation !== null)
                                        {{$customer->affiliation->customer_affiliation}}
                                    @endif
                                </td>
                                <td class="text-right">
                                    <button type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm "
                                            data-original-title="Փոխել Պատվիրատուի տվյալները" title="">
                                        <a type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " href="{{route('admin.customer.edit', ['id'=>$customer->id])}}" data-original-title="Փոխել մենեջերի անձնական տվյալները">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </button>
                                    <button type="button" rel="tooltip" class="btn btn-danger btn-icon btn-sm "
                                            data-original-title="" title="">
                                        <a type="button" rel="tooltip" class=" btn btn-danger btn-icon btn-sm " href="{{route('admin.customer.delete', ['id'=>$customer->id])}}">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{$customers->links('vendor.pagination.bootstrap-4')}}
            </div>
        </div>
    </div>
@endsection
