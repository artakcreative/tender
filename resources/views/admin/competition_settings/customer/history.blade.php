@extends('admin.layouts.app')
{{--{{dd($customersHistories)}}--}}
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Պատվիրատուների ստեղծման պատմություն</h4>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Պատվիրատու
                        </th>
                        <th>
                            Հապավում
                        </th>
                        <th>
                            Ով է ստեղծել
                        </th>
                        <th>
                            Երբ է ստեղծել
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($customersHistories as $customerHistory)
                            <tr>
                                <td>
                                    {{$customerHistory->new_values['name']}}
                                </td>
                                <td>
                                    {{$customerHistory->new_values['abbreviation']}}
                                </td>
                                <td>
                                    @if(isset($customerHistory->user))
                                        {{$customerHistory->user->name}}
                                        {{$customerHistory->user->surname}}
                                    @else Ադմին
                                    @endif
                                </td>
                                <td>
                                    {{$customerHistory->created_at}}
                                </td>
                            </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $customersHistories->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
