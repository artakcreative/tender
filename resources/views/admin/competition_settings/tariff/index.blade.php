@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    <div class="card">
        @if(auth()->guard('admin')->user()->hasRole('admin'))
            <div class="card-header">
                <h4 class="card-title">Փաթեթներ</h4>
            </div>
        @endif
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Անուն
                        </th>
                        <th>
                            Գին
                        </th>
                        <th class="text-right">
                            Գործողություններ
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tariffs as $tariff)
                        <tr>
                            <td>
                                {{$tariff->name}}
                            </td>
                            <td>
                                {{$tariff->price}}
                            </td>

                            <td class="text-right">
                                    <a type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm "
                                       href="{{route('admin.tariff.edit', ['id' => $tariff->id])}}"
                                       data-original-title="Փոխել փաթեթի գինը">
                                        <i class="fa fa-edit"></i>
                                    </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
