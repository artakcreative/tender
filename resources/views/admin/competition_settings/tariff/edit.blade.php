@extends('admin.layouts.app')

@section('content')
    <div class="card ">
        <div class="card-header ">
            <h4 class="card-title">Փոխել {{$tariff->name}} փաթեթի գինը</h4>
        </div>
        <div class="card-body ">
            <form method="post" enctype="multipart/form-data" action="{{route('admin.tariff.update', ['id' => $tariff->id])}}" class="form-horizontal">
                @csrf
                @method('PUT')
                <div class="row">
                    <label class="col-md-3 col-form-label" style="max-width: 100px">Ընթացիկ գին</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{$tariff->price}}" placeholder="Ընթացիկ գին" name="price">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 col-form-label" style="max-width: 100px"></div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-primary">Պահպանել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
