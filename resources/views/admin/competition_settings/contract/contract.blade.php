@extends('admin.layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Ավելացնել պայմանագրի օրինակ</h4>
        </div>
        <div class="card-body">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-12">
                        <form method="post" enctype="multipart/form-data"
                              action="{{route('admin.settings.add-contract')}}" class="form-horizontal">
                            @csrf
                            <div class="row">
                                <div class="d-flex file_name_show">
                                    <div>
                                        <label for="attachment3">
                                            <a class="btn btn-primary text-light" role="button" aria-disabled="false">Կցել
                                                պայմանագրի օրինակներ</a>
                                        </label>
                                        <input type="file" name="contract_files_example[]" id="attachment3" class="attachment"
                                               style="visibility: hidden; position: absolute;" multiple/>
                                    </div>
                                    <div>
                                        @forelse( $files->getMedia('contract-global') as $media )
                                            <span class="file-block">
                                                <span class="name">
                                                    <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }}</a>
                                                 </span>
                                                 <span class="file-delete">
                                                    <i class="fa fa-trash delete_file" data-id="{{ $media->id }}"
                                                    data-url="/admin/announcement/delete-file"></i>
                                                 </span>
                                                </span>
                                        @empty
                                            <p id="files-area">
                                                <span id="filesList">
                                                    <span id="files-names"></span>
                                                </span>
                                            </p>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-form-label"></div>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-primary">Ավելացնել</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
