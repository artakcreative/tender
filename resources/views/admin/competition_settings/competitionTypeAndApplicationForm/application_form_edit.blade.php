@extends('admin.layouts.app')
@section('content')

    <div class="card ">
        <div class="card-header ">
            <h4 class="card-title">Հայտի ներկայացման եղանակ</h4>
        </div>
        <div class="card-body ">
{{--            <form method="post" action="{{route('admin.application_form.store')}}" class="form-horizontal">--}}

            <form method="post" action="{{route('admin.application_form.update', ['id' => $how_to_apply->id])}}" class="form-horizontal">
                @csrf
                <div class="row">
                    <label class="col-md-3 col-form-label">Հայտի ներկայացման եղանակ</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control @error('how_to_apply') is-invalid @enderror" placeholder="Հայտի ներկայացման եղանակ" name="how_to_apply" value="{{ $how_to_apply->name}}">
                        </div>
                        <div>
                            @error('how_to_apply')<div class="alert alert-danger">{{ $errors->first('how_to_apply') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">Պահպանել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
