@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            <a href="{{route('admin.competition_type')}}" type="button" class="btn btn-primary float-right">Ստեղծել</a>
            <h4 class="card-title">Մրցույթի տիպ</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Մրցույթի տիպ
                        </th>
                        @if(auth()->guard('admin')->user()->hasRole('admin'))
                            <th class="text-right">
                                Գործողություններ
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($competitionTypes as $competitionType)
                        <tr>
                            <td>
                                {{$competitionType->name}}
                            </td>
                            <td class="text-right">
                                @if(auth()->guard('admin')->user()->hasRole('admin'))
                                    <a type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm "
                                       href="{{route('admin.competition_type.edit', ['id' => $competitionType->id])}}"
                                       data-original-title="Փոխել Մրցույթի տիպը">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$competitionTypes->links('vendor.pagination.bootstrap-4')}}
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <a href="{{route('admin.application_form')}}" type="button" class="btn btn-primary float-right">Ստեղծել</a>
            <h4 class="card-title">Հայտի ներկայացման եղանակ</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Հայտի ներկայացման եղանակ
                        </th>
                        @if(auth()->guard('admin')->user()->hasRole('admin'))
                            <th class="text-right">
                                Գործողություններ
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($competitionApplicationForms as $competitionApplicationForm)
                        <tr>
                            <td>
                                {{$competitionApplicationForm->name}}
                            </td>
                            <td class="text-right">
                                @if(auth()->guard('admin')->user()->hasRole('admin'))
                                    <a type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm "
                                       href="{{route('admin.application_form.edit', ['id' => $competitionApplicationForm->id])}}"
                                       data-original-title="Փոխել Հայտի ներկայացման եղանակը">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$competitionApplicationForms->links('vendor.pagination.bootstrap-4')}}
            </div>
        </div>
    </div>
@endsection

