
@extends('admin.layouts.app')
@section('content')
    <div class="card ">
        <div class="card-header ">
            <h4 class="card-title">Մրցույթի տիպ</h4>
        </div>
        <div class="card-body ">
            <form method="post" action="{{route('admin.competition_type.update', ['id' => $competitionType->id])}}" class="form-horizontal">
                @csrf
                <div class="row">
                    <label class="col-md-3 col-form-label">Մրցույթի տիպ</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control @error('competition_type') is-invalid @enderror" placeholder="Մրցույթի տիպ" name="competition_type" value="{{ $competitionType->name }}">
                        </div>
                        <div>
                            @error('competition_type')<div class="alert alert-danger">{{ $errors->first('competition_type') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary">Ստեղծել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
