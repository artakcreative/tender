@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.sphere.create') }}" type="button" class="btn btn-primary float-right">Ստեղծել</a>
            <h4 class="card-title">Ոլորտներ</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Ոլորտ
                        </th>
                        <th>
                            Ենթաոլորտ
                        </th>
                        <th>
                            Ստեղծվել Է
                        </th>
                    </tr>
                    </thead>
                     <tbody>
                         @foreach($spheres as $sphere)
                             <tr>
                                <td>{{ $sphere->name }}</td>
                                <td>
                                    @forelse($sphere->sub_spheres as $item)
                                        <span class="badge badge-pill badge-success">{{ $item->name }}</span>
                                        @empty
                                         Ենթաոլորտ չկա
                                    @endforelse
                                </td>
                                 <td>{{ $sphere->created_at }}</td>
                             </tr>
                          @endforeach
                     </tbody>
                </table>
                {{ $spheres->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
