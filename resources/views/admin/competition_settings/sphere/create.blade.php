@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{route('admin.sphere.store')}}" method="POST" class="repeater">
                @csrf
                <div data-repeater-list="sphere_group" class="outer-list"
                >
                    <h4 class="card-title">Ոլորտ</h4>
                    <div>
                        <div data-repeater-item class="outer-list-item ">
                            <div class="row">
                                <label class="col-md-1 col-form-label ">Ոլորտ</label>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control repeater-item_child_input " required
                                               name="sphere">
                                        <button
                                            class="delete  inner-repeater-item_child_input-delete remove"
                                            data-repeater-delete="" type="button"><i
                                                class="fa fa-trash" title="Ջնջել ոլորտը"></i></button>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="inner-repeater col-12">
                                    <div data-repeater-list="sub_sphere" class="inner-repeater-item ">

                                        <div data-repeater-item
                                             class="inner-repeater-item_child category-group-item row">
                                            <label class="col-md-1 col-form-label "><span
                                                    class="pl-3">Ենթաոլորտ</span></label>
                                            <div class="col-md-7">
                                                <div class="form-group pl-4">
                                                    <input type="text"
                                                           class="form-control inner-repeater-item_child_input "
                                                           required
                                                           name="name">
                                                    <button
                                                        class="delete  inner-repeater-item_child_input-delete remove"
                                                        data-repeater-delete="" type="button"><i
                                                            class="fa fa-trash" title="Ջնջել ենթոլորտը"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <label class="col-md-1 col-form-label"></label>
                                        <div class="col-md-7">
                                            <div class="form-group d-flex">
                                                <input data-repeater-create type="button" class="add-form btn btn-primary"
                                                       value="Ավելացնել ենթաոլորտ"/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="row ">
                    <label class="col-md-1 col-form-label"></label>
                    <div class="col-md-7">
                        <div class="form-group d-flex">
                            <input data-repeater-create type="button" class="sphere_button btn btn-primary add-form"
                                   value="Ավելացնել ոլորտ"/>

                        </div>
                    </div>
                </div>
                <input type="hidden" name="template_id" value="1"/>

                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary competition">Ստեղծել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
