@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
        <form action="{{route('admin.competition_settings.store')}}" method="POST" class="repeater">
            @csrf

{{--            <div class="row {{auth()->guard('admin')->user()->hasDirectPermission('Adding a new customer and industry') ? 'd-none' : '' }}">--}}
{{--                <div class="col-md-6">--}}
{{--                    <h4 class="card-title">Մրցույթի տիպ</h4>--}}
{{--                    <input type="text" class="tagsinput competition_type" name="competition_type" data-role="tagsinput"--}}
{{--                           data-settings="{{isset($setting['competition_type']) ? implode(',', $setting['competition_type']) : ''}}"--}}
{{--                           value="{{isset($setting['competition_type']) ? implode(',', $setting['competition_type']) : ''}}"/>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="row {{auth()->guard('admin')->user()->hasDirectPermission('Adding a new customer and industry') ? 'd-none' : ''}}">--}}
{{--                <div class="col-md-6">--}}
{{--                    <h4 class="card-title">Հայտի ներկայացման եղանակ</h4>--}}
{{--                    <input type="text" class="tagsinput" name="how_to_apply" data-role="tagsinput"--}}
{{--                           data-settings="{{isset($setting['how_to_apply']) ? implode(',', $setting['how_to_apply']) : ''}}"--}}
{{--                           value="{{isset($setting['how_to_apply']) ? implode(',', $setting['how_to_apply']) : ''}} "/>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-6">--}}
{{--                    <h4 class="card-title">Պատվիրատուների պատկանելիություն</h4>--}}
{{--                    <input type="text" class="tagsinput" name="customer_affiliation" data-role="tagsinput"--}}
{{--                           data-settings="{{isset($setting['customer_affiliation']) ? implode(',', $setting['customer_affiliation']) : ''}}"--}}
{{--                           value="{{isset($setting['customer_affiliation']) ? implode(',', $setting['customer_affiliation']) : ''}}"/>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="row {{auth()->guard('admin')->user()->hasDirectPermission('Adding a new customer and industry') ? 'd-none' : '' }}">--}}
{{--                <div class="col-md-6">--}}
{{--                    <h4 class="card-title">Պատվիրատուներ</h4>--}}
{{--                    <input type="text" class="tagsinput" name="customers" data-role="tagsinput"--}}
{{--                           data-settings="{{isset($setting['customers']) ? implode(',', $setting['customers']) : ''}}"--}}
{{--                           value="{{isset($setting['customers']) ? implode(',', $setting['customers']) : ''}}"/>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div data-repeater-list="sphere_group" class="outer-list"
                 data-setting="{{isset($setting['sphere']) ? json_encode($setting['sphere']) : ''}}">
                <h4 class="card-title">Ոլորտ</h4>
                @forelse($setting['sphere'] ?? [] as $item)
                    <div data-repeater-item class="outer-list-item">
                        Ոլորտ <input type="text" name="sphere"
                                     class="repeater-item_child_input"
                                     data-sphere="{{isset($setting['sphere']) ? $item['sphere'] : ''}}"
                                     value="{{ $item['sphere'] }}"
                                     required
                        />
                        <input data-repeater-delete class="remove" type="button" value="Ջնջել"/>

                        <!-- innner repeater -->
                        <div class="inner-repeater">
                            <div data-repeater-list="sub_sphere" class="inner-repeater-item">
                                @forelse($item['sub_sphere'] ?? [] as $sub_sphere)
                                    <div data-repeater-item class="inner-repeater-item_child">
                                        Ենթաոլորտ <input type="text" name="name" class="inner-repeater-item_child_input"
                                                         data-sphere="{{isset($item['sub_sphere']) ? $sub_sphere['name'] : ''}}"
                                                         value="{{$sub_sphere['name']}}"
                                                         required
                                        />
                                        <input data-repeater-delete class="remove" type="button" value="Ջնջել"/>
                                    </div>
                                @empty
                                    <div data-repeater-item class="inner-repeater-item_child">
                                        Ենթաոլորտ <input type="text" class="inner-repeater-item_child_input"
                                                         name="name"/>
                                        <input data-repeater-delete type="button" value="Ջնջել"/>
                                    </div>
                                @endforelse
                            </div>
                            <input data-repeater-create type="button" value="Ավելացնել ենթաոլորտ"/>
                        </div>
                    </div>
                @empty
                    <div data-repeater-item class="outer-list-item">
                        Ոլորտ <input type="text" name="sphere" required/>
                        <input data-repeater-delete type="button" value="Ջնջել"/>

                        <!-- innner repeater -->
                        <div class="inner-repeater">
                            <div data-repeater-list="sub_sphere" class="inner-repeater-item">
                                <div data-repeater-item>
                                    Ենթաոլորտ <input type="text" name="name" required/>
                                    <input data-repeater-delete type="button" value="Ջնջել"/>
                                </div>
                            </div>
                            <input data-repeater-create type="button" value="Ավելացնել ենթաոլորտ"/>
                        </div>
                    </div>
                @endforelse
            </div>
            <input data-repeater-create type="button" class="sphere_button" value="Ավելացնել ոլորտ"/>


            <input type="hidden" name="template_id" value="1"/>

            <div class="row">
                <div class="col-md-3 col-form-label"></div>
                <div class="col-md-10">
                    <button type="submit" class="btn btn-info btn-round competition">Ստեղծել</button>
                </div>
            </div>
        </form>
@endsection




