{{--{{ dd($user->getMedia()) }}--}}
<div class="row">
    <label class="col-md-3 col-form-label">Անուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Անուն" name="name" value="{{isset($contactUs->name) ? $contactUs->name : old('name') }}">
        </div>
        <div>
            @error('name')<div class="alert alert-danger">{{ $errors->first('name') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Ազգանուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('surname') is-invalid @enderror" placeholder="Ազգանուն" name="surname" value="{{isset($contactUs->surname) ? $contactUs->surname : old('surname')}}">
        </div>
        <div>
            @error('surname')<div class="alert alert-danger">{{ $errors->first('surname') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Էլեկտրոնային հասցե</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Էլեկտրոնային հասցե" name="email" value="{{isset($contactUs->email) ? $contactUs->email : old('email')}}">
        </div>
        <div>
            @error('email')<div class="alert alert-danger">{{ $errors->first('email') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Հեռախոսահամար</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control  @error('phone') is-invalid @enderror" placeholder="Հեռախոսահամար" name="phone" value="{{isset($contactUs->phone) ? $contactUs->phone : old('phone')}}">
        </div>
        <div>
            @error('phone')<div class="alert alert-danger">{{ $errors->first('phone') }}</div>@enderror
        </div>
    </div>
</div>

