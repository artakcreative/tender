@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            {{--<a href="{{ route('admin.contact-us.create') }}" type="button" class="btn btn-primary float-right">Ստեղծել</a>--}}
            <h4 class="card-title">Կապ մեզ հետ</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Անուն
                        </th>
                        <th>
                            Ազգանուն
                        </th>
                        <th>
                            Հեռախոսահամար
                        </th>
                        <th>
                            Էլ․ հասցե
                        </th>
                        <th class="text-right">
                            Գործողտւթյուններ
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                         <tr>
                             <td>{{ $contactUs->name }}</td>
                             <td>{{ $contactUs->surname }}</td>
                             <td>{{ $contactUs->phone }}</td>
                             <td>{{ $contactUs->email }}</td>
                             <td class="text-right">
                                 <a type="button" rel="tooltip" class="btn btn-primary btn-icon" href="{{route('admin.contact-us.edit', ['id'=>$contactUs->id])}}" data-original-title="Փոփոհություններ">
                                     <i class="fa fa-edit"></i>
                                 </a>
                             </td>
                         </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
