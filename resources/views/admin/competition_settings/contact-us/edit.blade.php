@extends('admin.layouts.app')

@section('content')
<div class="card ">
    <div class="card-header ">
        <a class="float-right" href="{{ route('admin.contact-us') }}">
            <i class="fa fa-arrow-left"></i>
        </a>
        <h4 class="card-title">Փոփոխել  մենեջերի տվյալները որը կունենա կապ բոլոր օգտատերերի հետ, որոնք չունեն կցված մենեջեր</h4>
    </div>
    <div class="card-body ">
        <form method="post" action="{{route('admin.contact-us.update', ['id'=>$contactUs->id])}}" class="form-horizontal repeater">
            @csrf
            @include('admin.competition_settings.contact-us.form',['mode' => 'edit'])
            <div class="row">
                <div class="col-md-3 col-form-label"></div>
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Պահպանել</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
