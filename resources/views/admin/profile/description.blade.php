@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Կայքի նկարագրություն</h4>
        </div>
        <div class="card-body">
            <form method="post" enctype="multipart/form-data" class="form-horizontal" action="{{route('admin.profile.description.create')}}">
                @csrf
                @if(isset($pageDescription))
                    <div class="form-group">
                        <label for="exampleFormControlTextarea4">Վերնագիր</label>
                        <input class="form-control  @error('title') is-invalid @enderror"
                               type="text" name="title" id="exampleFormControlTextarea4" value="{{ $pageDescription->title }}">
                    </div>
                    <div>
                        @error('title')
                        <div class="alert alert-danger">{{ $errors->first('title') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="description_textarea">Նկարագրություն</label>
                        <textarea class="form-control @error('description') is-invalid @enderror"
                                  id="description_textarea"
                                  name="description" rows="3">{{ $pageDescription->description }}</textarea>
                    </div>
                    <div>
                        @error('description')
                        <div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Փոփոխել</button>
                @else
                    <div class="form-group">
                        <label for="exampleFormControlTextarea3">Վերնագիր</label>
                        <input class="form-control  @error('title') is-invalid @enderror"
                               type="text" name="title" id="exampleFormControlTextarea3">
                    </div>
                    <div>
                        @error('title')
                        <div class="alert alert-danger">{{ $errors->first('title') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Նկարագրություն</label>
                        <textarea class="form-control @error('description') is-invalid @enderror"
                                  id="exampleFormControlTextarea1" name="description"
                                  rows="3"></textarea>
                    </div>
                    <div>
                        @error('description')
                        <div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Պահպանել</button>
                @endif
            </form>

            @if(isset($pageDescription))
                <form method="post" enctype="multipart/form-data"
                      class="form-horizontal" action="{{route('admin.profile.description.delete')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $pageDescription->id }}">
                    <button type="button" class="btn btn-danger delete-description" data-type="page-header">Հեռացնել</button>
                </form>
            @endif
        </div>
    </div>

@endsection
