@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Փոփոխել հայտարարությունը</h4>
        </div>

        <div class="card-body">
            <form method="post" enctype="multipart/form-data"
                  class="form-horizontal" action="{{route('admin.profile.connect.update')}}">
                @csrf
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-raised">
                        @forelse($pageConnect->getMedia('pageConnect') as $media)
                            <img src="{{ $media->getUrl() }}" alt="" rel="nofollow">
                        @empty
                            <img src="{{ asset('img/image_placeholder.jpg') }}" alt="" rel="nofollow">
                        @endforelse
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                    <div>
                        <span class="btn btn-raised btn-round btn-default btn-file">
                            <span class="fileinput-new">Ընտրել նկար</span>
                            <span class="fileinput-exists">Փոփոխել</span>
                            <input type="file" name="image"/>
                        </span>
                        <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                           data-dismiss="fileinput">
                            <i class="fa fa-times"></i> Ջնջել</a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Կարճ Նկարագրություն</label>
                    <input class="form-control  @error('subject') is-invalid @enderror"
                           type="text" name="subject"  value="{{ $pageConnect['subject'] }}">
                </div>
                <div>
                    @error('subject')<div class="alert alert-danger">{{ $errors->first('subject') }}</div>@enderror
                </div>
                <input type="hidden" name="id" value="{{ $pageConnect['id']}}">
                <button type="submit" class="btn btn-success">Փոփոխել</button>
            </form>

        </div>
    </div>

@endsection

