@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Փոփոխել հայտարարությունը</h4>
        </div>

        <div class="card-body">
            <form method="post" enctype="multipart/form-data"
                  class="form-horizontal" action="{{route('admin.profile.material.update')}}">
                @csrf
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-raised">
                        @forelse($pageMaterial->getMedia('pageMaterial') as $media)
                            <img src="{{ $media->getUrl() }}" alt="" rel="nofollow">
                        @empty
                            <img src="{{ asset('img/image_placeholder.jpg') }}" alt="" rel="nofollow">
                        @endforelse
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                    <div>
                        <span class="btn btn-raised btn-round btn-default btn-file">
                            <span class="fileinput-new">Ընտրել նկար</span>
                            <span class="fileinput-exists">Փոփոխել</span>
                            <input type="file" name="image"/>
                        </span>
                        <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                           data-dismiss="fileinput">
                            <i class="fa fa-times"></i> Ջնջել</a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea4">Վերնագիր</label>
                    <input class="form-control  @error('title') is-invalid @enderror" type="text"
                           name="title" id="exampleFormControlTextarea4" value="{{ $pageMaterial['title'] }}">
                </div>
                <div>
                    @error('title')<div class="alert alert-danger">{{ $errors->first('title') }}</div>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Նկարագրություն</label>
                    <textarea class="form-control @error('description') is-invalid @enderror"
                              id="exampleFormControlTextarea1" name="description"
                              rows="3">{{  $pageMaterial['description'] }}</textarea>
                </div>
                <div>
                    @error('description')
                    <div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                </div>
                <input type="hidden" name="id" value="{{$pageMaterial['id']}}">
                <button type="submit" class="btn btn-success">Փոփոխել</button>
            </form>
        </div>
    </div>

@endsection

