@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Փոփոխել աշխատակցին</h4>
        </div>

        <div class="card-body">
            <form method="post" enctype="multipart/form-data"
                  class="form-horizontal" action="{{route('admin.profile.about.update')}}">
                @csrf
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-raised">
                        @forelse($pageAbout->getMedia('pageAbout') as $media)
                            <img src="{{ $media->getUrl() }}" alt="" rel="nofollow">
                        @empty
                            <img src="{{ asset('img/image_placeholder.jpg') }}" alt="" rel="nofollow">
                        @endforelse
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                    <div>
                        <span class="btn btn-raised btn-round btn-default btn-file">
                            <span class="fileinput-new">Ընտրել նկար</span>
                            <span class="fileinput-exists">Փոփոխել</span>
                            <input type="file" name="image"/>
                        </span>
                        <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                           data-dismiss="fileinput">
                            <i class="fa fa-times"></i> Ջնջել</a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFullName">Աշխատակցի անուն, ազգանուն</label>
                    <input type="text" class="form-control @error('full_name') is-invalid @enderror"
                           name="full_name" id="exampleFormControlFullName" value="{{ $pageAbout['full_name'] }}">
                </div>
                <div>
                    @error('full_name')<div class="alert alert-danger">{{ $errors->first('full_ name') }}</div>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlPosition">Պաշտոն</label>
                    <input type="text" class="form-control @error('position') is-invalid @enderror"
                           name="position" id="exampleFormControlPosition" value="{{ $pageAbout['position'] }}">
                </div>
                <div>
                    @error('position')<div class="alert alert-danger">{{ $errors->first('position') }}</div>@enderror
                </div>
                <input type="hidden" name="id" value="{{ $pageAbout['id']}}">
                <button type="submit" class="btn btn-success">Փոփոխել</button>
            </form>

        </div>
    </div>

@endsection

