@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Գլխավոր նկարագրություն</h4>
        </div>
        <div class="card-body">
            <form method="post" enctype="multipart/form-data"
                  class="form-horizontal" action="{{route('admin.profile.header.create')}}">
                @csrf
                @if(isset($pageHeader))
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-raised">
                        @forelse($pageHeader->getMedia('pageHeader') as $media)
                                <img src="{{ $media->getUrl() }}" alt="" rel="nofollow">
                            @empty
                                <img src="{{ asset('img/image_placeholder.jpg') }}" alt="" rel="nofollow">
                        @endforelse
                    </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                        <div>
	                <span class="btn btn-raised btn-round btn-default btn-file">
	                    <span class="fileinput-new">Փոփոխել նկար</span>
	                    <span class="fileinput-exists">Փոփոխել</span>
	                    <input type="file" name="image"/>
	                </span>
                            <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                               data-dismiss="fileinput">
                                <i class="fa fa-times"></i> Ջնջել</a>
                        </div>
                </div>
                <div class="form-group">
                    <label for="description_textarea">Նկարագրություն</label>
                    <textarea class="form-control @error('description') is-invalid @enderror"
                              id="description_textarea"
                              name="description" rows="3">{{ $pageHeader->description }}</textarea>
                </div>
                    <div>
                        @error('description')<div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                    </div>
                <button type="submit" class="btn btn-primary">Փոփոխել</button>
                @else
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail img-raised">
                            <img src="{{ asset('img/image_placeholder.jpg') }}" alt="" rel="nofollow">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                        <div>
	                <span class="btn btn-raised btn-round btn-default btn-file">
	                    <span class="fileinput-new">Ընտրել նկար</span>
	                    <span class="fileinput-exists">Փոփոխել</span>
                        <input type="file" name="image"/>
	                </span>
                            <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists"
                               data-dismiss="fileinput">
                                <i class="fa fa-times"></i> Ջնջել</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description_textarea">Նկարագրություն</label>
                        <textarea class="form-control @error('description') is-invalid @enderror"
                                  id="description_textarea" name="description"
                                  rows="3"></textarea>
                    </div>
                    <div>
                        @error('description')<div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Պահպանել</button>
                @endif
            </form>

            @if(isset($pageHeader))
                <form method="post" enctype="multipart/form-data"
                      class="form-horizontal" action="{{route('admin.profile.header.delete')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $pageHeader->id }}">
                    <button type="button" class="btn btn-danger delete-header" data-type="page-header">Հեռացնել</button>
                </form>
            @endif

        </div>

    </div>
@endsection
@push('scripts')
    <script src="{{asset('js/admin/assets/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'description_textarea');
    </script>
@endpush
