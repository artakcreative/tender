@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Հետաքրքիր նյութեր</h4>
        </div>
        <div class="card-body">
            <form method="post" enctype="multipart/form-data"
                  class="form-horizontal" action="{{route('admin.profile.material.create')}}">
                @csrf
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail img-raised">
                        <img src="{{ asset('img/image_placeholder.jpg') }}" alt="" rel="nofollow">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                    <div>
	                <span class="btn btn-raised btn-round btn-default btn-file">
	                    <span class="fileinput-new">Ընտրել նկար</span>
	                    <span class="fileinput-exists">Փոփոխել</span>
                        <input type="file" name="image"/>
	                </span>
                        <a href="javascript:;" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                            <i class="fa fa-times"></i> Ջնջել</a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea3">Վերնագիր</label>
                    <input class="form-control  @error('title') is-invalid @enderror"
                           type="text" name="title" id="exampleFormControlTextarea3">
                </div>
                <div>
                    @error('title')<div class="alert alert-danger">{{ $errors->first('title') }}</div>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea2">Նկարագրություն</label>
                    <textarea class="form-control @error('description') is-invalid @enderror"
                              id="exampleFormControlTextarea2" name="description"
                              rows="3"></textarea>
                </div>
                <div>
                    @error('description')<div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                </div>
                <button type="submit" class="btn btn-primary">Պահպանել</button>
            </form>
        </div>
    </div>
    @if(count($pageMaterials) > 0)
        <div class="card">
            <div class="card-header">
                <h4>Ավելացված նյութեր</h4>
            </div>
            @foreach($pageMaterials  as $value)
                <div class="card-body">
                    <div class="d-flex justify-content-around">
                        <div class="col-2">
                            @if(isset($value['url_image']))
                                <img src="{{ $value['url_image'] }}" alt="" style="width:50px; height: 50px">
                            @else
                                <img src="{{ asset('img/image_placeholder.jpg') }}" alt="">
                            @endif
                        </div>
                        <div class="col-2"><p>{{  $value['title'] }}</p></div>
                        <div class="col-4"><p>{{  $value['description'] }}</p></div>
                        <div class="d-flex col-4">
                            <form method="get" class="form-horizontal" action="{{route('admin.profile.material.edit', ['id'=> $value['id']])}}">
                                @csrf
                                <button type="submit" class="btn btn-primary" data-type="page-header">Փոփոխել</button>
                            </form>

                            <form method="post" enctype="multipart/form-data"
                                  class="form-horizontal" action="{{route('admin.profile.material.delete')}}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $value['id'] }}">
                                <button type="button" class="btn btn-danger delete-advantage" data-type="page-advantage">Հեռացնել</button>
                            </form>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    @endif
@endsection
