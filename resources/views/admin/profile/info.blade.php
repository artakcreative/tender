@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Մեր մասին</h4>
        </div>
        <div class="card-body">
            <form method="post" enctype="multipart/form-data"
                  class="form-horizontal" action="{{route('admin.profile.info.create')}}">
                @csrf
                @if(isset($pageInfo))
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Նկարագրություն</label>
                        <textarea class="form-control @error('description') is-invalid @enderror"
                                  id="exampleFormControlTextarea1"
                                  name="description" rows="10">{{ $pageInfo->description }}</textarea>
                    </div>
                    <div>
                        @error('description')<div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl2">Էլեկտրոնային հասցե</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" id="exampleFormControl2" value="{{ $pageInfo->email }}">
                    </div>
                    <div>
                        @error('email')<div class="alert alert-danger">{{ $errors->first('email') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl3">Հեռախոս 1</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror"
                               name="phone" id="exampleFormControl3" value="{{ $pageInfo->phone }}">
                    </div>
                    <div>
                        @error('phone')<div class="alert alert-danger">{{ $errors->first('phone') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl4">Հեռախոս 2</label>
                        <input type="text" class="form-control @error('more_phone') is-invalid @enderror"
                               name="more_phone" id="exampleFormControl4" value="{{ $pageInfo->more_phone }}">
                    </div>
                    <div>
                        @error('more_phone')<div class="alert alert-danger">{{ $errors->first('more_phone') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl5">Youtube</label>
                        <input type="text" class="form-control @error('youtube') is-invalid @enderror"
                               name="youtube" id="exampleFormControl5" value="{{ $pageInfo->youtube }}">
                    </div>
                    <div>
                        @error('youtube')<div class="alert alert-danger">{{ $errors->first('youtube') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl6">Facebook</label>
                        <input type="text" class="form-control @error('facebook') is-invalid @enderror"
                               name="facebook" id="exampleFormControl6" value="{{ $pageInfo->facebook }}">
                    </div>
                    <div>
                        @error('facebook')<div class="alert alert-danger">{{ $errors->first('facebook') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl7">Linkedin</label>
                        <input type="text" class="form-control @error('linkedin') is-invalid @enderror"
                               name="linkedin" id="exampleFormControl7" value="{{ $pageInfo->linkedin }}">
                    </div>
                    <div>
                        @error('linkedin')<div class="alert alert-danger">{{ $errors->first('linkedin') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl8">Ամբողջական հասցե</label>
                        <input type="text" class="form-control @error('full_address') is-invalid @enderror"
                               name="full_address" id="exampleFormControl8" value="{{ $pageInfo->full_address }}">
                    </div>
                    <div>
                        @error('full_address')<div class="alert alert-danger">{{ $errors->first('full_address') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl10">Կայքի իրավասություն </label>
                        <input type="text" class="form-control @error('footer_info') is-invalid @enderror"
                               name="footer_info" id="exampleFormControl10" value="{{ $pageInfo->footer_info }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Փոփոխել</button>
                @else
                    <div class="form-group">
                        <label for="exampleFormControl1">Նկարագրություն</label>
                        <textarea class="form-control @error('description') is-invalid @enderror"
                                  id="exampleFormControl1" name="description" rows="3"></textarea>
                    </div>
                    <div>
                        @error('description')<div class="alert alert-danger">{{ $errors->first('description') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl2">Էլեկտրոնային հասցե</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" id="exampleFormControl2">
                    </div>
                    <div>
                        @error('email')<div class="alert alert-danger">{{ $errors->first('email') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl3">Հեռախոս 1</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror"
                               name="phone" id="exampleFormControl3">
                    </div>
                    <div>
                        @error('phone')<div class="alert alert-danger">{{ $errors->first('phone') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl4">Հեռախոս 2</label>
                        <input type="text" class="form-control @error('more_phone') is-invalid @enderror"
                               name="more_phone" id="exampleFormControl4">
                    </div>
                    <div>
                        @error('more_phone')<div class="alert alert-danger">{{ $errors->first('more_phone') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl5">Youtube</label>
                        <input type="text" class="form-control @error('youtube') is-invalid @enderror"
                               name="youtube" id="exampleFormControl5">
                    </div>
                    <div>
                        @error('youtube')<div class="alert alert-danger">{{ $errors->first('youtube') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl6">Facebook</label>
                        <input type="text" class="form-control @error('facebook') is-invalid @enderror"
                               name="facebook" id="exampleFormControl6">
                    </div>
                    <div>
                        @error('facebook')<div class="alert alert-danger">{{ $errors->first('facebook') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl7">Linkedin</label>
                        <input type="text" class="form-control @error('linkedin') is-invalid @enderror"
                               name="linkedin" id="exampleFormControl7">
                    </div>
                    <div>
                        @error('linkedin')<div class="alert alert-danger">{{ $errors->first('linkedin') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl8">Ամբողջական հասցե</label>
                        <input type="text" class="form-control @error('full_address') is-invalid @enderror"
                               name="full_address" id="exampleFormControl8">
                    </div>
                    <div>
                        @error('full_address')<div class="alert alert-danger">{{ $errors->first('full_address') }}</div>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControl10">Կայքի իրավասություն </label>
                        <input type="text" class="form-control @error('footer_info') is-invalid @enderror"
                               name="footer_info" id="exampleFormControl10">
                    </div>
                    <div>
                        @error('footer_info')<div class="alert alert-danger">{{ $errors->first('footer_info') }}</div>@enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Պահպանել</button>
                @endif
            </form>

            @if(isset($pageInfo))
                <form method="post" enctype="multipart/form-data"
                      class="form-horizontal" action="{{route('admin.profile.info.delete')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $pageInfo->id }}">
                    <button type="button" class="btn btn-danger delete-header" data-type="page-header">Հեռացնել</button>
                </form>
            @endif

        </div>

    </div>
@endsection
