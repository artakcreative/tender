@extends('admin.layouts.app')

@section('content')
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-sm-2">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-bullhorn"></i>
                                </div>
                            </div>
                            <div class="col-7 col-sm-10">
                                <div class="numbers">
                                    <a href="{{route('admin.announcements')}}">
                                        <p class="card-category">Հայտարարություններ</p>
                                    </a>
                                    <p class="card-title">{{$publishedAnnouncementCount}}<p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-sm-2">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-users"></i>
                                </div>
                            </div>
                            <div class="col-7 col-sm-10">
                                <div class="numbers">
                                    <a href="{{ route('admin.users') }}">
                                        <p class="card-category">Օգտատերեր</p>
                                    </a>
                                    <p class="card-title">{{$userCount}}<p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-sm-2">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-layout-11"></i>
                                </div>
                            </div>
                            <div class="col-7 col-sm-10">
                                <div class="numbers">
                                    <a href="{{route('admin.managers')}}">
                                        <p class="card-category">Մենեջերներ</p>
                                    </a>
                                    <p class="card-title">{{$managerCount}}<p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-sm-2">
                                <div class="icon-big text-center icon-warning">
                                    <i class="nc-icon nc-bell-55 icon-notification"></i>
                                </div>
                            </div>
                            <div class="col-7 col-sm-10">
                                <div class="numbers">
                                    <a href="#">
                                        <p class="card-category">Ծանուցումներ</p>
                                    </a>
                                    <p class="card-title notification_count"><p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


