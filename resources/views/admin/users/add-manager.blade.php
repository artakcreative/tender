
@extends('admin.layouts.app')

@section('content')

{{--{{dd($managers)}}--}}
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Կցել Մենեջեր</h4>
        </div>
        <div class="card-body">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-lg-5 col-md-6 col-sm-3">
                            <form method="post" enctype="multipart/form-data" action="{{route('admin.users.manager.store')}}" class="form-horizontal">
                                @csrf
                                <select class="selectpicker" name="manager_id" data-size="7" data-style="btn btn-primary btn-round" title="Ընտրել մենեջեր">
                                    @foreach($managers as $manager)
                                        <option value="{{$manager->id}}">{{$manager->name}}</option>
                                    @endforeach
                                </select>
                                @error('manager_id') <div class="alert alert-danger">{{ $errors->first('manager_id') }}</div>@enderror
                                <input type="text" style="display:none" value="{{request()->id}}" name="user_id">
                                <div class="row">
                                    <div class="col-md0"></div>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-primary">Ավելացնել</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
