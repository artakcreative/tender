@extends('admin.layouts.app')

@section('content')
    <div class="card ">
        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success') }}
            </div>
        @endif
        <div class="card-header ">
            <h5 class="card-title"><strong>{{ $user['name'] .' '. $user['surname']}}</strong></h5>
            <div class="d-flex justify-content-between" style="width: 250px">
                <p>Ակտիվե է մինչև`</p>
                <p>{{ $data['active'] }} </p>
                @if((auth()->guard('admin')->user()->hasRole('admin') == true
                || auth()->guard('admin')->user()->id == $user['manager_id']) || auth()->guard('admin')->user()->hasDirectPermission('Set a deadline for paid user service') && count($data['spheres']) > 0)
                    <a type="button" rel="tooltip" class="btn btn-success btn-simple btn-icon btn-sm"
                       style="margin-top: -6px"
                       href="{{route('admin.users.edit.plane', ['id'=> $user->id])}}" data-original-title="Փոփոխել">
                        <i class="fa fa-edit"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class="card-body ">
            <div class="row">
                <div class="d-flex col-md-8">
                    <input class="w-25" type="text" value="{{ $user['name'] }}" disabled>
                    <input class="w-25 ml-3" type="text" value="{{ $user['surname'] }}" disabled>
                </div>
            </div>
            <div class="row mt-4">
                <div class="d-flex col-md-8">
                    <input class="w-25" type="text" value="{{ $user['email'] }}" disabled>
                    <input class="w-25 ml-3" type="text" value="{{ $user['phone'] }}" disabled>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-3 ">
                    <h5 class="card-title"><strong>{{ $data['tariff'] }}</strong></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 ">
                    <p><span style="padding-right: 10px">Ոլորտների քանակը՝</span>
                        <span style="padding-right: 10px"><input type="text" value="{{count($data['spheres'])}}" disabled
                                     style="width: 50px; text-align:center"></span>
                        <span>Ամսական վճարը՝ </span>
                        <span>{{ $data['sums'] }}</span>
                        <span> դրամ</span>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 ">
                    <p><span style="padding-right: 5px">Ընտրված ոլորտները՝</span>
                        @if(count($data['spheres']) > 0)
                            @foreach($data['spheres'] as $value)
                                <span class="file-block" style="display: inline-block">
                                                        <span class="name">
                                                            {{ $value }}
                                                        </span>
                                                    </span>
{{--                                <span style="padding-right: 5px" class="user_checked_sphere file-block">{{ $value }}</span>--}}
                            @endforeach
                        @endif
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 ">
                    <p>Կցված վճարային կտրոններ</p>
                    @if(isset($user))
                        <div class="file-list file-list__column ">
                            <div class="file-item">
                                @foreach($user->getMedia() as $media)
                                        <p id="files-area" style="min-width: auto">
                                            <span id="filesList" class="mt-2 mb-2" style="height: 35px;">
                                                <span id="files-names">
                                                    <span class="file-block">
                                                        <span class="name">
                                                            <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }}</a>
                                                        </span>
                                                    </span>
                                                </span>
                                             </span>
                                        </p>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 ">
                    <p>Կցված պայմանագրի օրինակներ</p>
                    @if(isset($user))
                        <div class="file-list file-list__column ">
                            <div class="file-item">
                                @foreach($user->getMedia('contract') as $media)
                                        <p id="files-area" style="min-width: auto">
                                            <span id="filesList" class="mt-2 mb-2" style="height: 35px;">
                                                <span id="files-names">
                                                    <span class="file-block">
                                                        <span class="name">
                                                            <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }}</a>
                                                        </span>
                                                    </span>
                                                </span>
                                             </span>
                                        </p>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @if(auth()->guard('admin')->user()->hasRole('admin') || auth()->guard('admin')->user()->id == $user['manager_id'])
                <div class="row mt-2">
                    <div class="col-md-10">
                        <a class="btn btn-primary" href="{{route('admin.users.edit.profile', ['id'=> $user->id])}}">
                            Փոխել տվյալները
                        </a>
                    </div>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Պատմություն</h4>
                </div>
                <div class="card-body">

                </div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>Անուն Ազգանուն</th>
                        <th>Ամսաթիվ</th>
                        <th>Գործողություն</th>
                        <th>Մեկնաբանություն</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($history) > 0)
                        @foreach($history as $value)
                            <tr>
                                <th>{{ $value['admin']['name'] .' ' .$value['admin']['surname'] }}</th>
                                <th>{{ Carbon\Carbon::parse($value['date'])->format('d-m-Y') }}</th>
                                <th>@if($value['action'] == 1)
                                        Ակտիվացում
                                    @else Դեակտիվացում
                                    @endif
                                </th>
                                <th>{{ $value['comment'] }}</th>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{ $history->links('vendor.pagination.bootstrap-4') }}
            </div>

        </div>
    </div>

@endsection

