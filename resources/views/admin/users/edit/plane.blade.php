@extends('admin.layouts.app')
@section('content')

    <div class="card ">
        <div class="card-header ">
            <h5 class="card-title"><strong>{{ $data['tariff'] }}</strong></h5>
        </div>
        <div class="card-body">
            <form method="post" enctype="multipart/form-data" action="{{route('admin.users.active.plane', ['id'=> $user->id])}}"
                  class="form-horizontal">
                @csrf
                <input type="hidden" name="tariff" value="{{ $data['tariff_wanth'] }}">
                <div class="row">
                    <div class="col-md-5" style="display: flex">
                        <span>Ակտիվացնել մինչև </span>
                        @if($data['active'] != null)
                                <div class="col-md-4 form-group block-datepicker">
                                    <input type="text" name="date" class="form-control activate_user_datepicker @error('date') is-invalid @enderror" autocomplete="off" value="{{Carbon\Carbon::parse($data['active'])->format('Y-m-d')}}"n>
                                </div>

{{--                            <input type=""  style="display: inline-block; width: 110px" class="activate_user_datepicker form-control @error('date') is-invalid @enderror" name="date" value="{{Carbon\Carbon::parse($data['active'])->format('Y-m-d')}}" autocomplete="off">--}}
                        @else
                            <div class="col-md-4 form-group block-datepicker">
                                <input type="text" name="date" class="form-control activate_user_datepicker @error('date') is-invalid @enderror" autocomplete="off">
                            </div>
{{--                            <input type="" style="display: inline-block; width: 110px" class="activate_user_datepicker form-control @error('date') is-invalid @enderror" name="date" value="" autocomplete="off">--}}
                        @endif
                        Ընտրված ոլորտները
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-10 d-flex">
                        <div>
                            <span style="padding-right:10px">Ոլորտների քանակը՝ </span>
                            <span><input type="text" value="{{ count($data['spheres'] )}}" disabled
                                         style="width: 50px; text-align:center"></span>
                        </div>
                        <div class="col-md-3">
                            <span>Ամսական վճարը՝ </span><span>{{ $data['sums'] }}</span><span> դրամ</span>
                        </div>
                    </div>
                </div>
                @if(count($data['spheres']) > 0  && $data['tariff_wanth'] != $data['tariff'])
                    <div class="row mt-4">
                        <div class="col-md-10 d-flex">
                            <div>
                                <span>Օգտատիրոջ փոփոխած տարիֆը՝ <strong><span>{{ $data['tariff_wanth'] }}</span></strong></span>
                            </div>
                            <div class="col-md-3">
                                <span>Ամսական վճարը՝ </span><span>{{ $data['sums_wanth'] }}</span><span> դրամ</span>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="mt-4 w-100">
                    <div class="form-group">
                        <label for="comment"><strong>Մեկնաբանություն</strong></label>
                        <textarea class="form-control @error('comment') is-invalid @enderror" name="comment"
                                 style="resize: vertical" id="comment"  type="text" ></textarea>
                    </div>
                </div>
{{--                <div class="row mt-2">--}}
{{--                    <div class="col-md-5 ">--}}
{{--                        <span>Կցել ֆայլեր </span>--}}
{{--                        <input type="file" class="multi" name="file[]" multiple/>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="row">
                    <div class="col-9 d-flex file_name_show">
                        <label for="attachment">
                            <a class="btn btn-primary text-light" role="button" aria-disabled="false">Կցել ֆայլեր</a>
                        </label>
                        <input type="file" name="file[]" id="attachment" class="attachment" style="visibility: hidden; position: absolute;" multiple/>
                            <p id="files-area">
                                    <span id="filesList">
                                        <span id="files-names"></span>
                                    </span>
                            </p>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-5">
                                <p>Կցված պայմանագրի օրինակներ</p>
                                @if(isset($user))
                                    <div class="file-list file-list__column ">
                                        <div class="file-item">
                                            @foreach($user->getMedia('contract') as $media)
                                                <p id="files-area" style="min-width: auto">
                                            <span id="filesList" class="mt-2 mb-2" style="height: 35px;">
                                                <span id="files-names">
                                                    <span class="file-block">
                                                        <span class="name">
                                                            <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }}</a>
                                                        </span>
                                                    </span>
                                                </span>
                                             </span>
                                                </p>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary"> Ակտիվացնել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
