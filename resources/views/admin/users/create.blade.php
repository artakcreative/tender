@extends('admin.layouts.app')

@section('content')
        <div class="card ">
            <div class="card-header ">
                <a class="float-right" href="{{ route('admin.users') }}">
                    <i class="fa fa-arrow-left"></i>
                </a>
                <h4 class="card-title">Ստեղծել նոր օգտատեր</h4>
            </div>
            <div class="card-body ">
                <form method="post" enctype="multipart/form-data" action="{{ route('admin.users.store') }}" class="form-horizontal repeater">
                    @csrf
                    @include('admin.users.form',['mode' => 'create'])
                    <div class="row">
                        <div class="col-md-3 col-form-label"></div>
                        <div class="col-md-10">
                            <button type="submit" class="btn btn-primary">Ստեղծել</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
