<div class="row">
    <label class="col-md-3 col-form-label">Անուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Անուն" name="name"
                   value="{{isset($user->name) ? $user->name : old('name') }}">
        </div>
        <div>
            @error('name')
            <div class="alert alert-danger">{{ $errors->first('name') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Ազգանուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('surname') is-invalid @enderror" placeholder="Ազգանուն"
                   name="surname" value="{{isset($user->surname) ? $user->surname : old('surname')}}">
        </div>
        <div>
            @error('surname')
            <div class="alert alert-danger">{{ $errors->first('surname') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Էլեկտրոնային հասցե</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror"
                   placeholder="Էլեկտրոնային հասցե" name="email"
                   value="{{isset($user->email) ? $user->email : old('email')}}">
        </div>
        <div>
            @error('email')
            <div class="alert alert-danger">{{ $errors->first('email') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Հեռախոսահամար</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="Հեռախոսահամար"
                   name="phone" value="{{isset($user->phone) ? $user->phone : old('phone')}}">
        </div>
        <div>
            @error('phone')
            <div class="alert alert-danger">{{ $errors->first('phone') }}</div>@enderror
        </div>
    </div>
</div>

@if($mode === 'create')
    <div class="row">
        <label class="col-md-3 col-form-label">Գաղտնաբառ</label>
        <div class="col-md-9">
            <div class="form-group input-absolute">
                <i class="fa fa-magic icon generate_password"></i>
                <input type="text" class="form-control @error('password') is-invalid @enderror" placeholder="Գաղտնաբառ"
                       name="password" id="password">
            </div>
            <div>
                @error('password')
                <div class="alert alert-danger">{{ $errors->first('password') }}</div>@enderror
            </div>
        </div>
    </div>
@endif

<div class="row">
    <label class="col-md-3 col-form-label"></label>
    <div class="col-9 d-flex file_name_show">
        <label for="attachment">
            <a class="btn btn-primary text-light" role="button" aria-disabled="false">Կցել ֆայլեր</a>
        </label>
        <input type="file" name="file[]" id="attachment" class="attachment" style="visibility: hidden; position: absolute;" multiple/>
        @if($mode == 'create')
            <p id="files-area">
            <span id="filesList">
                <span id="files-names"></span>
            </span>
            </p>
        @endif
        @if($mode == 'edit' && isset($user))
            <div class="row">
            <p id="files-area">
            <span id="filesList">
                <span id="files-names">
                   @foreach( $user->getMedia() as $media )
                        <span class="file-block">
                                <span class="name">
                                    <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }}</a>
                                </span>
                               <span class="file-delete">
                                     <i class="fa fa-trash delete_file" data-id="{{ $media->id }}"
                                        data-url="/admin/announcement/delete-file"></i>
                                </span>
                            </span>
                    @endforeach
                </span>
            </span>
            </p>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label"></label>
    <div class="col-9 d-flex file_name_show">
        <label for="attachment1">
            <a class="btn btn-primary text-light" role="button" aria-disabled="false">Կցել պայմանագրի օրինակներ</a>
        </label>
        <input type="file" name="contract_files[]" id="attachment1" class="attachment" style="visibility: hidden; position: absolute;" multiple/>
        @if($mode == 'create')
            <p id="files-area">
            <span id="filesList">
                <span id="files-names" ></span>
            </span>
            </p>
        @endif
        @if($mode == 'edit' && isset($user))
            <div class="row">
            <p id="files-area">
            <span id="filesList">
                <span id="files-names">
                   @foreach( $user->getMedia('contract') as $media )
                        <span class="file-block">
                                <span class="name">
                                    <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }}</a>
                                </span>

                               <span class="file-delete">
                                     <i class="fa fa-trash delete_file" data-id="{{ $media->id }}"
                                        data-url="/admin/announcement/delete-file"></i>
                                </span>
                            </span>
                    @endforeach
                </span>
            </span>
            </p>
            </div>
        @endif
    </div>
</div>

