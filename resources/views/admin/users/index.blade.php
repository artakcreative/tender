@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{session()->get('error') }}
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.users.create') }}" type="button" class="btn btn-primary float-right">Ստեղծել</a>
            <h4 class="card-title">Օգտատերեր</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <tr>
                            <th>
                                Անուն
                            </th>
                            <th>
                                Ազգանուն
                            </th>
                            <th>
                                Հեռախոսահամար
                            </th>
                            <th>
                                Էլեկտրոնային հասցե
                            </th>
                            <th>
                                Մենեջեր
                            </th>
                            <th>
                                Կարգավիճակ
                            </th>
                            <th class="text-right">
                                Գործողություններ
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr class="{{$user->status == \App\Models\User::BLOCK ? 'blocked_user' : ''}}">
                            <td>
                                {{ $user->name }}
                            </td>
                            <td>
                                {{ $user->surname }}
                            </td>
                            <td>
                                {{ $user->phone }}
                            </td>
                            <td>
                                {{ $user->email }}
                            </td>
                            <td>
                                {{ $user->manager != null ? $user->manager->name .' '. $user->manager->surname : "Մենեջեր չկա կցված" }}
                            </td>
                            <td>
                                @if(isset($user->userSpheres[0]->tariff->name) && $user->status !== \App\Models\Admin::BLOCK)
                                    {{$user->userSpheres[0]->tariff->name}}
                                @else
                                    @if($user->status == \App\Models\Admin::ACTIVE)
                                        Ակտիվ է
                                    @else
                                        Բլոկավորված է
                                    @endif
                                @endif

                            </td>
                            <td class="text-right">
                                @if(auth()->guard('admin')->user()->hasRole(['admin']) || auth()->guard('admin')->user()->hasDirectPermission('Attach user manager'))
                                    <a href="{{route('admin.users.add-manager', ['id'=>$user->id])}}" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="Ավելացնել մենեջեր" title="" style="color:#fff">
                                        <i class="fa fa-anchor"></i>
                                    </a>
                                @endif

                                    <a type="button" rel="tooltip" class="btn btn-success btn-icon btn-sm " href="{{route('admin.users.edit', ['id'=>$user->id])}}" data-original-title="Փոխել օգտատիրոջ  անձնական տվյալները">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    @if(auth()->guard('admin')->user()->hasRole('admin') || auth()->guard('admin')->user()->id == $user->manager_id)
                                        <form  method="post" action="{{route('admin.users.sign-in', ['id'=>$user->id])}}" style="display: inline-block;">
                                            @csrf
                                            <button type="submit" rel="tooltip" class="btn btn-info btn-icon btn-sm " data-original-title="Մուտք օգտատիրոջ անունից">
                                                <i class="fa fa-user"></i>
                                            </button>
                                        </form>
                                    @endif
                                @if(auth()->guard('admin')->user()->hasRole('admin') || auth()->guard('admin')->user()->hasDirectPermission('Set a deadline for paid user service'))
                                    @if($user->status == \App\Models\User::BLOCK)
                                                <form method="post"
                                                      action="{{route('admin.users.unlock', ['id'=>$user->id])}}"
                                                      style="display: inline-block;">
                                                    @csrf
                                                    @method('PUT')
                                                    <button type="button" rel="tooltip"
                                                            class="btn btn-default btn-icon btn-sm unlock_manager"
                                                            data-original-title="Ապաբլոկավորել մենեջերին" data-type="unlock_user"
                                                            title="">
                                                        <i class="fa fa-unlock"></i>
                                                    </button>
                                                </form>
                                            @else
                                                <form method="post"
                                                      action="{{route('admin.users.block', ['id'=>$user->id])}}"
                                                      style="display: inline-block;">
                                                    @csrf
                                                    @method('PUT')
                                                    <button type="button" rel="tooltip"
                                                            class="btn btn-danger btn-icon btn-sm block_manager"
                                                            data-original-title="Բլոկավորել օգտատիրոջը" data-type="block_user"
                                                            title="">
                                                        <i class="fa fa-lock"></i>
                                                    </button>
                                                </form>
                                    @endif
                                @endif
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $users->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
