<div class="row">
    <label class="col-md-3 col-form-label">Անուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Անուն" name="name" value="{{ old('name') }}">
        </div>
        <div>
            @error('name')<div class="alert alert-danger">{{ $errors->first('name') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Ազգանուն</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('surname') is-invalid @enderror" placeholder="Ազգանուն" name="surname" value="{{ old('surname') }}">
        </div>
        <div>
            @error('surname')<div class="alert alert-danger">{{ $errors->first('surname') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Էլեկտրոնային հասցե</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Էլեկտրոնային հասցե" name="email" value="{{ old('email') }}">
        </div>
        <div>
            @error('email')<div class="alert alert-danger">{{ $errors->first('email') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Հեռախոսահամար</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="Հեռախոսահամար" name="phone" value="{{ old('phone') }}">
        </div>
        <div>
            @error('phone')<div class="alert alert-danger">{{ $errors->first('phone') }}</div>@enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label">Գաղտնաբառ</label>
    <div class="col-md-9">
        <div class="form-group input-absolute">
            <i class="fa fa-magic icon generate_password"></i>
            <input type="text" class="form-control @error('password') is-invalid @enderror" placeholder="Գաղտնաբառ" name="password" id="password">
        </div>
        <div>
            @error('password')<div class="alert alert-danger">{{ $errors->first('password') }}</div>@enderror
        </div>
    </div>
</div>

