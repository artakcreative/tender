@extends('admin.layouts.app')

@section('content')

    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin.admins.create') }}" type="button" class="btn btn-primary float-right">Ստեղծել</a>
            <h4 class="card-title">Ադմիններ</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr>
                        <th>
                            Անուն
                        </th>
                        <th>
                            Ազգանուն
                        </th>
                        <th>
                            Հեռախոսահամար
                        </th>
                        <th>
                            Էլեկտրոնային հասցե
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $admin)
                        <tr>
                            <td>
                                {{ $admin->name }}
                            </td>
                            <td>
                                {{ $admin->surname }}
                            </td>
                            <td>
                                {{ $admin->phone }}
                            </td>
                            <td>
                                {{ $admin->email }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $admins->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
