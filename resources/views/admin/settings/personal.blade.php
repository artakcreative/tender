@extends('admin.layouts.app')

@section('content')
    <div class="card ">
        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success') }}
            </div>
        @endif
        <div class="card-header ">
            <h4 class="card-title">Անձնական տվյալներ</h4>
        </div>
        <div class="card-body ">
            <form method="post" enctype="multipart/form-data" action="{{route('admin.settings.update')}}" class="form-horizontal">
                @csrf
                <div class="row">
                    <label class="col-md-3 col-form-label">Անուն</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Անուն" name="name" value="{{ auth()->guard('admin')->user()->name }}">
                        </div>
                        <div>
                            @error('name')<div class="alert alert-danger">{{ $errors->first('name') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 col-form-label">Ազգանուն</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control @error('surname') is-invalid @enderror" placeholder="Ազգանուն" name="surname" value="{{ auth()->guard('admin')->user()->surname}}">
                        </div>
                        <div>
                            @error('surname')<div class="alert alert-danger">{{ $errors->first('surname') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 col-form-label">Էլեկտրոնային հասցե</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Էլեկտրոնային հասցե" name="email" value="{{ auth()->guard('admin')->user()->email }}">
                        </div>
                        <div>
                            @error('email')<div class="alert alert-danger">{{ $errors->first('email') }}</div>@enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-3 col-form-label">Հեռախոսահամար</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" placeholder="Հեռախոսահամար" name="phone" value="{{ auth()->guard('admin')->user()->phone }}">
                        </div>
                        <div>
                            @error('phone')<div class="alert alert-danger">{{ $errors->first('phone') }}</div>@enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-primary">Պահպանել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
