@extends('admin.layouts.app')

@section('content')
    <div class="card ">
        @if(session()->has('error'))
            <div class="alert alert-danger" role="alert">
                {{session()->get('error') }}
            </div>
        @endif
        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success') }}
            </div>
        @endif
        <div class="card-header ">
            <h4 class="card-title">Փոխել գաղտնաբառը</h4>
        </div>
        <div class="card-body ">
            <form method="post" enctype="multipart/form-data" action="{{route('admin.settings.password.update')}}" class="form-horizontal">
                @csrf
                <div class="row">
                    <label class="col-md-3 col-form-label">Ընթացիկ գաղտնաբառը</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="password" class="form-control @error('current_password') is-invalid @enderror" placeholder="Ընթացիկ գաղտնաբառ" name="current_password">
                        </div>
                        @error('current_password')<div class="alert alert-danger">{{$errors->first('current_password')}}</div> @enderror
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-3 col-form-label">Նոր գաղտնաբառը</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="password" class="form-control @error('new_password') is-invalid @enderror" placeholder="Նոր գաղտնաբառ" name="new_password">
                        </div>
                        @error('new_password')<div class="alert alert-danger">{{$errors->first('new_password')}}</div> @enderror
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-3 col-form-label">Կրկնել գաղտնաբառը</label>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Ընթացիկ գաղտնաբառ" name="password_confirmation">
                        </div>
                        @error('password_confirmation')<div class="alert alert-danger">{{$errors->first('password_confirmation')}}</div> @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-primary">Պահպանել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
