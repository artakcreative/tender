@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Հայտարարության պատմություն</h4>
        </div>

        <div class="card-body">
            <div class="table-responsive">

                @forelse($histories->audits as $history)
                    <div style="border-bottom:1px solid #ddd; margin-bottom: 10px">
                        <p>Փոփոխությունը կատարվել է {{$history->created_at}}-ին
                            <b>{{isset($history->user) ? $history->user->name : ''}} {{isset($history->user->surname) ? $history->user->surname : ''}}ի</b> կողմից</p>
                        <p>Կատարված փոփոխություններն են՝</p>
                        <ul>
                            @if(isset($history->old_values['announcement_name']) || isset($history->new_values['announcement_name']))
                                <li>
                                    Մրցույթի անվանումը փոխվել է
                                    <b>{{ $history->old_values['announcement_name']}}</b>-ից
                                    <b>{{$history->new_values['announcement_name']}}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['announcement_password']) || isset($history->new_values['announcement_password'] ))
                                <li>
                                    Մրցույթի ծածկագիրը փոխվել է
                                    <b>{{ $history->old_values['announcement_password']}}</b>-ից
                                    <b>{{$history->new_values['announcement_password']}}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['competition_type_id']) || isset($history->new_values['competition_type_id']))
                                <li>
                                    Մրցույթի տիպը փոխվել է
                                    <b>{{ $histories->competition_type[$history->old_values['competition_type_id']]}}</b>-ից
                                    <b>{{$histories->competition_type[$history->new_values['competition_type_id']]}}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['how_to_apply_id']) || isset($history->new_values['how_to_apply_id']))
                                <li>
                                    Հայտի ներկայացման եղանակը փոխվել է
                                    <b>{{$histories->how_to_apply[$history->old_values['how_to_apply_id']]}}</b>-ից
                                    <b>{{$histories->how_to_apply[$history->new_values['how_to_apply_id']]}}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['customer_id']) || isset($history->new_values['customer_id'] ))
                                <li>
                                    Պատվիրատուն փոխվել է
                                    <b>{{$histories->customer[ $history->old_values['customer_id']]}}</b>-ից
                                    <b>{{$histories->customer[ $history->new_values['customer_id']]}}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['customer_affiliation_id']) || isset($history->new_values['customer_affiliation_id']))
                                <li>
                                    Պատվիրատուի պատկանելիությունը փոխվել է
                                    <b>{{$histories->customer_affiliation[$history->old_values['customer_affiliation_id']]}}</b>-ից
                                    <b>{{$histories->customer_affiliation[$history->new_values['customer_affiliation_id']]}}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['purchase_item']) || isset($history->new_values['purchase_item']))
                                <li>
                                    @if(isset($history->old_values['purchase_item']))
                                        Գնման առարկան փոխվել է
                                        <b style="display: flex; margin-bottom: 10px">{!! $history->old_values['purchase_item'] !!}
                                            -ից</b>
                                        <b style="display: flex">{!! $history->new_values['purchase_item']!!}-ի</b>
                                    @else
                                        Ավելացել է Գնման առարկա
                                        <b style="display: flex">{!! $history->new_values['purchase_item']!!}</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['results_of_monitoring']) || isset($history->new_values['results_of_monitoring']))
                                <li>
                                    @if(isset($history->old_values['results_of_monitoring']))
                                        Նմանատիպ մրցույթների մոնիտորինգային հետազոտության արդյունքները փոխվել է
                                        <b style="display: flex; margin-bottom: 10px">{!! $history->old_values['results_of_monitoring'] !!}
                                            -ից</b>
                                        <b style="display: flex;">{!! $history->new_values['results_of_monitoring'] !!}
                                            -ի</b>
                                    @else
                                        Ավելացել է նմանատիպ մրցույթների մոնիտորինգային հետազոտության արդյունքները
                                        <b style="display: flex;">{!! $history->new_values['results_of_monitoring'] !!}</b>
                                    @endif

                                </li>
                            @endif
                            @if(isset($history->old_values['estimated_price']) || isset($history->new_values['estimated_price']))
                                <li>
                                    Նախահաշվային գինը փոխվել է
                                    <b>{{ $history->old_values['estimated_price'] }}</b>-ից
                                    <b>{{ $history->new_values['estimated_price'] }}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['application_deadline']) || isset($history->new_values['application_deadline']))
                                <li>
                                    Հայտի ներկայացման վերջնաժամկետը փոխվել է
                                    <b>{{ $history->old_values['application_deadline'] }}</b>-ից
                                    <b>{{ $history->new_values['application_deadline'] }}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['return_auction_day']) || isset($history->new_values['return_auction_day']))
                                <li>
                                    @if($history->old_values['return_auction_day'] )
                                        Հետադարձ աճուրդի օրը փոխվել է
                                        <b>{{ $history->old_values['return_auction_day'] }}</b>-ից
                                        <b>{{ $history->new_values['return_auction_day'] }}</b>-ի
                                    @else
                                        Ավելացել է Հետադարձ աճուրդի օր
                                        <b>{{ $history->new_values['return_auction_day'] }}</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['application_opening_day']) || isset($history->new_values['application_opening_day']))
                                <li>
                                    @if(isset($history->old_values['application_opening_day']))
                                        Հայտերի բացման նիստի օրը փոխվել է
                                        <b>{{ $history->old_values['application_opening_day'] }}</b>-ից
                                        <b>{{ $history->new_values['application_opening_day'] }}</b>-ի
                                    @else
                                        Ավելացել է Հայտերի բացման նիստի օր
                                        <b>{{ $history->new_values['application_opening_day'] }}</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['deadline_for_supply']) || isset($history->new_values['deadline_for_supply']))
                                <li>
                                    @if(isset($history->old_values['deadline_for_supply'] ))
                                        Մատակարարման վերջնաժամկետը փոխվել է
                                        <b>{{ $history->old_values['deadline_for_supply'] }}</b>-ից
                                        <b>{{ $history->new_values['deadline_for_supply'] }}</b>-ի
                                    @else
                                        Ավելացել է Մատակարարման վերջնաժամկետ
                                        <b>{{ $history->new_values['deadline_for_supply'] }}</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['place_of_delivery']) || isset($history->new_values['place_of_delivery']))
                                <li>
                                    @if(isset(json_decode($history->old_values['place_of_delivery'])->area ))
                                        Մատակարարման վայրը փոխվել է
                                        <b>{{json_decode($history->old_values['place_of_delivery'])->area  }}</b>-ից
                                        <b>{{json_decode($history->new_values['place_of_delivery'])->area  }}</b>-ի
                                    @else
                                        Ավելացել է Մատակարարման վայր
                                        <b>{{json_decode($history->new_values['place_of_delivery'])->area  }}</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['availability_of_financial_resources']) || isset($history->new_values['availability_of_financial_resources']))
                                <li>
                                    Ֆինանսական միջոցի առկայությունը փոխվել է
                                    <b>{{ $history->old_values['availability_of_financial_resources'] == 1 ? 'Այո' : 'Ոչ' }}</b>-ից
                                    <b>{{$history->new_values['availability_of_financial_resources'] == 1 ? 'Այո' : 'Ոչ' }}</b>-ի
                                </li>
                            @endif
                            @if(isset($history->old_values['payment_schedule']) || isset($history->new_values['payment_schedule']))
                                <li>
                                    {{debug(json_decode($history->old_values['payment_schedule'])->date[0] == null)}}


                                    @if(json_decode($history->old_values['payment_schedule'])->date[0] !== null || json_decode($history->old_values['payment_schedule'])->percent[0] !== null)
                                        Վճարման ժամանակացույցը փոխվել է
                                        @foreach(json_decode($history->old_values['payment_schedule'])->date as $old_date)
                                            <b>{{  $old_date}}</b>-ից
                                        @endforeach
                                        @foreach(json_decode($history->new_values['payment_schedule'])->date as $new_date)
                                            <b>{{ $new_date}}</b>-ի
                                        @endforeach
                                        և
                                        @foreach(json_decode($history->old_values['payment_schedule'])->percent as $old_percent)
                                            <b>{{  $old_percent}}%</b>-ից
                                        @endforeach
                                        @foreach(json_decode($history->new_values['payment_schedule'])->percent as$new_percent)
                                            <b>{{  $new_percent}}%</b>-ի
                                        @endforeach
                                    @else
                                        Ավելացել է Վճարման ժամանակացույց
                                        @foreach(json_decode($history->new_values['payment_schedule'])->date as $new_date)
                                            <b>{{ $new_date}}</b>
                                        @endforeach
                                        @foreach(json_decode($history->new_values['payment_schedule'])->percent as$new_percent)
                                            <b>{{  $new_percent}}%</b>
                                        @endforeach
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['payment_schedule_for_pdf']) || isset($history->new_values['payment_schedule_for_pdf']))
                                <li>
                                    @if(isset($history->old_values['payment_schedule_for_pdf']))
                                        Վճարման ժամանակացույց(PDF)ը փոխվել է
                                        <b style="display: flex; margin-bottom: 10px">{!!  $history->old_values['payment_schedule_for_pdf'] !!}
                                            -ից</b>
                                        <b style="display: flex;">{!!  $history->new_values['payment_schedule_for_pdf'] !!}
                                            -ի</b>
                                    @else
                                        Ավելացել է Վճարման ժամանակացույց(PDF)
                                        <b style="display: flex;">{!!  $history->new_values['payment_schedule_for_pdf'] !!}</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['prepayment']) || isset($history->new_values['prepayment']))
                                <li>
                                    @if(json_decode($history->old_values['prepayment']) !== null)
                                        Կանխավճարը փոխվել է
                                        <b>{{json_decode($history->old_values['prepayment'])->dram_or_percent }}</b>-ից
                                        և
                                        <b>{{json_decode($history->old_values['prepayment'])->time_of_provision }}</b>
                                        -ից
                                        <b>{{ json_decode($history->new_values['prepayment'])->dram_or_percent ?? null }}</b>-ի
                                        և
                                        <b>{{ json_decode($history->new_values['prepayment'])->time_of_provision ?? null }}</b>
                                        -ի
                                    @else
                                        Ավելացել է Կանխավճար
                                        Դրամ/տոկոս:
                                        <b>{{ json_decode($history->new_values['prepayment'])->dram_or_percent }}</b>
                                        և
                                        Տրամադրման ժամկետ:
                                        <b>{{ json_decode($history->new_values['prepayment'])->time_of_provision }}</b>
                                    @endif
                                </li>

                            @endif
                            @if(isset($history->old_values['provides']) || isset($history->new_values['provides']))
                                <li>
                                    Ապահովումը փոխվել է`
                                    @if(json_decode($history->old_values['provides'] !== null))
                                        {!!  isset(json_decode($history->old_values['provides'])->application_provide) ? 'Հայտի ապահովում: <b>' . json_decode($history->old_values['provides'])->application_provide . '</b>-ից': ''!!}
                                        {!!  isset(json_decode($history->old_values['provides'])->contract_provide) ? 'Պայմանագրի ապահովում: <b>' .json_decode($history->old_values['provides'])->contract_provide . '</b>-ից' : '' !!}
                                        {!!  isset(json_decode($history->old_values['provides'])->qualification_provide) ?'Որակավորման ապահովում: <b>' . json_decode($history->old_values['provides'])->qualification_provide . '</b>-ից և': ''!!}
                                        {!!  isset(json_decode($history->old_values['provides'])->prepayment_provide) ? 'Կանխավճարի ապահովում: <b>' . json_decode($history->old_values['provides'])->prepayment_provide . '</b>-ից ': ''!!}

                                        {!!  isset(json_decode($history->new_values['provides'])->application_provide) ? 'Հայտի ապահովում: <b>' . json_decode($history->new_values['provides'])->application_provide . '</b>-ի': ''!!}
                                        {!!  isset(json_decode($history->new_values['provides'])->contract_provide) ? 'Պայմանագրի ապահովում: <b>' .json_decode($history->new_values['provides'])->contract_provide . '</b>-ի' : '' !!}
                                        {!!  isset(json_decode($history->new_values['provides'])->qualification_provide) ?'Որակավորման ապահովում: <b>' . json_decode($history->new_values['provides'])->qualification_provide . '</b>-ի' : ''!!}
                                        {!!  isset(json_decode($history->new_values['provides'])->prepayment_provide) ? 'Կանխավճարի ապահովում: <b>' . json_decode($history->new_values['provides'])->prepayment_provide . '</b>-ի': ''!!}
                                    @else
                                        Ավելացել է Ապահովում
                                        {!!  isset(json_decode($history->new_values['provides'])->application_provide) ? 'Հայտի ապահովում: <b>' . json_decode($history->new_values['provides'])->application_provide . '</b>': ''!!}
                                        {!!  isset(json_decode($history->new_values['provides'])->contract_provide) ? 'Պայմանագրի ապահովում: <b>' .json_decode($history->new_values['provides'])->contract_provide . '</b>' : '' !!}
                                        {!!  isset(json_decode($history->new_values['provides'])->qualification_provide) ?'Որակավորման ապահովում: <b>' . json_decode($history->new_values['provides'])->qualification_provide . '</b>' : ''!!}
                                        և
                                        {!!  isset(json_decode($history->new_values['provides'])->prepayment_provide) ? 'Կանխավճարի ապահովում: <b>' . json_decode($history->new_values['provides'])->prepayment_provide . '</b>': ''!!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['doses']) || isset($history->new_values['doses']))
                                <li>
                                    @if(isset($history->old_values['doses']))
                                        Չափաբաժինները փոխվել են
                                        <b style="display: flex; margin-bottom: 10px">{!!  $history->old_values['doses'] !!}
                                            -ից</b>
                                            <b style="display: flex;">{!!  $history->new_values['doses'] !!}-ի</b>
                                        @else
                                        Ավելացել են չափաբաժիններ
                                        <b style="display: flex;">{!!  $history->new_values['doses'] !!}-ի</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['other_conditions']) || isset($history->new_values['other_conditions']))
                                <li>
                                    @if(isset($history->old_values['other_conditions']))
                                        Այլ պայմանները փոխվել են
                                        <b style="display: flex; margin-bottom: 10px">{!!  $history->old_values['other_conditions'] !!}
                                            -ից</b>
                                        <b style="display: flex;">{!!  $history->new_values['other_conditions'] !!}-ի</b>
                                    @else
                                       Ավելացել են այլ պայմաններ
                                        <b style="display: flex;">{!!  $history->new_values['other_conditions'] !!}</b>
                                    @endif
                                </li>
                            @endif
                            @if(isset($history->old_values['status']) || isset($history->new_values['status']))
                                <li>
                                    Հայտարարության կարգավիճակը փոխվել է
                                    <b>{{ $history->old_values['status'] == 1 ? 'Հրապարակված' : 'Պահպանված' }}</b>-ից
                                    <b>{{$history->new_values['status'] == 1 ? 'Հրապարակված' : 'Պահպանված' }}</b>-ի
                                </li>
                        @endif
                    </div>
                @empty
                @endforelse

            </div>
        </div>
    </div>
@endsection

