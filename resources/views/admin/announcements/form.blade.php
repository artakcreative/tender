@if($mode == 'edit')
    <div class="row">
        <label class="col-md-3 col-form-label">ID</label>
        <div class="col-sm-1">
            <div class="form-group">
                <input type="text" class="form-control"
                       placeholder="{{isset($announcement->id) ? $announcement->id : ""}}" disabled>
            </div>
        </div>
    </div>
@endif
{{--{{dd($errors->all())}}--}}
<input type="hidden" class="error_spheres" name="error_spheres">
<div class="row">
    <label class="col-md-3 col-form-label required">Մրցույթի անվանում</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('announcement_name') is-invalid @enderror"
                   name="announcement_name" placeholder="Մրցույթի անվանում"
                   value="{{isset($announcement->announcement_name) ? $announcement->announcement_name : old('announcement_name')}}"
                   required>
        </div>
        <div>
            @error('announcement_name')
            <div class="alert alert-danger">{{ $errors->first('announcement_name') }}</div>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label required">Մրցույթի ծածկագիր</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control @error('announcement_password') is-invalid @enderror"
                   placeholder="Մրցույթի ծածկագիր" name="announcement_password"
                   value="{{isset($announcement->announcement_password) ? $announcement->announcement_password : old('announcement_password')}}"
                   required>
        </div>
        <div>
            @error('announcement_password')
            <div class="alert alert-danger">{{ $errors->first('announcement_password') }}</div>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label required">Մրցույթի տիպ</label>
    <div class="col-md-9">
        <div class="form-group">
            <select type="text" class="form-control @error('competition_type') is-invalid @enderror select2"
                    name="competition_type" value="" required>
                <option class="bs-title-option" value="" disabled hidden selected>Մրցույթի տիպ</option>
                @forelse($competitionTypes ?? [] as  $competitionType)
                    @if(isset($announcement->competition_types->name ) && $announcement->competition_types->name == $competitionType->name)
                        <option selected value="{{$competitionType->id}}">{{$competitionType->name}}</option>
                    @elseif(old('competition_type') == $competitionType->id)
                        <option selected value="{{$competitionType->id}}">{{$competitionType->name}}</option>
                    @else
                        <option value="{{$competitionType->id}}">{{$competitionType->name}}</option>
                    @endif
                @empty
                @endforelse
            </select>
        </div>
        <div>
            @error('competition_type')
            <div class="alert alert-danger">{{ $errors->first('competition_type') }}</div>
            @enderror
        </div>
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label required">Հայտի ներկայացման եղանակ</label>
    <div class="col-md-9">
        <div class="form-group">
            <select type="text" class="form-control how_to_apply @error('how_to_apply') is-invalid @enderror"
                    name="how_to_apply" required>
                <option class="bs-title-option" value="" disabled hidden selected>Հայտի ներկայացման եղանակ</option>
                @forelse($competitionApplicationForms ?? [] as  $applicationForm)
                    @if(isset($announcement->application_forms->name) && $announcement->application_forms->name == $applicationForm->name)
                        <option selected value="{{$applicationForm->id}}">{{$applicationForm->name}}</option>
                    @elseif(old('how_to_apply') == $applicationForm->id)
                        <option selected value="{{$applicationForm->id}}">{{$applicationForm->name}}</option>
                    @else
                        <option value="{{$applicationForm->id}}">{{$applicationForm->name}}</option>
                    @endif
                @empty
                @endforelse
            </select>
        </div>
        <div>
            @error('how_to_apply')
            <div class="alert alert-danger">{{ $errors->first('how_to_apply') }}</div>@enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label required">Պատվիրատու</label>
    <div class="col-md-9">
        <div class="form-group">
            <select type="text" class="selectCustomer form-control @error('customer') is-invalid @enderror"
                    name="customer" required>
                <option class="bs-title-option" value="" disabled hidden selected>Պատվիրատու</option>

                @forelse($customers ?? [] as  $customer)
                    @if(isset($announcement->customers->name) && $announcement->customers->name  == $customer->name)
                        <option selected value="{{$customer->id}}">{{$customer->name}} ({{$customer->abbreviation}})
                        </option>
                    @elseif(old('customer') == $customer->id)
                        <option selected value="{{$customer->id}}">{{$customer->name}} ({{$customer->abbreviation}})
                        </option>
                    @else
                        <option value="{{$customer->id}}">{{$customer->name}} ({{$customer->abbreviation}})</option>
                    @endif
                @empty
                @endforelse
            </select>
        </div>
        <div>
            @error('customer')
            <div class="alert alert-danger">{{ $errors->first('customer') }}</div>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label required">Պատվիրատուի պատկանելիություն</label>
    <div class="col-md-9">
        <div class="form-group">
            <select type="text" class="form-control @error('customer_affiliation') is-invalid @enderror select2"
                    name="customer_affiliation" value="" required>
                <option class="bs-title-option" value="" disabled hidden selected>Պատվիրատուի պատկանելիություն</option>
                @forelse($customerAffiliations ??  [] as   $customerAffiliation)
                    @if(isset($announcement->customer_affiliations->customer_affiliation ) && $announcement->customer_affiliations->customer_affiliation == $customerAffiliation->customer_affiliation)
                        <option selected value="{{$customerAffiliation->id}}">{{$customerAffiliation->customer_affiliation}}</option>
                    @elseif(old('customer_affiliation') == $customerAffiliation->id)
                        <option selected value="{{$customerAffiliation->id}}">{{$customerAffiliation->customer_affiliation}}</option>
                    @else
                        <option value="{{$customerAffiliation->id}}">{{$customerAffiliation->customer_affiliation}}</option>
                    @endif
                @empty
                @endforelse
            </select>
        </div>
        <div>
            @error('customer_affiliation')
            <div class="alert alert-danger">{{ $errors->first('customer_affiliation') }}</div>
            @enderror
        </div>
    </div>
</div>

<div data-repeater-list="sphere-group">

    @forelse($announcement->spheres ?? [] as $announcement_sphere)
        <div data-repeater-item class="category-group-item">
            <div class="row">
                <label class="col-md-3 col-form-label required">Ոլորտ</label>
                <div class="col-md-9">
                    <div class="form-group">
                        <select type="text" name="sphere"
                                class="form-control selectSphere sphere @error('sphere-group.*.sphere') is-invalid @enderror"
                                required>
                            <option value="" disabled hidden selected> Ոլորտ</option>
                            @forelse($spheres ?? [] as $sphere)
                                @if(isset($announcement_sphere) && $announcement_sphere->name == $sphere->name)
                                    <option value="{{$sphere->id}}" class="sphere_name"
                                            selected>{{$sphere->name}}</option>
                                @elseif(old('sphere-group.*.sphere') == $sphere->name)
                                    <option value="{{$sphere->id}}" class="sphere_name"
                                            selected>{{$sphere->name}}</option>
                                @else
                                    <option value="{{$sphere->id}}" class="sphere_name">{{$sphere->name}}</option>
                                @endif
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div>
                        @error('sphere-group.*.sphere')
                        <div class="alert alert-danger">{{ $errors->first('sphere-group.*.sphere') }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-md-3 col-form-label">Ենթաոլորտ</label>
                <div class="col-md-9">
                    <div class="form-group sub_sphere_delete">
                        <select type="text" class="form-control selectSubSphere sub_sphere @error('sphere') is-invalid @enderror"
                                multiple
                                name="subsphere"

                        >
                        </select>
                    </div>
                </div>
            </div>
            <button class="delete" data-repeater-delete type="button"><i class="fa fa-trash"></i></button>
        </div>
    @empty
    @endforelse

    @forelse($sphereSubSpheres ?? [] as $key => $sphereSubSphere)
        <div data-repeater-item class="category-group-item">
            <div class="row">
                <label class="col-md-3 col-form-label required">Ոլորտ1</label>
                <div class="col-md-9">
                    <div class="form-group">
                        <select type="text" name="sphere"
                                class="form-control selectSphere sphere @error('sphere-group.*.sphere') is-invalid @enderror"
                                required>
                            <option value="" disabled hidden selected> Ոլորտ</option>
                            @forelse($spheres ?? [] as $sphere)
                                @if(isset($key) && $key == $sphere->name)
                                    <option value="{{$sphere->id}}" class="sphere_name"
                                            selected>{{$sphere->name}}</option>
                                @else
                                    <option value="{{$sphere->id}}" class="sphere_name">{{$sphere->name}}</option>
                                @endif
                            @empty
                            @endforelse

                        </select>
                    </div>
                    <div>
                        @error('sphere-group.*.sphere')
                        <div class="alert alert-danger">{{ $errors->first('sphere-group.*.sphere') }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-md-3 col-form-label">Ենթաոլորտ</label>
                <div class="col-md-9">
                    <div class="form-group sub_sphere_delete">
                        <select type="text" class="form-control selectSubSphere sub_sphere"
                                multiple
                                name="subsphere"
                        >
                            @foreach($sphereSubSphere as $sub_sphere)
                                <option value="{{$sub_sphere['id']}}" class="sub_sphere_name"
                                        selected>{{$sub_sphere['name']}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button class="delete" data-repeater-delete type="button"><i class="fa fa-trash"></i></button>
        </div>
    @empty
    @endforelse
    @if(!isset($announcement->spheres) && !isset($sphereSubSpheres) || $announcement->spares == null && $sphereSubSpheres == null && $mode !== 'edit')
            <div data-repeater-item class="category-group-item">
            <div class="row">
                <label class="col-md-3 col-form-label required">Ոլորտ</label>
                <div class="col-md-9">
                    <div class="form-group">
                        <select type="text" name="sphere"
                                class="form-control selectSphere sphere @error('sphere-group') is-invalid @enderror"
                               required >
                            <option value="" selected> Ոլորտ</option>
                            @forelse($spheres ?? [] as $sphere)

                                <option value="{{$sphere->id}}" class="sphere_name">{{$sphere->name}}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="sphere_validation">
                        @error('sphere-group.*.sphere')
                        <div class="alert alert-danger">{{ $errors->first('sphere-group.*.sphere') }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-md-3 col-form-label">Ենթաոլորտ</label>
                <div class="col-md-9">
                    <div class="form-group sub_sphere_delete">
                        <select type="text" class="form-control selectSubSphere sub_sphere"
                                multiple
                                name="subsphere"
                        >
                        </select>
                    </div>
                    <div class="subsphere_validation">
                        @error('sphere-group.*.subsphere')
                        <div class="alert alert-danger ">{{ $errors->first('sphere-group.*.subsphere') }}</div>
                        @enderror
                    </div>
                </div>

            </div>
            <button class="delete" data-repeater-delete type="button"><i class="fa fa-trash"></i></button>
        </div>
    @endif
</div>


<div class="row">
    <label class="col-md-3 col-form-label "></label>
    <div class="col-md-9">
        <div class="form-group d-flex">
            <input class="add-form btn btn-primary" data-repeater-create type="button" value="Ավելացնել ոլորտ"/>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">Գնման առարկա</label>
    <div class="col-md-9">
        <div class="form-group">
            <textarea class="form-control textarea_style" id="purchase_item" name="purchase_item"
                      value="">{{isset($announcement->purchase_item) ? $announcement->purchase_item : old('purchase_item')}}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label">Նմանատիպ մրցույթների մոնիտորինգային հետազոտության արդյունքներ</label>
    <div class="col-md-9">
        <div class="form-group">
            <textarea class="form-control textarea_style" id="results_of_monitoring" name="results_of_monitoring"
                      value="">{{isset($announcement->results_of_monitoring) ? $announcement->results_of_monitoring : old('results_of_monitoring')}}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-md-3 col-form-label required">Նախահաշվային գին</label>
    <div class="col-md-9">
        <div class="form-group">
            <input type="text" class="form-control"
                   name="estimated_price" placeholder="Նախահաշվային գին"
                   value="{{isset($announcement->estimated_price) ? $announcement->estimated_price : old('estimated_price')}}">
        </div>
        <div>
            @error('estimated_price')
            <div class="alert alert-danger">{{ $errors->first('estimated_price') }}</div>
            @enderror
        </div>
    </div>
</div>
<div class="row">
    <label class="col-md-3 col-form-label required">Հայտի ներկայացման վերջնաժամկետ</label>
    <div class="col-md-9">
        <div class="form-group block-datepicker">
            <input class="form-control application_deadline datetimepicker" name="application_deadline"
                   placeholder="Հայտի ներկայացման վերջնաժամկետ"
                   value="{{isset($announcement->application_deadline) ? Carbon\Carbon::parse($announcement->application_deadline)->format('d.m.Y H:i') : old('application_deadline')}}"
                   autocomplete="off">
        </div>
        <div>
            @error('application_deadline')
            <div class="alert alert-danger">{{ $errors->first('application_deadline') }}</div>
            @enderror
        </div>
    </div>
</div>
{{--Հետադարձ աճուրդի օր--}}
<div class="row return_auction_day">
    @if(isset($announcement->return_auction_day) && $announcement->return_auction_day != '' || old('return_auction_day'))
        <label class="col-md-3 col-form-label">Հետադարձ աճուրդի օր</label>
        <div class="col-md-9">
            <div class="form-group block-datepicker">
                <input class="form-control return_auction_day_input datetimepicker" name="return_auction_day"
                       value="{{isset($announcement->return_auction_day) ? $announcement->return_auction_day : old('return_auction_day')}}"
                       autocomplete="off">
            </div>
        </div>
    @endif
</div>
{{--Հետադարձ աճուրդի օր--}}
<div class="row">
    <label class="col-md-3 col-form-label">Հայտերի բացման նիստի օր</label>
    <div class="col-md-9">
        <div class="form-group block-datepicker">
            <input class="form-control application_opening_day datetimepicker" name="application_opening_day"
                   placeholder="Հայտերի բացման նիստի օր"
                   value="{{isset($announcement->application_opening_day) ?  Carbon\Carbon::parse($announcement->application_opening_day)->format('d.m.Y H:i'): old('application_opening_day')}}"
                   autocomplete="off">
        </div>
    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">Մատակարարման վերջնաժամկետ</label>
    <div class="col-md-9">
        <div class="form-group block-datepicker">
            <input class="form-control deadline_for_supply datepicker" name="deadline_for_supply"
                   placeholder="Մատակարարման վերջնաժամկետ"
                   value="{{isset($announcement->deadline_for_supply) ? Carbon\Carbon::parse($announcement->deadline_for_supply)->format('d.m.Y') : old('deadline_for_supply')}}"
                   autocomplete="off">
        </div>
    </div>
</div>


<div class="row" style="margin-top: 10px">
    <label class="col-md-3 col-form-label">Մատակարարման վայր</label>
    <div class="col-md-9">
        <div class="form-group delivery_data" style="display:flex">
            <input type="text" class="form-control place_of_delivery" id="place_of_delivery"
                   name="place_of_delivery[area]" placeholder="Մատակարարման վայր"
                   value="{{isset($announcement->place_of_delivery['area']) ? $announcement->place_of_delivery['area'] : old('place_of_delivery.area')}}">
            <input type="text" class="d-none latitude" name="place_of_delivery[latitude]"
                   value="{{isset($announcement->place_of_delivery['area']) ? $announcement->place_of_delivery['latitude'] : old('place_of_delivery.latitude')}}">
            <input type="text" class="d-none longitude" name="place_of_delivery[longitude]"
                   value="{{isset($announcement->place_of_delivery['area']) ? $announcement->place_of_delivery['longitude']: old('place_of_delivery.longitude')}}">
        </div>
    </div>
</div>

{{--Քարտեզ--}}
<div class="row">
    <label class="col-md-3 col-form-label"></label>
    <div class="col-md-9 mb-2">
        <div id="map" name="map" class="d-none"></div>
    </div>
</div>
{{--Քարտեզ--}}

<div class="row">
    <label class="col-md-3 col-form-label">Ֆինանսական միջոցի առկայություն</label>
    <div class="col-md-9">
        <div class="form-group">
            <select type="text" class="form-control" name="availability_of_financial_resources">
                <option class="bs-title-option" value="" selected style="display:none">Ֆինանսական միջոցի առկայություն
                </option>
                <option
                    value="1" {{isset($announcement->availability_of_financial_resources) && $announcement->availability_of_financial_resources == 1 ? 'selected' : ''}}>
                    Այո
                </option>
                <option
                    value="0" {{isset($announcement->availability_of_financial_resources) && $announcement->availability_of_financial_resources == 0 ? 'selected' : ''}}>
                    Ոչ
                </option>
            </select>
        </div>
    </div>
</div>
<div class="payment_schedule">
    @forelse($announcement->payment_schedule['date'] ?? [] as $key => $payment_date)
        <div class="row payment_schedule_item">
            <label class="col-md-3 col-form-label">Վճարման ժամանակացույց</label>
            <div class="col-md-3">
                <div class="form-group block-datepicker">
                    <input class="form-control schedule_date datepicker" name="payment_schedule[date][]"
                           placeholder="Ամիս" value="{{$payment_date}}" autocomplete="off">
                </div>
            </div>
            <div class="col-md-3">
                    <input type="text" class="form-control percent" name="payment_schedule[percent][]"
                           placeholder="Տոկոս" value="{{$announcement->payment_schedule['percent'][$key]}}">

            </div>
        </div>
    @empty

        @forelse(old('payment_schedule.date') ?? [] as $key => $payment_date)
            <div class="row payment_schedule_item">
                <label class="col-md-3 col-form-label">Վճարման ժամանակացույց</label>
                <div class="col-md-3">
                    <div class="form-group block-datepicker">
                        <input class="form-control schedule_date datepicker" name="payment_schedule[date][]"
                               placeholder="Ամիս" value="{{$payment_date}}" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control percent" name="payment_schedule[percent][]"
                               placeholder="Տոկոս" value="{{old("payment_schedule.percent.$key")}}" autocomplete="off">
                    </div>
                </div>
            </div>
        @empty
            <div class="row payment_schedule_item">
                <label class="col-md-3 col-form-label">Վճարման ժամանակացույց</label>
                <div class="col-md-3">
                    <div class="form-group block-datepicker">
                        <input class="form-control schedule_date datepicker" name="payment_schedule[date][]"
                               placeholder="Ամիս" value="" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control percent" name="payment_schedule[percent][]"
                               placeholder="Տոկոս">
                    </div>
                </div>
            </div>
        @endforelse
    @endforelse
</div>
<div class="row">
    <label class="col-md-3 col-form-label"></label>
    <div class="col-md-9">
        <div class="form-group d-flex">
            <input class="add-form btn btn-primary reset_schedule" type="button"
                   value="Զրոյացնել վճարման ժամանակացույցը"/>
        </div>
    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">Վճարման ժամանակացույց(PDF)</label>
    <div class="col-md-9">
        <div class="form-group">
            <textarea class="form-control textarea_style" id="payment_schedule_for_pdf" name="payment_schedule_for_pdf"
                      value="">{{isset($announcement->payment_schedule_for_pdf) ? $announcement->payment_schedule_for_pdf : old('payment_schedule_for_pdf')}}</textarea>
        </div>
    </div>
</div>



<div class="row">
    <label class="col-md-3 col-form-label">Կանխավճար</label>
    <div class="form-check">
        <label class="form-check-label check_prepayment">
            <div class="form-group">
                <input class="form-check-input prepayment" type="checkbox"
                       name="prepayment" {{isset($announcement->prepayment) && $announcement->prepayment !== '' || old('prepayment') ? 'checked' : ''}}>
                <span class="form-check-sign"></span>

            </div>
        </label>
    </div>
</div>
{{--կանխավճարի չափ--}}
<div class="money">
    @if(isset($announcement->prepayment) && $announcement->prepayment !== '' || old('prepayment'))
        <div class="row">
            <label class="col-md-3 col-form-label dram_or_percent">Դրամ/Տոկոս</label>
            <div class="col-md-3 ">
                <div class="form-group">
                    <input type="text" class="form-control dram_or_percent_input" name="prepayment[dram_or_percent]"
                           placeholder="Դրամ/Տոկոս"
                           value="{{isset($announcement->prepayment) && $announcement->prepayment !== '' ? $announcement->prepayment['dram_or_percent'] : old('prepayment.dram_or_percent')}}"
                           required>
                </div>
                <div>
                    @error('prepayment.dram_or_percent')
                    <div class="alert alert-danger">{{ $errors->first('prepayment.dram_or_percent') }}</div>
                    @enderror
                </div>
            </div>

        </div>
        <div class="row">
            <label class="col-md-3 col-form-label">Տրամադրման ժամկետ</label>
            <div class="col-md-3">
                <div class="form-group block-datepicker">
                    <input class="form-control time_of_provision datepicker" name="prepayment[time_of_provision]"
                           placeholder="Տրամադրման ժամկետ"
                           value="{{isset($announcement->prepayment) && $announcement->prepayment !== '' ? $announcement->prepayment['time_of_provision'] : old('prepayment.time_of_provision')}}"
                           required>
                </div>
            </div>
        </div>
    @endif
</div>


<div class="row">
    <label class="col-md-3 col-form-label">Ապահովում</label>
    <div class="form-check">
        <label class="form-check-label check_provides">
            <div class="form-group">
                <input class="form-check-input provides" type="checkbox"
                       name="provides" {{ isset($announcement->provides)  && $announcement->provides !== '' || old('provides') ? 'checked' : ''}}>
                <span class="form-check-sign"></span>
            </div>
        </label>
    </div>
</div>

{{--Ենթաբաղադրիչներ--}}
<div class="subcomponents">
    @if(isset($announcement->provides)  && $announcement->provides !== '' || old('provides'))
        <div class="sub_content">
            <div class="row">
                <label class="col-md-3 col-form-label ">Հայտի ապահովում</label>
                <div class="form-check">
                    <label class="form-check-label check_provides">
                        <div class="form-group">
                            <input class="form-check-input subcomponent1"
                                   type="checkbox" {{ isset($announcement->provides['application_provide']) || old('provides.application_provide') ? 'checked' : ''}}>
                            <span class="form-check-sign"></span>
                        </div>
                    </label>
                </div>
            </div>
            @if(isset($announcement->provides['application_provide']) || old('provides.application_provide'))
                <div class="row subcomponent1_field">
                    <label class="col-md-3 col-form-label"></label>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text subcomponent_provides" name="provides[application_provide]"
                                   class="form-control"
                                   value="{{isset($announcement->provides['application_provide']) && $announcement->provides['application_provide'] !== '' ? $announcement->provides['application_provide'] : old('provides.application_provide') }}">
                        </div>
                        <div>
                            @error('provides.application_provide')
                            <div class="alert alert-danger">{{ $errors->first('provides.application_provide') }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            @else
                <div class="row subcomponent1_field">
                </div>
            @endif

            <div class="row">
                <label class="col-md-3 col-form-label">Պայմանագրի ապահովում</label>
                <div class="form-check">
                    <label class="form-check-label check_provides">
                        <div class="form-group">
                            <input class="form-check-input subcomponent2"
                                   type="checkbox" {{ isset($announcement->provides['contract_provide']) && $announcement->provides['contract_provide'] !== '' || old('provides.contract_provide') ? 'checked' : ''}}>
                            <span class="form-check-sign"></span>
                        </div>
                    </label>
                </div>
            </div>
            @if(isset($announcement->provides['contract_provide']) || old('provides.contract_provide'))
                <div class="row subcomponent2_field">
                    <label class="col-md-3 col-form-label"></label>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text subcomponent_provides" name="provides[contract_provide]"
                                   class="form-control"
                                   value="{{isset($announcement->provides['contract_provide']) ? $announcement->provides['contract_provide'] : old('provides.contract_provide') }}">
                        </div>

                        <div>
                            @error('provides.contract_provide')
                            <div class="alert alert-danger">{{ $errors->first('provides.contract_provide') }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            @else
                <div class="row subcomponent2_field">
                </div>
            @endif

            <div class="row">
                <label class="col-md-3 col-form-label">Որակավորման ապահովում</label>
                <div class="form-check">
                    <label class="form-check-label check_provides">
                        <div class="form-group">
                            <input class="form-check-input subcomponent3"
                                   type="checkbox" {{ isset($announcement->provides['qualification_provide'])  || old('provides.qualification_provide') ? 'checked' : ''}}>
                            <span class="form-check-sign"></span>
                        </div>
                    </label>
                </div>
            </div>
            @if(isset($announcement->provides['qualification_provide']) || old('provides.qualification_provide'))
                <div class="row subcomponent3_field">
                    <label class="col-md-3 col-form-label"></label>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text subcomponent_provides" name="provides[qualification_provide]"
                                   class="form-control"
                                   value="{{isset($announcement->provides['qualification_provide']) ? $announcement->provides['qualification_provide'] : old('provides.qualification_provide') }}">
                        </div>
                        <div>
                            @error('provides.qualification_provide')
                            <div class="alert alert-danger">{{ $errors->first('provides.qualification_provide') }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            @else
                <div class="row subcomponent3_field">
                </div>
            @endif

            <div class="row">
                <label class="col-md-3 col-form-label"> Կանխավճարի ապահովում</label>
                <div class="form-check">
                    <label class="form-check-label check_provides">
                        <div class="form-group">
                            <input class="form-check-input subcomponent4"
                                   type="checkbox" {{ isset($announcement->provides['prepayment_provide']) || old('provides.prepayment_provide') ? 'checked' : ''}}>
                            <span class="form-check-sign"></span>
                        </div>
                    </label>
                </div>
            </div>
            @if(isset($announcement->provides['prepayment_provide']) || old('provides.prepayment_provide'))
                <div class="row subcomponent4_field">
                    <label class="col-md-3 col-form-label"></label>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text subcomponent_provides" name="provides[prepayment_provide]"
                                   class="form-control"
                                   value="{{isset($announcement->provides['prepayment_provide']) ? $announcement->provides['prepayment_provide'] : old('provides.prepayment_provide') }}">
                        </div>
                        <div>
                            @error('provides.prepayment_provide')
                            <div class="alert alert-danger">{{ $errors->first('provides.prepayment_provide') }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            @else
                <div class="row subcomponent4_field">
                </div>
            @endif
        </div>
    @endif
</div>

<div class="row">
    <label class="col-md-3 col-form-label">Չափաբաժիններ</label>
    <div class="col-md-9">
        <div class="form-group">
            <textarea class="form-control textarea_style" name="doses" id="doses"
            >{{isset($announcement->doses) ? $announcement->doses : old('doses')}} </textarea>
        </div>
    </div>
</div>


<div class="row">
    <label class="col-md-3 col-form-label">Այլ պայմաններ</label>
    <div class="col-md-9">
        <div class="form-group">
            <textarea class="form-control textarea_style" name="other_conditions" id="other_conditions"
            >{{isset($announcement->other_conditions) ? $announcement->other_conditions : old('other_conditions')}} </textarea>
        </div>
    </div>
</div>



<input type="hidden" value="{{auth()->guard('admin')->user()->id}}" name="manager_id">
@if($mode == 'edit')
    @php
        $generate_pdf = $announcement->getMedia('generate_pdf');
        $default = $announcement->getMedia();
        $files = $default->merge($generate_pdf);
    @endphp
@endif

<div class="row">
    <label class="col-md-3 col-form-label" style="margin-left: 13px"></label>
    <div class="col-9 d-flex file_name_show">
        <label for="attachment">
            <a class="btn btn-primary text-light mt-2 mb-2" role="button" aria-disabled="false">Կցել ֆայլեր</a>
        </label>
        <input type="file" name="file[]" id="attachment" class="attachment" style="display: none;" multiple/>
        @if($mode == 'create')
            <p id="files-area">
            <span id="filesList">
                <span id="files-names"></span>
            </span>
            </p>
        @endif
        @if($mode == 'edit')
            <input type="hidden" name="new_or_old" value="{{ $announcement->new_or_old }}"/>
            <p id="files-area">
            <span id="filesList" class="mt-2 mb-2" style="height: 35px;">
                <span id="files-names">
                   @foreach( $files as $media )
                        <span class="file-block">
                                <span class="name">
                                    <a href="{{ $media->getUrl() }}" target="_blank">{{ $media->name }}</a>
                                </span>
                                <span class="file-delete">
                                     <i class="fa fa-trash delete_file" data-id="{{ $media->id }}"
                                        data-url="/admin/announcement/delete-file"></i>
                                </span>
                        </span>
                    @endforeach
                </span>
            </span>
            </p>
        @endif
    </div>

</div>
@if($mode == 'edit' && $announcement->status == \App\Models\Announcement::SAVED)
    <div class="row ">
        <label class="col-md-3 col-form-label">Հրապարակել</label>
        <input class="bootstrap-switch publicate" type="checkbox" name="publicate"
               {{isset($announcement->status) && $announcement->status == \App\Models\Announcement::PUBLISHED ? 'checked' : ''}}
               data-toggle="switch" data-on-label="<i class='nc-icon nc-check-2'></i>"
               data-off-label="<i class='nc-icon nc-simple-remove'></i>" data-on-color="success"
               data-off-color="success"
        />
    </div>
@endif
@push('scripts')
    <script src="{{asset('js/admin/assets/ckeditor/ckeditor.js')}}"></script>


    <script>
        CKEDITOR.replace( 'purchase_item');
        CKEDITOR.replace( 'other_conditions');
        CKEDITOR.replace( 'payment_schedule_for_pdf');
        CKEDITOR.replace( 'results_of_monitoring');
        CKEDITOR.replace( 'doses');
    </script>
    <script src="{{asset('js/admin/assets/js/core/map.js')}}"></script>

@endpush









