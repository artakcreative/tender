<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
        * {
            font-family: DejaVu Sans !important;
        }

        .table-border, .table-border tr, .table-border td, .table-border th {
            border: 1px solid black;
            text-align: center;
        }

        .table-border td, .table-border th {
            margin: 0 5px;
            page-break-inside: avoid;
        }

        .table-border {
            border-collapse: collapse;
        }

        table {
            max-width: 100%;
            width: 100%;
            page-break-inside: auto
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        .table-border.text-left td, .table-border.text-left th, .table-border.text-left tr {
            text-align: left;
        }

        table {
            width: 100% !important;
            border-spacing: 0 !important;
            border-collapse: collapse !important;
        }

        .marker {
            background: #ffff00 !important;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <table id="header" style="width: 100%">
        <tr>
            <td class="header-left" style="width: 70%;">

            </td>
            <td class="header-right">
                <img class="header-logo" src="{{asset('img/Pdf_logo.png')}}" alt="pdf_logo"
                     style="margin-left: auto; width: 300px; ">
            </td>
        </tr>

    </table>
    <table class="textLayer" style="width: 100%;">
        <tr>
            <td>
                <h2 style="font-size: 14px;line-height:1;font-family: sans-serif;text-align: center;margin: 0;">
                    ՄՐՑՈՒՅԹԻՆ ՄԱՍՆԱԿՑՈՒԹՅԱՆ
                </h2>
                <h2 style="font-size: 14px;line-height:1;font-family: sans-serif;text-align: center;margin: 0;">
                    ՀՐԱՎԵՐ</h2>
                <p style="font-family: sans-serif;text-align: center;margin: 0;">{{$competitionTypesName}}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-style: italic;font-size: 15px;line-height: 1;margin: 20px 0 0; ">
                    Հ/Հ: {{$announcement->id}}</p>

                <p style="font-style: italic;font-size: 15px;line-height: 1;margin: 0;">
                    @if($announcement->status == \App\Models\Announcement::PUBLISHED)
                        Ամսաթիվ: {{\Carbon\Carbon::now()->format('d.m.Y')}}

                    @else
                        Դեռ հրապարակված չէ
                    @endif
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 14px;line-height: 1">
                    Պատվիրատու՝ {{$customerName}}
                    <br>
                    Ծածկագիր՝ {{$announcement->announcement_password}}
                    <br>
                    Անվանումը՝ {{$announcement->announcement_name}}
                    կարիքների ձեռքբերման
                    @if($announcement->availability_of_financial_resources == 0)
                        <b style="color: red">պայմանով</b>
                    @endif
                    ընթացակարգի հրավեր:
                </p>
            </td>
        </tr>
    </table>
    {!! isset($announcement->purchase_item) ? $announcement->purchase_item : "---" !!}
    <div>
        <ol>
            <li style="font-size: 14px;line-height: 1; margin-bottom: 10px;">
                <p style="margin: 0 0 10px;">ԺԱՄԱՆԱԿԱՑՈՒՅՑ</p>
                <table class="table-border text-left">
                    <tr>
                        <td>Հայտեր ներկայացնելու վերջնաժամկետ</td>
                        <td>{{isset($announcement->application_deadline) ? date('d.m.Y', strtotime($announcement->application_deadline)).'թ.'. ' ' .'ժամը՝ ' . date('H:i', strtotime($announcement->application_deadline)): '---'}}</td>
                    </tr>
                    <tr>
                        <td>Հայտերի բացման նիստ</td>
                        <td>{{isset($announcement->application_opening_day) ? date('d.m.Y', strtotime($announcement->application_opening_day)).'թ.'. ' ' .'ժամը՝ ' . date('H:i', strtotime($announcement->application_opening_day)): '---'}}</td>
                    </tr>
                </table>
            </li>
            <li style="font-size: 14px;line-height: 1;margin-bottom: 10px;">
                <p style="margin: 0 0 10px;">ՊԱՅՄԱՆԱԳՐԻ ՊԱՅՄԱՆՆԵՐ</p>
                <table class="table-border text-left">
                    <tr>
                        <td><b>Կանխավճար</b></td>
                        <td>
                            @if (isset($announcement->prepayment['dram_or_percent']) && isset($announcement->prepayment['time_of_provision']))
                                Նախատեսվում է {{$announcement->prepayment['dram_or_percent']}}
                                @if($announcement->prepayment['dram_or_percent'] <= 100)
                                    %
                                @else
                                    Դրամ
                                @endif
                                առավելագույն չափը (ՀՀ դրամ) Ժամկետը
                                ` {{date('d.m.Y', strtotime($announcement->prepayment['time_of_provision']))}}
                            @else
                                '---'
                            @endif
                        </td>
                    </tr>
                    @if($announcement->provides != "" && $announcement->provides != 'on')
                        @forelse($announcement->provides ?? [] as $key => $item)
                            <tr>
                                <td><b>
                                        @if($key == "application_provide")
                                            Հայտի ապահովում
                                        @elseif($key == 'contract_provide')
                                            Պայմանագրի ապահովում
                                        @elseif($key == 'qualification_provide')
                                            Որակավորման ապահովում
                                        @elseif($key == 'prepayment_provide')
                                            Կանխավճարի ապահովում
                                        @endif
                                    </b>
                                </td>
                                <td>
                                    {{$item}}
                                </td>
                            </tr>
                        @empty
                        @endforelse
                    @endif
                </table>
            </li>
            {!! isset($announcement->results_of_monitoring) ? $announcement->results_of_monitoring : ''!!}
            {!! isset($announcement->payment_schedule_for_pdf) ? $announcement->payment_schedule_for_pdf : ''!!}
            {!! isset($announcement->doses) ? $announcement->doses : '' !!}
        </ol>
    </div>
    {{--        {!! isset($announcement->other_conditions) ? $announcement->other_conditions : '' !!}--}}
    @if(isset($announcement->other_conditions))
        {!!$announcement->other_conditions  !!}
    @else
            <span style="font-style : italic">Այլ պայմաններ</span>
            <div style="font-style: italic; color: red">
                Պայմանագրով նախատեսված գնման առարկայի մատակարարումն իրականացվում է այդ նպատակով
                ֆինանսական միջոցների
                առկայության և դրա հիման վրա կողմերի միջև համապատասխան համաձայնագրի կնքման միջոցով:
                Պայմանագիրը լուծվում է, եթե այն կնքելու օրվան հաջորդող վեց ամսվա ընթացքում այդ նպատակով
                պայմանագրի կատարման համար ֆինանսական միջոցներ չեն նախատեսվում:
                Հայտնում ենք, որ սույն ընթացակարգի համար Պատվիրատուի կողմից նախատեսված է գնման
                նախահաշվային արժեք, սակայն պատվիրատուի և բոլոր մասնակիցների միջև կարող են
                վարվել միաժամանակյա բանակցություններ, որոնք սակայն կարող են հանգեցնել միայն առաջարկված
                գնի նվազեցմանը
                (ընդ որում` նվազեցումը կարող է կատարվել մինչև ներկայացված ինքնարժեքի գումարը) կամ
                վճարման պայմանների փոփոխությանը:
            </div>
    @endif

    <p>©«ՔԵՅԷՄՋԻ ՔՈՆՍԱԼԹԻՆԳ» ՍՊԸ</p>
</div>
</body>
</html>

