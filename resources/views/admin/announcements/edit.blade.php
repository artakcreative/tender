
@extends('admin.layouts.app')

@section('content')

<div class="card ">
    <div class="card-header ">
        <h4 class="card-title">Փոփոխել հայտարարությունը</h4>
    </div>
    <div class="card-body ">
        <form method="post" enctype="multipart/form-data"  class="form-horizontal  repeater" action="{{route('admin.announcements.update',['id' => $announcement->id]) }}">
            @csrf
            @include('admin.announcements.form',['mode' => 'edit'])
            <div class="row">
                <div class="col-md-3 col-form-label"></div>
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Պահպանել</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection

