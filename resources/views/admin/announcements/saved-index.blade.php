@extends('admin.layouts.app')

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success" role="alert">
            {{session()->get('success') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <a href="{{route('admin.announcements.create')}}" type="button"
               class="btn btn-primary float-right">Ստեղծել</a>
            <h5>Հայտարարություններ</h5>
        </div>
        @include('admin.announcements.search')
        <div class="card-body">
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">

                    <ul id="tabs" class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active saved_announcement" href="{{route('admin.announcements.saved')}}">Պահպանված</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  active_announcement" href="{{route('admin.announcements')}}" role="tab">Ակտիվ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link archive_announcement" href="{{route('admin.announcements.archived')}}">Արխիվ</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="my-tab-content" class="tab-content text-center">
                <div class="tab-pane active" id="active" role="tabpanel" aria-expanded="true">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="text-primary">
                                <tr>
                                    <th>Կոդ</th>
                                    <th>Անվանում</th>
                                    <th>Հայտարարության կարգավիճակ</th>
                                    <th>Սեղմագիր</th>
                                    <th>Կցված մենեջեր</th>
                                    <th class="text-right">Գործողտւթյուններ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($saved_announcements as $announcement)
                                    <tr class="{{$announcement->manager_id == auth()->guard('admin')->user()->id ? 'my_announcement' : ''}}">
                                        <td>{{ $announcement->id }}</td>
                                        <td>{{ $announcement->announcement_name}}</td>
                                        <td>
                                            @if($announcement->status == \App\Models\Announcement::PUBLISHED)
                                                Հրապարակված է
                                            @else
                                                Պահպանված է
                                            @endif

                                        </td>
                                        <td>
                                            @forelse($announcement->getMedia('generate_pdf') ?? [] as $media)
                                                <a href="{{$media->getUrl()}}" target="_blank"
                                                   class="{{$announcement->manager_id == auth()->guard('admin')->user()->id ? 'media_pdf' : ''}}"> {{$media->name}} </a>
                                            @empty
                                            @endforelse
                                            <br>
                                            @forelse($announcement->getMedia() ?? [] as $uploadMedia)
                                                <a href="{{$uploadMedia->getUrl()}}" target="_blank"
                                                   class="{{$announcement->manager_id == auth()->guard('admin')->user()->id ? 'media_pdf' : ''}}"> {{$uploadMedia->name}} </a>
                                                <br>
                                            @empty
                                            @endforelse
                                        </td>

                                        @if(isset($announcement->manager_id))
                                            <td> {{$announcement->manager->full_name}}</td>
                                        @else
                                            <td> Մենեջեր կցված չէ</td>
                                        @endif
                                        <td class="text-right">
                                            @if($announcement->manager_id !== null || $announcement->file_path == null)

                                            @else
                                                <form
                                                    action="{{route('admin.announcements.add-manager', ['id'=>$announcement->id])}}"
                                                    method="post" style="display: inline">
                                                    @csrf
                                                    <button type="submit" name="manager_id"
                                                            value="{{auth()->guard('admin')->user()->id}}"
                                                            class="btn btn-info" data-original-title="" title="">
                                                        Կցել ինձ
                                                    </button>
                                                </form>
                                            @endif
                                            @if($announcement->status == \App\Models\Announcement::SAVED && $announcement->manager_id == auth()->guard('admin')->user()->id || auth()->guard('admin')->user()->hasRole('admin'))
                                                <a type="button" rel="tooltip" class="btn btn-success btn-icon"
                                                   href="{{route('admin.announcements.edit', ['id'=>$announcement->id])}}"
                                                   data-original-title="Խմբագրել">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @elseif($announcement->status == \App\Models\Announcement::PUBLISHED && auth()->guard('admin')->user()->hasRole('admin') || auth()->guard('admin')->user()->hasDirectPermission('Edit approved statement'))
                                                <a type="button" rel="tooltip" class="btn btn-success btn-icon"
                                                   href="{{route('admin.announcements.edit', ['id'=>$announcement->id])}}"
                                                   data-original-title="Խմբագրել">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endif
                                            <a type="button" rel="tooltip" class="btn btn-icon"
                                               href="{{route('admin.announcements.history', ['id'=>$announcement->id])}}"
                                               data-original-title="Հայտարարության պատմություն">
                                                <i class="fa fa-history"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $saved_announcements->links('vendor.pagination.bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
