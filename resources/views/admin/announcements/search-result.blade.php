@extends('admin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            {!!  isset($announcement) ? '<h5>Որոնման արդյունքում գտնվել է հետեվյալ հայտարարությունը</h5>' : ''!!}
            @include('admin.announcements.search')
        </div>
        <div class="card-body">
            @if(isset($announcement))
                <div id="my-tab-content" class="tab-content text-center">
                    <div class="tab-pane active" id="active" role="tabpanel" aria-expanded="true">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="text-primary">
                                    <tr>
                                        <th>Կոդ</th>
                                        <th>Անվանում</th>
                                        <th>Հայտարարության կարգավիճակ</th>
                                        <th>Սեղմագիր</th>
                                        <th>Կցված մենեջեր</th>
                                        <th class="text-right">Գործողտւթյուններ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="{{$announcement->manager_id == auth()->guard('admin')->user()->id ? 'my_announcement' : ''}}">
                                        <td>{{ $announcement->id }}</td>
                                        <td>{{ $announcement->announcement_name}}</td>
                                        <td>
                                            @if($announcement->status == \App\Models\Announcement::PUBLISHED && $announcement->application_deadline > \Carbon\Carbon::now()->format('Y-m-d H:i:s') )
                                                Հրապարակված է
                                            @elseif($announcement->status == \App\Models\Announcement::SAVED && $announcement->application_deadline > \Carbon\Carbon::now()->format('Y-m-d H:i:s'))
                                                Պահպանված է
                                            @else
                                                Արխիվացված է
                                            @endif

                                        </td>
                                        <td>
                                            @forelse($announcement->getMedia('generate_pdf') ?? [] as $media)
                                                <a href="{{$media->getUrl()}}" target="_blank"
                                                   class="{{$announcement->manager_id == auth()->guard('admin')->user()->id ? 'media_pdf' : ''}}"> {{$media->name}} </a>
                                            @empty
                                            @endforelse
                                            <br>
                                            @forelse($announcement->getMedia() ?? [] as $uploadMedia)
                                                <a href="{{$uploadMedia->getUrl()}}" target="_blank"
                                                   class="{{$announcement->manager_id == auth()->guard('admin')->user()->id ? 'media_pdf' : ''}}"> {{$uploadMedia->name}} </a>
                                                <br>
                                            @empty
                                            @endforelse
                                        </td>

                                        @if(isset($announcement->manager_id))
                                            <td> {{$announcement->manager->full_name}}</td>
                                        @else
                                            <td> Մենեջեր կցված չէ</td>
                                        @endif
                                        <td class="text-right">
                                            @if($announcement->manager_id !== null || $announcement->file_path == null)

                                            @else
                                                <form
                                                    action="{{route('admin.announcements.add-manager', ['id'=>$announcement->id])}}"
                                                    method="post" style="display: inline">
                                                    @csrf
                                                    <button type="submit" name="manager_id"
                                                            value="{{auth()->guard('admin')->user()->id}}"
                                                            class="btn btn-info" data-original-title="" title="">
                                                        Կցել ինձ
                                                    </button>
                                                </form>
                                            @endif
                                                @if(($announcement->status == \App\Models\Announcement::SAVED && $announcement->application_deadline > \Carbon\Carbon::now()->format('Y-m-d H:i:s')) && ($announcement->manager_id == auth()->guard('admin')->user()->id  || auth()->guard('admin')->user()->hasRole('admin')))
                                                <a type="button" rel="tooltip" class="btn btn-success btn-icon"
                                                   href="{{route('admin.announcements.edit', ['id'=>$announcement->id])}}"
                                                   data-original-title="Փոփոխություններ">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @elseif(($announcement->status == \App\Models\Announcement::PUBLISHED &&  $announcement->application_deadline > \Carbon\Carbon::now()->format('Y-m-d H:i:s')) && (auth()->guard('admin')->user()->hasRole('admin') || auth()->guard('admin')->user()->hasDirectPermission('Edit approved statement')) )
                                                <a type="button" rel="tooltip" class="btn btn-success btn-icon"
                                                   href="{{route('admin.announcements.edit', ['id'=>$announcement->id])}}"
                                                   data-original-title="Փոփոխություններ">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                            @else
                                            @endif
                                            <a type="button" rel="tooltip" class="btn btn-icon"
                                               href="{{route('admin.announcements.history', ['id'=>$announcement->id])}}"
                                               data-original-title="Հայտարարության պատմություն">
                                                <i class="fa fa-history"></i>
                                            </a>
                                        </td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                Որոնման արդյունքում հայտարարություն չի գնտվել
            @endif
        </div>
    </div>
@endsection
