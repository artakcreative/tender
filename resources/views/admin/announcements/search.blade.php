<form action="{{route('admin.announcements.search')}}" method="get">
{{--    @csrf--}}
{{--    @method('POST')--}}
    <div class="d-flex flex-wrap">
        <div class="col-md-2 form-group" style="margin: 0; padding:1px 0 5px 12px" >
            <input type="number" name="code" class="form-control" autocomplete="off"
                   placeholder="Փնտրել կոդով" >
            <div style="padding-top: 10px">
                @error('code')<div class="alert alert-danger">{{ $errors->first('code') }}</div>@enderror
            </div>
        </div>
        <div class="col-md-2 form-group" style="margin: 0; padding: 3px">
            <button class="btn btn-info filter_announcement-static">Փնտրել</button>
        </div>
    </div>
</form>
