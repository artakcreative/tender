@extends('admin.layouts.app')

@section('content')
    <div class="card ">
        <div class="card-header ">
            <h4 class="card-title">Ստեղծել նոր հայտարարություն</h4>
        </div>
        <div class="card-body ">
            <form method="post" enctype="multipart/form-data"  class="form-horizontal  repeater" action="{{route('admin.announcements.store')}}">
                @csrf
              @include('admin.announcements.form',['mode' => 'create'])
                <div class="row">
                    <div class="col-md-3 col-form-label"></div>
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-primary pahpanel">Պահպանել</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


