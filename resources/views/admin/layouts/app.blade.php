@include('admin.layouts.includes.header')

<body class="">

<div class="wrapper ">
    @include('admin.layouts.includes.sidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('admin.layouts.includes.nav')
        <!-- End Navbar -->
        <div class="content">
            @yield('content')
        </div>


        @include('admin.layouts.includes.footer')
    </div>
</div>

@include('admin.layouts.includes.scripts')

</body>

</html>
