<div class="sidebar" data-color="default" data-active-color="danger">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color=" default | primary | info | success | warning | danger |"
  -->
    <div class="logo">
        <a href="https://www.creative-tim.com/" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('img/logo-small.png') }}">
            </div>
            <!-- <p>CT</p> -->
        </a>
        <a href="{{ route('admin.dashboard') }}" class="simple-text logo-normal">
            <div class="logo-image-big">
                <img src="{{asset('img/logo.png')}}">
            </div>
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ asset('img/faces/kmg_new.png') }}"/>
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              <span>
                {{auth()->guard('admin')->user()->name}}
                  {{auth()->guard('admin')->user()->surname}}
                <b class="caret"></b>
              </span>
                </a>
                <div class="clearfix"></div>
                <div class="collapse
                     {{ set_show(['settings'])}}" id="collapseExample">
                    <ul class="nav">
                        <li class="{{ Route::currentRouteName() == 'admin.settings.personal' ? 'active' : ''}}">
                            <a href="{{route('admin.settings.personal')}}">
                                <i class="nc-icon nc-badge"></i>
                                <span class="sidebar-normal">Իմ պրոֆիլը</span>
                            </a>
                        </li>
                        <li class="{{ Route::currentRouteName() == 'admin.settings.password' ? 'active' : ''}}">
                            <a href="{{route('admin.settings.password')}}">
                                <i class="nc-icon nc-settings-gear-65"></i>
                                <span class="sidebar-normal">Փոխել գաղտնաբառը</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="{{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : ''}}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>Գլխավոր</p>
                </a>
            </li>

            <li class="{{ Route::currentRouteName() == 'admin.users' ||
                        Route::currentRouteName() == 'admin.users.edit' ||
                        Route::currentRouteName() == 'admin.users.edit.plane' ||
                        Route::currentRouteName() == 'admin.users.edit.profile' ||
                        Route::currentRouteName() == 'admin.users.add-manager'? 'active' : ''}}">
                <a href="{{ route('admin.users') }}">
                    <i class="fa fa-users"></i>
                    <p>
                        Օգտատերեր
                    </p>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.managers' || Route::currentRouteName() == 'admin.managers.create' || Route::currentRouteName() == 'admin.managers.edit' ? 'active' : ''}}">
                <a href="{{route('admin.managers')}}">
                    <i class="nc-icon nc-layout-11"></i>
                    <p>
                        Մենեջերներ
                    </p>
                </a>
            </li>
            @if(auth()->guard('admin')->user()->hasRole('admin'))
                <li class="{{ Route::currentRouteName() == 'admin.admins' || Route::currentRouteName() == 'admin.admins.create'? 'active' : ''}}">
                    <a href="{{ route('admin.admins') }}">
                        <i class="nc-icon nc-ruler-pencil"></i>
                        <p>
                            Ադմիններ
                        </p>
                    </a>
                </li>
            @endif
            <li>
                <a data-toggle="collapse" href="#formsStatistics"
                   aria-expanded="{{ set_show(['statistics']) == 'show'  ? 'true' : 'false' }}">
                    <i class="fa fa-bar-chart"></i>
                    <p>Վիճակագրւթյուն <b class="caret"></b></p>
                </a>
                <div class="collapse {{set_show(['statistics'])}} " id="formsStatistics">
                    <ul class="nav">
                        <li class="{{ Route::currentRouteName() == 'admin.statistics' ? 'active' : ''}}">
                            <a href="{{route('admin.statistics')}}">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="sidebar-normal"> Օգտատիրոջ ակտիվություն </span>
                            </a>
                        </li>
                        <li class="{{ Route::currentRouteName() == 'admin.statistics.managers' ? 'active' : ''}}">
                            <a href="{{route('admin.statistics.managers')}}">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="sidebar-normal"> Մենջերի ակտիվություն </span>
                            </a>
                        </li>
                        <li class="{{ Route::currentRouteName() == 'admin.statistics.announcements' ? 'active' : ''}}">
                            <a href="{{route('admin.statistics.announcements')}}">
                                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                <span class="sidebar-normal"> Հայտարարության ակտիվություն </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="{{ Route::currentRouteName() == 'admin.announcements' || Route::currentRouteName() ==  'admin.announcements.create' ? 'active' : ''}}">
                <a href="{{route('admin.announcements.saved')}}">
                    <i class="fa fa-bullhorn"></i>
                    <p>
                        Հայտարարություններ
                    </p>
                </a>
            </li>
            @if(auth()->guard('admin')->user()->hasRole('admin'))
                <li>
                    <a data-toggle="collapse" href="#formsProfile" aria-expanded="false">
                        <i class="nc-icon nc-layout-11"></i>
                        <p>Էջի կարգավորումներ <b class="caret"></b></p>
                    </a>
                    <div class="collapse  {{ set_show(['profile']) }}" id="formsProfile">
                        <ul class="nav">
                            <li class="{{ Route::currentRouteName() == 'admin.profile.description' ? 'active' : ''}}">
                                <a href="{{route('admin.profile.description')}}">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Կայքի նկարագրություն</span>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.profile.header' ? 'active' : ''}}">
                                <a href="{{route('admin.profile.header')}}">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Գլխավոր նկարագրություն</span>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.profile.advantage' ? 'active' : ''}}">
                                <a href="{{route('admin.profile.advantage')}}">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Աշխատանքային առավելություն</span>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.profile.connect' ? 'active' : ''}}">
                                <a href="{{route('admin.profile.connect')}}">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Միանալու նկարագրություն</span>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.profile.about' ? 'active' : ''}}">
                                <a href="{{route('admin.profile.about')}}">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Մեր աշխատակիցները</span>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.profile.material' ? 'active' : ''}}">
                                <a href="{{route('admin.profile.material')}}">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Հետաքրքիր նյութեր</span>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.profile.info' ? 'active' : ''}}">
                                <a href="{{route('admin.profile.info')}}">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="sidebar-normal"> Մեր մասին</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif
            @if(auth()->guard('admin')->user()->hasRole('admin') || auth()->guard('admin')->user()->hasDirectPermission('Adding a new customer and industry'))
                <li>
                    <a data-toggle="collapse" href="#formsExamples"
                       aria-expanded="{{ set_show(['competition_settings','settings']) == 'show'  ? 'true' : 'false' }}">
                        <i class="nc-icon nc-settings-gear-65"></i>
                        <p>
                            Կարգավորումներ <b class="caret"></b>
                        </p>
                    </a>


                    <div class="collapse {{ set_show(['competition_settings', 'settings']) }}" id="formsExamples">
                        <ul class="nav">
                            @if(auth()->guard('admin')->user()->hasRole('admin'))
                                <li class="{{set_active(['tariffs'])}}">
                                    <a href="{{route('admin.tariff')}}">
                                        <i class="fa  fa-wrench"></i>
                                        <span class="sidebar-normal"> Փաթեթներ </span>
                                    </a>
                                </li>
                            @endif
                            <li class="{{ set_active(['index'])}}
                            {{auth()->guard('admin')->user()->hasRole('admin') ? '' :'d-none'}}
                                ">
                                <a href="{{route('admin.competition_type_and_application_form')}}">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                    <div class="competition_type_and_application_form">
                                        <span class="sidebar-normal">
                                            Մրցույթի տիպ և
                                        </span>
                                         <span class="sidebar-normal">
                                                Հայտի ներկայացման եղանակ
                                         </span>
                                     </div>
                                </a>
                            </li>
                            <li class="{{ set_active(['customers']) }}">
                                <a href="{{route('admin.customer')}}">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Պատվիրատուներ</span>
                                </a>
                            </li>
                            <li class="{{ set_active(['sphere'])}}">
                                <a href="{{route('admin.sphere')}}">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                    <span class="sidebar-normal"> Ոլորտներ </span>
                                </a>
                            </li>
                            <li class="{{ set_active(['contact-us'])}}">
                                <a href="{{route('admin.contact-us')}}">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Կապ մեզ հետ</span>
                                </a>
                            </li>
                            <li class="{{ set_active(['contract'])}}">
                                <a href="{{route('admin.settings.add-contract-list')}}">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                    <span class="sidebar-normal">Պայմանագրի օրինակ</span>
                                </a>
                            </li>
                            <li class="{{ set_active(['add_data_processing_policy'])}}">
                                <a href="{{route('admin.settings.data_processing_policy.create')}}">
                                    <i class="fa fa-wrench" aria-hidden="true"></i>
                                    <div class="data_processing_policy">
                                        <span class="sidebar-normal">Տվյալների մշակման </span>
                                        <span class="sidebar-normal">քաղաքականության</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endif
            <li class="{{ Route::currentRouteName() == 'admin.help' ? 'active' : ''}}">
                <a href="{{route('admin.help')}}">
                    <i class="fa fa-info"></i>
                    <p>Օգնություն</p>
                </a>
            </li>
        </ul>
    </div>
</div>

