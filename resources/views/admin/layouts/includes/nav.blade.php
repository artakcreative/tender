<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-icon btn-round">
                    <i class="nc-icon nc-minimal-right text-center visible-on-sidebar-mini"></i>
                    <i class="nc-icon nc-minimal-left text-center visible-on-sidebar-regular"></i>
                </button>
            </div>
            <div class="navbar-toggle">
                <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <a class="navbar-brand">Փոքրացնել</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
                <li class="nav-item btn-rotate dropdown info-notify">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                       data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="nc-icon nc-bell-55 icon-notification"><small
                                class="count-notification">{{ count(json_decode($notifications,true))}}</small></i>

                        <p>
                            <span class="d-lg-none d-md-block">Ծանուցումներ</span>
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right d-none notification-dropdown"
                         aria-labelledby="navbarDropdownMenuLink" id="notification-div">
                        @foreach(json_decode($notifications,true) as $key => $value)
                            <div class="  notification-item">
                                <div class="notification-text">
                                    <small>{{ $value['subject'] }}</small><br>
                                    <small class="border-0">{{ $value['description'] }}</small>
                                    @if($value['linkId'] != null)
                                        <br>
                                        <a style="color: #007bff"
                                           href="{{route('admin.users.edit.profile', [ 'id'=> $value['linkId'] ])}}">
                                            <small>Անցնլ հղումով</small>
                                        </a>
                                    @endif
                                </div>
                                <div class="notification-btn">
                                    <button data-id="{{ $value['uuid']}}" type="button" aria-hidden="true"
                                            class="close notification-close"
                                    > ✕
                                    </button>
                                </div>
                            </div>
                        @endforeach
                        <div class="d-flex align-content-center justify-content-center">
                            <button id="read-all" class="btn btn-success"><small>Ընթեցել բոլոր
                                    հաղորդագրությունները</small></button>
                        </div>
                    </div>
                </li>
                <li class="nav-item btn-rotate dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuSettings" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="nc-icon nc-settings-gear-65"></i>
                        <p>
                            <span class="d-lg-none d-md-block">Իմ պրոֆիլը</span>
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuSettings">

                        <form method="post" action="{{ route('admin.logout') }}">
                            @csrf

                            <button type="submit" class="dropdown-item" href="{{ route('admin.logout') }}">Դուրս գալ
                            </button>
                        </form>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<script>
    window.authAdminId = `{{ auth()->guard('admin')->user()->id}}`;
    window.site = `{{ env('APP_URL') }}`;
    window.eventNotification = 'App\\Events\\NotificationEvent';
    window.broadcastNotificationCreated = 'Illuminate\\Notifications\\Events\\BroadcastNotificationCreated';
</script>

