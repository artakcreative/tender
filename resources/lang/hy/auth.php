<?php

/*
|--------------------------------------------------------------------------
| Authentication Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

return [
    'failed'   => 'Մուտքագրված տվյալները սխալ են։',
    'password' => 'Մուտքագրված ծածկագիրը սխալ է։',
    'throttle' => 'Չափազանց շատ մուտք գործելու փորձեր։ Խնդրում ենք փորձել կրկին :seconds վայրկյան անց։',
    'failed_email_verify'   => 'Մուտքագրված Էլկտրոնային հասցեն վերիիկացում անցած չէ։',
    'forgot_password_text' => 'Եթե մոռացել եք Ձեր գաղտնաբառը, ապա մուտքագրեք Ձեր էլ. փոստի հասցեն և սեղմեք «Վերականգնել»: Դրանից հետո կստանաք նամակ, որում ներառված հղումը սեղմելով կարող եք շարունակել գաղտնաբառի վերականգնումը',
    'forgot_success' => 'Դուք հաջողությամբ փոխել եք Ձեր գաղտնաբառը, խնդրում ենք նորից մուտք գործել նոր գաղտնաբառով։',
    'failed_email_block'   => 'Ձեր անձնական էջը արգելափակված է։',
];
