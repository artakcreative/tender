<?php
return [
    'unavailable_audits' => 'No Article Audits available',

    'updated'            => [
        'metadata' => 'On :audit_created_at, :user_name [:audit_ip_address] updated this record via :audit_url',
        'modified' => [
            'provides'   => 'The Title has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'create_date'   => 'The Title has been modified from <strong>:old</strong> to <strong>:new</strong>',
            'purchase_item' => 'The Content has been modified from <strong>:old</strong> to <strong>:new</strong>',
        ],
    ],
];
