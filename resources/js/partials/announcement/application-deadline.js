$(document).ready(function () {
    $('body').on('change', '.how_to_apply', function () {
        let how_to_apply = $('.how_to_apply option:selected').text().toLowerCase().trim();
        let e_auction = 'Էլեկտրոնային աճուրդ'.toLowerCase();

        if(how_to_apply == e_auction){
            let return_auction_day = ` <label class="col-md-3 col-form-label">Հետադարձ աճուրդի օր</label>
                                        <div class="col-md-9">
                                            <div class="form-group block-datepicker">
                                                <input  class="form-control return_auction_day_input datetimepicker" name="return_auction_day" placeholder="Հետադարձ աճուրդի օր" autocomplete="off">
                                            </div>
                                        </div>`

            $('.return_auction_day').append(return_auction_day);
            jQuery.datetimepicker.setLocale('hy');
            $('.return_auction_day_input').datetimepicker({
                timepicker:true,
                minDate: new Date,
                dateFormat: 'd.m.Y H:mm:ss',
            })

            let dayTime = moment().format('YYYY-MM-DDThh:mm');
            $('.return_auction_day_input').attr('min',dayTime);


        }else {
            $('.return_auction_day').empty();
        }
    })



})
