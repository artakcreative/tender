// հայտարարրոթյուններ բաժին վճարման ժամանակացույց



$('body').on('blur', '.percent', function () {
    if ($(this).val() !== '' && $(this).parents('.payment_schedule_item').find('.schedule_date').val() !== '') {
        if($(this).val() !== ''){
            $(this).removeClass('is-invalid')
        }
        if($(this).parents('.payment_schedule_item').find('.schedule_date').val() !== ''){
            $(this).parents('.payment_schedule_item').find('.schedule_date').removeClass('is-invalid')
        }
        let percentCounter = 0
        $('.payment_schedule_item').find('.percent').each(function (idx, item) {
            percentCounter += +$(item).val()
        });
        if (+$(this).val() == 0) {
            $(this).val(+$(this).val() - (percentCounter - 100))
            return false
        }

        if (percentCounter < 100) {
            let clone = $('.payment_schedule_item:last').clone();
            var newDate = moment(clone.find('.schedule_date').val()).add('30', 'days').format('YYYY-MM-DD');
            // clone.find('.schedule_date').attr('min', newDate)
            clone.find('.schedule_date').val(newDate)
            clone.appendTo('.payment_schedule');
            $('.payment_schedule_item:last').find('.percent').val(100 - percentCounter);



        } else {
            $(this).val(+$(this).val() - (percentCounter - 100))
        }
    } else if($(this).val() == '' && $(this).parents('.payment_schedule_item').find('.schedule_date').val() == ''){
        let percentCounter1 = 0;
        $('.payment_schedule_item').find('.percent').each(function (idx, item) {
            percentCounter1 += +$(item).val()
        });
        $(this).val(+$(this).val() - (percentCounter1 - 100));

    }else{
        if($(this).val() == ''){
            $(this).addClass('is-invalid')
        }
        if($(this).parents('.payment_schedule_item').find('.schedule_date').val() == ''){
            $(this).parents('.payment_schedule_item').find('.schedule_date').addClass('is-invalid')
        }
        return false
    }

    jQuery.datetimepicker.setLocale('hy');
    $('.schedule_date').datetimepicker({
        timepicker:false,
        minDate: new Date,
        format:'Y-m-d',
        scrollMonth : false,
        scrollInput : false,
    })
})
$('body').on('click','.reset_schedule', function(){
     $('.payment_schedule_item').not(':eq(0)').remove();
    $('.schedule_date').val('');
    $('.percent').val('');
} )
// հայտարարրոթյուններ բաժին վճարման ժամանակացույց ավարտ
