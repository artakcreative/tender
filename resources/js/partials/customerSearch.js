$(document).ready(function () {

    $('.selectCustomer').prepend('<option ></option>').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել պատվիրատուին",
        "language": {
            "noResults": function(){
                return "Պատվիրատո չի գտնվել";
            },
            searching: function() {
                return "Փնտրում...";
            },
            errorLoading:function(){
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        },
        ajax: {
            url: '/admin/competition_settings/customers/data',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (term) {
                return {
                    search: term
                };
            },
            processResults: function (data) {

                return {
                    results: $.map(data, function (item) {
                        let info;
                        if(item.name != null || item.abbreviation != null ) {
                            info = item.name;
                        } else {
                            info = item.abbreviation
                        }
                        return {
                            text: info,
                            id: item.id
                        }
                    })
                };
            }
        }

    });


});



