$(document).ready(function () {
    $('#forgot').on('click', function () {
        $('.card-login-admin').css("display", "none");
        $('.card-forgot').css("display", "block");
    });

    $('.close-forgot').on('click', function () {
        $('.card-forgot').css("display", "none");
        $('.card-login-admin').css("display", "block");
    });

    $('#btn-forgots').on('click', function () {
        let email = $('#email-forgot');
        $('.error-valid').empty();
        $.ajax({
            type: 'POST',
            url: '/admin/forgot-password',
            data: {
                email: email.val(),
            },
            success: function (res) {
                if(res.error) {
                    $('#forgot-email').append(`<small class="error-valid">${res.error}</small>`)
                }else{
                    $('.card-forgot').css("display", "none");
                    $('.card-login').css("display", "block");
                }
            },
            error: function (res) {
                let errors = res.responseJSON.errors;
                Object.entries(errors).forEach(([key, value]) => {
                    if (email.attr('name') === key) {
                        $('#forgot-email').append(`<div class="error-valid">${value}</div>`)
                    }
                });
            }
        });
    });

    let sessionToken = $('.card-password').data('token');
    let sessionEmail = $('.card-password').data('email');
    if(sessionToken && sessionEmail) {
        $('.card-login-admin').css("display", "none")
        $('.card-password').css("display", "block");
        $('#btn-reset-admin').on('click', function () {
            let passwordForgot = $('.password-reset');
            let passwordConfForgot = $('.password-conf-reset');
            $('.error-valid').empty();
            $.ajax({
                type: 'POST',
                url: '/admin/reset-password',
                data: {
                    token: sessionToken,
                    email: sessionEmail,
                    password: passwordForgot.val(),
                    password_confirmation: passwordConfForgot.val(),
                },
                success: function (res) {
                    $(".card-password").css("display", "none");
                    $('.text-card-verification').text(res.error ? res.error: res.success);
                    $('.card-verification').css("display", "block");
                },
                error: function (res) {
                    let errors = res.responseJSON.errors;
                    Object.entries(errors).forEach(([key, value]) => {
                        if (passwordForgot.attr('name') === key) {
                            $('#reset-password').append(`<small class="error-valid">${value}</small>`)
                        } else  if (passwordConfForgot.attr('name') === key) {
                            $('#reset-conf-password').append(`<small class="error-valid">${value}</small>`)
                        }
                    });
                }
            });
        });
    }

    $('.close-text').on('click', function () {
        $('.card-verification').css("display", "none");
        $('.card-login-admin').css("display", "block");
    })

});
