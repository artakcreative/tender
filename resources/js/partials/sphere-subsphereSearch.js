$(document).ready(function () {

    function sphereSearch(){
        $('.selectSphere').prepend('<option></option>').select2({
            theme: "classic",
            containerCssClass: "user-select2-container-class",
            placeholder: "Ընտրել ոլորտ",

            "language": {
                "noResults": function () {
                    return "Ոլորտ չի գտնվել";
                },
                searching: function () {
                    return "Փնտրում...";
                },
                errorLoading: function () {
                    return "Արդյունքները չհաջողվեց բեռնել"
                }
            },
            ajax: {
                url: '/admin/competition_settings/spheres/data',
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (term) {
                    return {
                        search: term,

                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            let info;
                            if (item.name != null) {
                                info = item.name;
                            }
                            return {
                                text: info,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
    }

    sphereSearch()

    $('.add-form').on('click', function(){
        sphereSearch()
    })

     // $('.selectSubSphere').prepend('<option></option>').select2({
     //        theme: "classic",
     //        containerCssClass: "user-select2-container-class",
     //        placeholder: "Ընտրել Ենթալորտ",
     //
     //        "language": {
     //            "noResults": function () {
     //                return "Ենթալորտ չի գտնվել";
     //            },
     //            searching: function () {
     //                return "Փնտրում...";
     //            },
     //            errorLoading: function () {
     //                return "Արդյունքները չհաջողվեց բեռնել"
     //            }
     //        },
     //        ajax: {
     //            url: '/admin/announcement/getSubSphere',
     //            dataType: 'json',
     //            type: "GET",
     //            quietMillis: 50,
     //            data: function (term) {
     //                return {
     //                    search: term
     //                };
     //            },
     //            processResults: function (data) {
     //                return {
     //                    results: $.map(data, function (item) {
     //                        let info;
     //                        if (item.name != null) {
     //                            info = item.name;
     //                        }
     //                        return {
     //                            text: info,
     //                            id: item.id
     //                        }
     //                    })
     //                };
     //            }
     //        }
     //
     //    });
    });





