// import {ClassicEditor} from "../../../public/js/admin/assets/editor";

$(document).ready(function () {

    // let toolbar = {
    //     toolbar: [{
    //         name: 'document',
    //         items: ['Print']
    //     },
    //         {
    //             name: 'clipboard',
    //             items: ['Undo', 'Redo']
    //         },
    //         {
    //             name: 'styles',
    //             items: ['Format', 'Font', 'FontSize']
    //         },
    //         {
    //             name: 'colors',
    //             items: ['TextColor', 'BGColor']
    //         },
    //         {
    //             name: 'align',
    //             items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
    //         },
    //         '/',
    //         {
    //             name: 'basicstyles',
    //             items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']
    //         },
    //         {
    //             name: 'links',
    //             items: ['Link', 'Unlink']
    //         },
    //         {
    //             name: 'paragraph',
    //             items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
    //         },
    //         {
    //             name: 'insert',
    //             items: ['Table']
    //         },
    //         {
    //             name: 'tools',
    //             items: ['Maximize']
    //         },
    //         {
    //             name: 'editing',
    //             items: ['Scayt']
    //         }
    //     ]};
    //


    $('.generate_password').on('click', function () {

        let chars = "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            passwordLength = 12,
            password = "";
        for (var i = 0; i <= passwordLength; i++) {
            var randomNumber = Math.floor(Math.random() * chars.length);
            password += chars.substring(randomNumber, randomNumber + 1);
        }

        $('#password').val(password);
    })

    $('.delete_user, .delete-description, .delete-header, .delete-advantage, .delete-about, .block_manager, .unlock_manager, .block_user, .unlock_user').on('click', function () {
        let $this = $(this),
            form = $this.parent('form'),
            type = $this.data('type'),
            text = '',
            confirmButtonText = 'Այո, Ջնջել!'
        switch (type) {
            case 'block_manager':
                text = ' որ ցանկանում եք բլոկավորել մենեջերին';
                confirmButtonText = 'Այո, Բլոկավորել!'
                break;
            case 'unlock_manager':
                text = ' որ ցանկանում եք ապաբլոկավորել մենեջերին';
                confirmButtonText = 'Այո, Ապաբլոկավորել!'
                break;
            case 'block_user':
                text = ' որ ցանկանում եք բլոկավորել օգտատիրոջը';
                confirmButtonText = 'Այո, Բլոկավորել!'
                break;
            case 'unlock_user':
                text = ' որ ցանկանում եք ապաբլոկավորել օգտատիրոջը';
                confirmButtonText = 'Այո, Ապաբլոկավորել!'
                break;
            case 'user':
                text = 'որ ցանկանում եք ջնջել օգտատիրոջը';
                break;
            case 'page-header':
                text = 'ցանկանում ե՞ք ջնջել նկարագրությունը';
                break;
            case 'page-advantage':
                text = 'ցանկանում ե՞ք ջնջել հայտարարությունը';
                break;
            case 'page-about':
                text = 'ցանկանում ե՞ք ջնջել աշխատակցին';
                break;
        }


        Swal.fire({
            title: 'Դուք համոզված ե՞ք,',
            text: text,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirmButtonText,
            cancelButtonText: 'Չեղարկել'
        }).then((result) => {
            if (result.value) {
                form.submit();
            }
        })
    });


    jQuery.datetimepicker.setLocale('hy');
    $('.datepicker').datetimepicker({
        timepicker: false,
        minDate: new Date,
        format: 'd.m.Y',
        scrollMonth: false,
        scrollInput: false
    })


    let now = new Date;
    let activateUserDate = new Date(now.setMonth(2));
    jQuery.datetimepicker.setLocale('hy');
    $('.activate_user_datepicker').datetimepicker({
        timepicker: false,
        minDate: activateUserDate,
        format: 'Y-m-d',
        scrollMonth: false,
        scrollInput: false
    })

    jQuery.datetimepicker.setLocale('hy');
    $('.datetimepicker').datetimepicker({
        timepicker: true,
        minDate: new Date,
        format:'d.m.Y H:i',
    });

    if ($('.tagsinput').data('settings') !== '') {
        $('.tagsinput').siblings('.bootstrap-tagsinput').find('span[data-role=remove]').addClass('d-none')
    }


    $('.sub_sphere').select2({
        theme: "classic",
        containerCssClass: "user-select2-container-class",
        placeholder: "Ընտրել Ենթալորտ",

        "language": {
            "noResults": function () {
                return "Ենթալորտ չի գտնվել";
            },
            searching: function () {
                return "Փնտրում...";
            },
            errorLoading: function () {
                return "Արդյունքները չհաջողվեց բեռնել"
            }
        }
    });

    $('body').on('change', '.sphere', function () {
        let $this = $(this);
        $(this).parents('.category-group-item').find('.sub_sphere').select2(
            {
                theme: "classic",
                containerCssClass: "user-select2-container-class",
                placeholder: "Ընտրել Ենթալորտ",

                "language": {
                    "noResults": function () {
                        return "Ենթալորտ չի գտնվել";
                    },
                    searching: function () {
                        return "Փնտրում...";
                    },
                    errorLoading: function () {
                        return "Արդյունքները չհաջողվեց բեռնել"
                    }
                }
            });
        // $('.sub_sphere').select2();
        let subsphere = $(this).parents('.category-group-item').find('.sub_sphere');
        $(this).parents('.category-group-item').find('.sub_sphere_delete').find('.select2:last').remove();


        let id = $(this).val();
        subsphere.attr('multiple', '')

        $.ajax({
            type: 'POST',
            url: '/admin/announcement/getSubSphere',
            data: {
                id: id,
            },
            success: function (res) {
                let option = ``

                $.each(res, function (idx, value) {
                    option += `
                                <option value="${value.id}" class="sub_sphere_name" >${value.text}</option>
                                `
                })

                if (res.length > 0) {
                    $this.parents('.category-group-item').find('.sub_sphere').attr('required', true);
                } else {
                    $this.parents('.category-group-item').find('.sub_sphere').removeAttr('required');
                }

                subsphere.html(option);
                subsphere.select2({
                    theme: "classic",
                    containerCssClass: "user-select2-container-class",
                    placeholder: "Ընտրել Ենթալորտ",

                    "language": {
                        "noResults": function () {
                            return "Ենթալորտ չի գտնվել";
                        },
                        searching: function () {
                            return "Փնտրում...";
                        },
                        errorLoading: function () {
                            return "Արդյունքները չհաջողվեց բեռնել"
                        }
                    }
                });

            },
            errors: function (res) {
                console.log(res)
            }
        });
    });

    $('body').on('click', '.add-form', function () {
        for (let i = 1; i < $('.sphere_validation').length; i++) {
            $('.sphere_validation')[i].remove();
            $('.subsphere_validation')[i].remove();
        }


    })

    // Kanxavchar ka te che

    let money = `
                    <div class="row">
                      <label class="col-md-3 col-form-label dram_or_percent">Դրամ/Տոկոս</label>
                      <div class="col-md-3 ">
                          <div class="form-group">
                              <input type="text" class="form-control dram_or_percent_input" name="prepayment[dram_or_percent]" placeholder="Դրամ/Տոկոս" required >
                           </div>
                       </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 col-form-label">Տրամադրման ժամկետր</label>
                        <div class="col-md-3 ">
                            <div class="form-group block-datepicker">
                                <input  class="form-control time_of_provision" name="prepayment[time_of_provision]" placeholder="Տրամադրման ժամկետր" required autocomplete="off">
                            </div>
                        </div>
                    </div>
                `


    $('.prepayment').on('click', function () {
        if ($(this).is(":checked")) {
            $('.money').append(money);
            jQuery.datetimepicker.setLocale('hy');
            $('.time_of_provision').datetimepicker({
                timepicker: false,
                minDate: new Date,
                format: 'd.m.Y',
            })

        } else {
            $('.money').empty();
        }
    })

    $('body').on('input', '.dram_or_percent_input', function () {
        if ($('.dram_or_percent_input').val() <= 100) {
            $('.dram_or_percent').html('Տոկոս')
        } else if ($('.dram_or_percent_input').val() >= 100) {
            $('.dram_or_percent').html('Դրամ')
        }
    })


    $('.provides').on('click', function () {
        if ($(this).is(":checked")) {

            let subcomponents = `
                                    <div class="sub_content">
                                         <div class="row">
                                            <label class="col-md-3 col-form-label">Հայտի ապահովում</label>
                                                <div class="form-check">
                                                    <label class="form-check-label check_provides">
                                                         <div class="form-group">
                                                            <input class="form-check-input subcomponent1" type="checkbox">
                                                            <span class="form-check-sign"></span>
                                                         </div>
                                                    </label>
                                                </div>
                                            </div>

                                         <div class="row subcomponent1_field">
                                        </div>

                                        <div class="row">
                                            <label class="col-md-3 col-form-label">Պայմանագրի ապահովում</label>
                                            <div class="form-check">
                                                <label class="form-check-label check_provides">
                                                    <div class="form-group">
                                                        <input class="form-check-input subcomponent2" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row subcomponent2_field">
                                        </div>

                                        <div class="row">
                                            <label class="col-md-3 col-form-label">Որակավորման ապահովում</label>
                                            <div class="form-check">
                                                <label class="form-check-label check_provides">
                                                    <div class="form-group">
                                                        <input class="form-check-input subcomponent3" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row subcomponent3_field">
                                        </div>

                                        <div class="row">
                                            <label class="col-md-3 col-form-label"> Կանխավճարի ապահովում</label>
                                            <div class="form-check">
                                                <label class="form-check-label check_provides">
                                                    <div class="form-group">
                                                        <input class="form-check-input subcomponent4" type="checkbox">
                                                        <span class="form-check-sign"></span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row subcomponent4_field">

                                        </div>
                                   </div> `
            $('.subcomponents').append(subcomponents);
        } else {
            $('.sub_content').remove();
        }
    })


    $('body').on('click', '.subcomponent1', function () {
        let subcomponent_field = `
                                    <label class="col-md-3 col-form-label"></label>
                                    <div class="col-md-3">
                                      <div class="form-group">
                                        <input type="text " name="provides[application_provide]" class="form-control" >
                                      </div>
                                    </div>
                               `
        if ($(this).is(":checked")) {
            $('.subcomponent1_field').append(subcomponent_field);
        } else {
            $('.subcomponent1_field').empty();
        }
    })

    $('body').on('click', '.subcomponent2', function () {
        let subcomponent_field = `
                                   <label class="col-md-3 col-form-label"></label>
                                   <div class="col-md-3">
                                      <div class="form-group">
                                         <input type="text" name="provides[contract_provide]" class="form-control" >
                                      </div>
                                   </div>
                               `
        if ($(this).is(":checked")) {
            $('.subcomponent2_field').append(subcomponent_field);
        } else {
            $('.subcomponent2_field').empty();
        }
    })

    $('body').on('click', '.subcomponent3', function () {
        let subcomponent_field = `
                                   <label class="col-md-3 col-form-label"></label>
                                   <div class="col-md-3">
                                      <div class="form-group">
                                         <input type="text" name="provides[qualification_provide]" class="form-control" >
                                      </div>
                                   </div>
                               `
        if ($(this).is(":checked")) {
            console.log(1)
            $('.subcomponent3_field').append(subcomponent_field);
        } else {
            console.log(2)
            $('.subcomponent3_field').empty();
        }
    })

    $('body').on('click', '.subcomponent4', function () {
        let subcomponent_field = `
                                   <label class="col-md-3 col-form-label"></label>
                                   <div class="col-md-3">
                                      <div class="form-group">
                                      <div class="form-group">
                                         <input type="text " name="provides[prepayment_provide]" class="form-control " >
                                      </div>
                                   </div>
                               `
        if ($(this).is(":checked")) {
            $('.subcomponent4_field').append(subcomponent_field);
        } else {
            $('.subcomponent4_field').empty();
        }
    })

    $('.delete_file').on('click', function () {
        let $this = $(this);
        let id = $this.data('id');
        let url = $this.data('url');
        $.ajax({
            type: 'delete',
            url: `${url}/${+id}`,
            success: function (res) {
                if (res.success === true) {
                    $this.parent('.file-item').remove()
                }
            },
            errors: function (res) {
                console.log(res)
            }
        });
    })

    $("body").on('click', '#minimizeSidebar', function () {
        var x = $('.navbar-brand');
        if (x.text() === "Փոքրացնել") {
            x.text("Մեծացնել");
        } else {
            x.text("Փոքրացնել");
        }
    })

    // $('body').on('click','.pahpanel', function(){
    //     // console.log('wehwsdey723647823eysdjkadh7812')
    //     setTimeout(function(){
    //         window.location.replace("https://google.com/");
    //     },2000);
    //
    // })
    const dt = new DataTransfer();

    function removeFile() {
        $('body').on('click', 'span.file-delete', function () {
            let name = $(this).next('span.name').text();
            $(this).parent().remove();
            for (let i = 0; i < dt.items.length; i++) {
                if (name === dt.items[i].getAsFile().name) {
                    dt.items.remove(i);
                    continue;
                }
            }
            document.getElementById('attachment').files = dt.files;
        });
    }

    removeFile()
    $(".attachment").on('change', function (e) {
        for (var i = 0; i < this.files.length; i++) {
            let fileBloc = $('<span/>', {class: 'file-block'}),
                fileName = $('<span/>', {class: 'name', text: this.files.item(i).name});
            fileBloc.prepend('<span class="file-delete"><i class="fa fa-trash delete_file"></i></span>')
                .prepend(fileName);
            $(this).parents('.file_name_show').find("#files-names").append(fileBloc)
        }
        ;
        //  for (let file of this.files) {
        //      dt.items.add(file);
        //      console.log(dt.items.add(file))
        //  }
        //  this.files = dt.files;
        // removeFile()
    });


    $("button").tooltip();
    $("a").tooltip();

    $('.navbar-minimize').on('click', function () {
        $('body').toggleClass('sidebar-mini');
    })
    $('.navbar-toggle').on('click', function () {
        $('html').toggleClass('nav-open')
    })

    // let user_checked_sphere_last = $('.user_checked_sphere:last').html().replace(/,/g, "");
    // $('.user_checked_sphere:last').html( user_checked_sphere_last);



});





