$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$( document ).ready(function() {
    var count = $('.count-notification').html();
    $('.notification_count').text(count);

    setTimeout(() => {
        window.Echo.private('notification.' + window.authAdminId)
            .listen('.NotificationEvent', (notification) => {
            let subject = notification.subject;
            let description = notification.description;
            let uuid = notification.uuid;
            var route = null;
            if(notification.linkId != null) {
                route =  window.site + '/admin/users/edit/profile/'+ notification.linkId;
            }
            let html =`
    <div class="  notification-item">
                                <div class="notification-text">
                                    <small>${subject}</small><br>
                                    <small class="border-0">${description}</small>`;

                if(route != null){
                    html +=`<br>
                                        <a style="color: #007bff"
                                           href="${route}">
                                            <small>Անցնլ հղումով</small>
                                        </a>`
                }
                html +=`
                                </div>
                                <div class="notification-btn">
                                    <button data-id="${uuid}" type="button" aria-hidden="true"
                                            class="close notification-close"
                                    > ✕
                                    </button>
                                </div>
                            </div> `;
            $("#notification-div").prepend(html);
            count++;
            $("#notification-div").removeClass('d-none');
            $("#notification-div").addClass('d-block');
            $('.count-notification').text(count);
            $('.icon-notification').css({"color": "red"})
        });
    }, 1000);


    $('body').on('click','.notification-close',function () {
        count --;
        $('.count-notification').text(count);
        let id = $(this).data('id');
        $.ajax({
            type: 'GET',
            url: '/admin/notification/read/' + id,
            data: {},
            success: function (res) {
                if(count === 0) {
                    $('.icon-notification').css({"color": "#66615b"});
                    $("#notification-div").removeClass('d-block');
                    $("#notification-div").addClass('d-none');
                }
                location.reload();
            },
            errors: function (res) {
                console.log(res)
            }
        });
    });

    $('#read-all').on('click', function(){
        count = 0;
        $('.count-notification').text(count);
        $('.icon-notification').css({"color": "#66615b"});
        $.ajax({
            type: 'POST',
            url: '/admin/notification/read/all',
            data: {
                id: window.authAdminId
            },
            success: function (res) {
                $("#notification-div").removeClass('d-block');
                $("#notification-div").addClass('d-none');
                location.reload();
            },
            errors: function (res) {
                console.log(res)
            }
        });

    });

    if (count > 0) {
        $('.icon-notification').css({"color": "red"})
        $("#notification-div").removeClass('d-none');
        $("#notification-div").addClass('d-block');
    } else if( count == 0) {
        $("#notification-div").removeClass('d-block');
        $("#notification-div").addClass('d-none');

    }

});
