$( document ).ready(function() {
    setTimeout(() => {
        window.Echo.private('notification-user.' + window.authUserId)
            .listen('.NotificationUserEvent', (notification) => {
                let subject = notification.subject;
                let description = notification.description;
                let id = notification.uuid;
                let userId = notification.userId;
                let time = moment(notification.time).format("YYYY-MM-DD  HH:mm:ss");
                let linkId = notification.linkId;
                $('.notifications').addClass('has-notifications');
                let html = ``
                if(linkId != null) {
                    $(".notification-list").prepend(`<div class="notification-item  notification_bg">
                    <div class="notification-header">
                    <div class="notification-title">${subject}</div>
                </div>
                <div class="notification-body">
                    <div class="description">
                     <p>${description}</p><br>
                    <p> ${time}</p><br>
                    <p><input type="hidden" name="id" class="append-link" value="${linkId}">
                    <button class="btn btn-info btn-round go-other">Մանրամասները</button></p>
                </div>
                </div>
                <div class="close remove-notify" data-id="${id}" data-user_id="${userId}"></div>
                    </div>`);
                }else {
                    $(".notification-list").prepend(`<div class="notification-item notification_bg">
                    <div class="notification-header">
                    <div class="notification-title">${subject}</div>
                </div>
                <div class="notification-body">
                    <div class="description">
                    <p>${description}</p><br>
                    <p> ${time}</p>
                </div>
                </div>
                <div class="close remove-notify" data-id="${id}" data-user_id="${userId}"></div>
                    </div>`);
                }
            });
    }, 1000);


    $("body").on('click', '.go-other', function () {
        let links = $('.append-link').val();
        $('.append-input').val(links);
        $( ".append-input" ).submit();
    });


    $.ajax({
        type: 'GET',
        url: 'notification/not-read',
        cache: false,
        success: function (res){
            if(res.length > 0) {
                $('.notifications').addClass('has-notifications');
                // console.log($('.unread_announcement_id'));
                $.each($('.unread_announcement_id'), function (idx, value){
                    if(res.includes(+$(this).val())) {
                        $(this).parents('.blog-item').addClass('new_announcement_bg');
                    }
                })
            }
        },
        error: function (res) {
            console.log(res)
        }

    })

    $('.notifications').on('click', function () {
        $this.removeClass('has-notifications');
    });
    $('body').on('click', '.remove-notify', function () {
        let $this = $(this),
            id = $this.data('id'),
            user_id = $this.data('user_id');
        $this.parent('.notification-item').remove();
        $('.notifications').removeClass('has-notifications');
        $.ajax({
            type: 'Post',
            url: 'notification/delete',
            cache: false,
            data:{
                id : id,
                user_id : user_id
            },
            success: function (res){
            },
            error: function (res) {
                console.log(res)
            }

        })
    })

});



