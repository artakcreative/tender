require('./bootstrap');
require('./partials/plugins/ajax');
require('./partials/notification');
require('./partials/plugins/chart');
require('./partials/main');
require('./partials/plugins/repeater');
require('./partials/announcement/payment-schedule');
require('./partials/announcement/application-deadline');
require('./partials/forgot-password');
require('./partials/customerSearch');
require('./partials/sphere-subsphereSearch');
require('./partials/announcement');

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
