<?php

namespace App\Observers;

use App\Events\NotificationEvent;
use App\Events\NotificationUserEvent;
use App\Models\Announcement;
use App\Models\CompetitionSphere;
use App\Models\CompetitionSubSphere;
use App\Models\CompetitionType;
use App\Models\Tariff;
use App\Models\UserSphere;
use App\Notifications\GlobalNotification;
use App\Notifications\User\GlobalUserNotification;
use App\Repositories\Admin\Announcement\AnnouncementRepositoryInterface;
use App\Services\Admin\UserServiceInterface;
use Carbon\Carbon;
use http\Client\Curl\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class AnnouncementObserver
{
    /**
     * @var AnnouncementRepositoryInterface
     */
    private $announcementRepository;

    /**
     * @var UserServiceInterface
     */
    private $userService;


    /**
     * @param AnnouncementRepositoryInterface $announcementRepository
     * @param UserServiceInterface $userService
     */
    public function __construct(AnnouncementRepositoryInterface $announcementRepository, UserServiceInterface $userService)
    {
        $this->announcementRepository = $announcementRepository;
        $this->userService = $userService;
    }

    /**
     * Handle the Announcement "created" event.
     *
     * @param Announcement $announcement
     * @return void
     */
    public function created(Announcement $announcement)
    {
        //
    }

    /**
     * Handle the Announcement "updated" event.
     * Выбираем всех пользователей у которых совпадаю сферы и под сферы с сферами и под сферами опублекоданого объявления и посылаем уведомления
     * @param Announcement $announcement
     * @return void
     */
    public function updated(Announcement $announcement)
    {
        $status = false;
        if(isset($announcement->getDirty()['status']) && $announcement->status == Announcement::PUBLISHED ) {
            if($announcement->new_or_old == Announcement::CREATEDNEW) {
                $status = true;
            }

            return  $this->userService->sendNotifyUserAnnouncement($announcement->where('id', $announcement->id)->with('customers')->first(), $announcement->customer_affiliations->customer_affiliation, $status, $announcement->id );
        }
    }

    /**
     * Handle the Announcement "deleted" event.
     *
     * @param Announcement $announcement
     * @return void
     */
    public function deleted(Announcement $announcement)
    {
        //
    }

    /**
     * Handle the Announcement "restored" event.
     *
     * @param Announcement $announcement
     * @return void
     */
    public function restored(Announcement $announcement)
    {
        //
    }

    /**
     * Handle the Announcement "force deleted" event.
     *
     * @param Announcement $announcement
     * @return void
     */
    public function forceDeleted(Announcement $announcement)
    {
        //
    }
}
