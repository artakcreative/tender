<?php


use Illuminate\Contracts\Auth\Authenticatable;

if (!function_exists('set_show')) {
    /**
     * @param array $route
     * @return string
     */
    function set_show (array $route): string
    {
        return in_array(request()->segment(2),$route) ? 'show' : '';
    }
}

if (!function_exists('set_active')) {
    /**
     * @param array $route
     * @return string
     */
    function set_active (array $route): string
    {
        return in_array(request()->segment(3),$route) ? 'active' : '';
    }
}

if (!function_exists('auth_user')) {
    /**
     * @return Authenticatable|null
     */
    function auth_user (): ?Authenticatable
    {
        return auth()->guard('web')->user();
    }
}

