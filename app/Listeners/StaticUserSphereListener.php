<?php

namespace App\Listeners;

use App\Events\StaticUserSphereEvent;
use App\Models\CompetitionSubSphere;
use App\Models\UserSphereStatic;
use App\Services\Admin\CompetitionSubSphere\CompetitionSubSphereServiceInterface;
use App\Services\User\UserSphereStatic\UserSphereStaticServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StaticUserSphereListener
{
    /**
     * @var CompetitionSubSphereServiceInterface
     */
    private $competitionSubSphereService;
    /**
     * @var UserSphereStaticServiceInterface
     */
    private $userSphereStaticService;

    /**
     * Create the event listener.
     *
     * @param CompetitionSubSphereServiceInterface $competitionSubSphereService
     * @param UserSphereStaticServiceInterface $userSphereStaticService
     */
    public function __construct(CompetitionSubSphereServiceInterface $competitionSubSphereService,
                                UserSphereStaticServiceInterface $userSphereStaticService)
    {
        $this->competitionSubSphereService = $competitionSubSphereService;
        $this->userSphereStaticService = $userSphereStaticService;
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\StaticUserSphereEvent  $event
     * @return void
     */
    public function handle(StaticUserSphereEvent $event)
    {
        $info = [];
        foreach ($event->data as $key => $user) {
            if ($user['sphere_type'] == CompetitionSubSphere::class) {
                $info[] = [
                    'user_id' => $user['user_id'],
                    'sphere_id' => $this->competitionSubSphereService->getById($user['sphere_id'])['competition_sphere_id'],
                    'announcement_id' => $event->id,
                ];
            } else {
                $info[] = [
                    'user_id' => $user['user_id'],
                    'sphere_id' => $user['sphere_id'],
                    'announcement_id' => $event->id,
                ];
            }
        }

        $input = array_values(array_map("unserialize", array_unique(array_map("serialize", $info))));

        foreach ($input as $value) {
            $this->userSphereStaticService->create($value);
        }
    }
}
