<?php

namespace App\Listeners;

use App\Services\Admin\LoginHistory\LoginHistoryServiceInterface;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;

class LogSuccessfulLogin
{
    /**
     * @var Request
     */
    protected $request;
    protected $loginHistoryService;


    /**
     * LogSuccessfulLogin constructor.
     * @param Request $request
     * @param LoginHistoryServiceInterface $loginHistoryService
     */
    public function __construct(Request $request, LoginHistoryServiceInterface $loginHistoryService)
    {
        $this->request = $request;
        $this->loginHistoryService = $loginHistoryService;
    }

    /**
     * Handle the event.
     *
     * @param  \Illuminate\Auth\Events\Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $this->loginHistoryService->createHistory($event->user['id'], $event->guard);
    }
}
