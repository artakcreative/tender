<?php

namespace App\Notifications;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GlobalOnlyEmailNotification extends Notification implements ShouldQueue, ShouldBroadcastNow
{
    use Queueable;

    public $subject;
    public $description;
    private $manager;
    /**
     * @var null
     */
    private $status;
    /**
     * @var null
     */
    private $role;


    /**
     * GlobalNotification constructor.
     * @param $subject
     * @param $description
     * @param null $status
     * @param null $role
     */
    public function __construct($subject, $description, $role = null, $status = null)
    {
        $this->subject = $subject;
        $this->description = $description;

        $this->status = $status;
        $this->role = $role;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if ($this->status === Admin::BLOCK && $this->role->hasRole('manager')){
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի ' . $this->role->name . ' ' . $this->role->surname)
                ->line($this->description)
                ->line('Եթե կարծում եք՝ անճշտություն է տեղի ունեցել, խնդրում ենք կապ հաստատել համակարգի ադմինիստրատորների հետ։');
        }else if($this->status === Admin::ACTIVE && $this->role->hasRole('manager')){
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի ' . $this->role->name . ' ' . $this->role->surname)
                ->line($this->description)
                ->line('Կարող եք կրկին մուտք գործել համակարգ՝ օգտագործելով Ձեր մուտքանունն ու գաղտնաբառը։');
        }else if($this->status === User::BLOCK){
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի օգտատեր')
                ->line($this->description)
                ->line('Հարցերի դեպքում գրեք մեզ info@kmg.am էլ. հասցեին կամ զանգահարեք 010-58-19-58 հեռախոսահամարով:');
        }else if($this->status === User::ACTIVE){
            return (new MailMessage)
                ->subject($this->subject)
                ->line( 'Հարգելի օգտատեր')
                ->line($this->description)
                ->line('Կրկին կարող եք մուտք գործել համակարգ՝ օգտագործելով Ձեր էլ. հասցեն ու գաղտնաբառը:');
        }else{
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի ' . $this->role->name . ' ' . $this->role->surname)
                ->line($this->description);
        }

    }

}
