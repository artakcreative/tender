<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GlobalOnlyDatabaseNotification extends Notification implements ShouldQueue, ShouldBroadcastNow
{
    use Queueable;

    public $id;
    public $subject;
    public $description;
    public $uuid;
    public $linkId;


    /**
     * GlobalNotification constructor.
     * @param $linkId
     * @param $id
     * @param $uuid
     * @param $subject
     * @param $description
     */
    public function __construct($id, $subject, $description, $uuid, $linkId)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->description = $description;
        $this->uuid = $uuid;
        $this->linkId = $linkId;

    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'uuid' => $this->uuid,
            'linkId' => $this->linkId
        ];
    }
}
