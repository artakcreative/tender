<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GlobalNotification extends Notification implements ShouldQueue, ShouldBroadcastNow
{
    use Queueable;

    public $id;
    public $subject;
    public $description;
    public $uuid;
    public $linkId;
    /**
     * @var null
     */
    private $email;
    /**
     * @var null
     */
    private $phone;
    /**
     * @var null
     */
    private $manager;


    /**
     * GlobalNotification constructor.
     * @param $id
     * @param $subject
     * @param $description
     * @param $uuid
     * @param null $linkId
     * @param null $email
     * @param null $phone
     * @param null $manager
     */
    public function __construct($id, $subject, $description, $uuid, $linkId = null, $email =null, $phone = null, $manager = null)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->description = $description;
        $this->uuid = $uuid;
        $this->linkId = $linkId;
        $this->email = $email;
        $this->phone = $phone;
        $this->manager = $manager;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if ($this->subject == 'Լիազորության արգելափակում'){
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ':' )
                ->line('Դուք այլևս չունեք` ' . "{$this->description}" . ' հասանելիությունը:');

        }else if ($this->subject == 'Լիազորության ստացում'){
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ':' )
                ->line('Այսուհետ Ձեզ հասանելի է` ' . "{$this->description}" . ' լիազորությունը:');
        }else if($this->subject == 'Օգտատիրոջ ոլորտների փոփոխություն'){
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ':' )
                ->line($this->description);
        }else if( $this->subject ==  "Օգտատիրոջ անձնական տվյալների փոփոխություն"){
            return (new MailMessage)
                ->subject($this->subject)
                ->line('Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ':' )
                ->line($this->description);
        }
        else{
            return (new MailMessage)
                ->subject($this->subject)
                ->line($this->description)
                ->line('Էլ. հասցե: ' .  $this->email . ',' )
                ->line('Հեռախոսահամար՝ ' .$this->phone . ':');
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {

        if ($this->subject == 'Լիազորության արգելափակում'){
            $text = 'Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ': ' . 'Դուք այլևս չունեք` ' . "{$this->description}" . ' հասանելիությունը:';
        }else if ($this->subject == 'Լիազորության ստացում'){
            $text = 'Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ': ' . 'Այսուհետ Ձեզ հասանելի է` ' . "{$this->description}" . ' լիազորությունը:';
        }else if($this->subject == 'Օգտատիրոջ ոլորտների փոփոխություն'){
            $text = 'Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ': ' . $this->description;
        }else if( $this->subject ==  "Օգտատիրոջ անձնական տվյալների փոփոխություն"){
            $text = 'Հարգելի ' . $this->manager->name . ' ' . $this->manager->surname . ': '. $this->description;
        }
        else{
            $text = $this->description . 'Էլ. հասցե: ' .  $this->email . ',' . 'Հեռախոսահամար՝ ' .$this->phone . ':' ;
        }


        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $text,
            'uuid' => $this->uuid,
            'linkId' => $this->linkId,
            'email' => $this->email,
            'phone' => $this->phone,
        ];
    }

}
