<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StoreNotification extends Notification implements  ShouldQueue
{
    use Queueable;

    private $managerData;
    private $name;
    private $surname;
    /**
     * @var null
     */
    private $isAdmin;
    private $host;

    /**
     * Create a new notification instance.
     *
     * @param $managerData
     * @param $name
     * @param $surname
     * @param null $isAdmin
     * @param null $host
     */
    public function __construct($managerData, $name, $surname, $isAdmin = null, $host= null)
    {
        $this->managerData = $managerData;
        $this->name = $name;
        $this->surname = $surname;
        $this->isAdmin = $isAdmin;
        $this->host = $host;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @param $displayableActionUrl
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        if ($this->isAdmin == 1){
            return (new MailMessage)
                ->subject('Մուտք համակարգ')
                ->greeting('Ողջույն, ')
                ->line('Հարգելի '. $this->name . ' ' . $this->surname)
                ->line('Համակարգի ադմինիստրատորի էջի Ձեր գաղտնաբառն է՝  '. ' ' . $this->managerData)
                ->line('Մուտք գործեք համակարգ հետևյալ հղումով ')
                ->action('Մուտք գործել համակարգ',route('admin.login'))
                ->line('կամ պատճենեք ու տեղադրեք հետևյալ հղումը Ձեր բրաուզերում՝ ' .  $this->host . '/admin/login')
                ->line('Գաղտնաբառը կարող եք փոխել համակարգի Ձեր էջի «Անձնական տվյալներ» բաժնից:');
        }else if($this->isAdmin == 2){
            return (new MailMessage)
                ->subject('Մուտք համակարգ')
                ->greeting('Ողջույն, ')
                ->line('Հարգելի '. $this->name . ' ' . $this->surname)
                ->line('Համակարգում որպես մենեջեր Ձեր էջի գաղտնաբառն է՝ '. ' ' . $this->managerData)
                ->line('Մուտք գործեք համակարգ հետևյալ հղումով ')
                ->action('Մուտք գործել համակարգ',route('admin.login'))
                ->line('կամ պատճենեք ու տեղադրեք հետևյալ հղումը Ձեր բրաուզերում՝ ' .  $this->host . '/admin/login')
                ->line('Գաղտնաբառը կարող եք փոխել համակարգի Ձեր էջի «Անձնական տվյալներ» բաժնից:');
        }else{
            return (new MailMessage)
                ->subject('Մուտք համակարգ')
                ->greeting('Ողջույն, ')
                ->line('Հարգելի օգտատեր')
                ->line('Բարի գալուստ tender.kmg.am համակարգ:')
                ->line('Մուտք գործեք համակարգ հետևյալ հղումով ')
                ->action('Մուտք գործել համակարգ',route('admin.login'))
                ->line('կամ պատճենեք ու տեղադրեք հետևյալ հղումը Ձեր բրաուզերում՝ ' .  $this->host . '/admin/login')
                ->line('Գաղտնաբառը կարող եք փոխել համակարգի Ձեր էջի «Անձնական տվյալներ» բաժնից:');
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'manager_id' => $this->managerData['id'],
        ];
    }
}
