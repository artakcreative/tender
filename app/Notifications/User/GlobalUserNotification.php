<?php

namespace App\Notifications\User;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class GlobalUserNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $userId;
    public $subject;
    public $description;
    public $uuid;
    public $linkId;
    private $time;
    private $users;
    public $i = -1;
    /**
     * @var mixed
     */
    private $user_name;
    /**
     * @var mixed
     */
    private $user_surname;
    /**
     * @var null
     */
    private $user;
    /**
     * @var null
     */
    private $data;


    /**
     * GlobalUserNotification constructor.
     * @param $userId
     * @param $subject
     * @param $description
     * @param $linkId
     * @param $time
     * @param $uuid
     * @param $user
     * @param null $data
     */
    public function __construct($userId, $subject, $description, $linkId, $time, $uuid, $user, $data = null)
    {
        $this->userId = $userId;
        $this->subject = $subject;
        $this->description = $description;
        $this->linkId = $linkId;
        $this->time = $time;
        $this->uuid = $uuid;
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {

        if ($this->subject == 'Փոփոխություն մրցույթի հայտարարության մեջ'){
            return (new MailMessage)
                ->subject($this->subject)
                ->line( $this->user->name !== null && $this->user->surname !== null ? 'Հարգելի ' . $this->user->name . ' ' . $this->user->surname : 'Հարգելի ' . $this->user->email )
                ->line($this->description)
                ->action('Մանրամասն',route('user.tenders.details', ['id' => $this->linkId]));
        }else if($this->subject == 'Նոր տենդեր'){
            return (new MailMessage)
                ->subject($this->subject)
                ->line( $this->user->name !== null && $this->user->surname !== null ? 'Հարգելի ' . $this->user->name . ' ' . $this->user->surname : 'Հարգելի ' . $this->user->email )
                ->line($this->description)
                ->line('Հայտարարող ' . $this->data->customers->name)
                ->line('Հայտերի ընդունման վերջնաժամկետը՝ ' . date('m.d.Y', strtotime($this->data['application_deadline'])))
                ->action('Մանրամասն',route('user.tenders.details', ['id' => $this->linkId]));
        }else if ($this->subject == 'Փաթեթի ակտիվացում'){
            return (new MailMessage)
                ->subject($this->subject)
                ->line( $this->user->name !== null && $this->user->surname !== null ? 'Հարգելի ' . $this->user->name . ' ' . $this->user->surname : 'Հարգելի ' . $this->user->email )
                ->line($this->description)
                ->line('Շնորհակալություն');
        }else if($this->subject == 'Փաթեթի գործողության ժամկետը սպառվում է'){
            return (new MailMessage)
                ->subject($this->subject)
                ->line( $this->user->name !== null && $this->user->surname !== null ? 'Հարգելի ' . $this->user->name . ' ' . $this->user->surname : 'Հարգելի ' . $this->user->email )
                ->line($this->description)
                ->line('Երկարաձգեք Ձեր փաթեթի գործողության ժամկետը կամ ակտիվացրեք այլ փաթեթ համակարգում ակտիվ տենդերները տեսնելու համար:')
                ->line('Մեր ծառայությունների փաթեթների մասին կարող եք տեղեկանալ անցնելով հետևյալ հղումով՝ ' . request()->getHttpHost() . '/settings')
                ->line('Ավելացնենք նաև, որ փաթեթի երկարաձգման դեպքում այն ուժի մեջ է մտնում արդեն իսկ գործող փաթեթի ժամկետի ավարտից հետո:')
                ->line('Հարցերի դեպքում գրեք մեզ info@kmg.am էլ. հասցեին կամ զանգահարեք 010-58-19-58 հեռախոսահամարով:');
        }

    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable): array
    {
        if ($this->subject == 'Փոփոխություն մրցույթի հայտարարության մեջ') {
            $text = $this->user->name !== null && $this->user->surname !== null ? 'Հարգելի ' . $this->user->name . ' '
                . $this->user->surname : 'Հարգելի ' . $this->user->email;
            $text = $text . ' ' . $this->description;
        } else if ($this->subject == 'Նոր տենդեր') {
            $text = $this->user->name !== null && $this->user->surname !== null ? 'Հարգելի ' . $this->user->name . ' '
                . $this->user->surname : 'Հարգելի ' . $this->user->email;
            $text = $text . ' ' . $this->description .
                ' Հայտարարող ' . $this->data->customers->name . ': ' . 'Հայտերի ընդունման վերջնաժամկետը՝ '
                . date('m.d.Y', strtotime($this->data['application_deadline'])) . ':';
        } else if ($this->subject == 'Փաթեթի ակտիվացում') {
            $text = $this->user->name !== null && $this->user->surname !== null ? 'Հարգելի ' . $this->user->name
                . ' ' . $this->user->surname : 'Հարգելի ' . $this->user->email;
            $text = $text . ' ' . $this->description . ' Շնորհակալություն:';
        } else if ($this->subject == 'Փաթեթի գործողության ժամկետը սպառվում է') {
            $text = $this->description . 'Երկարաձգեք Ձեր փաթեթի գործողության ժամկետը կամ ակտիվացրեք այլ փաթեթ համակարգում ակտիվ տենդերները տեսնելու համար:'
                . ' Մեր ծառայությունների փաթեթների մասին կարող եք տեղեկանալ անցնելով հետևյալ հղումով՝ ' . request()->getHttpHost() . '/settings'
                . ' Ավելացնենք նաև, որ փաթեթի երկարաձգման դեպքում այն ուժի մեջ է մտնում արդեն իսկ գործող փաթեթի ժամկետի ավարտից հետո:'
                . ' Հարցերի դեպքում գրեք մեզ info@kmg.am էլ. հասցեին կամ զանգահարեք 010-58-19-58 հեռախոսահամարով:';
        }


        return [
            'id' => $this->id,
            'user_id' => $this->userId,
            'subject' => $this->subject,
            'description' => $text,
            'linkId' => $this->linkId,
            'time' => $this->time,
            'uuid' => $this->uuid,
        ];
    }
}
