<?php

namespace App\Console;

use App\Console\Commands\ParseFiles;
use App\Console\Commands\UserSpheresCheckLimitCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ParseFiles::class,
        UserSpheresCheckLimitCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//         $schedule->command('file:parse')->dailyAt('00:00')->appendOutputTo('shcheduler.log');
         $schedule->command('check:limit')->dailyAt('00:00')->appendOutputTo(storage_path('logs/check-limit.log'));
         $schedule->command('tariff:deadline')->dailyAt('00:00')->appendOutputTo(storage_path('logs/tariff-deadline.log'));
         $schedule->command('archive:announcements')->everyMinute()->appendOutputTo(storage_path('logs/archive-announcements.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
