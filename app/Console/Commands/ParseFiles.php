<?php

namespace App\Console\Commands;

use App\Models\Announcement;
use Illuminate\Console\Command;
use PHPHtmlParser\Dom;

class ParseFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * @var \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private $announcement;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \PHPHtmlParser\Exceptions\ChildNotFoundException
     * @throws \PHPHtmlParser\Exceptions\CircularException
     * @throws \PHPHtmlParser\Exceptions\ContentLengthException
     * @throws \PHPHtmlParser\Exceptions\LogicalException
     * @throws \PHPHtmlParser\Exceptions\NotLoadedException
     * @throws \PHPHtmlParser\Exceptions\StrictException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function handle()
    {
        $this->info("Start parse...\n");

        $progress = $this->output->createProgressBar(100);

        $progress->setFormat("%message%\n %current%/%max% [%bar%] %percent:3s%%");

//        $progress->setMessage("100? I won't count all that!");

        $dom = new Dom;
        $url = 'https://www.gnumner.am/hy/page/gnumneri_haytararutyunner_/';
        $dom->loadFromUrl($url);
        $last_page_number = $dom->getElementsByTag('.bootom__line a');

        preg_match('~<a .*?href=[\'"]+(.*?)[\'"]+.*?>(.*?)</a>~ims', $last_page_number[2]->outerHtml, $result);

        $page_count = (int)$result[2];

        $this->parser($url,$page_count,$progress,$dom);

        $progress->setProgress(10);


        $progress->finish();
        $this->info("Finish\n");
    }


    private function parser($url,$page_count,$progress,$dom){
        $announcements = Announcement::query()->select('file_path')->get();
//           skzzum cikl@ petqa nvazman kargov anel heto poxel achman kargov
        for ($i=1; $i <=3 ; $i++){

            $dom->loadFromUrl($url.$i);
//                hxumner
            $tags = $dom->getElementsByTag('.tender a');
//                erba stexcvel haytararutyun@
            $tender_create_date = $dom->find('.tender .tender_time');

            foreach ($tags as $key => $item){
                preg_match('~<a .*?href=[\'"]+(.*?)[\'"]+.*?>(.*?)</a>~ims', $item->outerHtml, $result);
                $number = preg_replace("/[^0-9]/", '', $tender_create_date[$key]->outerHtml);
                $date = substr($number, 0, 4) .'-'. substr($number, 4,2) .'-'. substr($number,6,2) .' '. substr($number,8,2) .':'. substr($number,10,2) .':'. substr($number,12,2);

                if ($announcements->where('file_path',$result[1])->first() !== null) return false;

                $announcement = Announcement::query()->create([
                    'file_path' => $result[1],
                    'create_date' => $date
                ]);
                $announcement->addMediaFromUrl(str_replace(' ','%20',$result[1]))->toMediaCollection();
                $progress->advance();
            };
        }

    }
}
