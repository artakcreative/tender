<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UserArchivedAnnouncements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'archive:announcements';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When users announcements to archived';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Start ...\n");
        $progress = $this->output->createProgressBar(100);

        $progress->setProgress(10);
        $progress->finish();
        $this->info("Finish\n");
    }
}
