<?php

namespace App\Console\Commands;

use App\Models\UserSphere;
use App\Models\UserWantTariffAndActiveSelect;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UserSpheresCheckLimitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:limit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check User Spheres Active Limit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Start ...\n");
        $progress = $this->output->createProgressBar(100);
        /*when left users pay 15 days opened select spheres */
        $usersPayed = UserSphere::where('payed', UserSphere::PAYED)
            ->whereDate('active', '=',  Carbon::now()->addDays(15)->toDateString())->pluck('user_id')->toArray();

        if(count($usersPayed)) {
            UserWantTariffAndActiveSelect::whereIn('user_id', $usersPayed )->update(['actived' => UserWantTariffAndActiveSelect::OPEN]);
        }

      /* when finished users pay*/
        $spheres =  UserSphere::where('payed', UserSphere::PAYED)
                ->whereDate('active', '=',  Carbon::today()->toDateString());
        $users = $spheres->pluck('user_id')->toArray();
        if(count($users) > 0) {
            UserWantTariffAndActiveSelect::whereIn('user_id', $users)->update(['actived' => UserWantTariffAndActiveSelect::CLOSE]);
        }
        $spheres->update(['payed' => UserSphere::NOTPAYED, 'active' => null ]);

        $progress->setProgress(10);
        $progress->finish();
        $this->info("Finish\n");

    }
}
