<?php

namespace App\Console\Commands;

use App\Events\NotificationUserEvent;
use App\Models\UserSphere;
use App\Models\UserWantTariffAndActiveSelect;
use App\Notifications\User\GlobalUserNotification;
use App\Services\Admin\UserServiceInterface;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class UserTariffDeadlineCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tariff:deadline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'When the user has a week or two days left of the active tariff';
    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * Create a new command instance.
     *
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        parent::__construct();
        $this->userService = $userService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Start ...\n");
        $progress = $this->output->createProgressBar(100);

        /*when left users pay 7 days opened select spheres */
        $usersPayedSevenDays = UserSphere::where('payed', UserSphere::PAYED)
            ->whereDate('active', '=',  Carbon::now()->addDay(7)->toDateString())->pluck('user_id')->toArray();
        if(count($usersPayedSevenDays)) {
            $usersPayedSevenDays = array_unique($usersPayedSevenDays);
            foreach ($usersPayedSevenDays as $userId){
                $uuid = Str::uuid();
                $description = 'Տեղեկացնում ենք Ձեզ, որ մեկ շաբաթից՝ ' . Carbon::now()->addDay(7)->format('d.m.Y') . '-ին Ձեր փաթեթի գործողության ժամկետը սպառվում է: ' ;
                $text = $description . 'Երկարաձգեք Ձեր փաթեթի գործողության ժամկետը կամ ակտիվացրեք այլ փաթեթ համակարգում ակտիվ տենդերները տեսնելու համար:'
                    . ' Մեր ծառայությունների փաթեթների մասին կարող եք տեղեկանալ անցնելով հետևյալ հղումով՝ ' . request()->getHttpHost() . '/settings'
                    . ' Ավելացնենք նաև, որ փաթեթի երկարաձգման դեպքում այն ուժի մեջ է մտնում արդեն իսկ գործող փաթեթի ժամկետի ավարտից հետո:'
                    . ' Հարցերի դեպքում գրեք մեզ info@kmg.am էլ. հասցեին կամ զանգահարեք 010-58-19-58 հեռախոսահամարով:';
                $subject = 'Փաթեթի գործողության ժամկետը սպառվում է';
                $user = $this->userService->getUserById((int)$userId);
                broadcast(new NotificationUserEvent((int)$userId, $subject, $text, null, Carbon::now(), $uuid))->toOthers();
                Notification::send($user, new GlobalUserNotification((int)$userId, $subject, $description, null, Carbon::now(), $uuid, $user));
            }
        }

        /*when left users pay 2 days opened select spheres */
        $usersPayedTwoDays = UserSphere::where('payed', UserSphere::PAYED)
            ->whereDate('active', '=',  Carbon::now()->addDay(2)->toDateString())->pluck('user_id')->toArray();
        if(count($usersPayedTwoDays)) {
            $usersPayedTwoDays = array_unique($usersPayedTwoDays);
            foreach ($usersPayedTwoDays as $userId){
                $uuid = Str::uuid();
                $description = 'Տեղեկացնում ենք Ձեզ, 2 օրից՝ ' . Carbon::now()->addDay(2)->format('d.m.Y') . '-ին Ձեր փաթեթի գործողության ժամկետը սպառվում է: ' ;
                $text = $description . 'Երկարաձգեք Ձեր փաթեթի գործողության ժամկետը կամ ակտիվացրեք այլ փաթեթ համակարգում ակտիվ տենդերները տեսնելու համար:'
                    . ' Մեր ծառայությունների փաթեթների մասին կարող եք տեղեկանալ անցնելով հետևյալ հղումով՝ ' . request()->getHttpHost() . '/settings'
                    . ' Ավելացնենք նաև, որ փաթեթի երկարաձգման դեպքում այն ուժի մեջ է մտնում արդեն իսկ գործող փաթեթի ժամկետի ավարտից հետո:'
                    . ' Հարցերի դեպքում գրեք մեզ info@kmg.am էլ. հասցեին կամ զանգահարեք 010-58-19-58 հեռախոսահամարով:';
                $subject = 'Փաթեթի գործողության ժամկետը սպառվում է';
                $user = $this->userService->getUserById((int)$userId);
                broadcast(new NotificationUserEvent((int)$userId, $subject,  $text, null, Carbon::now(), $uuid))->toOthers();
                Notification::send($user, new GlobalUserNotification((int)$userId, $subject, $description, null, Carbon::now(), $uuid, $user));
            }
        }

        /* when finished users pay*/
        $progress->setProgress(10);
        $progress->finish();
        $this->info("Finish\n");
    }
}
