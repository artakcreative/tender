<?php

namespace App\Services\Admin\CompetitionCustomerAffiliation;

interface CompetitionCustomerAffiliationServiceInterface
{
//    /**
//     * @param $data
//     * @return mixed
//     */
//    public function saveCustomerAffiliation($data);

    /**
     * @return mixed
     */
    public function getAll();
}
