<?php

namespace App\Services\Admin\CompetitionCustomerAffiliation;

use App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepositoryInterface;
use App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationServiceInterface;

class CompetitionCustomerAffiliationService implements CompetitionCustomerAffiliationServiceInterface
{
    /**
     * @var CompetitionCustomerAffiliationRepositoryInterface
     */
    private $competitionCustomerAffiliationRepository;

    /**
     * @param CompetitionCustomerAffiliationRepositoryInterface $competitionCustomerAffiliationRepository
     */
    public function __construct(CompetitionCustomerAffiliationRepositoryInterface $competitionCustomerAffiliationRepository)
    {
        $this->competitionCustomerAffiliationRepository = $competitionCustomerAffiliationRepository;
    }

//    /**
//     * @param $data
//     * @return mixed
//     */
//    public function saveCustomerAffiliation($data)
//    {
//        return $this->competitionCustomerAffiliationRepository->saveCustomerAffiliation($data);
//    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->competitionCustomerAffiliationRepository->all();
    }
}
