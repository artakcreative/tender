<?php

namespace App\Services\Admin\PageAbout;

use App\Repositories\Admin\PageAbout\PageAboutRepositoryInterface;
use App\Services\Admin\PageAbout\PageAboutServiceInterface;

class PageAboutService implements PageAboutServiceInterface
{
    /**
     * @var PageAboutRepositoryInterface
     */
    protected $pageAboutRepository;

    /**
     * PageAboutService constructor.
     * @param PageAboutRepositoryInterface $pageAboutRepository
     */
    public function __construct(PageAboutRepositoryInterface $pageAboutRepository)
    {
        $this->pageAboutRepository = $pageAboutRepository;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function aboutCreate($request)
    {
        return $this->pageAboutRepository->aboutCreate($request);
    }

    /**
     * @return mixed
     */
    public function getAbouts()
    {
        return $this->pageAboutRepository->getAbouts();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function aboutDelete($id)
    {
        return $this->pageAboutRepository->aboutDelete($id);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function aboutUpdate($request)
    {
        return $this->pageAboutRepository->aboutUpdate($request);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getAbout($id)
    {
        return $this->pageAboutRepository->getAbout($id);
    }
}
