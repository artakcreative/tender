<?php

namespace App\Services\Admin\PageAbout;

interface PageAboutServiceInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function aboutCreate($request);

    /**
     * @return mixed
     */
    public function getAbouts();

    /**
     * @param $id
     * @return mixed
     */
    public function aboutDelete($id);

    /**
     * @param $request
     * @return mixed
     */
    public function aboutUpdate($request);


    /**
     * @param $id
     * @return mixed
     */
    public function getAbout($id);
}
