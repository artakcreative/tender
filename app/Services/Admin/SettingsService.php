<?php

namespace App\Services\Admin;

use App\Repositories\Admin\SettingsRepository;
use App\Services\Admin\SettingsServiceInterface;

class SettingsService implements SettingsServiceInterface
{
    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    /**
     * @param SettingsRepository $settingsRepository
     */
    public function __construct(SettingsRepository $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    public function updatePersonalInfo($id, $data)
    {
        return $this->settingsRepository->updateById($id, $data);
    }

    public function changePassword($data)
    {
        return $this->settingsRepository->changePassword($data);
    }

    public function addContract($data)
    {
        return $this->settingsRepository->addContract($data);
    }
}
