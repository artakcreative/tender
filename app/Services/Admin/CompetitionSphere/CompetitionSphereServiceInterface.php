<?php

namespace App\Services\Admin\CompetitionSphere;

interface CompetitionSphereServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function saveSphere($data);

    /**
     * @return mixed
     */
    public function getAllSphere();


    /**
     * @return mixed
     */
    public function getAllSphereForSelect();

//    /**
//     * @return mixed
//     */
//    public function sendNotificationToUser();

    /**
     * @return mixed
     */
    public function getAllSphereCategories();

    /**
     * @param $id
     * @return mixed
     */
    public function getSubSphereCategories($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getUserSpheres($id);

    /**
     * @param $data
     * @return mixed
     */
    public function getSphereData($data);

    /**
     *
     * @param $ids
     * @return mixed
     */
    public function getSavedAnnouncementSubSphere($ids);


}
