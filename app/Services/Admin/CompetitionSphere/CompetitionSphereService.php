<?php

namespace App\Services\Admin\CompetitionSphere;

use App\Notifications\GlobalOnlyEmailNotification;
use App\Repositories\Admin\CompetitionSphere\CompetitionSphereRepositoryInterface;
use App\Repositories\Admin\UserRepositoryInterface;
use App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface;
use Illuminate\Support\Facades\Notification;

class CompetitionSphereService implements CompetitionSphereServiceInterface
{
    /**
     * @var CompetitionSphereRepositoryInterface
     */
    private $competitionSphereRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;


    /**
     * CompetitionSphereService constructor.
     * @param CompetitionSphereRepositoryInterface $competitionSphereRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(CompetitionSphereRepositoryInterface $competitionSphereRepository,
                                UserRepositoryInterface $userRepository)
    {
        $this->competitionSphereRepository = $competitionSphereRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function saveSphere($data)
    {
        return $this->competitionSphereRepository->saveSphere($data);
    }

    /**
     * @return array|mixed
     */
    public function getAllSphere()
    {
        return $this->competitionSphereRepository->getAllSphere();

    }

    /**
     * @return mixed
     */
    public function getAllSphereForSelect()
    {
        return $this->competitionSphereRepository->getAllSphereForSelect();
    }


    /**
     * @return array|mixed
     */
    public function getAllSphereCategories()
    {
        $spheres = $this->competitionSphereRepository->all();
        $data = [];
        if(count( $spheres) > 0) {
            foreach ( $spheres as $key => $value) {
                $data[] = [
                    'id' => $value['id'],
                    'text' => $value['name']
                ] ;
            }
        }
         return $data;
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function getSubSphereCategories($id)
    {
        $subSpheres = $this->competitionSphereRepository->getSubSphereCategories($id);
        $data = [];
        if(count( $subSpheres) > 0) {
            foreach ( $subSpheres as $key => $value) {
                $data[] = [
                    'id' => $value['id'],
                    'text' => $value['name']
                ] ;
            }
        }
        return $data;

    }


//    /**
//     * @return mixed|void
//     */
//    public function sendNotificationToUser(){
//        $usersEmails = $this->userRepository->getAll();
//        foreach($usersEmails as $userEmail){
//            Notification::send($userEmail, new GlobalOnlyEmailNotification(__('message.add_sphere'), __('message.add_sphere_notification')));
//        }
//    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserSpheres($id)
    {
      return $this->competitionSphereRepository->getUserSpheres($id);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getSphereData($data)
    {
        $search = null;
        if($data && isset($data['term'])) {
            $search = $data['term'];
        }
        return $this->competitionSphereRepository->getSphereData($search);
    }


    /**
     * @param $ids
     * @return mixed
     */
    public function getSavedAnnouncementSubSphere($ids)
    {
        return $this->competitionSphereRepository->getSavedAnnouncementSubSphere($ids);
    }
}
