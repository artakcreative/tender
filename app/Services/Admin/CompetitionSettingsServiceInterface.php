<?php

namespace App\Services\Admin;

interface CompetitionSettingsServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function create_or_update_competition($data);

    /**
     * @return mixed
     */
    public function getAllSettings();
}
