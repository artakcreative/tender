<?php

namespace App\Services\Admin\PageMaterial;

use App\Repositories\Admin\PageMaterial\PageMaterialRepositoryInterface;
use App\Services\Admin\PageMaterial\PageMaterialServiceInterface;

class PageMaterialService implements PageMaterialServiceInterface
{
    /**
     * @var PageMaterialRepositoryInterface
     */
    protected $pageMaterialRepository;

    /**
     * PageMaterialService constructor.
     * @param PageMaterialRepositoryInterface $pageMaterialRepository
     */
    public function __construct(PageMaterialRepositoryInterface $pageMaterialRepository)
    {
        $this->pageMaterialRepository = $pageMaterialRepository;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function materialCreate($request)
    {
        return $this->pageMaterialRepository->materialCreate($request);
    }

    /**
     * @return mixed
     */
    public function getMaterials()
    {
        return $this->pageMaterialRepository->getMaterials();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function materialDelete($id)
    {
        return $this->pageMaterialRepository->materialDelete($id);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function materialUpdate($request)
    {
        return $this->pageMaterialRepository->materialUpdate($request);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getMaterial($id)
    {
        return $this->pageMaterialRepository->getMaterial($id);
    }
}
