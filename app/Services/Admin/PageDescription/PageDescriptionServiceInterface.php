<?php

namespace App\Services\Admin\PageDescription;

interface PageDescriptionServiceInterface
{
    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param $data
     * @return mixed
     */
    public function descriptionCreate($data);

    /**
     * @param $id
     * @return mixed
     */
    public function descriptionDelete($id);
}
