<?php

namespace App\Services\Admin\PageDescription;

use App\Repositories\Admin\PageDescription\PageDescriptionRepositoryInterface;
use App\Services\Admin\PageDescription\PageDescriptionServiceInterface;

class PageDescriptionService implements PageDescriptionServiceInterface
{
    /**
     * @var PageDescriptionRepositoryInterface
     */
    private $pageDescriptionRepository;

    /**
     * PageDescriptionService constructor.
     * @param PageDescriptionRepositoryInterface $pageDescriptionRepository
     */
    public function __construct(PageDescriptionRepositoryInterface $pageDescriptionRepository)
    {

        $this->pageDescriptionRepository = $pageDescriptionRepository;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return  $this->pageDescriptionRepository->getDescription();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function descriptionCreate($data)
    {
        return $this->pageDescriptionRepository->descriptionCreate($data);
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function descriptionDelete($id)
    {
        return $this->pageDescriptionRepository->descriptionDelete($id);
    }
}
