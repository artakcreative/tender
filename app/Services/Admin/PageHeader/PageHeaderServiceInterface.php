<?php

namespace App\Services\Admin\PageHeader;

interface PageHeaderServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function headerCreate($data);

    /**
     * @return mixed
     */
    public function getHeader();

    /**
     * @param $id
     * @return mixed
     */
    public function headerDelete($id);
}
