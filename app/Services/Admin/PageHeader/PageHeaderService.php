<?php

namespace App\Services\Admin\PageHeader;

use App\Repositories\Admin\PageHeader\PageHeaderRepositoryInterface;

class PageHeaderService implements PageHeaderServiceInterface
{
    /**
     * @var PageHeaderRepositoryInterface
     */
    protected $pageHeaderRepository;

    /**
     * PageHeaderService constructor.
     * @param PageHeaderRepositoryInterface $pageHeaderRepository
     */
    public function __construct(PageHeaderRepositoryInterface $pageHeaderRepository)
    {
        $this->pageHeaderRepository = $pageHeaderRepository;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function headerCreate($data)
    {
       return $this->pageHeaderRepository->headerCreate($data);
    }

    /**
     * @return PageHeaderRepositoryInterface|mixed
     */
    public function getHeader()
    {
        return $this->pageHeaderRepository->getHeader();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function headerDelete($id)
    {
        return $this->pageHeaderRepository->headerDelete($id);
    }
}
