<?php

namespace App\Services\Admin\CompetitionType;

use App\Repositories\Admin\CompetitionType\CompetitionTypeRepositoryInterface;
use App\Services\Admin\CompetitionType\CompetitionTypeServiceInterface;

class CompetitionTypeService implements CompetitionTypeServiceInterface
{
    /**
     * @var CompetitionTypeRepositoryInterface
     */
    private $competitionTypeRepository;

    /**
     * @param CompetitionTypeRepositoryInterface $competitionTypeRepository
     */
    public function __construct(CompetitionTypeRepositoryInterface $competitionTypeRepository)
    {

        $this->competitionTypeRepository = $competitionTypeRepository;
    }

    /**
     * Сохраненные типа соревнования
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        return $this->competitionTypeRepository->save($data);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->competitionTypeRepository->getAll();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->competitionTypeRepository->getById($id);
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data, $id)
    {
        return $this->competitionTypeRepository->update($data, $id);
    }
}
