<?php

namespace App\Services\Admin\CompetitionType;

interface CompetitionTypeServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function save($data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data, $id);

}
