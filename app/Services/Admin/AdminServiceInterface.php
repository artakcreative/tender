<?php

namespace App\Services\Admin;

interface AdminServiceInterface
{
    /**
     * @return mixed
     */

    public function getAll();
    /**
     * @param array $data
     * @return mixed
     */

    public function saveAdmin(array $data);


    /**
     * @param $admin
     * @param $password
     * @return mixed
     */
    public function notify($admin, $password);

    /**
     * @param $email
     * @return mixed
     */
    public function getAdminStatus($email);


    /**
     * @param $data
     * @param $id
     * @param $adminId
     * @return mixed
     */
    public function activePlane($data, $id, $adminId);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

}
