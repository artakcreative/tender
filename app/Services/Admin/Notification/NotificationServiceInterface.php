<?php

namespace App\Services\Admin\Notification;

interface NotificationServiceInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function read($id);


    /**
     * @param $id
     * @return mixed
     */
    public function readAll($id);


    /**
     * @param $id
     * @param $data
     * @param $userPermissions
     * @return mixed
     */
    public function sendPermissions($id, $data, $userPermissions);


    /**
     * @param $id
     * @param $subject
     * @param $description
     * @param $linkId
     */
    public function sendNotification($id, $subject, $description, $linkId);


    /**
     * @param $userId
     * @return mixed
     */
    public function getAllUserNotify($userId);


    /**
     * @param $id
     * @param $userId
     * @return mixed
     */
    public function deleteUserNotify($id, $userId);

    /**
     * @param $userId
     * @return mixed
     */
    public function notRead($userId);

    /**
     * @param $id
     * @param $subject
     * @param $description
     * @param $linkId
     * @return mixed
     */
    public function sendNotificationManagerAdmin($id,$email, $phone);
}
