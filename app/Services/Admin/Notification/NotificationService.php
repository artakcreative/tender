<?php

namespace App\Services\Admin\Notification;

use App\Events\NotificationEvent;
use App\Notifications\GlobalNotification;
use App\Notifications\GlobalOnlyDatabaseNotification;
use App\Repositories\Admin\AdminRepositoryInterface;
use App\Repositories\Admin\ManagerRepositoryInterface;
use App\Repositories\Admin\Notification\NotificationRepositoryInterface;
use App\Repositories\Admin\Permission\PermissionRepositoryInterface;
use App\Services\Admin\Notification\NotificationServiceInterface;;

use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class NotificationService implements NotificationServiceInterface
{
    /**
     * @var NotificationRepositoryInterface
     */
    protected $notificationRepository;
    protected $managerRepository;
    protected $permissionRepository;
    /**
     * @var AdminRepositoryInterface
     */
    private $adminRepository;


    /**
     * NotificationService constructor.
     * @param NotificationRepositoryInterface $notificationRepository
     * @param ManagerRepositoryInterface $managerRepository
     * @param AdminRepositoryInterface $adminRepository
     * @param PermissionRepositoryInterface $permissionRepository
     */
    public function __construct(NotificationRepositoryInterface $notificationRepository,
                                ManagerRepositoryInterface $managerRepository,
                                AdminRepositoryInterface $adminRepository,
                                PermissionRepositoryInterface $permissionRepository)
    {
        $this->notificationRepository = $notificationRepository;
        $this->managerRepository = $managerRepository;
        $this->permissionRepository = $permissionRepository;
        $this->adminRepository = $adminRepository;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function read($id)
    {
       return $this->notificationRepository->read($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function readAll($id)
    {
        return $this->notificationRepository->readAll($id);
    }


    /**
     * @param $id
     * @param $data
     * @param $userPermissions
     */
    public function sendPermissions($id, $data, $userPermissions)
    {
        $diffAdded = array_diff($data, $userPermissions);
        $diffRemove = array_diff($userPermissions, $data);
        if(count($diffAdded)){
                $notifyText = $this->permissionRepository->getTextTranslate(array_values($diffAdded));
                $subject = 'Լիազորության ստացում';
                $text = '';
                foreach ($notifyText as $value) {
                    $text .= $value->display_name . ", ";
                }
                $text = substr(mb_strtolower($text), 0, -2);
                $this->sendNotification($id,$subject, $text, null);
        }

        if(count($diffRemove)){
            $notifyText = $this->permissionRepository->getTextTranslate(array_values($diffRemove));
            $subject = 'Լիազորության արգելափակում';
            $text = '';
            foreach ($notifyText as $value) {
                $text .= $value->display_name . ", ";
            }
            $text = substr(mb_strtolower($text), 0, -2);
            $this->sendNotification($id, $subject, $text, null);

        }
    }

    /**
     * @param $id
     * @param $subject
     * @param $description
     * @param $linkId
     */
    public function sendNotification($id, $subject, $description, $linkId)
    {
        $uuid = Str::uuid();
        $checkManagerBlocked =$this->managerRepository->checkManagerBlocked($id);
        if( $checkManagerBlocked == true) {
            Notification::send($this->managerRepository->getById($id), new GlobalOnlyDatabaseNotification($id, $subject, $description, $uuid, $linkId));
        }else {
            $manager = $this->managerRepository->getById($id);
            if ($subject == 'Լիազորության արգելափակում'){
                $text = 'Հարգելի ' . $manager['name'] . ' ' . $manager['surname'] . ': ' . 'Դուք այլևս չունեք` ' . "{$description}" . ' հասանելիությունը:';
            }else if ($subject == 'Լիազորության ստացում'){
                $text = 'Հարգելի ' . $manager['name'] . ' ' . $manager['surname'] . ': ' . 'Այսուհետ Ձեզ հասանելի է` ' . "{$description}" . ' լիազորությունը:';
            }else if($subject == 'Օգտատիրոջ ոլորտների փոփոխություն'){
                $text = 'Հարգելի ' . $manager['name'] . ' ' . $manager['surname'] . ': ' . $description;
            }else if( $subject ==  "Օգտատիրոջ անձնական տվյալների փոփոխություն"){
                $text ='Հարգելի ' . $manager['name'] . ' ' . $manager['surname'] . ': '. $description;
            }

            broadcast(new NotificationEvent($id, $subject, $text, $uuid, $linkId ))->toOthers();
            Notification::send($manager, new GlobalNotification($id, $subject, $description, $uuid, $linkId, null,null, $manager));
        }

    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getAllUserNotify($userId)
    {
        return $this->notificationRepository->getAllUserNotify($userId);
    }

    /**
     * @param $id
     * @param $userId
     * @return mixed
     */
    public function deleteUserNotify($id, $userId)
    {
        return $this->notificationRepository->deleteUserNotify($id, $userId);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function notRead($userId)
    {
        return $this->notificationRepository->notRead($userId);
    }

    /**
     * @param $id
     * @param $email
     * @param $phone
     * @return mixed|void
     */
    public function sendNotificationManagerAdmin($id, $email, $phone){
        $ids =  $this->adminRepository->getAllManagersAdminsIds();
        $uuid = Str::uuid();
        $subject = 'Նոր օգտատեր համակարգում';
        $description = "Հարթակում գրանցվել է նոր օգտատեր՝";
        $text =  $description . 'Էլ. հասցե: ' .  $email . ',' . 'Հեռախոսահամար՝ ' .$phone . ':';
        foreach ($ids as $id){
            broadcast(new NotificationEvent($id, $subject,  $text, $uuid, null ));
            Notification::send($this->managerRepository->getById($id), new GlobalNotification($id, $subject, $description, $uuid, null, $email, $phone ));
        }
    }

    /**
     * только первая буква заглавная в строке
     * @param $string
     * @return string
     */
   public function mb_ucfirst($string): string
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
    }
}
