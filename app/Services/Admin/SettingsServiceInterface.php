<?php

namespace App\Services\Admin;

interface SettingsServiceInterface
{
    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updatePersonalInfo($data, $id);

    /**
     * @param $data
     * @return mixed
     */
    public function changePassword($data);

    /**
     * @param $data
     * @return mixed
     */
    public function addContract($data);
}
