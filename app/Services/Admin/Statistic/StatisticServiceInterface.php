<?php

namespace App\Services\Admin\Statistic;

interface StatisticServiceInterface
{
    /**
     * @param $search
     * @return mixed
     */
    public function getUsers($search);

    /**
     * @param $data
     * @param $type
     * @return mixed
     */
    public function getUserStatisticMount($data, $type);

    /**
     * @param $data
     * @param $type
     * @return mixed
     */
    public function getUserStatisticWeek($data, $type);

    /**
     * @param $data
     * @param $type
     * @return mixed
     */
    public function getUserStatisticQuarter($data, $type);

    /**
     * @param $search
     * @return mixed
     */
    public function getManagers($search);

    /**
     * @param $data
     * @return mixed
     */
    public function getManagerStatisticMount($data);


    /**
     * @param $data
     * @return mixed
     */
    public function getManagerStatisticWeek($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getManagerStatisticQuarter($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getAnnouncementStatisticMount($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getAnnouncementStatisticWeek($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getAnnouncementStatisticQuarter($data);

}
