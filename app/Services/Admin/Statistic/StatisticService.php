<?php

namespace App\Services\Admin\Statistic;

use App\Models\Announcement;
use App\Repositories\Admin\AdminRepositoryInterface;
use App\Repositories\Admin\Announcement\AnnouncementRepository;
use App\Repositories\Admin\Announcement\AnnouncementRepositoryInterface;
use App\Repositories\Admin\LoginHistory\LoginHistoryRepositoryInterface;
use App\Repositories\Admin\UserRepositoryInterface;
use App\Services\Admin\Statistic\StatisticServiceInterface;
use Carbon\Carbon;

class StatisticService implements StatisticServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var LoginHistoryRepositoryInterface
     */
    private $loginHistoryRepository;
    /**
     * @var AdminRepositoryInterface
     */
    private $adminRepository;
    /**
     * @var AnnouncementRepositoryInterface
     */
    private $announcementRepository;


    /**
     * StatisticService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param LoginHistoryRepositoryInterface $loginHistoryRepository
     * @param AdminRepositoryInterface $adminRepository
     * @param AnnouncementRepositoryInterface $announcementRepository
     */
    public function __construct(UserRepositoryInterface $userRepository,
                                LoginHistoryRepositoryInterface $loginHistoryRepository,
                                AdminRepositoryInterface $adminRepository,
                                AnnouncementRepositoryInterface $announcementRepository)
    {
        $this->userRepository = $userRepository;
        $this->loginHistoryRepository = $loginHistoryRepository;
        $this->adminRepository = $adminRepository;
        $this->announcementRepository = $announcementRepository;
    }



    /**
     * @param $data
     * @param $type
     * @return mixed
     */
    public function getUserStatisticMount($data, $type)
    {
        $dates = $this->loginHistoryRepository->getUserStatistic($data, $type);
        $monthsCount = config('statistic.monthsCount');
        $info = [];
        if(count($dates) > 0) {
            foreach ($dates as $date) {
                $info[]=(int) Carbon::parse($date)->format('m') - 1;
            }
        }
        $type = 'mounts';
        $info = array_count_values($info);
        $result = array_replace($monthsCount , $info);
        return [$result,  $type];
    }


    /**
     * @param $data
     * @param $type
     * @return array|mixed
     */
    public function getUserStatisticWeek($data, $type)
    {

        $dates = $this->loginHistoryRepository->getUserStatistic($data, $type);
        $weeksCount = config('statistic.weeksCount');
        $info = [];
        if(count($dates) > 0) {
            foreach ($dates as $date) {
                $info[]=(int) Carbon::parse($date)->format('W') - 1;
            }
        }
        $type = 'weeks';
        $info = array_count_values($info);
        $result = array_replace($weeksCount, $info);
        return [$result,  $type];
    }


    /**
     * @param $data
     * @param $type
     * @return array|mixed
     */
    public function getUserStatisticQuarter($data, $type)
    {
        $dates = $this->loginHistoryRepository->getUserStatistic($data, $type);
        $quarterlyCount = config('statistic.quarterlyCount');
        $info = [];

        if(count($dates) > 0) {
            foreach ($dates as $date) {
                $info[]= Carbon::parse($date)->quarter -1;
            }
        }

        $type = 'quarters';
        $info = array_count_values($info);
        $result = array_replace($quarterlyCount, $info);
        return [$result,  $type];
    }

    /**
     * @param $data;
     * @return mixed
     */
    public function getUsers($data)
    {
        $search = null;
        if($data && isset($data['term'])) {
            $search = $data['term'];
        }
        return $this->userRepository->getUsers($search);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getManagers($data)
    {
        $search = null;
        if($data && isset($data['term'])) {
            $search = $data['term'];
        }
        return $this->adminRepository->getManagers($search);
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function getManagerStatisticMount($data)
    {
        $monthsCount = config('statistic.monthsCount');
        $dates =  $this->announcementRepository->getManagerStatistic($data);
        $info = [];
        $type = 'mounts';
        if(count($dates) > 0) {
            foreach ($dates as $data) {
                $date = (int) Carbon::parse($data['created_at'])->format('m') - 1;
                $info[] = [
                    'label' => $data['announcement_name'],
                    'data' => array_replace($monthsCount, [$date => 1])
                ];
            }
        }
        return  [$info, $type];
    }


    /**
     * @param $data
     * @return array|mixed
     */
    public function getManagerStatisticWeek($data)
    {
        $weeksCount = config('statistic.weeksCount');
        $dates =  $this->announcementRepository->getManagerStatistic($data);
        $info = [];
        $type = 'weeks';
        if(count($dates) > 0) {
            foreach ($dates as $data) {
                $date = (int) Carbon::parse($data['created_at'])->format('W') - 1;
                $info[] = [
                    'label' => $data['announcement_name'],
                    'data' => array_replace($weeksCount , [$date => 1])
                ];
            }
        }
        return  [$info, $type];
    }


    /**
     * @param $data
     * @return array|mixed
     */
    public function getManagerStatisticQuarter($data)
    {
        $quarterlyCount = config('statistic.quarterlyCount');
        $dates =  $this->announcementRepository->getManagerStatistic($data);
        $info = [];
        $type = 'quarters';
        if(count($dates) > 0) {
            foreach ($dates as $data) {
                $date = (int) Carbon::parse($data['created_at'])->quarter - 1;
                $info[] = [
                    'label' => $data['announcement_name'],
                    'data' => array_replace($quarterlyCount , [$date => 1])
                ];
            }
        }
        return  [$info, $type];
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function getAnnouncementStatisticMount($data)
    {
        $basic = 1;
        $monthsCount = config('statistic.monthsCount');
        $info = [];
        if($data['name'] == Announcement::BASIC) {
            $dates = $this->announcementRepository->getAnnouncementStatistic($data, $basic, '=');
        }else{
            $dates = $this->announcementRepository->getAnnouncementStatistic($data, $basic, '>');
        }

        $type = 'mounts';
        if(count($dates) > 0) {
            foreach ($dates as $data) {
                $date = (int) Carbon::parse($data['created_at'])->format('m') - 1;
                $info[] = [
                    'label' => $data['announcement_name'],
                    'data' => array_replace($monthsCount , [$date => 1])
                ];
            }
        }
        return  [$info, $type];

    }


    /**
     * @param $data
     * @return array|mixed
     */
    public function getAnnouncementStatisticWeek($data)
    {
        $basic = 1;
        $weeksCount = config('statistic.weeksCount');
        $info = [];
        if($data['name'] == Announcement::BASIC) {
            $dates = $this->announcementRepository->getAnnouncementStatistic($data, $basic, '=');
        }else{
            $dates = $this->announcementRepository->getAnnouncementStatistic($data, $basic, '>');
        }

        $type = 'weeks';
        if(count($dates) > 0) {
            foreach ($dates as $data) {
                $date = (int) Carbon::parse($data['created_at'])->format('W') - 1;
                $info[] = [
                    'label' => $data['announcement_name'],
                    'data' => array_replace($weeksCount , [$date => 1])
                ];
            }
        }
        return  [$info, $type];
    }


    /**
     * @param $data
     * @return array|mixed
     */
    public function getAnnouncementStatisticQuarter($data)
    {
        $basic = 1;
        $quarterlyCount = config('statistic.quarterlyCount');
        $info = [];
        if($data['name'] == Announcement::BASIC) {
            $dates = $this->announcementRepository->getAnnouncementStatistic($data, $basic, '=');
        }else{
            $dates = $this->announcementRepository->getAnnouncementStatistic($data, $basic, '>');
        }

        $type = 'quarters';
        if(count($dates) > 0) {
            foreach ($dates as $data) {
                $date = (int) Carbon::parse($data['created_at'])->quarter - 1;
                $info[] = [
                    'label' => $data['announcement_name'],
                    'data' => array_replace($quarterlyCount, [$date => 1])
                ];
            }
        }
        return  [$info, $type];
    }

}
