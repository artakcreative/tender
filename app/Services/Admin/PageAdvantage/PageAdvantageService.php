<?php

namespace App\Services\Admin\PageAdvantage;

use App\Repositories\Admin\PageAdvantage\PageAdvantageRepositoryInterface;

class PageAdvantageService implements PageAdvantageServiceInterface
{
    /**
     * @var PageAdvantageRepositoryInterface
     */
    protected $pageAdvantageRepository;

    /**
     * PageAdvantageService constructor.
     * @param PageAdvantageRepositoryInterface $pageAdvantageRepository
     */
    public function __construct(PageAdvantageRepositoryInterface $pageAdvantageRepository)
    {
        $this->pageAdvantageRepository = $pageAdvantageRepository;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function advantageCreate($request)
    {
       return $this->pageAdvantageRepository->advantageCreate($request);
    }

    /**
     * @return mixed
     */
    public function getAdvantages()
    {
        return $this->pageAdvantageRepository->getAdvantages();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function advantageDelete($id)
    {
        return $this->pageAdvantageRepository->advantageDelete($id);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function advantageUpdate($request)
    {
        return $this->pageAdvantageRepository->advantageUpdate($request);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAdvantage($id)
    {
        return $this->pageAdvantageRepository->getAdvantage($id);
    }
}
