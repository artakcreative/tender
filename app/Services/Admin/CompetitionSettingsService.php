<?php

namespace App\Services\Admin;

use App\Repositories\Admin\CompetitionSettingsRepository;
use App\Services\Admin\CompetitionSettingsServiceInterface;

class CompetitionSettingsService implements CompetitionSettingsServiceInterface
{
    /**
     * @var CompetitionSettingsRepository
     */
    private $competitionSettingsRepository;

    public function __construct(CompetitionSettingsRepository $competitionSettingsRepository)
    {
        $this->competitionSettingsRepository = $competitionSettingsRepository;
    }

    public function create_or_update_competition($data)
    {
        return $this->competitionSettingsRepository->create_or_update_competition($data);
    }

    public function getAllSettings()
    {
        $settings = $this->competitionSettingsRepository->all();
        foreach($settings as $setting){
            return $setting;
        }
    }
}
