<?php

namespace App\Services\Admin;

use App\Models\ActivedPlaneHistory;
use App\Notifications\StoreNotification;
use App\Repositories\Admin\AdminRepository;
use App\Repositories\Admin\AdminRepositoryInterface;
use App\Services\Admin\AdminServiceInterface;
use Illuminate\Support\Facades\Notification;

class AdminService implements AdminServiceInterface
{
    /**
     * @var AdminRepositoryInterface
     */
    private $adminRepository;

    public function __construct(AdminRepositoryInterface  $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }


    /**
     * @return mixed
     */

    public function getAll()
    {
       return $this->adminRepository->getAll();
    }


    /**
     * @param array $data
     * @return mixed|void
     */

    public function saveAdmin(array $data)
    {
        return $this->adminRepository->save($data);
    }

    /**
     * @param $admin
     * @param $password
     */
    public function notify($admin, $password)
    {
        Notification::send($admin, new StoreNotification($password, $admin['name'], $admin['surname'], 1, $_SERVER['HTTP_HOST']));
    }


    /**
     * @param $email
     * @return mixed
     */
    public function getAdminStatus($email)
    {
        return $this->adminRepository->getAdminStatus($email);
    }

    /**
     * @param $data
     * @param $id
     * @param $adminId
     * @return mixed|void
     */
    public function activePlane($data, $id, $adminId)
    {
        return $this->adminRepository->activePlane($data, $id, $adminId);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->adminRepository->getById($id);
    }


}
