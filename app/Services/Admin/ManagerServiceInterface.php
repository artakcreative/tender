<?php

namespace App\Services\Admin;

interface ManagerServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function saveManager(array $data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     * @return mixed
     */
    public function getManagerById($id);

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function updateManager(array $data, $id);

    /**
     * @param $manager
     * @param $password
     * @return mixed
     */
    public function notify($manager, $password);

    /**
     * @return mixed
     */
    public function getManagerPermissions();

    /**
     * @param $id
     * @param $permission
     * @return mixed
     */
    public function giveManagerPermissions($id, $permission);


    /**
     * @param $id
     * @return mixed
     */
    public function givePermissionNames($id);

    /**
     * @param $id
     * @param $permissions
     * @return mixed
     */
    public function updatePermissions($id,  $permissions);

    /**
     * @param $id
     * @return mixed
     */
    public function blockManager($id);

    /**
     * @param $id
     * @return mixed
     */
    public function unlockManager($id);

    /**
     * @return mixed
     */
    public function managerCount();
}
