<?php

namespace App\Services\Admin;

use App\Events\NotificationEvent;
use App\Events\NotificationUserEvent;
use App\Events\StaticUserSphereEvent;
use App\Models\Announcement;
use App\Models\CompetitionCustomerAffiliation;
use App\Models\CompetitionSubSphere;
use App\Models\CompetitionType;
use App\Models\User;
use App\Models\UserSphere;
use App\Models\UserSphereStatic;
use App\Notifications\GlobalNotification;
use App\Notifications\GlobalOnlyEmailNotification;
use App\Notifications\StoreNotification;
use App\Notifications\User\GlobalUserNotification;
use App\Repositories\Admin\Announcement\AnnouncementRepository;
use App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepository;
use App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepositoryInterface;
use App\Repositories\Admin\CompetitionType\CompetitionTypeRepository;
use App\Repositories\Admin\ManagerRepositoryInterface;
use App\Repositories\Admin\UserRepository;
use App\Repositories\Admin\UserRepositoryInterface;
use App\Services\Admin\UserServiceInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class UserService implements UserServiceInterface
{

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var AnnouncementRepository
     */
    private $announcementRepository;
    /**
     * @var CompetitionTypeRepository
     */
    private $competitionTypeRepository;
    /**
     * @var CompetitionCustomerAffiliationRepositoryInterface
     */
    private $affiliationRepository;
    /**
     * @var ManagerRepositoryInterface
     */
    private $managerRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param AnnouncementRepository $announcementRepository
     * @param CompetitionTypeRepository $competitionTypeRepository
     * @param CompetitionCustomerAffiliationRepository $affiliationRepository
     * @param ManagerRepositoryInterface $managerRepository
     */
    public function __construct(UserRepositoryInterface                  $userRepository,
                                AnnouncementRepository                   $announcementRepository,
                                CompetitionTypeRepository                $competitionTypeRepository,
                                CompetitionCustomerAffiliationRepository $affiliationRepository,
                                ManagerRepositoryInterface $managerRepository)
    {
        $this->userRepository = $userRepository;
        $this->announcementRepository = $announcementRepository;
        $this->competitionTypeRepository = $competitionTypeRepository;
        $this->affiliationRepository = $affiliationRepository;
        $this->managerRepository = $managerRepository;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->userRepository->getAll();
    }


    /**
     * @param array $data
     * @return mixed|string
     */
    public function save(array $data)
    {
        return $this->userRepository->save($data);
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function addManager($data)
    {
        $manager = $this->managerRepository->getById((int) $data['manager_id']);
        $user = $this->userRepository->getById((int) $data['user_id']);
        $subject = 'Օգտատիրոջ կցում';
        $description = $user->name != null && $user->surname != null?  "$user->name $user->surname օգտատերը կցված է Ձեզ" : "$user->email օգտատերը կցված է Ձեզ";
        $uuid = Str::uuid();
        broadcast(new NotificationEvent($data['manager_id'], $subject, $description, $uuid,  $user->id ))->toOthers();
        Notification::send($manager, new GlobalNotification($data['manager_id'], $subject, $description, $uuid, $user->id, $user->email,$user->phone, $manager));
        return $this->userRepository->addManager($data);
    }

    /**
     * @param $id
     * @return mixed|void
     * @throws \Exception
     */
//    public function deleteUser($id)
//    {
//        $this->userRepository->deleteById($id);
//    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserById($id)
    {
        return $this->userRepository->getUser($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPlaneHistory($id)
    {
        return $this->userRepository->getPlaneHistory($id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed|string
     */
    public function updateUser($id, $data)
    {
        return $this->userRepository->update($id, $data);
    }


    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateUserDataBlocked($id, $data)
    {
        if($data) $blocked = true;
        else $blocked = false;
        return $this->userRepository->updateUserDataBlocked($id, $blocked);
    }


    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateDataFromUser($id, $data)
    {
        return $this->userRepository->update($id, $data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function addFile($id, $data)
    {
        return $this->userRepository->addFile($id, $data);
    }


    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function activePlaneUser($id, $data)
    {
        $checkUserStatusBlocked = $this->userRepository->checkUserStatusBlocked((int)$id);
        if ($checkUserStatusBlocked == false) {
            $uuid = Str::uuid();
            $description = 'Դուք ակտիվացրել եք` ' . $data['tariff'] . ' փաթեթը, որը ակտիվ կլինի մինչև՝  ' . date('d.m.Y', strtotime($data['date'])) ;
            $subject = 'Փաթեթի ակտիվացում';
            $user = $this->getUserById((int)$id);
            $text = $user['name'] !== null && $user['surname'] !== null ? 'Հարգելի ' . $user['name']
                . ' ' . $user['surname']  : 'Հարգելի ' . $user['email'] . ', ';
            $text = $text . $description . ': Շնորհակալություն:';
            broadcast(new NotificationUserEvent((int)$id, $subject, $text, null, Carbon::now(), $uuid))->toOthers();
            Notification::send($this->getUserById((int)$id), new GlobalUserNotification((int)$id, $subject, $description, null, Carbon::now(), $uuid, $user));
        }
        return $this->userRepository->activePlaneUser($id, $data);
    }

    /**
     * @param $user
     * @param $password
     * @param $name
     * @param $surname
     * @return mixed|void
     */
    public function notify($user, $password, $name, $surname)
    {
        Notification::send($user, new StoreNotification($password, $name, $surname));
    }


    /**
     * @param $media_id
     * @return int|mixed
     */
    public function deleteFile($media_id)
    {

        return $this->userRepository->deleteFile($media_id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function userEmailVerify($id)
    {
        return $this->userRepository->userEmailVerify($id);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function updatePassword($data)
    {
        $user = Auth::guard('web')->user();
        if (Hash::check($data['old_password'], $user->password)) {
            if (!Hash::check($data['password'], $user->password)) {
                $this->userRepository->updatePassword($user->id, Hash::make($data['password']));
                return response()->json(['success', 'Գաղտնաբառը հաջողությոմբ փոփոխվել է։']);
            } else {
                return response()->json(['error', 'Հին գաղտաբառը համընկնում է նոր գաղտանաբառի հետ։']);
            }
        } else {
            return response()->json(['error', 'Հին գաղտաբառ գույություն չունի։']);
        }

    }


    public function blockUser($id)
    {
        $user = $this->userRepository->getById($id);
        $subject = 'Հաշվի ապաակտիվացում';
        $description = "Ձեր էջը " . request()->getHttpHost() ." համակարգում բլոկավորվել է՝ Ձեր կողմից համակարգից օգտվելու կանոնների խախտման պատճառով։";
        Notification::send($user, new GlobalOnlyEmailNotification($subject, $description, $user, User::BLOCK));
        return $this->userRepository->blockUser($id);
    }


    public function unlockUser($id)
    {
        $user = $this->userRepository->getById($id);
        $subject = 'Հաշվի ապաակտիվացում';
        $description = 'Ձեր էջը ' . request()->getHttpHost() . ' համակարգում վերաակտիվացվել է:';
        Notification::send($user, new GlobalOnlyEmailNotification($subject, $description, $user, User::ACTIVE));
        return $this->userRepository->unlockUser($id);
    }

    /**
     * общее количество пользователей
     * @return mixed|void
     */
    public function userCount()
    {
        return $this->userRepository->userCount();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserFilter($id)
    {
        return $this->userRepository->getUserFilter($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function mapSuccess($id)
    {
        return $this->userRepository->mapSuccess($id);
    }

    /**
     * добавляем избранные объявление для пользователя
     * @param $data
     * @return mixed
     */
    public function addDeleteFavoriteTender($data)
    {
        return $this->userRepository->addDeleteFavoriteTender($data);
    }


    /**
     * @param $data
     * @param $competitionTypesName
     * @param $status
     * @param $id
     * @return mixed|void
     */
    public function sendNotifyUserAnnouncement($data, $competitionTypesName, $status, $id)
    {
        $date = Carbon::parse($data['application_deadline']);
        $now = Carbon::now();
        $diff = $now->diffInDays($date, false);
        $type1 = $this->affiliationRepository->getByName(CompetitionCustomerAffiliation::PRIVATE);
        $type2 = $this->affiliationRepository->getByName(CompetitionCustomerAffiliation::PRIVATE_INTERNATIONAL);
        if ($diff > -1) {
            if ($competitionTypesName == CompetitionCustomerAffiliation::PRIVATE) {
                $users = $this->userRepository->userAnnouncementPrivate($data['id'], $type1['id'], UserSphere::PAYED);
            } elseif ($competitionTypesName == CompetitionCustomerAffiliation::PRIVATE_INTERNATIONAL) {
                $users = $this->userRepository->userAnnouncementPrivate($data['id'], $type2['id'], UserSphere::PAYED);
            } else {
                $users = $this->userRepository->userAnnouncementState($data['id'], UserSphere::PAYED);
            }

        } else {
            if ($competitionTypesName == CompetitionCustomerAffiliation::PRIVATE) {
                $users = $this->userRepository->userAnnouncementPrivate($data['id'], $type1['id'], UserSphere::NOTPAYED);
            } elseif ($competitionTypesName == CompetitionCustomerAffiliation::PRIVATE_INTERNATIONAL) {
                $users = $this->userRepository->userAnnouncementPrivate($data['id'], $type2['id'], UserSphere::NOTPAYED);
            } else {
                $users = $this->userRepository->userAnnouncementState($data['id'], UserSphere::NOTPAYED);
            }
        }

        $usersId = $users->map(function ($item) {
            return $item->user_id;
        });
        $usersId = array_values(array_unique($usersId->toArray()));

        if (count($usersId) > 0) {
            $uuid = Str::uuid();
            $description = 'Փոփոխություն է կատարվել ' . $data->customers->name . 'ի կողմից հայտարարված ' . $data['announcement_name'] . ' մրցույթի հայտարարության մեջ։';
            $subject = 'Փոփոխություն մրցույթի հայտարարության մեջ';

            if ($status == true) {
                $subject = 'Նոր տենդեր';
                $description = "Հարթակում հասանելի է նոր տենդեր՝  {$data['announcement_name']}";
            }

            for ($i = 0; $i < count($usersId); $i++) {
                $user = $this->getUserByFilter($data['id'], $usersId[$i]);
                if($user) {
                    if($status == true) {
                        $text = $user['name'] !== null && $user['surname'] !== null ? 'Հարգելի ' . $user['name'] . ' '
                            . $user['surname'] : 'Հարգելի ' . $user['email'];
                        $text = $text . ' ' . $description .
                            ' Հայտարարող ' . $data['customers']['name'] . ': ' . 'Հայտերի ընդունման վերջնաժամկետը՝ '
                            . date('m.d.Y', strtotime($data['application_deadline'])) . ':';
                    }else {
                        $text = $user['name'] !== null && $user['surname'] !== null ? 'Հարգելի ' . $user['name'] . ' '
                            . $user['surname'] : 'Հարգելի ' . $user['email'];
                        $text = $text . ' ' . $description;
                    }

                    broadcast(new NotificationUserEvent($usersId[$i], $subject, $text, $data['id'], Carbon::now(), $uuid))->toOthers();
                    Notification::send($user, new GlobalUserNotification($usersId[$i], $subject, $description, $data['id'], Carbon::now(), $uuid, $user,$data));
                }
            }
        }

        if (count($users->toArray()) > 0 && $status == true) {
            Event::dispatch(new StaticUserSphereEvent($users->toArray(), $id));
        }

    }


    /**
     * @param $ids
     * @return mixed
     */
    public function getAllUsersById($ids)
    {
        return $this->userRepository->getAllUsersById($ids);
    }

    /**
     * @param $announcementId
     * @param $userId
     */
    public function getUserByFilter($announcementId, $userId)
    {
        $filter = $this->userRepository->getUserFilter($userId);
        $info = $this->announcementRepository->getUserByFilter($announcementId, $filter);
        if($info == false)  {
            return false;
        }
        return $this->getUserById($userId);
    }

}
