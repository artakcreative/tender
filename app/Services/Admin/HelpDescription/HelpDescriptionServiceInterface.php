<?php

namespace App\Services\Admin\HelpDescription;

interface HelpDescriptionServiceInterface
{
    /**
     * @return mixed
     */
    public function getHelp();

    /**
     * @param $data
     * @return mixed
     */
    public function helpCreate($data);

    /**
     * @param $id
     * @return mixed
     */
    public function helpDelete($id);
}
