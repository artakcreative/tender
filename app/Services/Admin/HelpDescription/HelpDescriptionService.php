<?php

namespace App\Services\Admin\HelpDescription;

use App\Repositories\Admin\HelpDescription\HelpDescriptionRepositoryInterface;
use App\Services\Admin\HelpDescription\HelpDescriptionServiceInterface;

class HelpDescriptionService implements HelpDescriptionServiceInterface
{
    /**
     * @var HelpDescriptionRepositoryInterface
     */
    private $descriptionRepository;

    /**
     * HelpDescriptionService constructor.
     * @param HelpDescriptionRepositoryInterface $descriptionRepository
     */
    public function __construct(HelpDescriptionRepositoryInterface $descriptionRepository)
    {
        $this->descriptionRepository = $descriptionRepository;
    }

    /**
     * @return mixed
     */
    public function getHelp()
    {
        return $this->descriptionRepository->getHelp();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function helpCreate($data)
    {
        return $this->descriptionRepository->helpCreate($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function helpDelete($id)
    {
        return $this->descriptionRepository->helpDelete($id);
    }
}
