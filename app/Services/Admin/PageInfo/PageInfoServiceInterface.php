<?php

namespace App\Services\Admin\PageInfo;

interface PageInfoServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function infoCreate($data);

    /**
     * @return mixed
     */
    public function getInfo();

    /**
     * @param $id
     * @return mixed
     */
    public function infoDelete($id);
}
