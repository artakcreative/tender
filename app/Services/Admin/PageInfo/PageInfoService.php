<?php

namespace App\Services\Admin\PageInfo;

use App\Repositories\Admin\PageInfo\PageInfoRepositoryInterface;
use App\Services\Admin\PageInfo\PageInfoServiceInterface;

class PageInfoService implements PageInfoServiceInterface
{
    /**
     * @var PageInfoRepositoryInterface
     */
    protected $pageInfoRepository;

    /**
     * PageInfoService constructor.
     * @param PageInfoRepositoryInterface $pageInfoRepository
     */
    public function __construct(PageInfoRepositoryInterface $pageInfoRepository)
    {
        $this->pageInfoRepository = $pageInfoRepository;
    }


    /**
     * @param $data
     * @return mixed
     */
    public function infoCreate($data)
    {
        return $this->pageInfoRepository->infoCreate($data);
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->pageInfoRepository->getInfo();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function infoDelete($id)
    {
        return $this->pageInfoRepository->infoDelete($id);
    }
}
