<?php

namespace App\Services\Admin;

interface UserServiceInterface
{
    /**
     * @return mixed
     */
    public  function  getAll();

    /**
     * @param array $data
     * @return mixed
     */
    public  function  save(array $data);

    /**
     * @param $data
     * @return mixed
     */
    public function addManager($data);

    /**
     * @param $id
     * @return mixed
     */
//    public function deleteUser($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getUserById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getPlaneHistory($id);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateUser($id, $data);

    /**
     * @param $id
     * @param $blocked
     * @return mixed
     */
    public function updateUserDataBlocked($id, $blocked);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateDataFromUser($id, $data);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function addFile($id, $data);


    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function activePlaneUser($id, $data);

    /**
     * @param $user
     * @param $password
     * @param $name
     * @param $surname
     * @return mixed
     */
    public function notify($user, $password, $name, $surname);

    /**
     * @param $media_id
     * @return mixed
     */
    public function deleteFile($media_id);

    /**
     * @param $id
     * @return mixed
     */
    public function userEmailVerify($id);

    /**
     * @param $data
     * @return mixed
     */
    public function updatePassword($data);

    /**
     * @param $id
     * @return mixed
     */
    public function blockUser($id);

    /**
     * @param $id
     * @return mixed
     */
    public function unlockUser($id);

    /**
     * @return mixed
     */
    public function userCount();

    /**
     * @param $id
     * @return mixed
     */
    public function getUserFilter($id);

    /**
     * @param $id
     * @return mixed
     */
    public function mapSuccess($id);

    /**
     * @param $data
     * @return mixed
     */
    public function addDeleteFavoriteTender($data);


    /**
     * @param $data
     * @param $competitionTypesName
     * @param $status
     * @param $id
     * @return mixed
     */
    public function sendNotifyUserAnnouncement($data, $competitionTypesName, $status, $id);

    /**
     * @param $ids
     * @return mixed
     */
    public function getAllUsersById($ids);
}
