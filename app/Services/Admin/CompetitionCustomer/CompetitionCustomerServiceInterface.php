<?php

namespace App\Services\Admin\CompetitionCustomer;

interface CompetitionCustomerServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function saveCustomer($data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @return mixed
     */
    public function getAllForSelect();

    /**
     * @param $id
     * @return mixed
     */
    public function getCustomerById($id);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateCustomer($id,$data);

    /**
     * @param $data
     * @return mixed
     */
    public function getCustomer($data);

    /**
     * @return mixed
     */
    public function allForFilter();


    /**
     * @param $id
     * @return mixed
     */
    public function remove($id);

}
