<?php

namespace App\Services\Admin\CompetitionCustomer;

use App\Repositories\Admin\CompetitionCustomer\CompetitionCustomerRepositoryInterface;
use App\Services\Admin\CompetitionCustomer\CompetitionCustomerServiceInterface;

class CompetitionCustomerService implements CompetitionCustomerServiceInterface
{
    /**
     * @var CompetitionCustomerRepositoryInterface
     */
    private $competitionCustomerRepository;

    /**
     * @param CompetitionCustomerRepositoryInterface $competitionCustomerRepository
     */
    public function __construct(CompetitionCustomerRepositoryInterface $competitionCustomerRepository)
    {
        $this->competitionCustomerRepository = $competitionCustomerRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function saveCustomer($data)
    {
        return $this->competitionCustomerRepository->saveCustomer($data);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->competitionCustomerRepository->getAll();
    }

    /**
     * @return mixed
     */
    public function getAllForSelect()
    {
        return $this->competitionCustomerRepository->getAllForSelect();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCustomerById($id){
        return $this->competitionCustomerRepository->getById($id);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateCustomer($id, $data)
    {
        return $this->competitionCustomerRepository->update((int)$id, $data);
    }

    /**
     * выбираем клиента по поиску
     * @param $data
     * @return mixed
     */
    public function getCustomer($data)
    {
        $search = null;
        if($data && isset($data['term'])) {
            $search = $data['term'];
        }
        return $this->competitionCustomerRepository->getCustomer($search);
    }

    /**
     * @return mixed
     */
    public function allForFilter()
    {
        return $this->competitionCustomerRepository->allForFilter();
    }


    /**
     * @param $id
     * @return mixed
     */
    public function remove($id)
    {
        return $this->competitionCustomerRepository->remove($id);
    }
}
