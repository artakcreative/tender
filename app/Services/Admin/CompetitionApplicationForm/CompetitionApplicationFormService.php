<?php

namespace App\Services\Admin\CompetitionApplicationForm;

use App\Repositories\Admin\CompetitionApplicationForm\CompetitionApplicationFormRepositoryInterface;
use App\Services\Admin\CompetitionApplicationForm\CompetitionApplicationFormServiceInterface;

class CompetitionApplicationFormService implements CompetitionApplicationFormServiceInterface
{
    /**
     * @var CompetitionApplicationFormRepositoryInterface
     */
    private $competitionApplicationFormRepository;

    /**
     * @param CompetitionApplicationFormRepositoryInterface $competitionApplicationFormRepository
     */
    public function __construct(CompetitionApplicationFormRepositoryInterface $competitionApplicationFormRepository)
    {
        //
        $this->competitionApplicationFormRepository = $competitionApplicationFormRepository;
    }

    /**
     * Сохранение конфигурации как подать заявления
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        return $this->competitionApplicationFormRepository->save($data);
    }

    /**
     * Показать все  конфигурации как подать заявления в index
     * @return mixed
     */
    public function getAll()
    {
        return $this->competitionApplicationFormRepository->getAll();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->competitionApplicationFormRepository->getById($id);
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data, $id)
    {
        return $this ->competitionApplicationFormRepository->update($data, $id);
    }
}
