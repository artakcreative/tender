<?php

namespace App\Services\Admin\CompetitionCustomerHistory;

interface CompetitionCustomerHistoryServiceInterface
{
    /**
     * @return mixed
     */
    public function getCustomerHistory();
}
