<?php

namespace App\Services\Admin\CompetitionCustomerHistory;

use App\Repositories\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryRepositoryInterface;
use App\Services\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryServiceInterface;

class CompetitionCustomerHistoryService implements CompetitionCustomerHistoryServiceInterface
{
    /**
     * @var CompetitionCustomerHistoryRepositoryInterface
     */
    private $competitionCustomerHistoryRepository;

    public function __construct(CompetitionCustomerHistoryRepositoryInterface $competitionCustomerHistoryRepository)
    {

        $this->competitionCustomerHistoryRepository = $competitionCustomerHistoryRepository;
    }

    /**
     * история пользователей
     * @return mixed
     */
    public function getCustomerHistory()
    {
        return $this->competitionCustomerHistoryRepository->getCustomerHistory();
    }
}
