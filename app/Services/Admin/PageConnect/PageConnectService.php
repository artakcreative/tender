<?php

namespace App\Services\Admin\PageConnect;

use App\Repositories\Admin\PageConnect\PageConnectRepositoryInterface;
use App\Services\Admin\PageConnect\PageConnectServiceInterface;

class PageConnectService implements PageConnectServiceInterface
{
    /**
     * @var PageConnectRepositoryInterface
     */
    protected $connectRepository;

    /**
     * PageConnectService constructor.
     * @param PageConnectRepositoryInterface $connectRepository
     */
    public function __construct(PageConnectRepositoryInterface $connectRepository)
    {
        $this->connectRepository = $connectRepository;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function connectCreate($request)
    {
       return $this->connectRepository->connectCreate($request);
    }

    /**
     * @return mixed
     */
    public function getConnects()
    {
        return $this->connectRepository->getConnects();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function connectDelete($id)
    {
        return $this->connectRepository->connectDelete($id);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function connectUpdate($request)
    {
        return $this->connectRepository->connectUpdate($request);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getConnect($id)
    {
        return $this->connectRepository->getConnect($id);
    }
}
