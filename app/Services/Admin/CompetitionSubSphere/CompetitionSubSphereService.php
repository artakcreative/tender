<?php

namespace App\Services\Admin\CompetitionSubSphere;

use App\Repositories\Admin\CompetitionSubSphere\CompetitionSubSphereRepositoryInterface;
use App\Services\Admin\CompetitionSubSphere\CompetitionSubSphereServiceInterface;

class CompetitionSubSphereService implements CompetitionSubSphereServiceInterface
{


    /**
     * @var CompetitionSubSphereRepositoryInterface
     */
    private $competitionSubSphereRepository;

    /**
     * CompetitionSubSphereService constructor.
     * @param CompetitionSubSphereRepositoryInterface $competitionSubSphereRepository
     */
    public function __construct(CompetitionSubSphereRepositoryInterface $competitionSubSphereRepository)
    {

        $this->competitionSubSphereRepository = $competitionSubSphereRepository;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return  $this->competitionSubSphereRepository->getById($id);
    }

    public function getSubSphereData($data)
    {
        $search = null;
        if($data && isset($data['term'])) {
            $search = $data['term'];
        }
        return $this->competitionSubSphereRepository->getSubSphereData($search);
    }
}
