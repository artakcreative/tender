<?php

namespace App\Services\Admin\CompetitionSubSphere;

interface CompetitionSubSphereServiceInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
    /**
     * @param $data
     * @return mixed
     */
    public function getSubSphereData($data);

}
