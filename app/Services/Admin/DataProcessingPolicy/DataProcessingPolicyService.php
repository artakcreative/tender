<?php

namespace App\Services\Admin\DataProcessingPolicy;

use App\Repositories\Admin\DataProcessingPolicy\DataProcessingPolicyRepository;
use App\Repositories\Admin\DataProcessingPolicy\DataProcessingPolicyRepositoryInterface;
use App\Services\Admin\DataProcessingPolicy\DataProcessingPolicyServiceInterface;

class DataProcessingPolicyService implements DataProcessingPolicyServiceInterface
{
    /**
     * @var DataProcessingPolicyRepositoryInterface
     */
    private $policyRepository;

    /**
     * @param DataProcessingPolicyRepositoryInterface $policyRepository
     */
    public function __construct(DataProcessingPolicyRepositoryInterface $policyRepository)
    {
        $this->policyRepository = $policyRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        return $this->policyRepository->save($data);
    }

    /**
     * @return mixed|void
     */
    public function getAll()
    {
        return $this->policyRepository->getAll();
    }
}
