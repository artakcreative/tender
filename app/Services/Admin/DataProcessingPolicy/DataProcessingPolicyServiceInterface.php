<?php

namespace App\Services\Admin\DataProcessingPolicy;

interface DataProcessingPolicyServiceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function save($data);

    /**
     * @return mixed
     */
    public function getAll();
}
