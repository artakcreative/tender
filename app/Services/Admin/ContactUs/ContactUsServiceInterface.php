<?php

namespace App\Services\Admin\ContactUs;

interface ContactUsServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data, $id);
}
