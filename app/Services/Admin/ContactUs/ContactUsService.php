<?php

namespace App\Services\Admin\ContactUs;

use App\Repositories\Admin\ContactUs\ContactUsRepositoryInterface;
use App\Services\Admin\ContactUs\ContactUsServiceInterface;

class ContactUsService implements ContactUsServiceInterface
{
    /**
     * @var ContactUsRepositoryInterface
     */
    private $contactUsRepository;

    public function __construct(ContactUsRepositoryInterface $contactUsRepository)
    {
        //
        $this->contactUsRepository = $contactUsRepository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data)
    {
        return $this->contactUsRepository->save($data);
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data, $id)
    {
        return $this->contactUsRepository->updateById($id, $data);
    }
}
