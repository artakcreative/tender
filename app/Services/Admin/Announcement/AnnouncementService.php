<?php

namespace App\Services\Admin\Announcement;

use App\MediaLibrary\CustomPathGenerator;
use App\Repositories\Admin\Announcement\AnnouncementRepositoryInterface;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class AnnouncementService  implements AnnouncementServiceInterface
{
    /**
     * @var AnnouncementRepositoryInterface
     */
    protected $announcementRepository;

    /**
     * AnnouncementService constructor.
     * @param AnnouncementRepositoryInterface $announcementRepository
     */
    public function __construct(AnnouncementRepositoryInterface $announcementRepository)
    {
        $this->announcementRepository = $announcementRepository;
    }


    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->announcementRepository->getAll();
    }

    public function getAllSaved()
    {
        return $this->announcementRepository->getAllSaved();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return $this->announcementRepository->getById($id);
    }

    /**
     * @param int $id
     * @param $data
     * @return mixed
     */
    public function update(int $id, $data)
    {
        return $this->announcementRepository->update($id,$data);
    }


    /**
     * @param $settings
     * @param $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function getSubsphere($settings, $request)
    {
        $settingsCollection = collect($settings->sphere);
        $sphere = $settingsCollection->pluck('sub_sphere','sphere');

        if (isset($request->sphere )){
            return response()->json([
                'success'=> true,
                'data'=>$sphere[$request->sphere],
            ],200);
        }
    }


    public function deleteFile(int $media_id)
    {
        return $this->announcementRepository->deleteFile($media_id);
    }

    /**
     * сохранение объявления и генерация PDF
     * @param $data
     * @return mixed
     */
    public function saveAnnouncement($data)
    {
        return $this->announcementRepository->saveAnnouncement($data);
    }

    /**
     *берем все категории и подкатегории связанные с объявлением
     * @param $id
     * @return mixed
     */
    public function getAnnouncementSubSphereAndSphere($id)
    {
        return $this->announcementRepository->getAnnouncementSubSphereAndSphere($id);
    }

    /**
     * общее количество опубликованных объявлений
     * @return mixed
     */
    public function publishedAnnouncementCount()
    {
        return $this->announcementRepository->publishedAnnouncementCount();
    }


    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return mixed
     */
    public function active($id, $tariff, $filter)
    {
        return $this->announcementRepository->active($id, $tariff, $filter);
    }


    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return mixed
     */
    public function archive($id, $tariff, $filter)
    {
        return $this->announcementRepository->archive($id, $tariff, $filter);
    }

    /**
     * Выбираем все активные объявления
     * @return mixed
     */
    public function getAllActiveAnnouncements()
    {
        return $this->announcementRepository->getAllActiveAnnouncements();
    }

    /**
     * Выбираем все архивированные  объявления
     * @return mixed
     */
    public function getAllArchivedAnnouncements()
    {
        return $this->announcementRepository->getAllArchivedAnnouncements();
    }

    /**
     * @return mixed
     */
    public function getAllActiveAnnouncementsCount()
    {
        return $this->announcementRepository->getAllActiveAnnouncementsCount();
    }

    /**
     * @return mixed
     */
    public function getAllArchivedAnnouncementsCount()
    {
        return $this->announcementRepository->getAllArchivedAnnouncementsCount();
    }

    /**
     * выбираем  активные и архивированные  объявлений по сферам
     * @param $id
     * @return mixed
     */
    public function selectActiveAndArchivedAnnouncementsBySpheres($id)
    {
        return $this->announcementRepository->selectActiveAndArchivedAnnouncementsBySpheres($id);
    }

    /**
     * прикрепляем менеджера в объявлению
     * @param $data
     * @param $id
     * @return mixed
     */
    public function addManagerToAnnouncement($data, $id)
    {
        return $this->announcementRepository->addManagerToAnnouncement($data, $id);
    }

    /**
     * история объявлений
     * @param $id
     * @return mixed
     */
    public function getAnnouncementHistory($id)
    {
        return $this->announcementRepository->getAnnouncementHistory($id);
    }
}
