<?php

namespace App\Services\Admin\Announcement;

interface AnnouncementServiceInterface
{

    /**
     * @return mixed
     */
    public function getAll();



    /**
     * @param int $id
     * @return mixed
     */
    public function getById(int $id);


    /**
     * @param $settings
     * @param $request
     * @return mixed
     */
    public function getSubsphere($settings, $request);

    /**
     * @param int $media_id
     * @return mixed
     */
    public function deleteFile(int $media_id);

    /**
     * @param int $id
     * @param $data
     * @return mixed
     */
    public function update(int $id, $data);

    /**
     * @param $data
     * @return mixed
     */
    public function saveAnnouncement($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getAnnouncementSubSphereAndSphere($id);

    /**
     * @return mixed
     */
    public function publishedAnnouncementCount();


    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return mixed
     */
    public function active($id, $tariff, $filter);

    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return mixed
     */
    public function archive($id, $tariff, $filter);

    /**
     * @return mixed
     */
    public function getAllActiveAnnouncements();

    /**
     * @return mixed
     */
    public function getAllArchivedAnnouncements();

    /**
     * @return mixed
     */
    public function getAllActiveAnnouncementsCount();

    /**
     * @return mixed
     */
    public function getAllArchivedAnnouncementsCount();

    /**
     * @param $id
     * @return mixed
     */
    public function selectActiveAndArchivedAnnouncementsBySpheres($id);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function addManagerToAnnouncement($data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function getAnnouncementHistory($id);

    /**
     * @return mixed
     */
    public function getAllSaved();

}
