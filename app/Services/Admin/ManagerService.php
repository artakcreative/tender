<?php

namespace App\Services\Admin;

use App\Events\NotificationEvent;
use App\Models\Admin;
use App\Notifications\GlobalNotification;
use App\Notifications\GlobalOnlyEmailNotification;
use App\Notifications\StoreNotification;
use App\Repositories\Admin\ManagerRepository;
use App\Repositories\Admin\ManagerRepositoryInterface;
use App\Services\Admin\ManagerServiceInterface;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use MongoDB\Driver\Manager;
use Spatie\Permission\Models\Role;

class ManagerService implements  ManagerServiceInterface
{
    /**
     * @var ManagerRepository
     */
    private $managerRepository;

    /**
     * @param ManagerRepository $managerRepository
     */
    public function __construct(ManagerRepositoryInterface $managerRepository)
    {
        $this->managerRepository = $managerRepository;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->managerRepository->getAll();
    }

    /**
     * @param $id
     * @return ManagerRepository|\Illuminate\Database\Eloquent\Model|mixed
     */
    public function getManagerById($id)
    {
        return $this->managerRepository->getById($id);
    }

    /**
     * @param array $data
     * @return mixed|void
     */
    public function saveManager(array $data)
    {
        return $this->managerRepository->save($data);
    }



    /**
     * @param array $data
     * @param $id
     * @return mixed|void
     */
    public function updateManager(array $data, $id)
    {
        $this->managerRepository->updateById($id, $data);
    }

    /**
     * @param $manager
     * @param $password
     * @return mixed|void
     */
    public function notify($manager, $password)
    {
        Notification::send($manager, new StoreNotification($password, $manager['name'], $manager['surname'], 2, $_SERVER['HTTP_HOST']));
    }

    /**
     * @return mixed|void
     */
    public function getManagerPermissions()
    {
        return  $this->managerRepository->getManagerPermissions();
    }


    /**
     * @param $id
     * @param $permission
     * @return mixed|void
     */
    public function giveManagerPermissions($id, $permission)
    {
      return $this->managerRepository->givePermissions($id, $permission);
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function givePermissionNames($id)
    {
       return $this->managerRepository->givePermissionNames($id);
    }

    /**
     * @param $id
     * @param $permissions
     * @return mixed|void
     */
    public function updatePermissions($id,  $permissions){

      return  $this->managerRepository->updatePermissions($id, $permissions);
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws Exception
     */
    public function blockManager($id)
    {

        $manager = $this->managerRepository->getById($id);
        $subject = 'Էջի արգելափակում';
        $description = 'Համակարգում Ձեր էջն արգելափակված է։';
        Notification::send($manager, new GlobalOnlyEmailNotification($subject, $description, $manager, Admin::BLOCK));
        return $this->managerRepository->blockManager($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    public function unlockManager($id)
    {
        $manager = $this->managerRepository->getById($id);
        $subject = 'Հաշվի վերաակտիվացում';
        $description = 'Համակարգում Ձեր էջն վերաակտիվացված է:';
        Notification::send($manager, new GlobalOnlyEmailNotification($subject, $description,$manager, Admin::ACTIVE));
        return $this->managerRepository->unlockManager($id);
    }

    /**
     * общее количество манажеров
     * @return mixed
     */
    public function managerCount()
    {
        return $this->managerRepository->managerCount();
    }
}
