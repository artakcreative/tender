<?php

namespace App\Services\Admin\LoginHistory;

interface LoginHistoryServiceInterface
{
    /**
     * @param $id
     * @param $type
     * @return mixed
     */
    public function createHistory($id, $type);
}
