<?php

namespace App\Services\Admin\LoginHistory;

use App\Models\Admin;
use App\Models\User;
use App\Repositories\Admin\LoginHistory\LoginHistoryRepositoryInterface;
use App\Services\Admin\LoginHistory\LoginHistoryServiceInterface;

class LoginHistoryService implements LoginHistoryServiceInterface
{
    /**
     * @var LoginHistoryRepositoryInterface
     */
    protected $loginHistoryRepository;

    /**
     * LoginHistoryService constructor.
     * @param LoginHistoryRepositoryInterface $loginHistoryRepository
     */
    public function __construct(LoginHistoryRepositoryInterface $loginHistoryRepository)
    {
        $this->loginHistoryRepository = $loginHistoryRepository;
    }

    /**
     * @param $id
     * @param $type
     * @return mixed
     */
    public function createHistory($id, $type)
    {
        if($type == 'admin') $type = Admin::class;
        else $type = User::class;
        return $this->loginHistoryRepository->createHistory($id, $type);
    }
}
