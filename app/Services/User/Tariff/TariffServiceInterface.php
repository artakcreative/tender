<?php

namespace App\Services\User\Tariff;

interface TariffServiceInterface
{
    /**
     * @return mixed
     */
    public function getTariffs();

    /**
     * @param $id
     * @return mixed
     */
    public function getTariffById($id);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updatePrice($data, $id);
}
