<?php

namespace App\Services\User\Tariff;

use App\Repositories\User\Tariff\TariffRepositoryInterface;
use App\Services\User\Tariff\TariffServiceInterface;

class TariffService implements TariffServiceInterface
{
    /**
     * @var TariffRepositoryInterface
     */
    private $tariffRepository;

    /**
     * @param TariffRepositoryInterface $tariffRepository
     */
    public function __construct(TariffRepositoryInterface $tariffRepository)
    {
        $this->tariffRepository = $tariffRepository;
    }

    /**
     * @return mixed
     */
    public function getTariffs()
    {
        return $this->tariffRepository->getTariffs();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTariffById($id)
    {
        return $this->tariffRepository->getTariffById($id);
    }

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updatePrice($data, $id)
    {
        return $this->tariffRepository->updateById($id, $data);
    }
}
