<?php

namespace App\Services\User\UserSphereStatic;

use App\Repositories\User\UserSphereStatic\UserSphereStaticRepositoryInterface;
use App\Services\User\UserSphereStatic\UserSphereStaticServiceInterface;

class UserSphereStaticService implements UserSphereStaticServiceInterface
{
    /**
     * @var UserSphereStaticRepositoryInterface
     */
    private $userSphereStaticRepository;

    /**
     * UserSphereStaticService constructor.
     * @param UserSphereStaticRepositoryInterface $userSphereStaticRepository
     */
    public function __construct(UserSphereStaticRepositoryInterface $userSphereStaticRepository)
    {
        $this->userSphereStaticRepository = $userSphereStaticRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->userSphereStaticRepository->createNew($data);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getStatisticDefault($userId)
    {
        return $this->userSphereStaticRepository->getStatisticDefault($userId);
    }

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function getStatisticMount($userId, $data)
    {
        return $this->userSphereStaticRepository->getStatisticMount($userId, $data);
    }

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function getStatisticWeek($userId, $data)
    {
        return $this->userSphereStaticRepository->getStatisticWeek($userId, $data);
    }

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function getStatisticQuarter($userId, $data)
    {
        return $this->userSphereStaticRepository->getStatisticQuarter($userId, $data);
    }
}
