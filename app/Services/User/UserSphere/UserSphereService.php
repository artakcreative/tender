<?php

namespace App\Services\User\UserSphere;

use App\Models\UserSphere;
use App\Repositories\Admin\UserRepository;
use App\Repositories\Admin\UserRepositoryInterface;
use App\Repositories\User\UserSphere\UserSphereRepositoryInterface;
use App\Services\User\UserSphere\UserSphereServiceInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class UserSphereService implements UserSphereServiceInterface
{
    /**
     * @var UserSphereRepositoryInterface
     */
    private $userSphereRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserSphereService constructor.
     * @param UserSphereRepositoryInterface $userSphereRepository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserSphereRepositoryInterface $userSphereRepository, UserRepositoryInterface $userRepository)
    {
        $this->userSphereRepository = $userSphereRepository;
        $this->userRepository = $userRepository;
    }

    public function getAllUserSpheres($userId)
    {
        $spheres = [];
        $tariff = null;
        $active = null;
        $tariffWanth = null;
        $amountPayed = 0;
        $sums = 0;
        $priceWanth = 0;
        $sumsWanth = 0;
        $now = Carbon::now();
        $month =  UserSphere::MONTH;
        $userWantTariff = $this->userRepository->userWantTariff($userId);
        $price = $this->userSphereRepository->getTariff($userId);
        $payed = $this->getSelectedSpheres($userId);
        if(count($userWantTariff) > 0) {
            $tariffWanth = $userWantTariff['name'];
            $priceWanth = $userWantTariff['price'];
        }

        if(count($price[1]) > 0) {
            $price = $price[1]['price'];
        }
        if(count($payed) > 0) {
            $active = $payed[0][0]["user_spheres"][0]["active"];
            $dateWork = Carbon::createFromDate($active)->format('d.m.Y');
            $countDays = $now->diffInDays($dateWork, false);
            if ($countDays > 0) {
                if(count($payed) > 1) {
                    if(count($payed) == 2) {
                        $sum = $price + ($price - ($price * 10 /100)) ;
                        $amountPayed = $sum / $month * $countDays;
                    }else {
                        $sum = $price + ($price - ($price * 10 /100)) + (count($payed)  - 2) * ($price- ($price * 20 /100));
                        $amountPayed = $sum / $month * $countDays;
                    }
                }else {
                  $amountPayed = $price / $month * $countDays;
                }
            }
            $active = Carbon::parse($active)->format('d.m.Y');
        }
        $notPayed = $this->getNotPayed($userId);
        $data = array_merge($payed, $notPayed);
        foreach ($data as $value) {
            $tariff = $value[0]['user_spheres'][0]['tariff']['name'];
            $spheres[] = $value[0]["sphere"]["name"];
        }
        if(count($spheres) > 0) {
            if(count($spheres) > 1) {
                if(count($spheres) == 2) {
                    $sums = round( $price + ($price - ($price * 10 /100)) - $amountPayed);
                    $sumsWanth = round($priceWanth + ($priceWanth - ($priceWanth * 10 /100)) - $amountPayed);
                }else {
                    $sums = round($price + ($price - ($price * 10 /100)) + (count($spheres)  - 2) * ($price - ($price * 20 /100)) - $amountPayed);
                    $sumsWanth = round($priceWanth + ($priceWanth - ($priceWanth* 10 /100)) + (count($spheres)  - 2) * ($priceWanth- ($priceWanth * 20 /100)) - $amountPayed);

                }

            } else {
                $sums =round(($price - $amountPayed));
                $sumsWanth = round(($priceWanth - $amountPayed));
            }
        }
        $info = [
            'spheres' => $spheres,
            'tariff' => $tariff,
            'tariff_wanth' => $tariffWanth,
            'active' => $active,
            'sums' => $sums,
            'sums_wanth' => $sumsWanth,
        ];

        return $info;
    }

    /**
     * @return mixed
     */
    public function activeSelected()
    {
        $userId = Auth::guard('web')->user()->id;
        return $this->userRepository->activeSelected($userId);
    }


    /**
     * @param $userId
     * @return mixed
     */
    public function getSelectedSpheres($userId)
    {
        return $this->userSphereRepository->getSelectedSpheres($userId);
    }


    /**
     * @param $userId
     * @return mixed
     */
    public function getNotPayed($userId)
    {
        return $this->userSphereRepository->getNotPayed($userId);
    }

    /**
     * @return mixed
     */
    public function tariffs() {
        return $this->userSphereRepository->tariffs();
    }

    /**
     * @return mixed
     */
    public function getTariff()
    {
        $userId = Auth::guard('web')->user()->id;
        return $this->userSphereRepository->getTariff($userId);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserSpheres($userId)
    {
        return $this->userSphereRepository->getUserSpheres($userId);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function saveUserFirstSphere($data)
    {
        return $this->userSphereRepository->saveUserFirstSphere($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function deleteNotPayed($data)
    {
        return $this->userSphereRepository->deleteNotPayed($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        return $this->userSphereRepository->save($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function filterSave($data)
    {
        return $this->userRepository->filterSave($data);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getUserTariffTrueOrFalse($id)
    {
        return $this->userSphereRepository->getUserTariffTrueOrFalse($id);
    }


}
