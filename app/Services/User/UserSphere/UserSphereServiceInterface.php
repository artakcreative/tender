<?php

namespace App\Services\User\UserSphere;

interface UserSphereServiceInterface
{

    /**
     * @return mixed
     */
    public function activeSelected();

    /**
     * @param $userId
     * @return mixed
     */
    public function getAllUserSpheres($userId);


    /**
     * @param $userId
     * @return mixed
     */
    public function getSelectedSpheres($userId);


    /**
     * @param $userId
     * @return mixed
     */
    public function getNotPayed($userId);

    /**
     * @return mixed
     */
    public function tariffs();

    /**
     * @return mixed
     */
    public function getTariff();

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserSpheres($userId);

    /**
     * @param $data
     * @return mixed
     */
    public function saveUserFirstSphere($data);


    /**
     * @param $data
     * @return mixed
     */
    public function deleteNotPayed($data);

    /**
     * @param $data
     * @return mixed
     */
    public function save($data);


    /**
     * @param $data
     * @return mixed
     */
    public function filterSave($data);


    /**
     * @param $id
     * @return mixed
     */
    public function getUserTariffTrueOrFalse($id);

}
