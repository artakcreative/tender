<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15.11.2021
 * Time: 18:29
 */

namespace App\View\Components;


use Illuminate\View\Component;

class AdminLayout extends Component
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('admin.layouts.app');
    }
}