<?php

namespace App\View\Components;


use Illuminate\View\Component;

class AdminLoginLayout extends Component
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('admin.auth.login');
    }
}
