<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class CompetitionSphere extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * @return HasMany
     */
    public function sub_spheres(): HasMany
    {
         return $this->hasMany(CompetitionSubSphere::class);
    }

    /**
     * @return MorphMany
     */
    public function userSpheres(): MorphMany
    {
        return $this->morphMany(UserSphere::class, 'sphereable');
    }

    /**
     * @return MorphToMany
     */
    public function announcements(): MorphToMany
    {
        return $this->morphToMany(Announcement::class, 'sphere')->withTimestamps();
    }

    /**
     * @return HasMany
     */
    public function userSphereStatic(): HasMany
    {
        return $this->hasMany(UserSphereStatic::class, 'sphere_id', 'id');
    }

}
