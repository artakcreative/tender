<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageDescription extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $guarded = ['id'];
}
