<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CompetitionCustomerAffiliation extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_affiliation',
    ];

    const PRIVATE = 'Մասնավոր տեղական';
    const PRIVATE_INTERNATIONAL = 'Մասնավոր միջազգային';
    const STATE = 'Պետական';

    /**
     * @return HasMany
     */
    public function customers(): HasMany
    {
        return $this->hasMany(CompetitionCustomer::class, 'customer_id');
    }

    public function announcement(): HasMany
    {
        return $this->hasMany(Announcement::class, 'customer_affiliation_id');
    }
}
