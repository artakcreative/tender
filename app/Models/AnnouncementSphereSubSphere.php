<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;

class AnnouncementSphereSubSphere extends Model implements Auditable
{
    use HasFactory, AuditableTrait;

    protected $table = 'announcement_sphere_sub_sphere';
}
