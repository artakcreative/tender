<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;


class CompetitionCustomer extends Model implements Auditable
{
    use HasFactory, AuditableTrait;

    protected $guarded = ['id'];

    protected $auditInclude = [
        'name',
        'abbreviation',
        'affiliation_id',
    ];


    /**
     * @return BelongsTo
     */
    public function affiliation(): BelongsTo
    {
        return $this->BelongsTo(CompetitionCustomerAffiliation::class, 'affiliation_id');
    }

    /**
     * @return HasMany
     */
    public function announcement(): HasMany
    {
        return $this->hasMany(Announcement::class, 'customer_id');
    }
}
