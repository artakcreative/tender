<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompetitionTypeAndApplicationForm extends Model
{
    use HasFactory;
    protected $fillable = [
        'competition_type',
        'how_to_apply',
        'template_id'
    ];

    protected $casts = [
        'competition_type'=> 'array',
        'how_to_apply'=> 'array',
    ];
}
