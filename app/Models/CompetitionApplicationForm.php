<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompetitionApplicationForm extends Model
{
    use HasFactory;
    protected $fillable = [
      'name',
    ];

    public function announcement(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Announcement::class, 'how_to_apply_id');
    }
}
