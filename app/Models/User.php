<?php

namespace App\Models;

use App\Notifications\User\VerifyEmailQueued;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;
use function Symfony\Component\String\b;

class User extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, InteractsWithMedia;


    protected $guard = 'web';
    const BLOCK = 0;
    const ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'surname',
        'phone',
        'manager_id',
        'email_verified_at',
        'status',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'full_name'
    ];

    /**
     * @return BelongsTo
     */
    public function manager(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'manager_id');
    }


    /**
     * @return HasMany
     */
    public function userSpheres(): HasMany
    {
        return $this->hasMany(UserSphere::class);
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn(): string
    {
        return 'users.'.$this->id;
    }


    /**
     * @return HasOne
     */
    public function tariffActived()
    {
        return $this->hasOne(UserWantTariffAndActiveSelect::class);
    }


    /**
     * @return HasMany
     */
    public function planeHistory()
    {
        return $this->hasMany(ActivedPlaneHistory::class);
    }

    /**
     * @return HasOne
     */
    public function userFilter(): HasOne
    {
        return $this->hasOne(UserFilter::class);
    }


    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images')
            ->useFallbackUrl('/images/anonymous-user.jpg')
            ->useFallbackPath(public_path('/images/anonymous-user.jpg'));
    }


    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailQueued);
    }

    /*Append*/

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return ucfirst($this->name) . ' ' . ucfirst($this->surname);
    }


    /**
     * @return BelongsToMany
     */
    public function announcements(): BelongsToMany
    {
        return $this->belongsToMany(Announcement::class);
    }


    /**
     * @return HasOne
     */
    public function legalPerson()
    {
        return $this->hasOne(LegalPerson::class);
    }


    /**
     * @return HasOne
     */
    public function physicalPerson()
    {
        return $this->hasOne(PhysicalPerson::class);
    }



}
