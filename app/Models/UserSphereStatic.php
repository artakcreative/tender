<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSphereStatic extends Model
{
    use HasFactory;

    const WEEKLY = 1;
    const MONTHLY = 2;
    const QUARTERLY = 3;

    protected $guarded = ['id'];

    public function sphere()
    {
        return $this->belongsTo(CompetitionSphere::class,  'sphere_id', 'id');
    }
}
