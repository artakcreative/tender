<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserWantTariffAndActiveSelect extends Model
{
    use HasFactory;

    const BLOCKED = false;
    const ACTIVED = true;
    const OPEN = 1;
    const CLOSE = 0;

    protected $guarded = ['id'];

    public $timestamps = false;

    /**
     * @return HasOne
     */
    public function tariff(): HasOne
    {
        return $this->hasOne(Tariff::class, 'id', 'tariff_id');
    }
}
