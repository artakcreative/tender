<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    use HasFactory;
    const BASIC = 'Basic';
    const PRO = 'Pro';

    protected $guarded = ['id', 'name'];
    protected $fillable = ['price'];

}
