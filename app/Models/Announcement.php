<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Announcement extends Model implements HasMedia, Auditable
{
    use HasFactory, InteractsWithMedia, AuditableTrait;

    const SAVED = 0;
    const PUBLISHED = 1;
    const IS_ARCHIVED = 0;
    const DEARCHIVED = 1;
    const CREATEDNEW = 2;
    const BASIC = 'basic';

    protected $guarded = [];

    protected $casts = [
        'payment_schedule'=> 'array',
        'prepayment'=> 'array',
        'provides'=> 'array',
        'place_of_delivery'=>'array',
        'file'=>'array',
    ];

//    protected $auditInclude = [
//        'manager_id',
//        'announcement_name',
//        'announcement_password',
//        'competition_type_id',
//        'how_to_apply_id',
//        'customer_id',
//        'customer_affiliation_id',
//        'purchase_item',
//        'results_of_monitoring',
//        'estimated_price',
//        'application_deadline',
//        'return_auction_day',
//        'application_opening_day',
//        'deadline_for_supply',
//        'place_of_delivery',
//        'availability_of_financial_resources',
//        'payment_schedule',
//        'payment_schedule_for_pdf',
//        'prepayment',
//        'provides',
//        'other_conditions',
//        'doses',
//        'status',
//    ];
    /**
     * @var mixed
     */
    private $spheres;


    public function manager(): BelongsTo
    {
        return $this->belongsTo(Admin::class, 'manager_id');
    }

    /**
     * @return BelongsTo
     */
    public function competition_types(): BelongsTo
    {
        return $this->belongsTo(CompetitionType::class, 'competition_type_id');
    }

    /**
     * @return BelongsTo
     */
    public function customer_affiliations(): BelongsTo
    {
        return $this->belongsTo(CompetitionCustomerAffiliation::class, 'customer_affiliation_id');
    }

    /**
     * @return BelongsTo
     */
    public function application_forms(): BelongsTo
    {
        return $this->belongsTo(CompetitionApplicationForm::class, 'how_to_apply_id');
    }

    /**
     * @return BelongsTo
     */
    public function customers(): BelongsTo
    {
        return$this->belongsTo(CompetitionCustomer::class, 'customer_id');
    }

    /**
     * @return MorphToMany
     */
    public function spheres(): MorphToMany
    {
        return $this->morphedByMany(CompetitionSphere::class, 'sphere','announcement_sphere_sub_sphere')->withTimestamps();
    }

    /**
     * @return MorphToMany
     */
    public function sub_spheres(): MorphToMany
    {
        return $this->morphedByMany(CompetitionSubSphere::class, 'sphere','announcement_sphere_sub_sphere')->withTimestamps();
    }


    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }


     //scope

    /**
     * @param $query
     * @param $coordinates
     * @param int $radius
     * @return mixed
     */
    public function scopeIsWithinMaxDistance($query, $coordinates, $radius = 5)
    {
        $latitude = (float)$coordinates['latitude'][0];
        $longitude = (float)$coordinates['longitude'][0];

        return $query->select('*', DB::raw("(6373 * acos(cos(radians({$latitude})) * cos(radians(json_extract(`place_of_delivery`, '$.\"latitude\"')))
                * cos(radians(json_extract(`place_of_delivery`, '$.\"longitude\"'))
                - radians({$longitude}))
                + sin(radians({$latitude}))
                * sin(radians(json_extract(`place_of_delivery`, '$.\"latitude\"'))))) as distance"))
            ->having('distance', '<', $radius);
    }
}
