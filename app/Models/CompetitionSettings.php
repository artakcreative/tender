<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompetitionSettings extends Model
{
    use HasFactory;
    protected $fillable = [
        'competition_type',
        'how_to_apply',
        'customer_affiliation',
        'customers',
        'sphere',
        'admin_id',
        'template_id'
    ];

    protected $casts = [
        'competition_type'=> 'array',
        'how_to_apply'=> 'array',
        'customer_affiliation'=> 'array',
        'customers'=> 'array',
        'sphere'=>'array'
    ];

    public function admin(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }


}
