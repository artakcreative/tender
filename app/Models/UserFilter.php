<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFilter extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $casts = [
        'business_address'=>'array',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;


}
