<?php

namespace App\Providers;

use App\Models\ContactUs;
use App\Models\Profile\PageDescription;
use App\Models\Profile\PageInfoUs;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.layouts.includes.nav', function ($view) {
            $data = [];
            foreach (auth()->guard('admin')->user()->unreadNotifications as $key => $value) {
                $data[] = $value['data'];
            }
            $view->with(['notifications' => json_encode($data)]);
        });

        view()->composer('profile.layouts.includes.footer', function ($view) {
            $pageInfo = PageInfoUs::first();
            $view->with(['pageInfo' => $pageInfo]);

        });

        view()->composer('profile.layouts.includes.header', function ($view) {
            $pageDescription = PageDescription::first();
            $title = null;
            $description = null;
            if(isset($pageDescription )) {
                $title = $pageDescription['title'];
                $description = $pageDescription['description'];
            }
            $view->with(['title' => $title , 'description' => $description]);

        });

        $pageInfo = PageInfoUs::first();
        View::share('pageInfo', $pageInfo);

        $data = ContactUs::first();

        View::share('contactUs', $data);



        VerifyEmail::toMailUsing(function (User $user, string $verificationUrl) {
            $name = $user->name ?  $user->name . ' ' . $user->surname : $user->email;
            return (new MailMessage)
                ->subject('Մուտք համակարգ')
                ->line('Հարգելի ' . $name . ',')
                ->line('Բարի գալուստ '. request()->getHttpHost() .' համակարգ:')
                ->line('Գրանցումը ավարտելու համար խնդրում ենք հաստատել Ձեր էլ. հասցեն անցնելով հետևյալ հղումով`')
                ->action('Նույնականացում էլ.փոստի միջոցով', $verificationUrl)
                ->line('Եթե Դուք չեք գրացվել համակարգում, խնդրում եք անտեսել այս նամակը:');
        });


        ResetPassword::toMailUsing(function ($data, $token) {
            $routeName = Route::currentRouteName();
            if($routeName == 'password.email') {
                $route = '/reset-password/';
                $name = $data->name ?  $data->name . ' ' . $data->surname : $data->email;
                return (new MailMessage)
                    ->subject('Գաղտնաբառի վերականգնում')
                    ->greeting('Ողջույն,')
                    ->line('Հարգելի ' . $name)
                    ->line('Գաղտնաբառը վերականգնելու համար անցնեք հետևյալ հղումով՝')
                    ->action('Վերականգնել գաղտնաբառը', url($route . $data->email .'/'. $token))
                    ->line('կամ պատճենեք և տեղադրեք հետևյալ հղումը Ձեր բրաուզերում՝ ' .request()->getHttpHost())
                    ->line('Եթե այս նամակը սխալմամբ եք ստացել, խնդրում ենք  անտեսել այն։');
            } else {
                $route = '/admin/reset-password/';
                $name = $data->name ?  $data->name . ' ' . $data->surname : $data->email;
                return (new MailMessage)
                    ->subject('Գաղտնաբառի վերականգնում')
                    ->greeting('Ողջույն,')
                    ->line('Հարգելի ' . $name)
                    ->line('Գաղտնաբառը վերականգնելու համար անցնեք հետևյալ հղումով՝')
                    ->action('Վերականգնել գաղտնաբառը', url($route . $data->email .'/'. $token))
                    ->line('կամ պատճենեք և տեղադրեք հետևյալ հղումը Ձեր բրաուզերում՝ ' .request()->getHttpHost() . '/admin/login')
                    ->line('Եթե այս նամակը սխալմամբ եք ստացել, խնդրում ենք  անտեսել այն։');
            }

        });


    }

}
