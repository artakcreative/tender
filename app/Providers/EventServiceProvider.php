<?php

namespace App\Providers;

use App\Events\StaticUserSphereEvent;
use App\Listeners\LogSuccessfulLogin;
use App\Listeners\StaticUserSphereListener;
use App\Models\Announcement;
use App\Models\User;
use App\Observers\AnnouncementObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'Illuminate\Auth\Events\Login' => [
            LogSuccessfulLogin::class,
        ],
        StaticUserSphereEvent::class => [
            StaticUserSphereListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Announcement::observe(AnnouncementObserver::class);
        User::observe(UserObserver::class);
    }
}
