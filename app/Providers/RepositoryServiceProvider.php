<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\Admin\UserRepositoryInterface::class, \App\Repositories\Admin\UserRepository::class);
        $this->app->bind(\App\Repositories\Admin\ManagerRepositoryInterface::class, \App\Repositories\Admin\ManagerRepository::class);
        $this->app->bind(\App\Repositories\Admin\AdminRepositoryInterface::class, \App\Repositories\Admin\AdminRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionSetingsRepositoryInterface::class, \App\Repositories\Admin\CompetitionSetingsRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionSettingsRepositoryInterface::class, \App\Repositories\Admin\CompetitionSettingsRepository::class);
        $this->app->bind(\App\Repositories\Admin\Notification\NotificationRepositoryInterface::class, \App\Repositories\Admin\Notification\NotificationRepository::class);
        $this->app->bind(\App\Repositories\Admin\PermissionRepositoryInterface::class, \App\Repositories\Admin\PermissionRepository::class);
        $this->app->bind(\App\Repositories\Admin\Permission\PermissionRepositoryInterface::class, \App\Repositories\Admin\Permission\PermissionRepository::class);
        $this->app->bind(\App\Repositories\Admin\LoginHistory\LoginHistoryRepositoryInterface::class, \App\Repositories\Admin\LoginHistory\LoginHistoryRepository::class);
        $this->app->bind(\App\Repositories\Admin\Announcement\AnnouncementRepositoryInterface::class, \App\Repositories\Admin\Announcement\AnnouncementRepository::class);
        $this->app->bind(\App\Repositories\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormRepositoryInterface::class, \App\Repositories\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormRepositoryInterface::class, \App\Repositories\Admin\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepositoryInterface::class, \App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepositoryInterface::class, \App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionCustomer\CompetitionCustomerRepositoryInterface::class, \App\Repositories\Admin\CompetitionCustomer\CompetitionCustomerRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageHeaderRepositoryInterface::class, \App\Repositories\Admin\PageHeaderRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageHeader\PageHeaderRepositoryInterface::class, \App\Repositories\Admin\PageHeader\PageHeaderRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageAdvantage\PageAdvantageRepositoryInterface::class, \App\Repositories\Admin\PageAdvantage\PageAdvantageRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageConnect\PageConnnectRepositoryInterface::class, \App\Repositories\Admin\PageConnect\PageConnnectRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageConnect\PageConnectRepositoryInterface::class, \App\Repositories\Admin\PageConnect\PageConnectRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageAbout\PageAboutRepositoryInterface::class, \App\Repositories\Admin\PageAbout\PageAboutRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageMaterial\PageMaterialRepositoryInterface::class, \App\Repositories\Admin\PageMaterial\PageMaterialRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageInfo\PageInfoRepositoryInterface::class, \App\Repositories\Admin\PageInfo\PageInfoRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionSphere\CompetitionSphereRepositoryInterface::class, \App\Repositories\Admin\CompetitionSphere\CompetitionSphereRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryRepositoryInterface::class, \App\Repositories\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryRepositoryInterface::class, \App\Repositories\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryRepository::class);
        $this->app->bind(\App\Repositories\User\UserSphere\UserSphereRepositoryInterface::class, \App\Repositories\User\UserSphere\UserSphereRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionSubSphere\CompetitionSubSphereRepositoryInterface::class, \App\Repositories\Admin\CompetitionSubSphere\CompetitionSubSphereRepository::class);
        $this->app->bind(\App\Repositories\Admin\ContactUs\ContactUsRepositoryInterface::class, \App\Repositories\Admin\ContactUs\ContactUsRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionType\CompetitionTypeRepositoryInterface::class, \App\Repositories\Admin\CompetitionType\CompetitionTypeRepository::class);
        $this->app->bind(\App\Repositories\Admin\CompetitionApplicationForm\CompetitionApplicationFormRepositoryInterface::class, \App\Repositories\Admin\CompetitionApplicationForm\CompetitionApplicationFormRepository::class);
        $this->app->bind(\App\Repositories\User\Tariff\TariffRepositoryInterface::class, \App\Repositories\User\Tariff\TariffRepository::class);
        $this->app->bind(\App\Repositories\User\UserSphereStatic\UserSphereStaticRepositoryInterface::class, \App\Repositories\User\UserSphereStatic\UserSphereStaticRepository::class);
        $this->app->bind(\App\Repositories\Admin\DataProcessingPolicy\DataProcessingPolicyRepositoryInterface::class, \App\Repositories\Admin\DataProcessingPolicy\DataProcessingPolicyRepository::class);
        $this->app->bind(\App\Repositories\Admin\Tariff\TariffRepositoryInterface::class, \App\Repositories\Admin\Tariff\TariffRepository::class);
        $this->app->bind(\App\Repositories\Admin\PageDescription\PageDescriptionRepositoryInterface::class, \App\Repositories\Admin\PageDescription\PageDescriptionRepository::class);
        $this->app->bind(\App\Repositories\Admin\HelpDescription\HelpDescriptionRepositoryInterface::class, \App\Repositories\Admin\HelpDescription\HelpDescriptionRepository::class);
        //:end-bindings:
    }
}
