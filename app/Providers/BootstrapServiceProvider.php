<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BootstrapServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Services\Admin\UserServiceInterface::class, \App\Services\Admin\UserService::class);
        $this->app->bind(\App\Services\Admin\SettingsServiceInterface::class, \App\Services\Admin\SettingsService::class);
        $this->app->bind(\App\Services\Admin\ManagerServiceInterface::class, \App\Services\Admin\ManagerService::class);
        $this->app->bind(\App\Services\Admin\AdminServiceInterface::class, \App\Services\Admin\AdminService::class);
        $this->app->bind(\App\Services\Admin\Notification\NotificationServiceInterface::class, \App\Services\Admin\Notification\NotificationService::class);
        $this->app->bind(\App\Services\Admin\CompetitionSetingsServiceInterface::class, \App\Services\Admin\CompetitionSetingsService::class);
        $this->app->bind(\App\Services\Admin\CompetitionSettingsServiceInterface::class, \App\Services\Admin\CompetitionSettingsService::class);
        $this->app->bind(\App\Services\Admin\Statistic\StatisticServiceInterface::class, \App\Services\Admin\Statistic\StatisticService::class);
        $this->app->bind(\App\Services\Admin\LoginHistory\LoginHistoryServiceInterface::class, \App\Services\Admin\LoginHistory\LoginHistoryService::class);
        $this->app->bind(\App\Services\Admin\Announcements\AnnouncementsServiceInterface::class, \App\Services\Admin\Announcements\AnnouncementsService::class);
        $this->app->bind(\App\Services\Admin\Announcement\AnnouncementServiceInterface::class, \App\Services\Admin\Announcement\AnnouncementService::class);
        $this->app->bind(\App\Services\Admin\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormServiceInterface::class, \App\Services\Admin\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormService::class);
        $this->app->bind(\App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationServiceInterface::class, \App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationService::class);
        $this->app->bind(\App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationServiceInterface::class, \App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationService::class);
        $this->app->bind(\App\Services\Admin\CompetitionCustomer\CompetitionCustomerServiceInterface::class, \App\Services\Admin\CompetitionCustomer\CompetitionCustomerService::class);
        $this->app->bind(\App\Services\Admin\PageHeaderServiceInterface::class, \App\Services\Admin\PageHeaderService::class);
        $this->app->bind(\App\Services\Admin\PageHeader\PageHeaderServiceInterface::class, \App\Services\Admin\PageHeader\PageHeaderService::class);
        $this->app->bind(\App\Services\Admin\PageAdvantage\PageAdvantageServiceInterface::class, \App\Services\Admin\PageAdvantage\PageAdvantageService::class);
        $this->app->bind(\App\Services\Admin\PageConnect\PageConnnectServiceInterface::class, \App\Services\Admin\PageConnect\PageConnnectService::class);
        $this->app->bind(\App\Services\Admin\PageConnect\PageConnectServiceInterface::class, \App\Services\Admin\PageConnect\PageConnectService::class);
        $this->app->bind(\App\Services\Admin\PageAbout\PageAboutServiceInterface::class, \App\Services\Admin\PageAbout\PageAboutService::class);
        $this->app->bind(\App\Services\Admin\PageMaterial\PageMaterialServiceInterface::class, \App\Services\Admin\PageMaterial\PageMaterialService::class);
        $this->app->bind(\App\Services\Admin\PageInfo\PageInfoServiceInterface::class, \App\Services\Admin\PageInfo\PageInfoService::class);
        $this->app->bind(\App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface::class, \App\Services\Admin\CompetitionSphere\CompetitionSphereService::class);
        $this->app->bind(\App\Services\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryServiceInterface::class, \App\Services\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryService::class);
        $this->app->bind(\App\Services\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryServiceInterface::class, \App\Services\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryService::class);
        $this->app->bind(\App\Services\User\UserSphere\UserSphereServiceInterface::class, \App\Services\User\UserSphere\UserSphereService::class);
        $this->app->bind(\App\Services\Admin\CompetitionSubSphere\CompetitionSubSphereServiceInterface::class, \App\Services\Admin\CompetitionSubSphere\CompetitionSubSphereService::class);
        $this->app->bind(\App\Services\Admin\ContactUs\ContactUsServiceInterface::class, \App\Services\Admin\ContactUs\ContactUsService::class);
        $this->app->bind(\App\Services\CompetitionType\CompetitionTypeServiceInterface::class, \App\Services\CompetitionType\CompetitionTypeService::class);
        $this->app->bind(\App\Services\Admin\CompetitionType\CompetitionTypeServiceInterface::class, \App\Services\Admin\CompetitionType\CompetitionTypeService::class);
        $this->app->bind(\App\Services\Admin\CompetitionApplicationForm\CompetitionApplicationFormServiceInterface::class, \App\Services\Admin\CompetitionApplicationForm\CompetitionApplicationFormService::class);
        $this->app->bind(\App\Services\User\Tariff\TariffServiceInterface::class, \App\Services\User\Tariff\TariffService::class);
        $this->app->bind(\App\Services\User\UserSphereStatic\UserSphereStaticServiceInterface::class, \App\Services\User\UserSphereStatic\UserSphereStaticService::class);
        $this->app->bind(\App\Services\Admin\DataProcessingPolicy\DataProcessingPolicyServiceInterface::class, \App\Services\Admin\DataProcessingPolicy\DataProcessingPolicyService::class);
        $this->app->bind(\App\Services\Admin\Tariff\TariffServiceInterface::class, \App\Services\Admin\Tariff\TariffService::class);
        $this->app->bind(\App\Services\PageDescription\PageDescriptionServiceInterface::class, \App\Services\PageDescription\PageDescriptionService::class);
        $this->app->bind(\App\Services\Admin\PageDescription\PageDescriptionServiceInterface::class, \App\Services\Admin\PageDescription\PageDescriptionService::class);
        $this->app->bind(\App\Services\Admin\HelpDescription\HelpDescriptionServiceInterface::class, \App\Services\Admin\HelpDescription\HelpDescriptionService::class);
        //:end-bindings:
    }
}
