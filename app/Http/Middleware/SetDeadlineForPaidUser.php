<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class SetDeadlineForPaidUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $id = (int) $request->route()->parameters()['id'];
        $userId = User::find($id)->manager_id;
        if (auth()->guard('admin')->user()->hasRole('admin')
            || auth()->guard('admin')->user()->id == $userId
            || auth()->guard('admin')->user()->hasDirectPermission('Set a deadline for paid user service')){
            return $next($request);
        }

        return redirect()->back();
    }
}
