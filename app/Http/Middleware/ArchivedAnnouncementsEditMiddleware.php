<?php

namespace App\Http\Middleware;

use App\Models\Announcement;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class ArchivedAnnouncementsEditMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return void
     */
    public function handle(Request $request, Closure $next)
    {
        $id = (int) $request->route()->parameters()['id'];
        $announcement = Announcement::where('id', $id)->first();
        if ($announcement->application_deadline < Carbon::now()->format('Y-m-d H:i:s')){
            return redirect()->back();
        }

        return $next($request);
    }
}
