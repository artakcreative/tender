<?php

namespace App\Http\Middleware\Admin;

use App\Models\Announcement;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CanEditPublishedAnnouncmentsMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $id = (int) $request->route()->parameters()['id'];
        $managerId = Announcement::find($id)->manager_id;

        if(!Auth::guard('admin')->user()->hasRole('admin') && !Auth::guard('admin')->user()->hasDirectPermission('Edit approved statement')
            && Auth::guard('admin')->user()->id != $managerId){
            return redirect()->back();
        }
        return $next($request);
    }
}
