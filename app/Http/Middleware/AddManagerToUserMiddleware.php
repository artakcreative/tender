<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AddManagerToUserMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::guard('admin')->user()->hasRole('admin') && !Auth::guard('admin')->user()->hasDirectPermission('Attach user manager')){
            return redirect()->back();
        }
        return $next($request);
    }

}
