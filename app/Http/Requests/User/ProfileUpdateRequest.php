<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'public_service_license_plate' => 'required_with:physical_success',
            'passport_number' => 'required_with:physical_success',
            'passport_take_date' => 'required_with:physical_success',
            'passport_take_from' => 'required_with:physical_success',
            'tin' => 'required_with:legal_success',
            'legal_address' => 'required_with:legal_success',
            'bank' => 'required_with:legal_success',
            'bank_account' => 'required_with:legal_success',
        ];
    }
}
