<?php

namespace App\Http\Requests\User;
use Illuminate\Foundation\Http\FormRequest;

class RegistracionRequset extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'phone' => 'required|string',
            'password'=> 'required',
            'password_confirmation'=> 'required|same:password',
            'checkbox' => 'required|in:true',
        ];
    }
}
