<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required|min:3',
            'surname'=>'required|min:4',
            'phone'=>'required',
            'password' => 'sometimes|required'
        ];

        if($this->getMethod() == 'POST'){
            $rules += ['email'=>'required|email|unique:App\Models\User,email'];
        }elseif($this->getMethod() == 'PUT'){
            $rules +=['email'=>'required|email'];
        }
        return $rules;
    }
}
