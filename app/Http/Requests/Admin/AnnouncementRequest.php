<?php

namespace App\Http\Requests\Admin;

use App\Rules\SubsphereRequiredIf;
use Illuminate\Auth\Events\Validated;
use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'announcement_name' => 'required',
            'announcement_password' => 'required',
            'competition_type' => 'required',
            'customer_affiliation' => 'required',
            'how_to_apply' => 'required',
            'customer' => 'required',
            'application_deadline' =>'required',
            'sphere-group.*.sphere' => 'required',
//            'sphere-group.*.subsphere' => 'required',
            'estimated_price' => 'required|numeric|',
            'prepayment.dram_or_percent' => 'numeric'
        ];
    }
}
