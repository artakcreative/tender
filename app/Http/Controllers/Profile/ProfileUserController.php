<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\UserSphereStatic;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use App\Services\Admin\UserServiceInterface;
use App\Services\User\UserSphereStatic\UserSphereStaticServiceInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileUserController extends Controller
{
    /**
     * @var UserServiceInterface
     */
    private $userService;
    /**
     * @var AnnouncementServiceInterface
     */
    private $announcementService;
    /**
     * @var UserSphereStaticServiceInterface
     */
    private $userSphereStaticService;

    /**
     * @param UserServiceInterface $userService
     * @param AnnouncementServiceInterface $announcementService
     * @param UserSphereStaticServiceInterface $userSphereStaticService
     */
    public function __construct(UserServiceInterface $userService,
                                AnnouncementServiceInterface $announcementService,
                                UserSphereStaticServiceInterface $userSphereStaticService)
    {
        $this->userService = $userService;
        $this->announcementService = $announcementService;
        $this->userSphereStaticService = $userSphereStaticService;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('profile.personal');
    }


    /**
     * @return Factory|View
     */
    public function report()
    {
        return view('profile.pages.report');
    }


    /**
     * @return Factory|View
     */
    public function saved()
    {
        $id = auth_user()->id;
        $user = $this->userService->getUserById($id);
        $user_announcements = $user->announcements->sortBy('application_deadline');
        return view('profile.other.saved', compact('user_announcements'));
    }


    /**
     * @param Request $request
     * @return JsonResponse|mixed
     */
    public function reportStatistic(Request $request)
    {
        $userId = auth_user()->id;
        if (isset($request->type)) {
            if ($request->type == UserSphereStatic::MONTHLY) {
                return response()->json($this->userSphereStaticService->getStatisticMount($userId, $request->all()));
            } elseif ($request->type == UserSphereStatic::WEEKLY) {
                return $this->userSphereStaticService->getStatisticWeek($userId, $request->all());
            } else {
                return $this->userSphereStaticService->getStatisticQuarter($userId, $request->all());
            }
        }
        return response()->json($this->userSphereStaticService->getStatisticDefault($userId));
    }

}
