<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\SectorRequest;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface;
use App\Services\User\UserSphere\UserSphereServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CompetitionController extends Controller
{


    /**
     * @var CompetitionSphereServiceInterface
     */
    private $competitionSphereService;

    /**
     * @var AnnouncementServiceInterface
     */
    private $announcementService;
    /**
     * @var UserSphereServiceInterface
     */
    private $userSphereService;


    /**
     * CompetitionController constructor.
     * @param CompetitionSphereServiceInterface $competitionSphereService
     * @param AnnouncementServiceInterface $announcementService
     * @param UserSphereServiceInterface $userSphereService
     */
    public function __construct(CompetitionSphereServiceInterface $competitionSphereService,
                                AnnouncementServiceInterface $announcementService,
                                UserSphereServiceInterface $userSphereService)
    {
        $this->competitionSphereService = $competitionSphereService;
        $this->announcementService = $announcementService;
        $this->userSphereService = $userSphereService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $userSpheres = $this->userSphereService->getUserSpheres((int)$request->user_id);
        if (count($userSpheres) == 0) {
            return  response()->json(['error', $this->competitionSphereService->getAllSphereCategories()]);
        }

        return response()->json(['success', 'Օգտատերը ընտրված ոլորտ ունի']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getSubcategories(Request $request)
    {
        $value = $this->competitionSphereService->getSubSphereCategories($request->id);
        if(count($value) == 0) {
            return response()->json(['sphere_id', $request->id]);
        }
        $html = view('profile.components.checkbox', compact('value'))->render();
        return $html;
    }


    /**
     * @param SectorRequest $request
     * @return mixed
     */
    public function saveSphere(SectorRequest $request)
    {
        return $this->userSphereService->saveUserFirstSphere($request->all());
    }

}
