<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface;
use App\Services\Admin\PageAbout\PageAboutServiceInterface;
use App\Services\Admin\PageAdvantage\PageAdvantageServiceInterface;
use App\Services\Admin\PageConnect\PageConnectServiceInterface;
use App\Services\Admin\PageHeader\PageHeaderServiceInterface;
use App\Services\Admin\PageInfo\PageInfoServiceInterface;
use App\Services\Admin\PageMaterial\PageMaterialServiceInterface;
use App\Services\User\Tariff\TariffServiceInterface;
use Illuminate\Http\Request;

class ProfileProjectController extends Controller
{
    /**
     * @var PageHeaderServiceInterface
     */
    protected $pageHeaderService;
    protected $pageAdvantageService;
    protected $pageConnectService;
    protected $pageAboutService;
    protected $pageMaterialService;
    protected $pageInfoService;
    /**
     * @var CompetitionSphereServiceInterface
     */
    private $competitionSphereService;
    /**
     * @var AnnouncementServiceInterface
     */
    private $announcementService;
    /**
     * @var TariffServiceInterface
     */
    private $tariffService;


    /**
     * ProfileProjectController constructor.
     * @param PageHeaderServiceInterface $pageHeaderService
     * @param PageAdvantageServiceInterface $pageAdvantageService
     * @param PageConnectServiceInterface $pageConnectService
     * @param PageAboutServiceInterface $pageAboutService
     * @param PageMaterialServiceInterface $pageMaterialService
     * @param PageInfoServiceInterface $pageInfoService
     * @param CompetitionSphereServiceInterface $competitionSphereService
     * @param AnnouncementServiceInterface $announcementService
     * @param TariffServiceInterface $tariffService
     */
    public function __construct(PageHeaderServiceInterface $pageHeaderService,
                                PageAdvantageServiceInterface $pageAdvantageService,
                                PageConnectServiceInterface $pageConnectService,
                                PageAboutServiceInterface $pageAboutService,
                                PageMaterialServiceInterface $pageMaterialService,
                                PageInfoServiceInterface $pageInfoService,
                                CompetitionSphereServiceInterface $competitionSphereService,
                                AnnouncementServiceInterface $announcementService,
                                TariffServiceInterface $tariffService)
    {
        $this->pageHeaderService = $pageHeaderService;
        $this->pageAdvantageService = $pageAdvantageService;
        $this->pageConnectService = $pageConnectService;
        $this->pageAboutService = $pageAboutService;
        $this->pageMaterialService = $pageMaterialService;
        $this->pageInfoService = $pageInfoService;
        $this->competitionSphereService = $competitionSphereService;
        $this->announcementService = $announcementService;
        $this->tariffService = $tariffService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $pageHeader = $this->pageHeaderService->getHeader();
        $pageAdvantages = $this->pageAdvantageService->getAdvantages();
        $pageConnects = $this->pageConnectService->getConnects();
        $pageAbouts = $this->pageAboutService->getAbouts();
        $pageMaterials = $this->pageMaterialService->getMaterials();
        $pageInfo = $this->pageInfoService->getInfo();
        $spheres = $this->competitionSphereService->getAllSphereForSelect();
        $activeAnnouncementsCount = $this->announcementService->getAllActiveAnnouncementsCount();
        $archivedAnnouncementsCount =  $this->announcementService->getAllArchivedAnnouncementsCount();
        $tariffs = $this->tariffService->getTariffs();
        return view('profile.home', compact('pageHeader',
            'pageAdvantages', 'pageConnects', 'pageAbouts','pageMaterials', 'pageInfo', 'spheres',
                     'activeAnnouncementsCount', 'archivedAnnouncementsCount', 'tariffs'));
    }
}
