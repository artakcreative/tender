<?php

namespace App\Http\Controllers\Profile\Personal;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\SpheresRequest;
use App\Models\UserWantTariffAndActiveSelect;
use App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface;
use App\Services\Admin\Notification\NotificationServiceInterface;
use App\Services\Admin\UserServiceInterface;
use App\Services\User\UserSphere\UserSphereServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{

    /**
     * @var CompetitionSphereServiceInterface
     */
    private $competitionSphereService;
    /**
     * @var UserSphereServiceInterface
     */
    private $userSphereService;
    /**
     * @var UserServiceInterface
     */
    private $userService;
    /**
     * @var NotificationServiceInterface
     */
    private $notificationService;


    /**
     * SettingsController constructor.
     * @param CompetitionSphereServiceInterface $competitionSphereService
     * @param UserSphereServiceInterface $userSphereService
     * @param UserServiceInterface $userService
     * @param NotificationServiceInterface $notificationService
     */
    public function __construct(CompetitionSphereServiceInterface $competitionSphereService,
                                UserSphereServiceInterface $userSphereService,
                                UserServiceInterface $userService,
                                NotificationServiceInterface $notificationService)
{
    $this->competitionSphereService = $competitionSphereService;
    $this->userSphereService = $userSphereService;
    $this->userService = $userService;
    $this->notificationService = $notificationService;
}

    /**
     * @param $map
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($map = null)
    {
        $id = auth()->guard('web')->user()->id;
        $actived =  $this->userSphereService->activeSelected();
        $tariffs =  $this->userSphereService->tariffs();
        $filter = $this->userService->getUserFilter($id);
        return view('profile.pages.settings', compact('actived', 'tariffs', 'filter', 'map'));
    }

    /**
     * @return mixed
     */
    public function getTariff()
    {
        if(! $this->userSphereService->getTariff()[0]) {
            return response()->json(['error'  => $this->userSphereService->getTariff()[1]]);
        }
        return response()->json($this->userSphereService->getTariff()[0]);
    }

    /**
     * @return mixed
     */
    public function getPayedSelectedSpheres()
    {
        $userId = Auth::guard('web')->user()->id;
        return $this->userSphereService->getSelectedSpheres($userId);
    }

    /**
     * @return mixed
     */
    public function getSelectedSpheres()
    {
        $userId = Auth::guard('web')->user()->id;
        return $this->userSphereService->getNotPayed($userId);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpheres()
    {
        return response()->json($this->competitionSphereService->getAllSphereCategories());
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteNotPayed(Request $request)
    {
        $this->userSphereService->deleteNotPayed($request->all());
        return response()->json('success', 200);
    }


    /**
     * @param SpheresRequest $request
     * @return mixed
     */
    public function save(SpheresRequest $request)
    {
        $data = $request->all();
        $user = $this->userService->getUserById((int)$data['user_id']);
        if($user['manager_id']) {
            $subject = 'Օգտատիրոջ ոլորտների փոփոխություն';
            if($user['name']) {
                $description = $user['name']. ' ' .$user['surname'].'ը փոփոխել կամ ավելացրել է ոլորտներ:';
            }else{
                $description = $user['email']. '-ը փոփոխել կամ ավելացրել է ոլորտներ:';
            }
            $this->notificationService->sendNotification((int)$user['manager_id'], $subject, $description, (int)$data['user_id']);
        }
        $this->userSphereService->save($data);
        return response()->json(__('message.update_person_spheres'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function filterSave(Request $request)
    {
        $this->userSphereService->filterSave($request->all());
        return back()->with('success', __('message.update_person_filter'));
    }
}
