<?php

namespace App\Http\Controllers\Profile\Personal;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\PasswordUpdateRequest;
use App\Http\Requests\User\ProfileUpdateRequest;
use App\Models\Admin;
use App\Models\User;
use App\Services\Admin\AdminServiceInterface;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use App\Services\Admin\Notification\NotificationServiceInterface;
use App\Services\Admin\UserServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PersonSettingsController extends Controller
{

    /**
     * @var UserServiceInterface
     */
    private $userService;
    /**
     * @var AnnouncementServiceInterface
     */
    private $announcementService;
    /**
     * @var AdminServiceInterface
     */
    private $adminService;
    /**
     * @var NotificationServiceInterface
     */
    private $notificationService;

    /**
     * @param UserServiceInterface $userService
     * @param AnnouncementServiceInterface $announcementService
     * @param AdminServiceInterface $adminService
     * @param NotificationServiceInterface $notificationService
     */
    public function __construct(UserServiceInterface $userService,
                                AnnouncementServiceInterface $announcementService,
                                AdminServiceInterface $adminService,
                                NotificationServiceInterface $notificationService)
    {
        $this->userService = $userService;
        $this->announcementService = $announcementService;
        $this->adminService = $adminService;
        $this->notificationService = $notificationService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $id = Auth::guard('web')->user()->id;
        $user = User::where('id', $id )->with(['legalPerson', 'physicalPerson'])->first();
        $admin = $this->adminService->getById(Admin::DEAFULD_ID);;
        return view('profile.pages.person_settings', compact('user', 'admin'));
    }

    /**
     * @param PasswordUpdateRequest $request
     * @return mixed
     */
    public function passwordUpdate(PasswordUpdateRequest $request)
    {
        return $this->userService->updatePassword($request->all());
    }

    /**
     * @param ProfileUpdateRequest $request
     * @return mixed
     */
    public function profileUpdate(ProfileUpdateRequest $request)
    {
        $user = $this->userService->getUserById((int)$request->id);

        if($user['manager_id']) {
            $subject = 'Օգտատիրոջ անձնական տվյալների փոփոխություն';
            $description = isset($user['name']) && isset( $user['surname']) ?  $user['name'] . ' ' .  $user['surname'].' օգտատիրոջ անձնական տվյալները փոփոխվել են:'
                            : $user['email'] . ' օգտատիրոջ անձնական տվյալները փոփոխվել են:';
            $this->notificationService->sendNotification((int)$user['manager_id'], $subject, $description, (int)$request->id);
        }
        $this->userService->updateDataFromUser($request->id, $request->all());
        return response()->json(['success', 'Ձեր տվյալները հաջողությամբ թարմացվել են']);
    }

    /**
     * @param $media_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyFile($media_id){
        $this->announcementService->deleteFile(+$media_id);
        return response()->json([
            'success' => true
        ]);
    }

}


