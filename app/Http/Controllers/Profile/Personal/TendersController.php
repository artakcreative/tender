<?php

namespace App\Http\Controllers\Profile\Personal;

use App\Http\Controllers\Controller;
use App\Models\UserTemporaryFilter;
use App\Repositories\Admin\CompetitionSphere\CompetitionSphereRepository;
use App\Repositories\Admin\CompetitionSphere\CompetitionSphereRepositoryInterface;
use App\Services\Admin\Announcement\AnnouncementService;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use App\Services\Admin\CompetitionCustomer\CompetitionCustomerServiceInterface;
use App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface;
use App\Services\Admin\UserServiceInterface;
use App\Services\User\UserSphere\UserSphereServiceInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TendersController extends Controller
{

    /**
     * @var UserSphereServiceInterface
     */
    private $userSphereService;
    /**
     * @var AnnouncementServiceInterface
     */
    private $announcementService;
    /**
     * @var CompetitionSphereServiceInterface
     */
    private $sphereService;
    /**
     * @var CompetitionCustomerServiceInterface
     */
    private $competitionCustomerService;
    /**
     * @var UserServiceInterface
     */
    private $userService;

    /**
     * TendersController constructor.
     * @param UserSphereServiceInterface $userSphereService
     * @param AnnouncementServiceInterface $announcementService
     * @param CompetitionSphereServiceInterface $sphereService
     * @param CompetitionCustomerServiceInterface $competitionCustomerService
     * @param UserServiceInterface $userService
     */
    public function __construct(UserSphereServiceInterface $userSphereService,
                                AnnouncementServiceInterface $announcementService,
                                CompetitionSphereServiceInterface $sphereService,
                                CompetitionCustomerServiceInterface $competitionCustomerService,
                                UserServiceInterface $userService)
    {

        $this->userSphereService = $userSphereService;
        $this->announcementService = $announcementService;
        $this->sphereService = $sphereService;
        $this->competitionCustomerService = $competitionCustomerService;
        $this->userService = $userService;
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $filter = $request->all();
        $success = false;
        $id = auth()->guard('web')->user()->id;
        if (isset($request->my_filter)) {
            $myFilter = $this->userService->getUserFilter($id);
            $filter = array_merge($filter, $myFilter );
            $success = true;
        }
        $mapSuccess = $this->userService->mapSuccess($id);
        $tariff = $this->userSphereService->getTariff()[1];

        $active =  $this->announcementService->active($id, $tariff, $filter );
        $archive = $this->announcementService->archive($id, $tariff, $filter);
        $spheres =  $this->sphereService->getUserSpheres($id);
        $customer = $this->competitionCustomerService->allForFilter();
        $user = $this->userService->getUserById($id);
        $user_announcements = $user->announcements;
        $tariff_success = $this->userSphereService->getUserTariffTrueOrFalse($id);
        $temporaryFilter = UserTemporaryFilter::where('user_id', (int) $id)->first();
        if($temporaryFilter) {
            $userTemporaryFilter = $temporaryFilter->toArray();
        }else {
            $userTemporaryFilter = [];
        }
        return view('profile.pages.tenders', compact('active', 'archive', 'tariff', 'spheres', 'customer', 'mapSuccess','user_announcements', 'success', 'tariff_success', 'userTemporaryFilter'));

    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function details(Request $request)
    {
        $details = $this->announcementService->getById((int)$request->id);
        return view('profile.pages.sub_pages.details', compact('details'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function  addDeleteFavoriteTender(Request $request): JsonResponse
    {
        $data = $request->all();
        return response()->json($this->userService->addDeleteFavoriteTender($data)) ;
    }
}
