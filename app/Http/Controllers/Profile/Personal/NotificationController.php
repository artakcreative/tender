<?php

namespace App\Http\Controllers\Profile\Personal;

use App\Http\Controllers\Controller;
use App\Services\Admin\Notification\NotificationServiceInterface;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    /**
     * @var NotificationServiceInterface
     */
    protected $notificationService;

    /**
     * Notification constructor.
     * @param NotificationServiceInterface $notificationService
     */
    public function __construct(NotificationServiceInterface $notificationService)
    {
        $this->notificationService = $notificationService;
    }


        /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $userId = auth()->guard('web')->user()->id;
        $notification = $this->notificationService->getAllUserNotify($userId);
        return view('profile.other.notification', compact('notification'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function delete(Request $request)
    {
        return $this->notificationService->deleteUserNotify($request->id, $request->user_id);
    }


    /**
     * @return mixed
     */
    public function notRead()
    {
        $userId = auth()->guard('web')->user()->id;
        return $this->notificationService->notRead($userId);
    }
}
