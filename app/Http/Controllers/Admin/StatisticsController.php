<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserSphereStatic;
use App\Services\Admin\Statistic\StatisticServiceInterface;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    /**
     * @var StatisticServiceInterface
     */
    protected $statisticService;

    /**
     * StatisticsController constructor.
     * @param StatisticServiceInterface $statisticService
     */
    public function __construct(StatisticServiceInterface $statisticService)
    {
        $this->statisticService = $statisticService;
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getUsers()
    {
        return view('admin.statistics.user');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersData(Request $request)
    {
        return response()->json($this->statisticService->getUsers($request->search));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserStatistic(Request $request)
    {
        if ($request->type == UserSphereStatic::MONTHLY) {
            return response()->json($this->statisticService->getUserStatisticMount($request->all(), User::class));
        } elseif ($request->type == UserSphereStatic::WEEKLY) {
            return response()->json($this->statisticService->getUserStatisticWeek($request->all(), User::class));
        } else {
            return response()->json($this->statisticService->getUserStatisticQuarter($request->all(), User::class));
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getManagers()
    {
        return view('admin.statistics.manager');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getManagersData(Request $request)
    {
        return response()->json($this->statisticService->getManagers($request->search));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getManagerStatistic(Request $request)
    {
        if ($request->type == UserSphereStatic::MONTHLY) {
            return  response()->json($this->statisticService->getManagerStatisticMount($request->all()));
        } elseif ($request->type == UserSphereStatic::WEEKLY) {
            return  response()->json($this->statisticService->getManagerStatisticWeek($request->all()));
        } else {
            return  response()->json($this->statisticService->getManagerStatisticQuarter($request->all()));

        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getAnnouncements()
    {
        return view('admin.statistics.announcement');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnnouncementStatistic(Request $request)
    {
        if ($request->type == UserSphereStatic::MONTHLY) {
            return response()->json($this->statisticService->getAnnouncementStatisticMount($request->all()));
        } elseif ($request->type == UserSphereStatic::WEEKLY) {
            return response()->json($this->statisticService->getAnnouncementStatisticWeek($request->all()));
        } else {
            return response()->json($this->statisticService->getAnnouncementStatisticQuarter($request->all()));
        }

    }

}
