<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompetitionCustomerRequest;
use App\Services\Admin\AdminServiceInterface;
use App\Services\Admin\CompetitionCustomer\CompetitionCustomerServiceInterface;
use App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationServiceInterface;
use App\Services\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryServiceInterface;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;

class CompetitionCustomerController extends Controller
{
    /**
     * @var CompetitionCustomerServiceInterface
     */
    private $competitionCustomerService;
    /**
     * @var CompetitionCustomerAffiliationServiceInterface
     */
    private $competitionCustomerAffiliationService;
    /**
     * @var CompetitionCustomerHistoryServiceInterface
     */
    private $competitionCustomerHistoryService;


    /**
     * @param CompetitionCustomerServiceInterface $competitionCustomerService
     * @param CompetitionCustomerAffiliationServiceInterface $competitionCustomerAffiliationService
     * @param CompetitionCustomerHistoryServiceInterface $competitionCustomerHistoryService
     */
    public function __construct(CompetitionCustomerServiceInterface $competitionCustomerService, CompetitionCustomerAffiliationServiceInterface $competitionCustomerAffiliationService, CompetitionCustomerHistoryServiceInterface $competitionCustomerHistoryService){
        $this->competitionCustomerService = $competitionCustomerService;
        $this->competitionCustomerAffiliationService = $competitionCustomerAffiliationService;
        $this->competitionCustomerHistoryService = $competitionCustomerHistoryService;
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(){
        $customerAffiliations = $this->competitionCustomerAffiliationService->getAll();
        return view('admin.competition_settings.customer.customer_create', compact('customerAffiliations'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CompetitionCustomerRequest $request){
        $data = $request->all();
        $this->competitionCustomerService->saveCustomer($data);
        return redirect()->route('admin.customer')->with('success', __('message.customer_create'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id){
        $customerAffiliations = $this->competitionCustomerAffiliationService->getAll();
        $customer = $this->competitionCustomerService->getCustomerById($id);
        foreach ($customerAffiliations as $key => $customerAffiliation) {
            if($customerAffiliation->customer_affiliation == $customer->affiliation->customer_affiliation) {
                unset($customerAffiliations[$key]);
            }
        }
        return view('admin.competition_settings.customer.customer_edit', compact('customerAffiliations', 'customer'));
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->competitionCustomerService->remove((int) $id);
        return back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CompetitionCustomerRequest $request, $id){
        $data = $request->all();
        $this->competitionCustomerService->updateCustomer($id, $data);
        return redirect()->route('admin.customer')->with('success', __('message.customer_update'));
    }

    /**
     * история пользователей
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function customerHistory(){
        $customersHistories =  $this->competitionCustomerHistoryService->getCustomerHistory();
         return view('admin.competition_settings.customer.history', compact('customersHistories'));
    }

    /**
     * выбираем клиента по поиску
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomerData(Request $request)
    {
        return response()->json($this->competitionCustomerService->getCustomer($request->search));
    }
}
