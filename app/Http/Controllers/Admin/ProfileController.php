<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Profile\AdvantageRequest;
use App\Http\Requests\Admin\Profile\PageAboutRequest;
use App\Http\Requests\Admin\Profile\PageConnectRequest;
use App\Http\Requests\Admin\Profile\PageHeaderRequest;
use App\Http\Requests\Admin\Profile\PageInfoRequest;
use App\Http\Requests\Admin\Profile\PageMaterialRequest;
use App\Services\Admin\PageAbout\PageAboutServiceInterface;
use App\Services\Admin\PageAdvantage\PageAdvantageServiceInterface;
use App\Services\Admin\PageConnect\PageConnectServiceInterface;
use App\Services\Admin\PageDescription\PageDescriptionServiceInterface;
use App\Services\Admin\PageHeader\PageHeaderServiceInterface;
use App\Services\Admin\PageInfo\PageInfoServiceInterface;
use App\Services\Admin\PageMaterial\PageMaterialServiceInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    /**
     * @var PageHeaderServiceInterface
     */
    protected $pageHeaderService;
    protected $pageAdvantageService;
    protected $pageConnectService;
    protected $pageAboutService;
    protected $pageMaterialService;
    protected $pageInfoService;
    /**
     * @var PageDescriptionServiceInterface
     */
    private $pageDescriptionService;

    /***
     * ProfileController constructor.
     * @param PageHeaderServiceInterface $pageHeaderService
     * @param PageAdvantageServiceInterface $pageAdvantageService
     * @param PageConnectServiceInterface $pageConnectService
     * @param PageAboutServiceInterface $pageAboutService
     * @param PageMaterialServiceInterface $pageMaterialService
     * @param PageInfoServiceInterface $pageInfoService
     * @param PageDescriptionServiceInterface $pageDescriptionService
     */
    public  function __construct(PageHeaderServiceInterface $pageHeaderService,
                                 PageAdvantageServiceInterface $pageAdvantageService,
                                 PageConnectServiceInterface $pageConnectService,
                                 PageAboutServiceInterface $pageAboutService,
                                 PageMaterialServiceInterface $pageMaterialService,
                                 PageInfoServiceInterface $pageInfoService,
                                 PageDescriptionServiceInterface $pageDescriptionService)
    {

        $this->pageHeaderService = $pageHeaderService;
        $this->pageAdvantageService = $pageAdvantageService;
        $this->pageConnectService = $pageConnectService;
        $this->pageAboutService = $pageAboutService;
        $this->pageMaterialService = $pageMaterialService;
        $this->pageInfoService = $pageInfoService;
        $this->pageDescriptionService = $pageDescriptionService;
    }

    /**
     * @return Factory|View
     */
    public function header()
    {
        $pageHeader = $this->pageHeaderService->getHeader();
        return view('admin.profile.header', compact('pageHeader'));
    }


    /**
     * @param PageHeaderRequest $request
     * @return RedirectResponse
     */
    public function headerCreate(PageHeaderRequest $request): RedirectResponse
    {
       $data = $request->all();
       $this->pageHeaderService->headerCreate($data);
        return back()->with('success', __('message.create_post'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function headerDelete(Request $request)
    {
        $this->pageHeaderService->headerDelete($request->id);
        return back()->with('success', __('message.delete_post'));
    }

    /**
     * @return Factory|View
     */
    public function advantage()
    {
        $pageAdvantages = $this->pageAdvantageService->getAdvantages();
        return view('admin.profile.advantage',compact('pageAdvantages'));
    }

    /**
     * @param AdvantageRequest $request
     * @return RedirectResponse
     */
    public function advantageCreate(AdvantageRequest $request): RedirectResponse
    {
        $this->pageAdvantageService->advantageCreate($request->all());
        return back()->with('success', __('message.create_post'));
    }


    /**
     * @param $id
     * @return Factory|View
     */
    public function advantageEdit($id)
    {
        $pageAdvantage = $this->pageAdvantageService->getAdvantage($id);
        return view('admin.profile.edit.advantage-update',compact('pageAdvantage'));
    }

    /**
     * @param AdvantageRequest $request
     * @return RedirectResponse
     */
    public function advantageUpdate(AdvantageRequest $request): RedirectResponse
    {
        $this->pageAdvantageService->advantageUpdate($request->all());
        return redirect()->route('admin.profile.advantage');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function advantageDelete(Request $request)
    {
        $this->pageAdvantageService->advantageDelete($request->id);
        return back()->with('success', __('message.delete_post'));
    }


    /**
     * @return Factory|View
     */
    public function connect()
    {
        $pageConnects = $this->pageConnectService->getConnects();
        return view('admin.profile.connect', compact('pageConnects'));
    }


    /**
     * @param PageConnectRequest $request
     * @return RedirectResponse
     */
    public function connectCreate(PageConnectRequest $request)
    {
       $this->pageConnectService->connectCreate($request->all());
       return back()->with('success', __('message.create_post'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function connectEdit($id)
    {
        $pageConnect= $this->pageConnectService->getConnect($id);
        return view('admin.profile.edit.connect-update',compact('pageConnect'));
    }


    /**
     * @param PageConnectRequest $request
     * @return RedirectResponse
     */
    public function connectUpdate(PageConnectRequest $request)
    {
        $this->pageConnectService->connectUpdate($request->all());
        return redirect()->route('admin.profile.connect');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function connectDelete(Request $request)
    {
        $this->pageConnectService->connectDelete($request->id);
        return back()->with('success', __('message.delete_post'));
    }


    /**
     * @return Factory|View
     */
    public function about()
    {
        $pageAbouts = $this->pageAboutService->getAbouts();
        return view('admin.profile.about', compact('pageAbouts'));
    }


    /**
     * @param PageAboutRequest $request
     * @return RedirectResponse
     */
    public function aboutCreate(PageAboutRequest $request)
    {
        $this->pageAboutService->aboutCreate($request->all());
        return back()->with('success', __('message.create_worker'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function aboutEdit($id)
    {
        $pageAbout= $this->pageAboutService->getAbout($id);
        return view('admin.profile.edit.about-update',compact('pageAbout'));
    }


    /**
     * @param PageAboutRequest $request
     * @return RedirectResponse
     */
    public function aboutUpdate(PageAboutRequest $request)
    {
        $this->pageAboutService->aboutUpdate($request->all());
        return redirect()->route('admin.profile.about');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function aboutDelete(Request $request): RedirectResponse
    {
        $this->pageAboutService->aboutDelete($request->id);
        return back()->with('success', __('message.delete_worker'));
    }


    /**
     * @return Factory|View
     */
    public function material()
    {
        $pageMaterials = $this->pageMaterialService->getMaterials();
        return view('admin.profile.material', compact('pageMaterials'));
    }


    /**
     * @param PageMaterialRequest $request
     * @return RedirectResponse
     */
    public function materialCreate(PageMaterialRequest $request): RedirectResponse
    {
        $this->pageMaterialService->materialCreate($request->all());
        return back()->with('success', __('message.create_post'));
    }


    /**
     * @param $id
     * @return Factory|View
     */
    public function materialEdit($id)
    {
        $pageMaterial =  $this->pageMaterialService->getMaterial($id);
        return view('admin.profile.edit.material-update',compact('pageMaterial'));
    }


    /**
     * @param PageMaterialRequest $request
     * @return RedirectResponse
     */
    public function materialUpdate(PageMaterialRequest $request): RedirectResponse
    {
        $this->pageMaterialService->materialUpdate($request->all());
        return redirect()->route('admin.profile.material');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function materialDelete(Request $request)
    {
        $this->pageMaterialService->materialDelete($request->id);
        return back()->with('success', __('message.delete_post'));
    }


    /**
     * @return Factory|View
     */
    public function info()
    {
        $pageInfo = $this->pageInfoService->getInfo();
        return view('admin.profile.info', compact('pageInfo'));
    }

    /**
     * @param PageInfoRequest $request
     * @return RedirectResponse
     */
    public function infoCreate(PageInfoRequest $request): RedirectResponse
    {
        $this->pageInfoService->infoCreate($request->all());
        return back()->with('success', __('message.create_post'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function infoDelete(Request $request): RedirectResponse
    {
        $this->pageInfoService->infoDelete($request->id);
        return back()->with('success', __('message.delete_post'));
    }


    /**
     * @return Factory|View
     */
    public function description()
    {
        $pageDescription = $this->pageDescriptionService->getDescription();
        return view('admin.profile.description', compact('pageDescription'));
    }


    /**
     * @param AdvantageRequest $request
     * @return RedirectResponse
     */
    public function descriptionCreate(AdvantageRequest $request)
    {
        $data = $request->all();
        $this->pageDescriptionService->descriptionCreate($data);
        return back()->with('success', __('message.create_post'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function descriptionDelete(Request $request)
    {
        $this->pageDescriptionService->descriptionDelete($request->id);
        return back()->with('success', __('message.delete_post'));
    }


}
