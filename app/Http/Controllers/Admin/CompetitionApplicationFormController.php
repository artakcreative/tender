<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompetitionApplicationFormRequest;
use App\Services\Admin\CompetitionApplicationForm\CompetitionApplicationFormServiceInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CompetitionApplicationFormController extends Controller
{

    /**
     * @var CompetitionApplicationFormServiceInterface
     */
    private $competitionApplicationFormService;

    /**
     * @param CompetitionApplicationFormServiceInterface $competitionApplicationFormService
     */
    public function __construct(CompetitionApplicationFormServiceInterface $competitionApplicationFormService)
    {
        $this->competitionApplicationFormService = $competitionApplicationFormService;
    }

    /**
     * @return Application|Factory|View
     */
    public function create(){
        return view('admin.competition_settings.competitionTypeAndApplicationForm.application_form_create');
    }

    /**
     *  Сохранение конфигурации как подать заявления
     * @param CompetitionApplicationFormRequest $request
     * @return RedirectResponse
     */
    public function store(CompetitionApplicationFormRequest $request): RedirectResponse
    {
        $data = $request->all();
        $this->competitionApplicationFormService->save($data);
        return redirect()->route('admin.competition_type_and_application_form')->with('success', __('message.application_form'));
    }

    /**
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id){
        $how_to_apply = $this->competitionApplicationFormService->getById($id);
        return view('admin.competition_settings.competitionTypeAndApplicationForm.application_form_edit', compact('how_to_apply'));
    }

    /**
     * @param CompetitionApplicationFormRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(CompetitionApplicationFormRequest $request, $id): RedirectResponse
    {
        $data = $request->all();
        $this->competitionApplicationFormService->update($data, $id);
        return redirect()->route('admin.competition_type_and_application_form')->with('success', __('message.application_form_edit'));
    }
}
