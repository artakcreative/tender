<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserResetPassword;
use App\Models\Admin;
use App\Repositories\Admin\AdminRepositoryInterface;
use App\Services\Admin\AdminServiceInterface;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class PasswordResetLinkController extends Controller
{

    /**
     * @var AdminServiceInterface
     */
    private $adminService;

    public function __construct(AdminServiceInterface $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker()
    {
        return Password::broker('admins');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        $statusAdmin = $this->adminService->getAdminStatus($request->only('email'));
        if($statusAdmin && $statusAdmin == Admin::BLOCK) {
            return response()->json(['error' => 'Դուք բլոկավորված եք։']);
        }

        $status = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $status == Password::RESET_LINK_SENT
            ? response()->json(['status', __($status)])
            : response()->json(['error' => __($status)]);
    }


    /**
     * @param $email
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset($email, $token)
    {
        Session::put('admin-email', $email);
        Session::put('admin-token', $token);
        return redirect()->route('admin.login');
    }

    /**
     * @param UserResetPassword $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserResetPassword  $request)
    {
        $status = $this->broker()->reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($request->password),
                    'remember_token' => Str::random(60),
                ])->save();
                event(new PasswordReset($user));
            }
        );
        session()->forget(['admin-email', 'admin-token']);

        return $status == Password::RESET_LINK_SENT
            ? response()->json(['status', __($status)])
            : response()->json(['error' => __($status)]);
    }
}
