<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use App\Services\Admin\ManagerServiceInterface;
use App\Services\Admin\UserServiceInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @var ManagerServiceInterface
     */
    private $managerService;
    /**
     * @var UserServiceInterface
     */
    private $userService;
    /**
     * @var AnnouncementServiceInterface
     */
    private $announcementService;

    /**
     * @param ManagerServiceInterface $managerService
     * @param UserServiceInterface $userService
     * @param AnnouncementServiceInterface $announcementService
     */
    public function __construct(ManagerServiceInterface $managerService,
                                UserServiceInterface $userService,
                                AnnouncementServiceInterface $announcementService
                                )
    {
        $this->managerService = $managerService;
        $this->userService = $userService;
        $this->announcementService = $announcementService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $managerCount = $this->managerService->managerCount();
        $userCount = $this->userService->userCount();
        $publishedAnnouncementCount = $this->announcementService->publishedAnnouncementCount();
        return view('admin.dashboard', compact('managerCount', 'userCount', 'publishedAnnouncementCount'));
    }
}
