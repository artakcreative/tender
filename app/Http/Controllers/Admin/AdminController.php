<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRequest;
use App\Services\Admin\AdminServiceInterface;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * @var AdminServiceInterface
     */
    protected $adminService;


    public function __construct(AdminServiceInterface $adminService)
    {

        $this->adminService = $adminService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    public function index(){
        $admins = $this->adminService->getAll();

        return view('admin.admins.index', compact('admins'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    public function create(){
        return view('admin.admins.create');
    }


    /**
     * @param AdminRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminRequest $request){
        $data = $request->all();
        $admin = $this->adminService->saveAdmin($data);
        $this->adminService->notify($admin, $data['password']);
        return redirect()->route('admin.admins')->with('success', __('message.create_admin'));
    }

}
