<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\CompetitionApplicationForm\CompetitionApplicationFormServiceInterface;
use App\Services\Admin\CompetitionType\CompetitionTypeServiceInterface;
use App\Services\Admin\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormServiceInterface;
use Illuminate\Http\Request;

class CompetitionTypeAndApplicationFormController extends Controller
{
    /**
     * @var CompetitionTypeServiceInterface
     */
    private $competitionTypeService;
    /**
     * @var CompetitionApplicationFormServiceInterface
     */
    private $competitionApplicationFormService;

    /**
     * @param CompetitionTypeServiceInterface $competitionTypeService
     * @param CompetitionApplicationFormServiceInterface $competitionApplicationFormService
     */
    public function __construct(CompetitionTypeServiceInterface $competitionTypeService, CompetitionApplicationFormServiceInterface $competitionApplicationFormService)
    {
        $this->competitionTypeService = $competitionTypeService;
        $this->competitionApplicationFormService = $competitionApplicationFormService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        /**
         * Берём все типы соревнования для показа в index
         */
        $competitionTypes = $this->competitionTypeService->getAll();

        /**
         * Показать все  конфигурации как подать заявления в index
         */
        $competitionApplicationForms = $this->competitionApplicationFormService->getAll();
        return view('admin.competition_settings.competitionTypeAndApplicationForm.index', compact('competitionTypes', 'competitionApplicationForms'));
    }

}
