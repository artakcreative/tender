<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Profile\PageHeaderRequest;
use App\Services\Admin\HelpDescription\HelpDescriptionServiceInterface;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    /**
     * @var HelpDescriptionServiceInterface
     */
    private $helpDescriptionService;

    /**
     * HelpController constructor.
     * @param HelpDescriptionServiceInterface $helpDescriptionService
     */
    public function __construct(HelpDescriptionServiceInterface $helpDescriptionService)
    {
        $this->helpDescriptionService = $helpDescriptionService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $help = $this->helpDescriptionService->getHelp();
        return view('admin.help.index', compact('help'));
    }


    /**
     * @param PageHeaderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function helpCreate(PageHeaderRequest  $request)
    {
        $data = $request->all();
        $this->helpDescriptionService->helpCreate($data);
        return back()->with('success', __('message.create_post'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function helpDelete(Request $request)
    {
        $this->helpDescriptionService->helpDelete($request->id);;
        return back()->with('success', __('message.delete_post'));
    }

}

