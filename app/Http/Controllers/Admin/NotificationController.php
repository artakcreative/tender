<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\Notification\NotificationServiceInterface;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $notificationService;

    /**
     * Notification constructor.
     * @param NotificationServiceInterface $notificationService
     */
    public function __construct(NotificationServiceInterface $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function read($id)
    {
        $this->notificationService->read($id);
        return response()->json('Հաղորթագրությունը հաջողությամբ ընթերցվել է', 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function readAll(Request $request)
    {
        $this->notificationService->readAll($request->id);
        return response()->json('ՀաղորթագրությունՄերը հաջողությամբ ընթերցվել է', 200);
    }

}
