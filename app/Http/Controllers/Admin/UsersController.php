<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddManagerToUserRequest;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\User\PlaneRequest;
use App\Models\Admin;
use App\Models\User;
use App\Services\Admin\AdminServiceInterface;
use App\Services\Admin\ManagerServiceInterface;
use App\Services\Admin\UserServiceInterface;
use App\Services\User\UserSphere\UserSphereServiceInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class UsersController extends Controller
{
    /**
     * @var UserServiceInterface
     */
    private $userService;
    private $managerService;
    private $userSphereService;
    /**
     * @var AdminServiceInterface
     */
    private $adminService;

    /**
     * @param UserServiceInterface $userService
     * @param ManagerServiceInterface $managerService
     * @param UserSphereServiceInterface $userSphereService
     * @param AdminServiceInterface $adminService
     */
    public function __construct(UserServiceInterface $userService, ManagerServiceInterface $managerService,
                                UserSphereServiceInterface $userSphereService, AdminServiceInterface $adminService)
    {
        $this->userService = $userService;
        $this->managerService = $managerService;
        $this->userSphereService = $userSphereService;
        $this->adminService = $adminService;
    }

    /**
     * @return Application|Factory|View
     */
    public function index(){
        $users = $this->userService->getAll();
        return view('admin.users.index',compact('users'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create(){
        return view('admin.users.create');
    }

    /**
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function store(UserRequest $request): RedirectResponse
    {
        $data = $request->all();
        $file = $request->file('file');
        $user = $this->userService->save($data,$file);
        $this->userService->notify($user, $data['password'], $data['name'], $data['surname']);
        return redirect()->route('admin.users')->with('success', __('message.create_user'));
    }

//    /**
//     * @param $id
//     * @return RedirectResponse
//     */
//    public function destroy($id): RedirectResponse
//    {
//        $this->userService->deleteUser($id);
//        return redirect()->route('admin.users')->with('success', __('message.delete_user'));
//    }

    /**
     * @return Application|Factory|View
     */
    public function addManagerView(){
        $managers = $this->managerService->getAll()->where('status', Admin::ACTIVE);
        return view('admin.users.add-manager', compact('managers'));
    }

    /**
     * @param AddManagerToUserRequest $request
     * @return RedirectResponse
     */
    public function addManager(AddManagerToUserRequest $request): RedirectResponse
    {
        $data = $request->all();
        $this->userService->addManager($data);
        return redirect()->route('admin.users')->with('success', __('message.add_manager'));
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $data= $this->userSphereService->getAllUserSpheres((int) $id);
        $user = $this->userService->getUserById($id);
        $history =  $this->userService->getPlaneHistory($id);
        return view('admin.users.edit', compact('user', 'data', 'history'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function editProfile($id)
    {
        $user = $this->userService->getUserById($id);
        return view('admin.users.edit.profile', compact('user'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function editPlane($id)
    {
        $data= $this->userSphereService->getAllUserSpheres((int) $id);
        $user = $this->userService->getUserById($id);
        return view('admin.users.edit.plane', compact('user','data'));
    }


    /**
     * @param UserRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(UserRequest $request, $id): RedirectResponse
    {
        $data = $request->all();
        $this->userService->updateUser($id, $data);
        return redirect()->route('admin.users')->with('success', __('message.update_user'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function dataBlocked(Request $request, $id): RedirectResponse
    {
        $this->userService->updateUserDataBlocked($id, $request->blocked);
        return redirect()->route('admin.users')->with('success', __('message.update_user'));
    }

    /**
     * @param PlaneRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function activePlane(PlaneRequest $request, $id): RedirectResponse
    {
        $data = $request->all();
        $file = [];
        if(isset($data['file'])) {
            $file = ['file' => $data['file']];
        }
        $adminId = Auth::guard('admin')->user()->id;
        $this->userService->addFile($id, $file);
        $this->userService->activePlaneUser($id, $data);
        $this->adminService->activePlane($data, $id, $adminId);
        return redirect()->route('admin.users.edit', ['id'=> $id])->with('success', __('message.user_active_plane'));
    }

    /**
     * @param int $media_id
     * @return JsonResponse
     */
    public function destroyFile(int $media_id): JsonResponse
    {

        $this->userService->deleteFile($media_id);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Block the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function block(int $id): RedirectResponse
    {
        $this->userService->blockUser($id);
        return redirect()->route('admin.users')->with('success', __('message.block_user'));
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function unlock($id): RedirectResponse
    {
        $this->userService->unlockUser($id);
        return redirect()->route('admin.users')->with('success', __('message.unlock_user'));

    }

    /**
     * @param $id
     * Логин под пользователя
     * @return RedirectResponse
     */
    public function SignInByUser($id): RedirectResponse
    {
        $user = $this->userService->getUserById($id);
        if ($user->status == User::ACTIVE){
            Auth::guard('admin')->logout();
            Auth::loginUsingId($id);
            return redirect()->route('user.person.settings');

        }
       return redirect()->back()->with('error',__('message.block_user_log_in'));
    }
}
