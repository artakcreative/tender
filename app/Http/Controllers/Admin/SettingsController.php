<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PasswordRequest;
use App\Http\Requests\Admin\SettingsRequest;
use App\Models\Admin;
use App\Services\Admin\AdminServiceInterface;
use App\Services\Admin\SettingsServiceInterface;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * @var SettingsServiceInterface
     */
    private $settingsService;
    /**
     * @var AdminServiceInterface
     */
    private $adminService;

    /**
     * @param SettingsServiceInterface $settingsService
     * @param AdminServiceInterface $adminService
     */
    public function __construct(SettingsServiceInterface $settingsService, AdminServiceInterface $adminService){

        $this->settingsService = $settingsService;
        $this->adminService = $adminService;
    }
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editPersonalInfo()
    {
        return view('admin.settings.personal');
    }

    /**
     * @param SettingsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePersonalInfo(SettingsRequest $request): \Illuminate\Http\RedirectResponse
    {
        $id = auth()->guard('admin')->user()->id;
        $data = $request->all();
        $this->settingsService->updatePersonalInfo($id, $data);
        return redirect()->back()->with('success', __('message.profile_edit_success'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editPassword()
    {
        return view('admin.settings.password');
    }

    /**
     * @param PasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword (PasswordRequest $request)
    {
       $data = $request->all();
       $this->settingsService->changePassword($data);
       return redirect()->back()->with('success', __('message.password_change_success'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function contract(){
        $files = $this->adminService->getById(Admin::DEAFULD_ID);
        return view('admin.competition_settings.contract.contract', compact('files'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addContract(Request $request){
        $files = $this->settingsService->addContract($request->all());
        return redirect()->back()->with(compact('files'));
    }
}
