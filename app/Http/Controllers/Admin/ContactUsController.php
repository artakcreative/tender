<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\ContactUs\ContactUsServiceInterface;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * @var ContactUsServiceInterface
     */
    private $contactUsService;

    /**
     * @param ContactUsServiceInterface $contactUsService
     */
    public function __construct(ContactUsServiceInterface $contactUsService){

        $this->contactUsService = $contactUsService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        return view('admin.competition_settings.contact-us.index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(){
        return view('admin.competition_settings.contact-us.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){

        $data = $request->all();
        $this->contactUsService->save($data);
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(){
        return view('admin.competition_settings.contact-us.edit');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $data = $request->all();
        $this->contactUsService->update($data, $id);
        return redirect()->route('admin.contact-us')->with('success', __('message.contact_us_edit'));
    }
}
