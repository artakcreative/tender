<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface;
use App\Services\Admin\CompetitionSubSphere\CompetitionSubSphereServiceInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CompetitionSphereController extends Controller
{
    /**
     * @var CompetitionSphereServiceInterface
     */
    private $competitionSphereService;
    /**
     * @var CompetitionSubSphereServiceInterface
     */
    private $competitionSubSphereService;

    /**
     * @param CompetitionSphereServiceInterface $competitionSphereService
     * @param CompetitionSubSphereServiceInterface $competitionSubSphereService
     */
    public function __construct(CompetitionSphereServiceInterface $competitionSphereService, CompetitionSubSphereServiceInterface $competitionSubSphereService){

        $this->competitionSphereService = $competitionSphereService;
        $this->competitionSubSphereService = $competitionSubSphereService;
    }


    public function index (){
        $spheres = $this->competitionSphereService->getAllSphere();
        return view('admin.competition_settings.sphere.index',compact('spheres'));
    }

    /**
     * @return Factory|View
     */
    public function create(){
        return view('admin.competition_settings.sphere.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request){
        $data = $request->all();
        $this->competitionSphereService->saveSphere($data);
//        $this->competitionSphereService->sendNotificationToUser();
        return redirect()->route('admin.sphere')->with('success', __('message.sphere_store'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getSphereData(Request $request): JsonResponse
    {
        return response()->json($this->competitionSphereService->getSphereData($request->search));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getSubSphereData(Request $request): JsonResponse
    {
        return response()->json($this->competitionSubSphereService->getSubSphereData($request->search));
    }
}
