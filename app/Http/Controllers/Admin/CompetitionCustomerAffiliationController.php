<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompetitionCustomerAffiliationRequest;
use App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationServiceInterface;
use Illuminate\Http\Request;

class CompetitionCustomerAffiliationController extends Controller
{
    /**
     * @var CompetitionCustomerAffiliationServiceInterface
     */
    private $competitionCustomerAffiliationService;

    /**
     * @param CompetitionCustomerAffiliationServiceInterface $competitionCustomerAffiliationService
     */
    public function __construct(CompetitionCustomerAffiliationServiceInterface $competitionCustomerAffiliationService){

        $this->competitionCustomerAffiliationService = $competitionCustomerAffiliationService;
    }

//    /**
//     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
//     */
//    public function create(){
//        return view('admin.competition_settings.customer.customer_affiliation_create');
//    }
//
//    /**
//     * @param Request $request
//     * @return \Illuminate\Http\RedirectResponse
//     */
//    public function store(CompetitionCustomerAffiliationRequest $request){
//        $data = $request->all();
//        $this->competitionCustomerAffiliationService->saveCustomerAffiliation($data);
//        return redirect()->route('admin.customer')->with('success', __('message.customer_affiliation'));
//    }


}
