<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\User\Tariff\TariffServiceInterface;
use Illuminate\Http\Request;

class TariffsController extends Controller
{

    /**
     * @var TariffServiceInterface
     */
    private $tariffService;

    /**
     * @param TariffServiceInterface $tariffService
     */
    public function __construct(TariffServiceInterface $tariffService)
    {

        $this->tariffService = $tariffService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $tariffs = $this->tariffService->getTariffs();
        return view('admin.competition_settings.tariff.index', compact('tariffs'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editTariffPrice($id){
        $tariff = $this->tariffService->getTariffById($id);
        return view('admin.competition_settings.tariff.edit', compact('tariff'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateTariffPrice(Request $request, $id){
        $data = $request->all();
        $this->tariffService->updatePrice($data, $id);
        return redirect()->route('admin.tariff')->with('success', __('message.tariff_price_update'));
    }

}
