<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ManagerRequest;
use App\Services\Admin\ManagerServiceInterface;
use App\Services\Admin\Notification\NotificationServiceInterface;
use Illuminate\Support\Arr;


class ManagerController extends Controller
{

    /**
     * @var ManagerServiceInterface
     */
    private $managerService;
    private $notificationService;


    /**
     * ManagerController constructor.
     * @param ManagerServiceInterface $managerService
     * @param NotificationServiceInterface $notificationService
     */
    public function __construct(ManagerServiceInterface $managerService, NotificationServiceInterface $notificationService)
    {

        $this->managerService = $managerService;
        $this->notificationService = $notificationService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $managers = $this->managerService->getAll();
        return view('admin.managers.index', compact('managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->managerService->getManagerPermissions();
        return view('admin.managers.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param ManagerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ManagerRequest $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->all();
        $manager = $this->managerService->saveManager($data);
        if(isset($data['permissions'])) {
            $this->notificationService->sendPermissions($manager->id, $data['permissions'], []);
        }
        $this->managerService->giveManagerPermissions($manager->id, Arr::exists($data, 'permissions') ? $data['permissions'] : [] );
        $this->managerService->notify($manager, $data['password']);
        return redirect()->route('admin.managers')->with('success', __('message.create_manager'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $manager = $this->managerService->getManagerById($id);
        $roles = $this->managerService->getManagerPermissions();
        return view('admin.managers.edit', compact('manager','roles'));
    }

    /**
     * @param ManagerRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ManagerRequest $request, $id)
    {
        $data = $request->all();
        $permissions = [];
        $userPermissions = $this->managerService->givePermissionNames((int) $id);
        $this->managerService->updateManager($data, $id);
        $this->managerService->updatePermissions($id, Arr::exists($data, 'permissions') ? $data['permissions'] : [] );
        if(isset($data['permissions'])) {
            $permissions = $data['permissions'];
        }
        $this->notificationService->sendPermissions($id, $permissions, $userPermissions);
        return redirect()->route('admin.managers')->with('success', __('message.update_manager'));
    }

    /**
     * Block the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function block($id)
    {
        $this->managerService->blockManager($id);
        return redirect()->route('admin.managers')->with('success', __('message.block_manager'));
    }

    public function unlock($id)
    {
        $this->managerService->unlockManager($id);
        return redirect()->route('admin.managers')->with('success', __('message.unlock_manager'));
    }
}
