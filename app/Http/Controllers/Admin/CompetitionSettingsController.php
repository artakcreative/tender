<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompetitionSettings;
use App\Services\Admin\CompetitionCustomer\CompetitionCustomerServiceInterface;
use App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationServiceInterface;
use App\Services\Admin\CompetitionSettingsServiceInterface;
use Illuminate\Http\Request;

class CompetitionSettingsController extends Controller
{

    /**
     * @var CompetitionCustomerAffiliationServiceInterface
     */
    private $competitionCustomerAffiliationService;
    /**
     * @var CompetitionCustomerServiceInterface
     */
    private $competitionCustomerService;

    /**
     * @param CompetitionCustomerAffiliationServiceInterface $competitionCustomerAffiliationService
     * @param CompetitionCustomerServiceInterface $competitionCustomerService
     */
    public function __construct(CompetitionCustomerAffiliationServiceInterface $competitionCustomerAffiliationService, CompetitionCustomerServiceInterface $competitionCustomerService){
        $this->competitionCustomerAffiliationService = $competitionCustomerAffiliationService;
        $this->competitionCustomerService = $competitionCustomerService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(){
        $customerAffiliations = $this->competitionCustomerAffiliationService->getAll();
        $customers = $this->competitionCustomerService->getAll();
        return view('admin.competition_settings.customer.index', compact('customerAffiliations', 'customers'));
    }


}
