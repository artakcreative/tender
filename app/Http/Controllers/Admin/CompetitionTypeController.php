<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompetitionTypeRequest;
use App\Services\Admin\CompetitionType\CompetitionTypeServiceInterface;
use Illuminate\Http\Request;

class CompetitionTypeController extends Controller
{
    /**
     * @var CompetitionTypeServiceInterface
     */
    private $competitionTypeService;

    /**
     * @param CompetitionTypeServiceInterface $competitionTypeService
     */
    public function __construct(CompetitionTypeServiceInterface $competitionTypeService)
    {
        $this->competitionTypeService = $competitionTypeService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(){
        return view('admin.competition_settings.competitionTypeAndApplicationForm.competition_type_create');
    }

    /**
     * Сохраненные типа соревнования
     * @param CompetitionTypeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CompetitionTypeRequest $request){
        $data = $request->all();
        $this->competitionTypeService->save($data);
        return redirect()->route('admin.competition_type_and_application_form')->with('success', __('message.competition_type'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id){
        $competitionType = $this->competitionTypeService->getById($id);
        return view('admin.competition_settings.competitionTypeAndApplicationForm.competition_type_edit', compact('competitionType'));
    }

    public function update(CompetitionTypeRequest $request, $id){
        $data = $request->all();
        $this->competitionTypeService->update($data, $id);
        return redirect()->route('admin.competition_type_and_application_form')->with('success', __('message.competition_type_edit'));
    }
}
