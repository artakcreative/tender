<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AnnouncementRequest;
use App\Http\Requests\Admin\AnnouncementSearchRequest;
use App\Models\Announcement;
use App\Services\Admin\Announcement\AnnouncementServiceInterface;
use App\Services\Admin\CompetitionApplicationForm\CompetitionApplicationFormServiceInterface;
use App\Services\Admin\CompetitionCustomer\CompetitionCustomerServiceInterface;
use App\Services\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationServiceInterface;
use App\Services\Admin\CompetitionSettingsServiceInterface;
use App\Services\Admin\CompetitionSphere\CompetitionSphereServiceInterface;
use App\Services\Admin\CompetitionType\CompetitionTypeServiceInterface;
use App\Services\Admin\CompetitionTypeAndApplicationForm\CompetitionTypeAndApplicationFormServiceInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use PHPHtmlParser\Dom;


//use Symfony\Component\HttpFoundation\Response;

class AnnouncementController extends Controller
{
    /**
     * @var AnnouncementServiceInterface
     */
    protected $announcementService;
    /**
     * @var CompetitionSphereServiceInterface
     */
    private $competitionSphereService;
    /**
     * @var CompetitionCustomerServiceInterface
     */
    private $competitionCustomerService;
    /**
     * @var \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    private $a;
    /**
     * @var RedirectResponse
     */
    private $b;
    /**
     * @var CompetitionTypeServiceInterface
     */
    private $competitionTypeService;
    /**
     * @var CompetitionApplicationFormServiceInterface
     */
    private $competitionApplicationFormService;
    /**
     * @var CompetitionCustomerAffiliationServiceInterface
     */
    private $customerAffiliationService;


    /**
     * AnnouncementController constructor.
     * @param CompetitionSphereServiceInterface $competitionSphereService
     * @param AnnouncementServiceInterface $announcementService
     * @param CompetitionCustomerServiceInterface $competitionCustomerService
     * @param CompetitionTypeServiceInterface $competitionTypeService
     * @param CompetitionApplicationFormServiceInterface $competitionApplicationFormService
     * @param CompetitionCustomerAffiliationServiceInterface $customerAffiliationService
     */
    public function __construct(CompetitionSphereServiceInterface $competitionSphereService,
                                AnnouncementServiceInterface $announcementService,
                                CompetitionCustomerServiceInterface $competitionCustomerService,
                                CompetitionTypeServiceInterface $competitionTypeService,
                                CompetitionApplicationFormServiceInterface $competitionApplicationFormService,
                                CompetitionCustomerAffiliationServiceInterface $customerAffiliationService)
    {
        $this->announcementService = $announcementService;
        $this->competitionSphereService = $competitionSphereService;
        $this->competitionCustomerService = $competitionCustomerService;
        $this->competitionTypeService = $competitionTypeService;
        $this->competitionApplicationFormService = $competitionApplicationFormService;
        $this->customerAffiliationService = $customerAffiliationService;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
//        $dom = new Dom;
//        $url = 'https://www.gnumner.am/hy/page/gnumneri_haytararutyunner_/';
//        $dom->loadFromUrl($url);
//        $tender_create_date = $dom->find('.tender .tender_time');
//        $d = preg_replace("/[^0-9]/", '', $tender_create_date[0]->outerHtml);
//        $x = substr($d, 0, 4) .'-'. substr($d, 4,2) .'-'. substr($d,6,2) .' '. substr($d,8,2) .':'. substr($d,10,2) .':'. substr($d,12,2);
        $announcements = $this->announcementService->getAll();
        $published_announcements = $this->announcementService->getAllActiveAnnouncements();
        return view('admin.announcements.index',compact( 'published_announcements'));
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
//        $applicationForms = $this->competitionTypeAndApplicationFormService->getAll();
        $competitionTypes = $this->competitionTypeService->getAll();
        $competitionApplicationForms = $this->competitionApplicationFormService->getAll();
        $spheres =$this->competitionSphereService->getAllSphere();
        $customers = $this->competitionCustomerService->getAll();
        $customerAffiliations = $this->customerAffiliationService->getAll();
        return view('admin.announcements.create', compact('spheres', 'competitionTypes', 'competitionApplicationForms', 'customers','customerAffiliations'));
    }

    /**
     * сохранение объявления и генерация PDF
     * @param AnnouncementRequest $request
     * @return RedirectResponse
     */
    public function store(AnnouncementRequest $request): RedirectResponse
    {
        $data = $request->all();
        $this->announcementService->saveAnnouncement($data);
        return response()->redirectTo('/admin/announcement/saved')->with('success', __('message.create_post'));
    }


    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
//        $applicationForms = $this->competitionTypeAndApplicationFormService->getAll();
        $competitionTypes = $this->competitionTypeService->getAll();
        $competitionApplicationForms = $this->competitionApplicationFormService->getAll();
        $announcement = $this->announcementService->getById(+$id);
        $customers = $this->competitionCustomerService->getAllForSelect();
        $spheres = $this->competitionSphereService->getAllSphereForSelect();
        $sphereSubSpheres = $this->announcementService->getAnnouncementSubSphereAndSphere($id);
//        $sphereIds = $this->getSphereIds($sphereSubSpheres);

//        $xs = $this->competitionSphereService->getSavedAnnouncementSubSphere($sphereIds);
        $customerAffiliations = $this->customerAffiliationService->getAll();
        return view('admin.announcements.edit', compact('announcement', 'competitionTypes','competitionApplicationForms', 'customers', 'spheres', 'sphereSubSpheres', 'customerAffiliations'));
    }

    /**
     * @param AnnouncementRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(AnnouncementRequest $request,$id): RedirectResponse
    {
        $data = $request->all();
        $this->announcementService->update($id, $data);
        return response()->redirectTo('/admin/announcement/saved')->with('success', __('message.edit_post'));

    }

    /**
     * При выборе сферы автоматически подставляются под сферы
     * @param Request $request
     * @return mixed
     */
    public function getSubSphere(Request $request)
    {
        return $this->competitionSphereService->getSubSphereCategories($request->id);
    }

    /**
     * @param $media_id
     * @return JsonResponse
     */
    public function destroyFile($media_id): JsonResponse
    {
        $this->announcementService->deleteFile(+$media_id);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return void
     */
    public function publicate(Request $request, $id){
        $data = $request->all();
    }

    /**
     * выбираем  активные и архивированные  объявлений по сферам
     * @param Request $request
     * @return JsonResponse
     */
    public function selectActiveAndArchivedAnnouncementsBySpheres(Request $request): JsonResponse
    {
        $data = $request->all();
        $announcementsBySpheres =$this->announcementService->selectActiveAndArchivedAnnouncementsBySpheres($data['id']);
        return response()->json([
            'activeAnnouncements' => $announcementsBySpheres[0]->count(),
            'archivedAnnouncements' =>  $announcementsBySpheres[1]->count(),
        ]);
    }

    /**
     * прикрепляем менеджера в объявлению
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function addManagerToAnnouncement(Request $request, $id): RedirectResponse
    {
        $data = $request->all();
        $this->announcementService->addManagerToAnnouncement($data, $id);
        return redirect()->back()->with('success', __('message.add_manager_to_announcement'));
    }

    /**
     * история объявлений
     * @param $id
     * @return Application|Factory|View
     */
    public function history($id){
        $histories = $this->announcementService->getAnnouncementHistory($id);
        return view('admin.announcements.history', compact('histories'));
    }

    /**
     * @return Application|Factory|View
     */
    public function savedAnnouncements(){
        $saved_announcements = $this->announcementService->getAllSaved();
        return view('admin.announcements.saved-index', compact('saved_announcements'));
    }

    /**
     * @return Application|Factory|View
     */
    public function archivedAnnouncements(){
        $archived_announcements = $this->announcementService->getAllArchivedAnnouncements();
        return view('admin.announcements.archived-index', compact('archived_announcements'));
    }

    public function searchAnnouncement(AnnouncementSearchRequest $request){
       $data = $request->all();
//       dd($data);
       $announcement = $this->announcementService->getById((int)$data['code']);
       return view('admin.announcements.search-result', compact('announcement'));
    }

    public function getSphereIds($spheres){
        $sphereIds = [];
//        debug($spheres);
        foreach ($spheres as $sphere){
//            debug($sphere);
            foreach ($sphere as $sphere_id){
                $sphereIds[] = $sphere_id['competition_sphere_id'];
            }
        }
        return array_unique($sphereIds);
//        foreach ($spheres as $sphere){
//            debug($sphere);
////            foreach ($sphere as $sphere_id){
////                $sphereIds[] = $sphere_id['id'];
////            }
//            return $sphereIds;
//        }
    }
}
