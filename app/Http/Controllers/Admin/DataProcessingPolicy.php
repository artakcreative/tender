<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\DataProcessingPolicy\DataProcessingPolicyServiceInterface;
use Illuminate\Http\Request;

class DataProcessingPolicy extends Controller
{
    /**
     * @var DataProcessingPolicyServiceInterface
     */
    private $policyService;

    /**
     * @param DataProcessingPolicyServiceInterface $policyService
     */
    public function __construct(DataProcessingPolicyServiceInterface $policyService)
    {

        $this->policyService = $policyService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public  function createDataProcessingPolicy()
    {
        return view('profile.layouts.includes.auth.data-processing-policy.data-processing-policy-create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public  function storeDataProcessingPolicy(Request $request)
    {
        $data = $request->all();
        $this->policyService->save($data);
        $dataProcessingPolicy = $this->policyService->getAll();

        return view('profile.layouts.includes.auth.data-processing-policy.data-processing-policy', compact('dataProcessingPolicy'));
    }


}
