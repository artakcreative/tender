<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Admin\UserServiceInterface;
use Illuminate\Http\Request;

class VerifyEmailController extends Controller
{

    /**
     * @var UserServiceInterface
     */
    protected  $userService;

    /**
     * VerifyEmailController constructor.
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(Request $request)
    {
        if($request->id){
            $this->userService->userEmailVerify($request->id);
        }

        return redirect()->intended('/');
    }
}
