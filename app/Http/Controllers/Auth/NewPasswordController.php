<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserResetPassword;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;

class NewPasswordController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker()
    {
        return Password::broker('users');
    }

    /**
     * @param $email
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create($email, $token)
    {
        Session::put('email', $email);
        Session::put('token', $token);
        return redirect()->route('home');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserResetPassword  $request)
    {
        $status = $this->broker()->reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($request->password),
                    'remember_token' => Str::random(60),
                ])->save();
                event(new PasswordReset($user));
            }
        );
        session()->forget(['email', 'token']);
        return $status == Password::RESET_LINK_SENT
            ? response()->json(['status', __($status)])
            : response()->json(['error' => __($status)]);

//        return response()->json('Օգտատերի գաղտնաբառը հաջողությամբ փոփոխվել է', 200);
    }
}
