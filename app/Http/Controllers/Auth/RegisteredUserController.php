<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegistracionRequset;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Repositories\Admin\UserRepositoryInterface;
use App\Services\Admin\DataProcessingPolicy\DataProcessingPolicyServiceInterface;
use http\Env\Response;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class RegisteredUserController extends Controller
{

    /**
     * @var UserRepositoryInterface
     */
    protected  $userRepository;
    /**
     * @var DataProcessingPolicyServiceInterface
     */
    private $policyService;

    /**
     * RegisteredUserController constructor.
     * @param UserRepositoryInterface $userRepository
     * @param DataProcessingPolicyServiceInterface $policyService
     */
    public function __construct(UserRepositoryInterface $userRepository, DataProcessingPolicyServiceInterface $policyService)
    {
        $this->userRepository = $userRepository;
        $this->policyService = $policyService;
    }

    /**
     * @param RegistracionRequset $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegistracionRequset $request)
    {
        $user = $this->userRepository->saveForProfile($request->email, $request->phone, $request->password);
        event(new Registered($user));
        return response()->json('Օգտատերը հաջողությամբ ավելացվել է', 200);
    }

    public function dataProcessingPolicy(){

        $dataProcessingPolicy = $this->policyService->getAll();
        return view('profile.layouts.includes.auth.data-processing-policy.data-processing-policy', compact('dataProcessingPolicy'));
    }
}
