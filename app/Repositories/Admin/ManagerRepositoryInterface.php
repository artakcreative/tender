<?php

namespace App\Repositories\Admin;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface ManagerRepositoryInterface extends RepositoryContract
{
    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
    * @param $id
    * @return mixed
    */
    public function getById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function checkManagerBlocked($id);

    /**
     * @param $id
     * @param $permission
     * @return mixed
     */
    public function givePermissions($id, $permission);


    /**
     * @param $id
     * @return mixed
     */
    public function givePermissionNames($id);

    /**
     * @param $id
     * @param $permissions
     * @return mixed
     */
    public function updatePermissions($id, $permissions);

    /**
     * @return mixed
     */
    public function getManagerPermissions();

    /**
     * @param $id
     * @return mixed
     */
    public function blockManager($id);

    /**
     * @param $id
     * @return mixed
     */
    public function unlockManager($id);

    /**
     * @return mixed
     */
    public function managerCount();
}
