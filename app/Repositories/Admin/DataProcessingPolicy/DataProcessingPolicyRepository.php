<?php

namespace App\Repositories\Admin\DataProcessingPolicy;

use App\Models\DataProcessingPolicy;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\DataProcessingPolicy\DataProcessingPolicyRepositoryInterface;

class DataProcessingPolicyRepository extends BaseRepository implements DataProcessingPolicyRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DataProcessingPolicy::class;
    }

    public function save($data)
    {
        DB::beginTransaction();
        try{
            $dataProcessingPolicy = $this->model->updateOrCreate(
                [
                    'template_id'=>1
                ],
                [
                    'content'=> $data['content'],
                ]);
            DB::commit();
            return $dataProcessingPolicy;
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }

    public function getAll()
    {
        return $this->newQuery()->get()->toArray();
    }
}
