<?php

namespace App\Repositories\Admin\DataProcessingPolicy;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface DataProcessingPolicyRepositoryInterface extends RepositoryContract
{
    /**
     * @param $data
     * @return mixed
     */
    public function save($data);

    /**
     * @return mixed
     */
    public function getAll();
}
