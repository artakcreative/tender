<?php

namespace App\Repositories\Admin\CompetitionCustomerHistory;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface CompetitionCustomerHistoryRepositoryInterface extends RepositoryContract
{
    /**
     * @return mixed
     */
    public function getCustomerHistory();
}
