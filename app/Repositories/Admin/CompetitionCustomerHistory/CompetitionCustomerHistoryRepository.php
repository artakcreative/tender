<?php

namespace App\Repositories\Admin\CompetitionCustomerHistory;

use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionCustomerHistory\CompetitionCustomerHistoryRepositoryInterface;
use OwenIt\Auditing\Models\Audit;

class CompetitionCustomerHistoryRepository extends BaseRepository implements CompetitionCustomerHistoryRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Audit::class;
    }

    /**
     * история пользователей
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getCustomerHistory()
    {
        return $this->model->with('user')
                            ->where('event', 'created')
                            ->where('auditable_type', 'App\Models\CompetitionCustomer')
                            ->orderBy('id', 'desc')
                            ->paginate(config('app.paginate'));
    }
}
