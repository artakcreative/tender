<?php

namespace App\Repositories\Admin;

use App\Models\CompetitionSettings;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionSettingsRepositoryInterface;

class CompetitionSettingsRepository extends BaseRepository implements CompetitionSettingsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompetitionSettings::class;
    }

    public function create_or_update_competition($data)
    {
        DB::beginTransaction();
        try{
            $competition = $this->model->updateOrCreate([
                'template_id' => $data['template_id']
            ],
                [
                    'competition_type'=> $this->explode_trim($data['competition_type']),
                    'how_to_apply'=> $this->explode_trim($data['how_to_apply']),
                    'customer_affiliation'=> $this->explode_trim($data['customer_affiliation']),
                    'customers'=> $this->explode_trim($data['customers']),
                    'sphere'=>$data['sphere_group'],
                ]);
            DB::commit();
            return $competition;
        }catch (\Exception $e){

            DB::rollBack();
            return $e->getMessage();
        }
    }

    public function explode_trim($data): array
    {
        return array_map('trim', explode(',',  $data));
    }
}
