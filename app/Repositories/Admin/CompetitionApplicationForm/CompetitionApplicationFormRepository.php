<?php

namespace App\Repositories\Admin\CompetitionApplicationForm;

use App\Models\CompetitionApplicationForm;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionApplicationForm\CompetitionApplicationFormRepositoryInterface;

class CompetitionApplicationFormRepository extends BaseRepository implements CompetitionApplicationFormRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompetitionApplicationForm::class;
    }

    /**
     *  Сохранение конфигурации как подать заявления
     * @param $data
     * @return mixed|string
     */
    public function save($data)
    {
        DB::beginTransaction();
        try{
            $how_to_apply = $this->model->create(
                [
                    'name'=> $data['how_to_apply'],
                ]);
            DB::commit();
            return $how_to_apply;
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * Показать все  конфигурации как подать заявления в index
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('id', 'desc')
            ->paginate(config('app.paginate'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    /**
     * @param $data
     * @param $id
     * @return mixed|string
     */
    public function update($data, $id)
    {
        $how_to_apply = $this->getById($id);
        DB::beginTransaction();
        try{
            $how_to_apply->update(
                [
                    'name'=> $data['how_to_apply'],
                ]);
            DB::commit();
            return $how_to_apply;
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }

    }
}
