<?php

namespace App\Repositories\Admin\CompetitionApplicationForm;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface CompetitionApplicationFormRepositoryInterface extends RepositoryContract
{
    /**
     * @param $data
     * @return mixed
     */
    public  function save($data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($data,$id);

}
