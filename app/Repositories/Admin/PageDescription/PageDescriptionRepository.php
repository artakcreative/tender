<?php

namespace App\Repositories\Admin\PageDescription;

use App\Models\Profile\PageDescription;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\PageDescription\PageDescriptionRepositoryInterface;

class PageDescriptionRepository extends BaseRepository implements PageDescriptionRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageDescription::class;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->model->first();
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function descriptionCreate($data)
    {
        DB::beginTransaction();
        try {
            $description = $this->model->first();
            if (isset($description)) {
                $description->update([
                    'title' => $data['title'],
                    'description' => $data['description']
                ]);
            } else {
                $description = $this->model->create([
                    'title' => $data['title'],
                    'description' => $data['description']
                ]);
            }
            DB::commit();

            return $description;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }


    /**
     * @param $id
     * @return mixed
     */
    public function descriptionDelete($id)
    {
        return $this->model->find($id)->delete();
    }
}
