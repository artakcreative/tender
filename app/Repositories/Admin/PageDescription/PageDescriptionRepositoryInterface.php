<?php

namespace App\Repositories\Admin\PageDescription;

interface PageDescriptionRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param $data
     * @return mixed
     */
    public function descriptionCreate($data);


    /**
     * @param $id
     * @return mixed
     */
    public function descriptionDelete($id);
}
