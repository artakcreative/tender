<?php

namespace App\Repositories\Admin\PageMaterial;

use App\Models\Profile\PageInterestingMaterial;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\PageMaterial\PageMaterialRepositoryInterface;

class PageMaterialRepository extends BaseRepository implements PageMaterialRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageInterestingMaterial::class;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function materialCreate($data)
    {
        DB::beginTransaction();
        try {
            $material = $this->model->create([
                'title' => $data['title'],
                'description' => $data['description'],

            ]);
            if (isset($data['image'])) {
                $material->addMedia($data['image'])->toMediaCollection('pageMaterial');
            }

            DB::commit();

            return $material;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getMaterials()
    {
        $materials = null;
        $materials = $this->model->get()->toArray();
        if (count($materials) > 0) {
            foreach ($materials as $key => $value) {
                $material = $this->model->find($value['id']);
                $urlImage = $material->getMedia('pageMaterial');
                if (count($urlImage) > 0) {
                    $materials[$key]['url_image'] = $urlImage[0]->getUrl();
                }
            }
        }
        return $materials;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function materialDelete($id)
    {
        $material = $this->model->find($id);;
        if(isset( $material)) {
            $material->clearMediaCollection('pageMaterial');
        }
        return  $material->delete();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function materialUpdate($data)
    {
        DB::beginTransaction();
        try {
            $material = $this->model->find((int) $data['id']);
            $material->update([
                'title' => $data['title'],
                'description' => $data['description'],
            ]);
            if (isset($data['image'])) {
                $material->clearMediaCollection('pageMaterial');
                $material->addMedia($data['image'])->toMediaCollection('pageMaterial');
            }

            DB::commit();

            return $material;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMaterial($id)
    {
        return $this->model->find($id);
    }
}
