<?php

namespace App\Repositories\Admin\PageMaterial;

interface PageMaterialRepositoryInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function materialCreate($request);

    /**
     * @return mixed
     */
    public function getMaterials();

    /**
     * @param $id
     * @return mixed
     */
    public function materialDelete($id);

    /**
     * @param $request
     * @return mixed
     */
    public function materialUpdate($request);


    /**
     * @param $id
     * @return mixed
     */
    public function getMaterial($id);
}
