<?php

namespace App\Repositories\Admin\CompetitionSphere;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface CompetitionSphereRepositoryInterface extends RepositoryContract
{
    /**
     * @param $data
     * @return mixed
     */
    public function saveSphere($data);

    /**
     * @return mixed
     */
    public function getAllSphere();

    /**
     * @return mixed
     */
    public function getAllSphereForSelect();

    /**
     * @param $id
     * @return mixed
     */
    public function getSubSphereCategories($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getUserSpheres($id);

    /**
     * @param $search
     * @return mixed
     */
    public function getSphereData($search);

    /**
     * @param $search
     * @return mixed
     */

    /**
     * @param $ids
     * @return mixed
     */
    public function getSavedAnnouncementSubSphere($ids);

}
