<?php

namespace App\Repositories\Admin\CompetitionSphere;

use App\Models\CompetitionSphere;
use App\Models\CompetitionSubSphere;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionSphere\CompetitionSphereRepositoryInterface;

class CompetitionSphereRepository extends BaseRepository implements CompetitionSphereRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return CompetitionSphere::class;
    }


    /**
     * @param $data
     * @return string|void
     */
    public function saveSphere($data)
    {
        DB::beginTransaction();
        try{

            foreach ($data['sphere_group'] as $value ) {
                $sphere =  $this->model->create([
                    'name' => $value['sphere']
                ]);
                if (isset($value['sub_sphere'])){
                    foreach ($value['sub_sphere'] as $item){
                        $sphere->sub_spheres()->create([
                            'name' => $item['name']
                        ]);
                    }
                }

            }
            DB::commit();
        }catch (\Exception $e){

            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getAllSphere(): LengthAwarePaginator
    {
        return $this->model->with('sub_spheres')->orderBy('id', 'desc')->paginate(config('app.paginate'));
    }

    /**
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllSphereForSelect()
    {
        return $this->model->with('sub_spheres')->orderBy('id', 'desc')->get();
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getSubSphereCategories($id)
    {
//        dd($this->model->find($id)->sub_spheres);
        return $this->model->find($id)->sub_spheres;
    }


    /**
     * @param $id
     * @return array|mixed
     */
    public function getUserSpheres($id)
    {
        $data1 = $this->model->with('sub_spheres')->whereHas('userSpheres', function ($q) use ($id) {
            $q->where('user_id', $id);
        })->get()->toArray();

        $data2 = $this->model->with('sub_spheres')->whereHas('sub_spheres', function ($q) use ($id) {
            $q->whereHas('userSpheres', function ($q) use ($id) {
                $q->where('user_id', $id);
            });
        })->get()->toArray();
        return array_merge($data1, $data2);
    }



    /**
     * @param $search
     * @return mixed
     */
    public function getSphereData($search)
    {
        return $this->model->select( 'name', 'id')->when($search,  function ($q) use ($search) {
            return $q->where('name', 'like', '%'. $search .'%');
        })->limit(10)->get()->toArray();
    }

    public function getSavedAnnouncementSubSphere($ids)
    {
        $xs = $this->model->whereIn('id', $ids)->with('sub_spheres')->get();
        $y = [];
        foreach ($xs as $x) {
           $y[] = $x->getRelations()['sub_spheres']->pluck('name', 'id')->toArray();
        }
        return $y;
    }
}

