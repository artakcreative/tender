<?php

namespace App\Repositories\Admin;

use App\Models\Admin;
use App\Models\CreatedUserHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\ManagerRepositoryInterface;
use Spatie\Permission\Models\Role;

class ManagerRepository extends BaseRepository implements ManagerRepositoryInterface
{
    /**
     * @var
     */
    protected $manager;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Admin::class;
    }

    /**
     * @param array $data
     * @return void
     */
    public function save(array $data)
    {
        DB::transaction(function() use ($data) {
           $manager = Admin::create([
                'name' => $data['name'],
                'surname' => $data['surname'],
                'password' => Hash::make($data['password']),
                'phone' => $data['phone'],
                'email' => $data['email'],
           ]);
            $manager->assignRole('manager');

            CreatedUserHistory::create([
                'from_id' =>  auth()->guard('admin')->user()->id,
                'to_id' => $manager['id']
            ]);
            $this->manager = $manager;
        });

        return $this->manager;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->role('manager')->orderBy('status', 'desc')->get();
    }

    /**
     * @param $id
     * @return Model|ManagerRepository
     */
    public function getById($id)
    {
        return $this->newQuery()->where('id', $id)->first();
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function checkManagerBlocked($id) {
        $manager = $this->model->where('id', $id)
            ->where('status', Admin::BLOCK )
            ->get()->toArray();
        if(count($manager) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @param $permission
     * @return mixed
     */
    public function givePermissions($id, $permission)
    {
        return $this->model
            ->where('id', $id)
            ->first()
            ->givePermissionTo($permission);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function givePermissionNames($id)
    {
        $user = $this->model->find($id);
        return $user->getPermissionNames()->toArray();
    }

    /**
     * @param $id
     * @param $permissions
     * @return mixed
     */
    public function updatePermissions($id, $permissions){
        return $this->model
            ->where('id', $id)
            ->first()
            ->syncPermissions($permissions);
    }

    /**
     * @return mixed
     */
    public function getManagerPermissions()
    {
        return Role::where('name', 'manager')
            ->with('permissions')
            ->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|Model|mixed
     */
    public function blockManager($id)
    {
        return $this->updateById($id, [
            'status'=> Admin::BLOCK,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|Model|mixed
     */
    public function unlockManager($id)
    {
        return $this->updateById($id, [
            'status'=> Admin::ACTIVE,
        ]);
    }

    /**
     * общее количество манажеров
     * @return mixed
     */
    public function managerCount()
    {
        return $this->model->role('manager')->count();
    }
}
