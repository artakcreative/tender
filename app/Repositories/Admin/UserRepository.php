<?php

namespace App\Repositories\Admin;

use App\Events\NotificationUserEvent;
use App\Models\Announcement;
use App\Models\CompetitionType;
use App\Models\CreatedUserHistory;
use App\Models\Tariff;
use App\Models\User;
use App\Models\UserSphere;
use App\Models\UserWantTariffAndActiveSelect;
use App\Notifications\User\GlobalUserNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\UserRepositoryInterface;
use phpDocumentor\Reflection\Types\Integer;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return User::class;
    }


    /**
     * @param array $data
     * @return mixed|string
     */
    public function save(array $data)
    {

        DB::beginTransaction();
        try{
            $user = $this->model->create([
                'name' => $data['name'],
                'surname' => $data['surname'],
                'password' => Hash::make($data['password']),
                'phone' => $data['phone'],
                'email' => $data['email'],
                'email_verified_at' => Carbon::now()->toDateTimeString()
            ]);
            $user->assignRole('user');
            if (isset($data['file'])){
                foreach ($data['file'] as $file){
                    $user->addMedia($file)->toMediaCollection();
                }
            }
            if (isset($data['contract_files'])){
                foreach ($data['contract_files'] as $file){
                    $user->addMedia($file)->toMediaCollection('contract');
                }
            }
            CreatedUserHistory::create([
                'from_id' =>  auth()->guard('admin')->user()->id,
                'to_id' => $user['id']
            ]);
            DB::commit();

            return $user;

        }catch (\Exeption $e){
            DB::rollBack();
            return $e->getMessage();
        }

    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed|string
     */
    public function update(int $id, array $data)
    {
        DB::beginTransaction();
        try {
            $user = $this->model->where('id', $id)->first();

            if (isset($data['physical_success'])) {
                $user->physicalPerson()->updateOrCreate(
                    ['user_id' => $id],
                    [
                        "public_service_license_plate" => $data['public_service_license_plate'],
                        "passport_number" => $data['passport_number'],
                        "passport_take_date" => $data['passport_take_date'],
                        "passport-take_from" => $data['passport_take_from'],
                    ]
                );
            }

            if (isset($data['legal_success'])) {
                $user->legalPerson()->updateOrCreate(
                    ['user_id' => $id],
                    [
                        "tin" => $data['tin'],
                        "legal_address" => $data['legal_address'],
                        "bank" => $data['bank'],
                        "bank_account" => $data['bank_account'],
                    ]
                );
            }

            $user->update([
                'name' => $data['name'],
                'surname' => $data['surname'],
                'email' => $data['email'],
                'phone' => $data['phone'],
            ]);

            if (isset($data['file'])) {
                foreach ($data['file'] as $file) {
                    $user->addMedia($file)->toMediaCollection();
                }
            }

            if (isset($data['contract_files'])) {
                foreach ($data['contract_files'] as $file) {
                    $user->addMedia($file)->toMediaCollection('contract');
                }
            }

            DB::commit();

            return $user;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            DB::rollBack();

            return $e->getMessage();
        }

    }

    /**
     * @param $id
     * @param $blocked
     * @return mixed
     */
    public function updateUserDataBlocked($id, $blocked)
    {
       return $this->model->where('id', $id)->update(['data_blocked' =>  $blocked]);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function activePlaneUser($id, $data)
    {
        $tariffId = Tariff::where('name', $data['tariff'])->first()->id;
        return  $this->model->find((int)$id)->userSpheres()->update([
            'tariff_id' => $tariffId,
            'payed' => UserSphere::PAYED,
            'active' => $data['date']
        ]);
    }

    /**
     * @param int $id
     * @param array $data
     */
    public function addFile(int $id, array $data)
    {
        $user = User::find($id);
        if (isset($data['file'])){
            foreach ($data['file'] as $file){
                $user->addMedia($file)->toMediaCollection();
            }
        }
    }

    public function addManager($data)
    {
        $user = $this->model->find($data['user_id']);
        $user->update([
            'manager_id' => $data['manager_id'],
        ]);
    }

    /**
     * @param $id
     * @return UserRepository|Model|mixed
     */
    public function getUser($id)
    {
        return $this->newQuery()
            ->where('id', $id)
            ->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPlaneHistory($id)
    {
        return $this->model->find((int) $id)->planeHistory()->with('admin')->orderBy('id', 'desc')
            ->paginate(config('app.paginate'));
    }


    /**
     * @param $search
     * @return mixed
     */
    public function getUsers($search)
    {
        return $this->model->select('id', 'name', 'surname', 'email')->when($search,  function ($q) use ($search) {
            return $q->where('name', 'like', '%'. $search .'%')
                ->orWhere('surname', 'like', '%'. $search .'%')
                ->orWhere('email', 'like', '%'. $search .'%');
        })->limit(10)->get()->toArray();
    }

    /**
     * @param $media_id
     * @return int|mixed
     */
    public function deleteFile($media_id){

       return Media::destroy($media_id);
    }


    /**
     * @param $email
     * @param $phone
     * @param $password
     * @return mixed
     */
    public function saveForProfile($email, $phone, $password)
    {
      return $this->model->create([
          'email' => $email,
          'phone' => $phone,
          'password' => Hash::make($password),
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function userEmailVerify($id)
    {
        $user = $this->model->find($id);
        if($user != null) {
            if($user['email_verified_at'] == null) {
                $user->update([
                    'email_verified_at' => Carbon::now()->toDateTimeString()
                ]);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getAll(){
        return $this->model->with('manager')->with(['tariffActived' => function($q){
           return $q->with('tariff');
        }])->orderBy('status','desc')->orderBy('created_at', 'desc')->paginate(config('app.paginate'));
    }

    /**
     * @param $id
     * @param $password
     * @return mixed
     */
    public function updatePassword($id, $password)
    {
        $user = $this->model->find($id);
        return $user->fill(['password' => $password])->save();
    }

    /**
     * @param $id
     * @return Collection|Model
     */
    public function blockUser($id)
    {
        return $this->updateById($id, [
            'status'=> User::BLOCK,
        ]);
    }

    /**
     * @param $id
     * @return Collection|Model
     */
    public function unlockUser($id)
    {
        return $this->updateById($id, [
            'status'=> User::ACTIVE,
        ]);
    }

    /**
     * @param $id
     * @return Model|UserRepository
     */
    public function getById($id)
    {
        return $this->newQuery()->where('id', $id)->first();
    }

    /**
     * @param $userId
     * @return mixed|void
     */
    public function activeSelected($userId)
    {
        return $this->model->find($userId)->tariffActived->actived;
    }


    /**
     * общее количество пользователей
     * @return mixed
     */
    public function userCount()
    {
        return $this->model->count();
    }

    /**
     * @param $userId
     * @return array|mixed
     */
    public function userWantTariff($userId)
    {
        $data = $this->model->find($userId)->tariffActived;
        if($data == null) {
            return [];
        }
        return Tariff::find($data['tariff_id'])->toArray();

    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function filterSave($data)
    {
        $this->model->find((int) $data['id'])->userFilter()->updateOrCreate([
            'user_id' => (int) $data['id'],
         ],[
            'prepayment_available' => $data['prepayment'],
            'date_deadline' => $data['date'],
            'estimated_price_start' =>(int) $data['start'],
            'estimated_price_end' =>(int) $data['end'],
            'business_address' => $data['business_address']['area'][0] ? $data['business_address'] : null,
            'place_of_delivery' => $data['delivery'],
            'availability_of_financial_means' => (int) $data['means'],
            'proof' => (int) $data['proof']
            ]);
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getUserFilter($id)
    {
        $filter = $this->model->find($id)->userFilter;

        if(! $filter) {
            return [];
        }
        return $filter->toArray();
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function mapSuccess($id)
    {
        $action = false;
        $filter = $this->model->find($id)->userFilter;
        if($filter) {
            if(isset($filter['business_address']) &&
                isset($filter['business_address']['latitude'])
                && isset($filter['business_address']['longitude'])){
                $action = true;
            }
        }
        return $action;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function addDeleteFavoriteTender($data)
    {
         return $this->model->find($data['user_id'])->announcements()->toggle($data['announcement_id']);
    }


    /**
     * @param $announcementId
     * @param $typeId
     * @param $pay
     * @return Builder[]|Collection
     */
    public function userAnnouncementPrivate($announcementId, $typeId, $pay)
    {
        return $this->model->query()->where('status', User::ACTIVE)
            ->join('user_want_tariff_and_active_selects as user_want',function ($join){
                $join->on('user_want.user_id', '=', 'users.id');
            })
            ->join('user_spheres as us',function ($join) use($typeId, $pay){
                $join->on('us.user_id', '=', 'users.id')->where('us.tariff_id', '=', $typeId )
                    ->where('us.payed', '=', $pay);
            })
            ->join('announcement_sphere_sub_sphere as ans', function ($join) use ($announcementId){
                $join->on('ans.sphere_id', '=', 'us.sphereable_id')
                    ->whereColumn('us.sphereable_type','ans.sphere_type')
                    ->where('ans.announcement_id', '=', $announcementId);
            })->get();

    }


    /**
     * @param $announcementId
     * @param $pay
     * @return Builder[]|Collection
     */
    public function userAnnouncementState($announcementId, $pay)
    {
        return $this->model->query()->where('status', User::ACTIVE)
            ->join('user_want_tariff_and_active_selects as user_want',function ($join){
                $join->on('user_want.user_id', '=', 'users.id');
            })
            ->join('user_spheres as us',function ($join) use($pay){
                $join->on('us.user_id', '=', 'users.id')
                    ->where('us.payed', '=', $pay);
            })
            ->join('announcement_sphere_sub_sphere as ans', function ($join) use ($announcementId){
                $join->on('ans.sphere_id', '=', 'us.sphereable_id')
                    ->whereColumn('us.sphereable_type','ans.sphere_type')
                    ->where('ans.announcement_id', '=', $announcementId);
            })->get();
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function checkUserStatusBlocked($id)
    {
        $user = $this->model->where('id', $id)->where('status', User::BLOCK)->get()->toArray();
        if(count($user) > 0){
            return true;
        }
        return false;
    }


    /**
     * @param $ids
     * @return mixed
     */
    public function getAllUsersById($ids){
        return $this->model->whereIn('id', $ids)->get();
    }
}
