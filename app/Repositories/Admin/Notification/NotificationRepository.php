<?php

namespace App\Repositories\Admin\Notification;

use App\Models\Admin;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\Notification\NotificationRepositoryInterface;

class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }


    /**
     * @param $id
     * @return mixed
     */
    public function read($id)
    {
        return $this->model()::where('notifiable_type', Admin::class)
            ->whereJsonContains('data->uuid', $id)->update(['read_at' => Carbon::now()]);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function readAll($id)
    {
        return $this->model()::where('notifiable_type', Admin::class)
            ->where('notifiable_id', $id)->update(['read_at' => Carbon::now()]);
    }

    /**
     * @param $id
     * @param $text
     * @param $subject
     * @return mixed|void
     */
    public function getNotify($id, $text, $subject)
    {
       return $this->model()::where('notifiable_type', Admin::class)
           ->where('notifiable_id', $id)
           ->whereJsonContains('data->subject', $subject)
           ->whereJsonContains('data->description', $text)->first();
    }


    /**
     * @param $id
     * @param $subject
     * @return mixed
     */
    public function oldNotifies($id, $subject)
    {
         return $this->model()::where('notifiable_type', Admin::class)
             ->where('notifiable_id', $id)
             ->whereJsonContains('data->subject', $subject)
             ->pluck('data')->toArray();
    }

    /**
     * @param $id
     * @return bool|mixed|null
     */
    public function deleteNotify($id)
    {
         return $this->newQuery()->find($id)->delete();
    }


    /**
     * @param $userId
     * @return mixed
     */
    public function getAllUserNotify($userId)
    {
        $this->model->where('notifiable_type', User::class)
            ->where('notifiable_id', $userId)->update(['read_at' => Carbon::now()]);

        return  $this->model->where('notifiable_type', User::class)
            ->where('notifiable_id', $userId)
            ->orderBy('created_at', 'desc')
            ->select('data')->paginate(10);
    }

    /**
     * @param $id
     * @param $userId
     * @return mixed
     */
    public function deleteUserNotify($id, $userId)
    {
        return $this->model->where('notifiable_type', User::class)
            ->where('notifiable_id', $userId)->whereJsonContains('data->uuid', $id)->delete();
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function notRead($userId)
    {
        $notify = $this->model->where('notifiable_type', User::class)
            ->where('notifiable_id', $userId)->whereNull('read_at')->get()->toArray();

        $data = [];
        if(count($notify) > 0) {
            foreach ($notify as $item) {
                if(isset(json_decode($item['data'])->linkId)) {
                    $data[]= json_decode($item['data'])->linkId;
                }
            }
        }

        return response()->json($data);
    }


}
