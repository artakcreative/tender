<?php

namespace App\Repositories\Admin\Notification;

interface NotificationRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function read($id);

    /**
     * @param $id
     * @return mixed
     */
    public function readAll($id);

    /**
     * @param $id
     * @param $text
     * @param $subject
     * @return mixed
     */
    public function getNotify($id, $text, $subject);

    /**
     * @param $id
     * @param $subject
     * @return mixed
     */
    public function oldNotifies($id, $subject);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteNotify($id);


    /**
     * @param $userId
     * @return mixed
     */
    public function getAllUserNotify($userId);


    /**
     * @param $id
     * @param $userId
     * @return mixed
     */
    public function deleteUserNotify($id, $userId);

    /**
     * @param $userId
     * @return mixed
     */
    public function notRead($userId);
}
