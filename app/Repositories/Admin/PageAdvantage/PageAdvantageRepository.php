<?php

namespace App\Repositories\Admin\PageAdvantage;

use App\Models\Profile\PageAdvantage;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\PageAdvantage\PageAdvantageRepositoryInterface;

class PageAdvantageRepository extends BaseRepository implements PageAdvantageRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageAdvantage::class;
    }

    /**
     * @param $data
     * @return mixed|string
     */
    public function advantageCreate($data)
    {
        DB::beginTransaction();
        try {
            $header = $this->model->create([
                'title' => $data['title'],
                'description' => $data['description']
            ]);
            if (isset($data['image'])) {
                $header->addMedia($data['image'])->toMediaCollection('pageAdvantage');
            }

            DB::commit();

            return $header;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }


    /**
     * @return mixed
     */
    public function getAdvantages()
    {
        $advantages = null;
        $advantages = $this->model->get()->toArray();
        if (count($advantages) > 0) {
            foreach ($advantages as $key => $value) {
                $advantage = $this->model->find($value['id']);
                $urlImage = $advantage->getMedia('pageAdvantage');
                if (count($urlImage) > 0) {
                    $advantages[$key]['url_image'] = $urlImage[0]->getUrl();
                }
            }
        }
        return $advantages;

    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function advantageDelete($id)
    {
        $advantage = $this->model->find($id);;
        if(isset($advantage)) {
            $advantage->clearMediaCollection('pageAdvantage');
        }
      return  $advantage->delete();

    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function advantageUpdate($data)
    {
        DB::beginTransaction();
        try {
            $advantage = $this->model->find((int) $data['id']);
                $advantage->update([
                    'title' => $data['title'],
                    'description' => $data['description']
                ]);
                if (isset($data['image'])) {
                    $advantage->clearMediaCollection('pageAdvantage');
                    $advantage->addMedia($data['image'])->toMediaCollection('pageAdvantage');
                }

            DB::commit();

            return $advantage;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAdvantage($id)
    {
        return $this->model->find($id);
    }
}
