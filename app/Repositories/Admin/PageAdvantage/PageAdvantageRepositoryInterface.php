<?php

namespace App\Repositories\Admin\PageAdvantage;

interface PageAdvantageRepositoryInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function advantageCreate($request);

    /**
     * @return mixed
     */
    public function getAdvantages();

    /**
     * @param $id
     * @return mixed
     */
    public function advantageDelete($id);

    /**
     * @param $request
     * @return mixed
     */
    public function advantageUpdate($request);

    /**
     * @param $id
     * @return mixed
     */
    public function getAdvantage($id);
}
