<?php

namespace App\Repositories\Admin;

use App\Models\Admin;
use App\Models\CreatedUserHistory;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\SettingsRepositoryInterface;

class SettingsRepository extends BaseRepository implements SettingsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Admin::class;
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function changePassword($data)
    {
        $user = auth()->guard('admin')->user();
        $user->password = Hash::make($data['new_password']);
        $user->save();
    }

    /**
     * @param $data
     * @return \Illuminate\Contracts\Auth\Authenticatable|mixed|string|null
     */
    public function addContract($data)
    {
        DB::beginTransaction();
        try{
            $admin = Admin::find(Admin::DEAFULD_ID);
            if (isset($data['contract_files_example'])){
                foreach ($data['contract_files_example'] as $file){
                    $admin->addMedia($file)->toMediaCollection('contract-global');
                }
            }

            DB::commit();
            return $admin;

        }catch (\Exeption $e){
            DB::rollBack();
            return $e->getMessage();
        }

    }
}
