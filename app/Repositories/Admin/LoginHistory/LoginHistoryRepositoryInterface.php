<?php

namespace App\Repositories\Admin\LoginHistory;


interface LoginHistoryRepositoryInterface
{

    /**
     * @param $id
     * @param $type
     * @return mixed
     */
    public function createHistory($id, $type);

    /**
     * @param $data
     * @param $type
     * @return mixed
     */
    public function getUserStatistic($data, $type);

}
