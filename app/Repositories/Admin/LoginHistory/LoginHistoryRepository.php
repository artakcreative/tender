<?php

namespace App\Repositories\Admin\LoginHistory;

use App\Models\LoginHistory;
use Carbon\Carbon;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\LoginHistory\LoginHistoryRepositoryInterface;

class LoginHistoryRepository extends BaseRepository implements LoginHistoryRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LoginHistory::class;
    }


    /**
     * @param $id
     * @param $type
     * @return \Illuminate\Database\Eloquent\Model|mixed
     */
    public function createHistory($id, $type)
    {
        return $this->model()::create([
            'type' => $type,
            'user_id' => $id,
            'login' => Carbon::now(),
        ]);
    }


    /**
     * @param $data
     * @param $type
     * @return mixed
     */
    public function getUserStatistic($data, $type)
    {
        $dateStart = Carbon::parse($data['start'])->toDateTimeString();
        $dateEnd = Carbon::parse($data['end'])->toDateTimeString();
        return $this->model->where('type', $type)
            ->where('user_id', (int) $data['id'])
            ->whereBetween('login', [$dateStart, $dateEnd])
            ->pluck('login')->toArray();

    }

}
