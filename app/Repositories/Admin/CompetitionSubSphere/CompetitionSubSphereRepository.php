<?php

namespace App\Repositories\Admin\CompetitionSubSphere;

use App\Models\CompetitionSubSphere;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionSubSphere\CompetitionSubSphereRepositoryInterface;

class CompetitionSubSphereRepository extends BaseRepository implements CompetitionSubSphereRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompetitionSubSphere::class;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->find($id)->toArray();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function getSubSphereData($search)
    {
        return $this->model->select( 'name', 'id')->when($search,  function ($q) use ($search) {
            return $q->where('name', 'like', '%'. $search .'%');
        })->limit(10)->get()->toArray();
    }
}
