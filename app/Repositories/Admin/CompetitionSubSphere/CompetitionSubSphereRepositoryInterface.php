<?php

namespace App\Repositories\Admin\CompetitionSubSphere;


interface CompetitionSubSphereRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
    /**
     * @param $search
     * @return mixed
     */
    public function getSubSphereData($search);
}
