<?php

namespace App\Repositories\Admin\PageAbout;

use App\Models\Profile\PageAboutUs;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\PageAbout\PageAboutRepositoryInterface;

class PageAboutRepository extends BaseRepository implements PageAboutRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageAboutUs::class;
    }


    /**
     * @param $data
     * @return mixed
     */
    public function aboutCreate($data)
    {
        DB::beginTransaction();
        try {
            $about = $this->model->create([
                'full_name' => $data['full_name'],
                'position' => $data['position'],
            ]);
            if (isset($data['image'])) {
                $about->addMedia($data['image'])->toMediaCollection('pageAbout');
            }

            DB::commit();

            return $about;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getAbouts()
    {
        $abouts = null;
        $abouts = $this->model->get()->toArray();
        if (count( $abouts) > 0) {
            foreach ($abouts as $key => $value) {
                $about = $this->model->find($value['id']);
                $urlImage = $about->getMedia('pageAbout');
                if (count($urlImage) > 0) {
                    $abouts[$key]['url_image'] = $urlImage[0]->getUrl();
                }
            }
        }
        return $abouts;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function aboutDelete($id)
    {
        $about = $this->model->find($id);;
        if(isset($about)) {
            $about->clearMediaCollection('pageAbout');
        }
        return  $about->delete();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function aboutUpdate($data)
    {
        DB::beginTransaction();
        try {
            $about  = $this->model->find((int) $data['id']);
            $about->update([
                'full_name' => $data['full_name'],
                'position' => $data['position'],
            ]);
            if (isset($data['image'])) {
                $about->clearMediaCollection('pageAbout');
                $about->addMedia($data['image'])->toMediaCollection('pageAbout');
            }

            DB::commit();

            return $about;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAbout($id)
    {
        return $this->model->find($id);
    }
}
