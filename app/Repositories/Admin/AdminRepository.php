<?php

namespace App\Repositories\Admin;

use App\Models\ActivedPlaneHistory;
use App\Models\Admin;
use App\Models\CreatedUserHistory;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\AdminRepositoryInterface;

class AdminRepository extends BaseRepository implements AdminRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */

    public function model()
    {
        return Admin::class;
    }

    /**
     * @return mixed
     */

    public function getAll()
    {
        return $this->model->role('admin')->orderBy('id','desc')->paginate(config('app.paginate'));
    }

    /**
     * @param array $data
     * @return mixed
     */

    public function save(array $data)
    {
        DB::beginTransaction();
            try{
               $admin = $this->model->create([
                   'name' => $data['name'],
                   'surname' => $data['surname'],
                   'password' => Hash::make($data['password']),
                   'phone' => $data['phone'],
                   'email' => $data['email'],
               ]);

               $admin->assignRole('admin');

                CreatedUserHistory::create([
                    'from_id' =>  auth()->guard('admin')->user()->id,
                    'to_id' => $admin['id']
                ]);
               DB::commit();

               return $admin;
            }catch (\Exception $e){
              DB::rollBack();
              return $e->getMessage();
            }

    }

    /**
     * @param $email
     * @return mixed
     */
    public function getAdminStatus($email)
    {
        $admin = $this->model->where($email)->first();
        if($admin) {
            return $admin->status;
        }
    }


    /**
     * @param $data
     * @param $id
     * @param $adminId
     * @return mixed
     */
    public function activePlane($data, $id, $adminId)
    {
      return  $this->model->find((int) $adminId)->planeHistory()->create([
            'comment' => $data['comment'],
            'date' => Carbon::now(),
            'action' => ActivedPlaneHistory::ACTIVE,
            'user_id' => (int) $id
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return  $this->model->find($id);
    }


    /**
     * @param $search
     * @return mixed
     */
    public function getManagers($search)
    {
        return $this->model->select('id', 'name', 'surname', 'email')->whereHas('roles', function ($q){
            $q->where('name', 'manager');
        })->when($search,  function ($q) use ($search) {
            return $q->where('name', 'like', '%'. $search .'%')
                ->orWhere('surname', 'like', '%'. $search .'%')
                ->orWhere('email', 'like', '%'. $search .'%');
        })->limit(10)->get()->toArray();
    }

    /**
     * @return mixed
     */
    public function getAllManagersAdminsIds()
    {
        return $this->model->get()->pluck('id');
    }
}
