<?php

namespace App\Repositories\Admin\PageConnect;

interface PageConnectRepositoryInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function connectCreate($request);

    /**
     * @return mixed
     */
    public function getConnects();

    /**
     * @param $id
     * @return mixed
     */
    public function connectDelete($id);

    /**
     * @param $request
     * @return mixed
     */
    public function connectUpdate($request);


    /**
     * @param $id
     * @return mixed
     */
    public function getConnect($id);
}
