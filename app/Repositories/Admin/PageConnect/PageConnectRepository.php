<?php

namespace App\Repositories\Admin\PageConnect;

use App\Models\Profile\PageConnectUs;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\PageConnect\PageConnectRepositoryInterface;

class PageConnectRepository extends BaseRepository implements PageConnectRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageConnectUs::class;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function connectCreate($data)
    {
        DB::beginTransaction();
        try {
            $connect = $this->model->create([
                'subject' => $data['subject']
            ]);
            if (isset($data['image'])) {
                $connect->addMedia($data['image'])->toMediaCollection('pageConnect');
            }

            DB::commit();

            return $connect;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return mixed|null
     */
    public function getConnects()
    {
        $connects = null;
        $connects = $this->model->get()->toArray();
        if (count($connects) > 0) {
            foreach ($connects as $key => $value) {
                $connect = $this->model->find($value['id']);
                $urlImage = $connect->getMedia('pageConnect');
                if (count($urlImage) > 0) {
                    $connects[$key]['url_image'] = $urlImage[0]->getUrl();
                }
            }
        }
        return $connects;
    }



    /**
     * @param $id
     * @return mixed
     */
    public function connectDelete($id)
    {
        $connect = $this->model->find($id);;
        if(isset($connect)) {
            $connect->clearMediaCollection('pageConnect');
        }
        return  $connect->delete();
    }


    /**
     * @param $data
     * @return mixed
     */
    public function connectUpdate($data)
    {
        DB::beginTransaction();
        try {
            $connect  = $this->model->find((int) $data['id']);
            $connect ->update([
                'subject' => $data['subject']
            ]);
            if (isset($data['image'])) {
                $connect->clearMediaCollection('pageConnect');
                $connect->addMedia($data['image'])->toMediaCollection('pageConnect');
            }

            DB::commit();

            return $connect ;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getConnect($id)
    {
        return $this->model->find($id);
    }

}
