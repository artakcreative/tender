<?php

namespace App\Repositories\Admin\HelpDescription;

use App\Models\HelpDescription;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\HelpDescription\HelpDescriptionRepositoryInterface;

class HelpDescriptionRepository extends BaseRepository implements HelpDescriptionRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return  HelpDescription::class;
    }

    /**
     * @return mixed
     */
    public function getHelp()
    {
        return $this->model->first();
    }

    /**
     * @param $data
     * @return mixed|string
     */
    public function helpCreate($data)
    {
        DB::beginTransaction();
        try {
            $description = $this->model->first();
            if (isset($description)) {
                $description->update([
                    'description' => $data['description']
                ]);
            } else {
                $description = $this->model->create([
                    'description' => $data['description']
                ]);
            }
            DB::commit();

            return $description;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function helpDelete($id)
    {
        return $this->model->find($id)->delete();
    }
}
