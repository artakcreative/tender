<?php

namespace App\Repositories\Admin\HelpDescription;


interface HelpDescriptionRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getHelp();

    /**
     * @param $data
     * @return mixed
     */
    public function helpCreate($data);


    /**
     * @param $id
     * @return mixed
     */
    public function helpDelete($id);
}
