<?php

namespace App\Repositories\Admin\ContactUs;

use App\Models\ContactUs;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\ContactUs\ContactUsRepositoryInterface;

class ContactUsRepository extends BaseRepository implements ContactUsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return  ContactUs::class ;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data)
    {
       return $this->model->create([
           'name' => $data['name'],
           'surname' => $data['surname'],
           'email' => $data['email'],
           'phone' => $data['phone'],
       ]);
    }


//    public function update($id, $data)
//    {
//        return $this->model->update([
//            'name' => $data['name'],
//            'surname' => $data['surname'],
//            'email' => $data['email'],
//            'phone' => $data['phone'],
//        ]);
//    }
}
