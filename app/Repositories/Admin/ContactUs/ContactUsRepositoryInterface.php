<?php

namespace App\Repositories\Admin\ContactUs;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface ContactUsRepositoryInterface extends RepositoryContract
{
    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

//    /**
//     * @param $id
//     * @param $data
//     * @return mixed
//     */
//    public function update($id, $data);
}
