<?php

namespace App\Repositories\Admin\CompetitionCustomer;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface CompetitionCustomerRepositoryInterface extends RepositoryContract
{
    /**
     * @param $data
     * @return mixed
     */
    public function saveCustomer($data);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @return mixed
     */
    public function getAllForSelect();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $search
     * @return mixed
     */
    public function getCustomer($search);

    /**
     * @return mixed
     */
    public function allForFilter();

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data);


    /**
     * @param $id
     * @return mixed
     */
    public function remove($id);

}
