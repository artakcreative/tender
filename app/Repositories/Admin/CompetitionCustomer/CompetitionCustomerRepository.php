<?php

namespace App\Repositories\Admin\CompetitionCustomer;

use App\Models\CompetitionCustomer;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionCustomer\CompetitionCustomerRepositoryInterface;

class CompetitionCustomerRepository extends BaseRepository implements CompetitionCustomerRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompetitionCustomer::class;
    }

    public function saveCustomer($data)
    {
        DB::beginTransaction();
        try{
            $customer = $this->model->create(
                [
                    'name'=> $data['customer'],
                    'abbreviation'=>$data['abbreviation'] ?? null,
                    'affiliation_id'=>$data['affiliation_id']
                ]);
            DB::commit();
            return $customer;
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getAll()
    {
        return $this->model->with('affiliation')
                            ->orderBy('id', 'desc')
                            ->paginate(config('app.paginate'));
    }


    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getAllForSelect()
    {
        return $this->model->with('affiliation')
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param $id
     * @return CompetitionCustomerRepository|\Illuminate\Database\Eloquent\Model|mixedgit
     */
    public function getById($id)
    {
        return $this->newQuery()->with('affiliation')->where('id', $id)->first();
    }

    /**
     * выбираем клиента по поиску
     * @param $search
     * @return mixed
     */
    public function getCustomer($search)
    {
        return $this->model->select('id', 'name', 'abbreviation', 'affiliation_id')->when($search,  function ($q) use ($search) {
            return $q->where('name', 'like', '%'. $search .'%')
                ->orWhere('abbreviation', 'like', '%'. $search .'%');

        })->limit(10)->get()->toArray();
    }

    /**
     * @return mixed
     */
    public function allForFilter()
    {
        return $this->model->get()->toArray();
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data)
    {
        return $this->model->find($id)->update([
            'name' => $data['customer'],
            'abbreviation'=> $data['abbreviation'],
            'affiliation_id' => (int) $data['affiliation_id']
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function remove($id)
    {
        return $this->model->find($id)->delete();
    }
}
