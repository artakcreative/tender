<?php

namespace App\Repositories\Admin\CompetitionType;

use App\Models\CompetitionType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionType\CompetitionTypeRepositoryInterface;

class CompetitionTypeRepository extends BaseRepository implements CompetitionTypeRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompetitionType::class;
    }

    /**
     * Сохраненные типа соревнования
     * @param $data
     * @return mixed|string
     */
    public function save($data)
    {
        DB::beginTransaction();
        try{
            $competition_type = $this->model->create(
                [
                    'name'=> $data['competition_type'],
                ]);
            DB::commit();
            return $competition_type;
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * Берём все типы соревнования для показа в index
     * @return mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('id', 'desc')
            ->paginate(config('app.paginate'));
    }



    /**
     * @param $id
     * @return CompetitionTypeRepository|Model
     */
    public function getById($id)
    {
        return $this->newQuery()->where('id', $id)->first();
    }

    /**
     * @param $data
     * @param $id
     * @return CompetitionTypeRepository|Model|mixed|string
     */
    public function update($data, $id)
    {
        $competition_type = $this->getById($id);
        DB::beginTransaction();
        try{
            $competition_type->update(
                [
                    'name'=> $data['competition_type'],
                ]);
            DB::commit();
            return $competition_type;
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }
}
