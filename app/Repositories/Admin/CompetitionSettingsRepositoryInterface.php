<?php

namespace App\Repositories\Admin;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface CompetitionSettingsRepositoryInterface extends RepositoryContract
{
    /**
     * @param $data
     * @return mixed
     */
    public function create_or_update_competition($data);

    /**
     * jnjum enq probelner@ u tox@ sarqum array
     * @param $data
     * @return mixed
     */
    public function explode_trim($data);

}
