<?php

namespace App\Repositories\Admin;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface SettingsRepositoryInterface extends RepositoryContract
{
    /**
     * @param $data
     * @return mixed
     */
    public function changePassword($data);

    /**
     * @param $data
     * @return mixed
     */
    public function addContract($data);
}
