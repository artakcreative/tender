<?php

namespace App\Repositories\Admin\Announcement;


interface AnnouncementRepositoryInterface
{
    /**
     * @return mixed
     */
   public function getAll();

    /**
     * @param int $id
     * @return mixed
     */
   public function getById(int $id);

    /**
     * @param int $id
     * @param $data
     * @return mixed
     */
   public function update(int $id, $data);

    /**
     * @param int $media_id
     * @return mixed
     */
   public function deleteFile(int $media_id);

    /**
     * @param $data
     * @return mixed
     */
    public function saveAnnouncement($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getAnnouncementSubSphereAndSphere($id);

    /**
     * @return mixed
     */
    public function publishedAnnouncementCount();

    /**
     * @param $announcement
     * @param $data
     * @return mixed
     */
    public function saveOrUpdateSphereSubsphere($announcement, $data);


    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return mixed
     */
    public function active($id, $tariff, $filter);


    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return mixed
     */
    public function archive($id, $tariff, $filter);

    /**
     * @return mixed
     */
    public function getAllActiveAnnouncements();

    /**
     * @return mixed
     */
    public function getAllArchivedAnnouncements();

    /**
     * @return mixed
     */
    public function getAllActiveAnnouncementsCount();

    /**
     * @return mixed
     */
    public function getAllArchivedAnnouncementsCount();

    /**
     * @param $id
     * @return mixed
     */
    public function selectActiveAndArchivedAnnouncementsBySpheres($id);

    /**
     * @param $data
     * @return mixed
     */
    public function getManagerStatistic($data);

    /**
     * @param $data
     * @param $id
     * @param $operator
     * @return mixed
     */
    public function getAnnouncementStatistic($data, $id, $operator);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function addManagerToAnnouncement($data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function getAnnouncementHistory($id);

    /**
     * @return mixed
     */
    public function getAllSaved();

}
