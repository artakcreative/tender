<?php

namespace App\Repositories\Admin\Announcement;

use App\Models\Announcement;
use App\Models\AnnouncementSphereSubSphere;
use App\Models\AnnouncementSphereSubSpheres;
use App\Models\AnnouncementSubSphereSphere;
use App\Models\CompetitionApplicationForm;
use App\Models\CompetitionCustomer;
use App\Models\CompetitionCustomerAffiliation;
use App\Models\CompetitionSphere;
use App\Models\CompetitionSubSphere;
use App\Models\CompetitionType;
use App\Models\Tariff;
use App\Models\User;
use App\Models\UserSphere;
use App\Models\UserTemporaryFilter;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\Announcement\AnnouncementRepositoryInterface;
use OwenIt\Auditing\Models\Audit;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class AnnouncementRepository extends BaseRepository implements AnnouncementRepositoryInterface
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Announcement::class;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getAll()
    {
        return $this->model->with('manager')->orderBy('create_date', 'DESC')->paginate(config('app.paginate'));
    }

    /**
     * @return mixed
     */
    public function getAllSaved() {
        return $this->model->where('status', Announcement::SAVED)->where('application_deadline', '>=',Carbon::now()->format('Y-m-d H:i:s'))->with('manager')->orderBy('create_date', 'DESC')->paginate(config('app.paginate'));
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return $this->model->with(['competition_types', 'application_forms', 'customers', 'spheres', 'sub_spheres.sphere', 'users'])->where('id', $id)->first();
    }

    /**
     * @param int $id
     * @param $data
     * @return array|string
     */
    public function update(int $id, $data)
    {
        $applicationProvide = 0;
        $newOrOld = (int)$data['new_or_old'];
        if (isset($data['publicate'])) {
            $data['publicate'] = Announcement::PUBLISHED;
            $newOrOld = $newOrOld + 1;
        } else {
            $data['publicate'] = Announcement::SAVED;
        }


//        DB::beginTransaction();
//            try {

            $announcement = $this->model->with('competition_types','customers')->find($id);
            $chek = $data['provides']['application_provide'] ?? false;
            if($chek != false) {
                $applicationProvide = 1;
            }

            $announcement->update([
                'announcement_name' => $data['announcement_name'],
                'announcement_password' => $data['announcement_password'],
                'competition_type_id' => $data['competition_type'],
                'how_to_apply_id' => $data['how_to_apply'],
                'customer_id' => (int) $data['customer'],
                'customer_affiliation_id' => $data['customer_affiliation'],
                'purchase_item' => $data['purchase_item'],
                'estimated_price' => $data['estimated_price'],
                'return_auction_day' => $data['return_auction_day'] ?? null,
                'application_deadline' => Carbon::parse($data['application_deadline'])->format('Y-m-d H:i:s'),
                'application_opening_day' => $data['application_opening_day'] ? Carbon::parse($data['application_opening_day'])->format('Y-m-d H:i:s') : null,
                'deadline_for_supply' => $data['deadline_for_supply'] ? Carbon::parse($data['deadline_for_supply'])->format('Y-m-d') : null,
                'place_of_delivery' => $data['place_of_delivery'],
                'availability_of_financial_resources' => $data['availability_of_financial_resources'],
                'payment_schedule' => $data['payment_schedule'],
                'prepayment' => $data['prepayment'] ?? null,
                'provides' => $data['provides'] ?? null,
                'application_provide' => $applicationProvide,
                'other_conditions' => $data['other_conditions'],
                'create_date' => now()->format('Y-m-d H:i:s'),
                'status' => $data['publicate'],
                'new_or_old' => $newOrOld,
                'payment_schedule_for_pdf' => $data['payment_schedule_for_pdf'],
                'results_of_monitoring' => $data['results_of_monitoring'],
                'doses' => $data['doses']
            ]);
            if (isset($data['file'])) {
                foreach ($data['file'] as $file) {
                    $announcement->addMedia($file)->toMediaCollection();
                }
            }

            if ($newOrOld == 2 && $announcement->manager_id == null) {
                $announcement->update([
                    'manager_id' => $data['manager_id'],
                ]);
            }
            $this->saveOrUpdateSphereSubsphere($announcement, $data);
            foreach ($announcement->getMedia('generate_pdf') as $pdf) {
                $this->deleteFile($pdf->id);
            }
            $pdf = $this->generatePdfNew($announcement);
            if ($announcement->publishing_date == null && isset($data['publicate']) && $data['publicate'] == 1){
                $announcement->update([
                    'publishing_date' =>now()->format('Y-m-d H:i:s'),
                ]);
            }
//            DB::commit();

            return $pdf;
//        } catch (\Exception $e) {
//            DB::rollBack();
//            return $e->getMessage();
//        }
    }


    public function deleteFile(int $media_id)
    {
        Media::query()->where('id', $media_id)->delete();
    }

    /**
     * сохранение объявления и генерация PDF
     * @param $data
     * @return mixed|string
     */
    public function saveAnnouncement($data)
    {
        $applicationProvide = 0;
        $chek = $data['provides']['application_provide'] ?? false;
        if($chek != false) {
            $applicationProvide = 1;
        }
            $announcement = $this->model->create([
                'announcement_name' => $data['announcement_name'],
                'announcement_password' => $data['announcement_password'],
                'competition_type_id' => $data['competition_type'],
                'how_to_apply_id' => $data['how_to_apply'],
                'customer_id' => $data['customer'],
                'customer_affiliation_id' => $data['customer_affiliation'],
                'purchase_item' => $data['purchase_item'],
                'estimated_price' => $data['estimated_price'],
                'return_auction_day' => $data['return_auction_day'] ?? null,
                'application_opening_day' => $data['application_opening_day'] ? Carbon::parse($data['application_opening_day'])->format('Y-m-d H:i:s') : null,
                'application_deadline' => $data['application_deadline'] ? Carbon::parse($data['application_deadline'])->format('Y-m-d H:i:s') : null,
                'deadline_for_supply' => $data['deadline_for_supply'] ? Carbon::parse($data['deadline_for_supply'])->format('Y-m-d') : null,
                'place_of_delivery' => $data['place_of_delivery'],
                'availability_of_financial_resources' => $data['availability_of_financial_resources'] ?? 0,
                'payment_schedule' => $data['payment_schedule'],
                'prepayment' => $data['prepayment'] ?? null,
                'provides' => $data['provides'] ?? null,
                'application_provide' => $applicationProvide,
                'other_conditions' => $data['other_conditions'],
                'create_date' => now()->format('Y-m-d H:i:s'),
                'payment_schedule_for_pdf' => $data['payment_schedule_for_pdf'],
                'results_of_monitoring' => $data['results_of_monitoring'],
                'doses' => $data['doses']
            ]);

            if (isset($data['file'])) {
                foreach ($data['file'] as $file) {
                    $announcement->addMedia($file)->toMediaCollection();
                }
            }

            $this->saveOrUpdateSphereSubsphere($announcement, $data);
            $pdf = $this->generatePdf($announcement);
            return $pdf;
    }


    /**
     * берем все категории и подкатегории связанные с объявлением
     * @param $id
     * @return array|mixed
     */

    public function getAnnouncementSubSphereAndSphere($id)
    {

        $subSphereIds = $this->model->find($id)->sub_spheres->pluck('id');
        $data = CompetitionSubSphere::with('sphere')
            ->whereIn('id', $subSphereIds)
            ->get()
            ->groupBy('sphere.name')
            ->toArray();
        return $data;
    }


    /**
     * общее количество опубликованных объявлений
     * @return mixed|void
     */
    public function publishedAnnouncementCount()
    {
        return $this->model->where('status', Announcement::PUBLISHED)->count();
    }

    /**
     * функция для сохранения и обновления категорий и подкатегорий
     * @param $announcement
     * @param $data
     * @return void
     */
    public function saveOrUpdateSphereSubsphere($announcement, $data)
    {
        $sphere = [];
        $subsphere = [];
        foreach ($data['sphere-group'] as $item) {
            if (isset($item['subsphere'])) {
                $subsphere = array_merge($subsphere, $item['subsphere']);
            } else {
                $sphere[] = $item['sphere'];
            }
        }
        $announcement->spheres()->sync($sphere);
        $announcement->sub_spheres()->sync($subsphere);
//        dd($announcement->sub_spheres()->auditAttach('competition_sub_spheres', $subsphere));
    }

    /**
     * генерация PDF
     * @param $announcement
     * @return array
     */
    public function generatePdf($announcement): array
    {

        $customerName = CompetitionCustomer::find($announcement->customer_id)->name;
        $competitionTypesName = CompetitionType::find($announcement->competition_type_id)->name;
        $pdf_file = [];
        $pdf = PDF::loadView('admin.announcements.pdf', [
            'announcement' => $announcement,
            'customerName' => $customerName,
            'competitionTypesName' => $competitionTypesName
        ]);
        $pdf->setPaper('A4', 'landscape');
        $stream = $pdf->stream('download.pdf');
        $file = $announcement->addMediaFromStream($stream)->usingFileName('custom-filename.pdf')->usingName('Սեղմագիր-'."$announcement->announcement_name" . '-' ."կոդ$announcement->id")->toMediaCollection('generate_pdf');
        $pdf_file['announcement'] = $announcement;
        $pdf_file['file'] = $file;
        return $pdf_file;
    }


    /**
     * генерация PDF
     * @param $announcement
     * @return array
     */
    public function generatePdfNew($announcement): array
    {

        $customerName = CompetitionCustomer::find($announcement->customer_id)->name;
        $competitionTypesName = CompetitionType::find($announcement->competition_type_id)->name;
        $pdf_file = [];
        $pdf = PDF::loadView('admin.announcements.pdf', [
            'announcement' => $announcement,
            'customerName' => $customerName,
            'competitionTypesName' => $competitionTypesName
        ]);
        $pdf->setPaper('A4', 'landscape');
        $stream = $pdf->stream('download.pdf');
        $file = $announcement->addMediaFromStream($stream)->usingFileName('custom-filename.pdf')->usingName('Սեղմագիր-'."$announcement->announcement_name" . '-' ."կոդ$announcement->id")->toMediaCollection('generate_pdf');
        $pdf_file['announcement'] = $announcement;
        $pdf_file['file'] = $file;
        return $pdf_file;
    }


    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return LengthAwarePaginator
     */
    public function active($id, $tariff, $filter): LengthAwarePaginator
    {
        if ($tariff['name'] == Tariff::BASIC) {
            $typeId = CompetitionCustomerAffiliation::where('customer_affiliation', CompetitionCustomerAffiliation::STATE)->first()->id;
            return $this->queryUserAllTariffs($id, '>=', $typeId, '=', UserSphere::PAYED, '=', $filter);
        }
        return $this->queryUserAllTariffs($id, '>=', 0, '>', UserSphere::PAYED, '=', $filter);
    }


    /**
     * @param $id
     * @param $tariff
     * @param $filter
     * @return LengthAwarePaginator
     */
    public function archive($id, $tariff, $filter): LengthAwarePaginator
    {
        if ($tariff['name'] == Tariff::BASIC) {
            $typeId = CompetitionCustomerAffiliation::where('customer_affiliation', CompetitionCustomerAffiliation::STATE)->first()->id;
            return $this->queryUserAllTariffs($id, '<', $typeId, '=', UserSphere::NOTPAYED, '>=', $filter);
        }
        return $this->queryUserAllTariffs($id, '<', 0, '>', UserSphere::NOTPAYED, '>=', $filter);
    }


    /**
     * @param $id
     * @param $operator
     * @param $typeId
     * @param $operatorType
     * @param $action
     * @param $operatorPay
     * @param $filter
     * @return LengthAwarePaginator
     */
    public function queryUserAllTariffs($id, $operator, $typeId, $operatorType, $action, $operatorPay, $filter): LengthAwarePaginator
    {
        $sphereIds = [];
        $subSphereIds = [];
        $organizationIds = [];
        $date = null;
        $start = null;
        $end = null;
        $prepayment = null;
        $means = null;
        $proof = null;
        $radius = null;
        $coordinates = [];
        if (count($filter) > 0 && isset($filter['reject'])) {
            $filter = [];
            UserTemporaryFilter::where('user_id', $id)->delete();
        }

        if (count($filter) > 0 && isset($filter['my_filter'])) {
            if (isset($filter['estimated_price_start'])) $start = $filter['estimated_price_start'];
            if (isset($filter['estimated_price_end'])) $end = $filter['estimated_price_end'];
            if (isset($filter['place_of_delivery'])) $radius = $filter['place_of_delivery'];
            if (isset($filter['business_address'])) $coordinates = $filter['business_address'];
            if (isset($filter['availability_of_financial_means'])) $means = $filter['availability_of_financial_means'];
            if (isset($filter['proof'])) $proof = $filter['proof'];
            if (isset($filter['date_deadline'])) $date = date('Y-m-d H:i:s', strtotime($filter['date_deadline']));
            if (isset($filter['prepayment_available'])) $prepayment = $filter['prepayment_available'];
        }

        if (count($filter) > 0 && !isset($filter['my_filter'])) {
            if (isset($filter['sphere'])) $sphereIds = array_map('intval', $filter['sphere']);
            if (isset($filter['sub_sphere'])) $subSphereIds = array_map('intval', $filter['sub_sphere']);
            if (isset($filter['date'])) $date = date('Y-m-d H:i:s', strtotime($filter['date']));
            if (isset($filter['organization'])) $organizationIds = array_map('intval', $filter['organization']);
            if (isset($filter['means'])) $means = (int)$filter['means'];
            if (isset($filter['proof'])) $proof = (int)$filter['proof'];
            if (isset($filter['delivery'])) {
                $coordinates = User::find((int)$id)->userFilter['business_address'];
                $radius = (int)$filter['delivery'];
            }
            if (isset($filter['prepayment'])) $prepayment = (int)$filter['prepayment'];
            if (isset($filter['start'])) $start = (int)$filter['start'];
            if (isset($filter['end'])) $end = (int)$filter['end'];

            UserTemporaryFilter::updateOrCreate(
                [
                  'user_id' => $id,
                ], [
                'estimated_price_start' => $start,
                'estimated_price_end' => $end,
                'spheres' => $sphereIds,
                'place_of_delivery' => $radius,
                'availability_of_financial_means' => $means,
                'date_deadline' => $date,
                'prepayment_available' => $prepayment,
                'proof' => $proof,
            ]);

        }

        $payedSpheres = $this->model->with('competition_types', 'spheres')
            ->where('status', '=',Announcement::PUBLISHED)
            ->where('customer_affiliation_id', $operatorType, $typeId)
            ->where('application_deadline', $operator, Carbon::now()->format('Y-m-d H:i:s'))
            ->whereHas('spheres', function ($q) use ($id, $action, $operatorPay, $sphereIds) {
                $q->whereHas('userSpheres', function ($q) use ($id, $action, $operatorPay, $sphereIds) {
                      $q->where('user_id', '=', $id)
                        ->where('sphereable_type', '=', CompetitionSphere::class)
                        ->where('payed', $operatorPay, $action)
                        ->when($sphereIds, function ($q) use ($sphereIds) {
                            $q->whereIn('sphereable_id', $sphereIds);
                        });
                });
            })->when($date, function ($q) use ($date) {
                 $q->whereDate('application_deadline', '<=', $date);
            })->when(isset($start), function ($q) use ($start, $end) {
                 $q->whereBetween('estimated_price', [$start, $end]);
            })->when(count($organizationIds) > 0, function ($q) use ($organizationIds) {
                 $q->whereIn('customer_id', $organizationIds);
            })->when(isset($prepayment), function ($q) use ($prepayment) {
                 $q->where('prepayment->dram_or_percent', $prepayment);
            })->when(isset($means) and $means == 1, function ($q) use ($means) {
                 $q->where('availability_of_financial_resources', $means);
            })->when(isset($means) and $means == 2, function ($q) use ($means) {
                 $q->where('availability_of_financial_resources', 0);
            })->when(isset($proof) and $proof == 1,function ($q) use ($proof) {
                $q->where('application_provide','=', 1);
            })->when(isset($proof) and $proof == 2, function ($q) use ($proof) {
                $q->where('application_provide','=', 0);
            })->when($radius, function ($q) use ($radius, $coordinates) {
                 $q->isWithinMaxDistance($coordinates, $radius);
            });
         return $this->model->with('competition_types', 'sub_spheres.sphere')
            ->where('status', '=', Announcement::PUBLISHED)
            ->where('customer_affiliation_id', $operatorType, $typeId)
            ->where('application_deadline', $operator, Carbon::now()->format('Y-m-d H:i:s'))
            ->whereHas('sub_spheres', function ($q) use ($id, $action, $operatorPay, $subSphereIds, $sphereIds) {
                $q->whereHas('userSpheres', function ($q) use ($id, $action, $operatorPay, $subSphereIds) {
                      $q->where('user_id', $id)
                        ->where('sphereable_type', CompetitionSubSphere::class)
                        ->where('payed', $operatorPay, $action)
                        ->when($subSphereIds, function ($q) use ($subSphereIds) {
                            $q->whereIn('sphereable_id', $subSphereIds);
                        });
                })->when($sphereIds, function ($q) use ($sphereIds) {
                     $q->whereIn('competition_sphere_id', $sphereIds);
                });
            })->when($date, function ($q) use ($date) {
                 $q->whereDate('application_deadline', '<=', $date);
            })->when(isset($start), function ($q) use ($start, $end) {
                $q->whereBetween('estimated_price', [$start, $end]);
            })->when(count($organizationIds) > 0, function ($q) use ($organizationIds) {
                $q->whereIn('customer_id', $organizationIds);
            })->when(isset($prepayment), function ($q) use ($prepayment) {
                $q->where('prepayment->dram_or_percent', $prepayment);
            })->when(isset($means) and $means == 1, function ($q) use ($means) {
                 $q->where('availability_of_financial_resources', $means);
            })->when(isset($means) and $means == 2, function ($q) use ($means) {
                 $q->where('availability_of_financial_resources', 0);
             })->when(isset($proof) and $proof == 1,function ($q) use ($proof) {
                 $q->where('application_provide','=', 1);
             })->when(isset($proof) and $proof == 2, function ($q) use ($proof) {
                 $q->where('application_provide','=', 0);
            })->when($radius, function ($q) use ($radius, $coordinates) {
                 $q->isWithinMaxDistance($coordinates, $radius);
            })->union($payedSpheres)
                ->orderBy('updated_at', 'desc')->paginate(config('app.paginate'));

    }

    /**
     * Выбираем все активные объявления
     * @return mixed
     */
    public function getAllActiveAnnouncements()
    {
        return $this->model->where('application_deadline', '>=', Carbon::now()->format('Y-m-d H:i:s'))
            ->where('status', Announcement::PUBLISHED)
            ->paginate(config('app.paginate'));
    }

    /**
     * Выбираем все архивированные  объявления
     * @return mixed
     */
    public function getAllArchivedAnnouncements()
    {
        return $this->model->where('application_deadline', '<',Carbon::now()->format('Y-m-d H:i:s'))
            ->paginate(config('app.paginate'));
    }

    /**
     * @return mixed
     */
    public function getAllActiveAnnouncementsCount()
    {
        return $this->model->where('application_deadline', '>=', Carbon::now()->format('Y-m-d H:i:s'))
            ->where('status', Announcement::PUBLISHED)
            ->get()->count();
    }

    /**
     * @return mixed
     */
    public function getAllArchivedAnnouncementsCount()
    {
        return $this->model->where('application_deadline', '<',Carbon::now()->format('Y-m-d H:i:s'))
            ->get()->count();
    }

    /**
     * выбираем  активные и архивированные  объявлений по сферам
     * @param $id
     * @return array
     */
    public function selectActiveAndArchivedAnnouncementsBySpheres($id): array
    {
        $activeAnnouncementsBySpheres = $this->queryForSelectActiveAndArchivedAnnouncementsBySpheres($id, '>=');
        $archivedAnnouncementsBySpheres = $this->queryForSelectActiveAndArchivedAnnouncementsBySpheres($id, '<');
        return [$activeAnnouncementsBySpheres, $archivedAnnouncementsBySpheres];
    }

    /**
     * запрос для выборки активных и архивированных объявлений по сферам
     * @param $id
     * @param $operator
     * @return mixed
     */
    public function queryForSelectActiveAndArchivedAnnouncementsBySpheres($id, $operator)
    {
        return $this->model->whereHas('sub_spheres', function ($q) use ($id, $operator) {
            $q->where('sphere_type', CompetitionSubSphere::class)
                ->whereHas('sphere', function ($q) use ($id, $operator) {
                    $q->whereIn('competition_sphere_id', $id)
                        ->where('status', Announcement::PUBLISHED)
                        ->whereDate('application_deadline', $operator, Carbon::today()->toDateString());
                });
        })->orWhereHas('spheres', function ($q) use ($id, $operator) {
            $q->whereIn('sphere_id', $id)
                ->where('status', Announcement::PUBLISHED)
                ->whereDate('application_deadline', $operator, Carbon::today()->toDateString());
        })->get();

    }

    /**
     * @param $data
     * @return mixed
     */
    public function getManagerStatistic($data)
    {
        $dateStart = Carbon::parse($data['start'])->toDateTimeString();
        $dateEnd = Carbon::parse($data['end'])->toDateTimeString();
        return $this->model->select('manager_id', 'status', 'announcement_name', 'created_at')
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('manager_id', (int)$data['id'])
            ->where('status', Announcement::PUBLISHED)
            ->get()->toArray();
    }

    /**
     * @param $data
     * @param $id
     * @param $operator
     * @return mixed
     */
    public function getAnnouncementStatistic($data, $id, $operator)
    {
        $dateStart = Carbon::parse($data['start'])->toDateTimeString();
        $dateEnd = Carbon::parse($data['end'])->toDateTimeString();
        return $this->model->select('manager_id', 'status', 'announcement_name', 'created_at')
            ->whereBetween('created_at', [$dateStart, $dateEnd])
            ->where('status', Announcement::PUBLISHED)
            ->where('customer_affiliation_id', $operator, $id)
            ->get()->toArray();
    }

    /**
     * @param $data
     * @param $id
     * @return mixed|string
     */
    public function addManagerToAnnouncement($data, $id)
    {
        DB::beginTransaction();
        try {
            $announcement = $this->model->find($id);
            $announcement->update([
                'manager_id' => $data['manager_id'],
            ]);

            DB::commit();

            return $announcement;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * история объявлений
     * @param $id
     * @return LengthAwarePaginator|mixed
     */
    public function getAnnouncementHistory($id)
    {
        $announcement_audit = $this->model->where('id', $id)->with('audits', function ($q) {
            return $q->where('event', 'updated')->orderBy('created_at', 'DESC');
        })->first();

        $competition_type = $this->getParametersForAudit(CompetitionType::class);
        $announcement_audit['competition_type'] = $competition_type;

        $how_to_apply = $this->getParametersForAudit(CompetitionApplicationForm::class);
        $announcement_audit['how_to_apply'] = $how_to_apply;

        $customer = $this->getParametersForAudit(CompetitionCustomer::class);
        $announcement_audit['customer'] = $customer;

        $customer_affiliation = $this->getParametersForAudit(CompetitionCustomerAffiliation::class, 'customer_affiliation');
        $announcement_audit['customer_affiliation'] = $customer_affiliation;

        return $announcement_audit;

    }

    /**
     * @param $model
     * @param string $param
     * @return mixed
     */
    public function getParametersForAudit($model, string $param = 'name')
    {
        return $model::get()->pluck($param, 'id')->toArray();
    }


    /**
     * @param $announcementId
     * @param $filter
     * @return false
     */
    public function getUserByFilter($announcementId, $filter)
    {
        $date = null;
        $start = null;
        $end = null;
        $prepayment = null;
        $means = null;
        $proof = null;
        $radius = null;
        $coordinates = [];

        if (count($filter) > 0) {
            if (isset($filter['estimated_price_start'])) $start = $filter['estimated_price_start'];
            if (isset($filter['estimated_price_end'])) $end = $filter['estimated_price_end'];
            if (isset($filter['place_of_delivery'])) $radius = $filter['place_of_delivery'];
            if (isset($filter['business_address'])) $coordinates = $filter['business_address'];
            if (isset($filter['availability_of_financial_means'])) $means = $filter['availability_of_financial_means'];
            if (isset($filter['proof'])) $proof = $filter['proof'];
            if (isset($filter['date_deadline'])) $date = date('Y-m-d H:i:s', strtotime($filter['date_deadline']));
            if (isset($filter['prepayment_available'])) $prepayment = $filter['prepayment_available'];
            $announcement = $this->model->where('id', $announcementId)
                ->when($date, function ($q) use ($date) {
                    $q->whereDate('application_deadline', '<=', $date);
                })->when(isset($start), function ($q) use ($start, $end) {
                    $q->whereBetween('estimated_price', [$start, $end]);
                })->when(isset($prepayment), function ($q) use ($prepayment) {
                    $q->where('prepayment->dram_or_percent', $prepayment);
                })->when(isset($means) and $means == 1, function ($q) use ($means) {
                    $q->where('availability_of_financial_resources', $means);
                })->when(isset($means) and $means == 2, function ($q) use ($means) {
                    $q->where('availability_of_financial_resources', 0);
                })->when(isset($proof) and $proof == 1, function ($q) use ($proof) {
                    $q->where('application_provide', '=', 1);
                })->when(isset($proof) and $proof == 2, function ($q) use ($proof) {
                    $q->where('application_provide', '=', 0);
                })->when($radius, function ($q) use ($radius, $coordinates) {
                    $q->isWithinMaxDistance($coordinates, $radius);
                })->get()->toArray();
            if(count($announcement) > 0) {
                return true;
            }
            return false;
        }

        return true;
    }
}
