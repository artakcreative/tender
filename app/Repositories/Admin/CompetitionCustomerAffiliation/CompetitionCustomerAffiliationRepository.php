<?php

namespace App\Repositories\Admin\CompetitionCustomerAffiliation;

use App\Models\CompetitionCustomerAffiliation;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\CompetitionCustomerAffiliation\CompetitionCustomerAffiliationRepositoryInterface;

class CompetitionCustomerAffiliationRepository extends BaseRepository implements CompetitionCustomerAffiliationRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompetitionCustomerAffiliation::class;
    }


    /**
     * @param $data
     * @return mixed|string
     */
    public function saveCustomerAffiliation($data)
    {
        DB::beginTransaction();
        try{
            $customer_affiliation = $this->model->create(
                [
                    'customer_affiliation'=> $data['customer_affiliation'],
                ]);
            DB::commit();
            return $customer_affiliation;
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }

    public function getByName($name)
    {
        return $this->model->where('customer_affiliation', $name)->first()->toArray();
    }
}
