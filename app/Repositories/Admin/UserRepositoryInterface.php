<?php

namespace App\Repositories\Admin;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface UserRepositoryInterface extends RepositoryContract
{
    /**
     * @param array $dada
     * @return mixed
     */
    public function save(array $data);

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id,array $data);

    /**
     * @param $id
     * @param $blocked
     * @return mixed
     */
    public function updateUserDataBlocked($id, $blocked);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function activePlaneUser($id, $data);


    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function addFile(int $id, array $data);

    /**
     * @param $data
     * @return mixed
     */
    public function addManager($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getUser($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getPlaneHistory($id);

    /**
     * @param $search
     * @return mixed
     */
    public function getUsers($search);

    /**
     * @param $media_id
     * @return mixed
     */
    public function deleteFile($media_id);


    /**
     * @param $email
     * @param $phone
     * @param $password
     * @return mixed
     */
    public function saveForProfile($email, $phone, $password);

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $id
     * @param $password
     * @return mixed
     */
    public function updatePassword($id, $password);

    /**
     * @param $id
     * @return mixed
     */
    public function blockUser($id);

    /**
     * @param $id
     * @return mixed
     */
    public function unlockUser($id);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);


    /**
     * @param $userId
     * @return mixed
     */
    public function activeSelected($userId);

    /**
     * @return mixed
     */
    public function userCount();

    /**
     * @param $userId
     * @return mixed
     */
    public function userWantTariff($userId);

    /**
     * @param $data
     * @return mixed
     */
    public function filterSave($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getUserFilter($id);

    /**
     * @param $id
     * @return mixed
     */
    public function mapSuccess($id);

    /**
     * @param $data
     * @return mixed
     */
    public function addDeleteFavoriteTender($data);

    /**
     * @param $announcementId
     * @param $typeId
     * @param $pay
     * @return mixed
     */
    public function userAnnouncementPrivate($announcementId, $typeId, $pay);

    /**
     * @param $announcementId
     * @param $pay
     * @return mixed
     */
    public function userAnnouncementState($announcementId, $pay);

    /**
     * @param $id
     * @return mixed
     */
    public function checkUserStatusBlocked($id);

    /**
     * @param $ids
     * @return mixed
     */
    public function getAllUsersById($ids);

}
