<?php

namespace App\Repositories\Admin;


interface AdminRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

    /**
     * @param $email
     * @return mixed
     */
    public function getAdminStatus($email);


    /**
     * @param $data
     * @param $id
     * @param $adminId
     * @return mixed
     */
    public function activePlane($data, $id, $adminId);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $search
     * @return mixed
     */
    public function getManagers($search);

    /**
     * @return mixed
     */
    public function getAllManagersAdminsIds();
}
