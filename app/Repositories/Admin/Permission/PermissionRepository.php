<?php

namespace App\Repositories\Admin\Permission;

use App\Models\Permission;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\Permission\PermissionRepositoryInterface;

class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }

    /**
     * @param $text
     * @return mixed
     */
    public function getTextTranslate($text)
    {
        return $this->newQuery()->whereIn('name' , $text)->get();
    }
}
