<?php

namespace App\Repositories\Admin\Permission;


interface PermissionRepositoryInterface
{
    /**
     * @param $text
     * @return mixed
     */
    public function getTextTranslate($text);
}
