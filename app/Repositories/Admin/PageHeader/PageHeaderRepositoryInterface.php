<?php

namespace App\Repositories\Admin\PageHeader;


interface PageHeaderRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function headerCreate($data);

    /**
     * @return mixed
     */
    public function getHeader();


    /**
     * @param $id
     * @return mixed
     */
    public function headerDelete($id);

}
