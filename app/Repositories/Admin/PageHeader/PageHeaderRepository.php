<?php

namespace App\Repositories\Admin\PageHeader;

use App\Models\Profile\PageHeader;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\PageHeader\PageHeaderRepositoryInterface;

class PageHeaderRepository extends BaseRepository implements PageHeaderRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageHeader::class;
    }


    /**
     * @param $data
     * @return mixed|string
     */
    public function headerCreate($data)
    {
        DB::beginTransaction();
        try {
            $header = $this->model->first();
            if (isset($header)) {
                $header->update([
                    'description' => $data['description']
                ]);
                if (isset($data['image'])) {
                    $header->clearMediaCollection('pageHeader');
                    $header->addMedia($data['image'])->toMediaCollection('pageHeader');
                }
            } else {
                $header = $this->model->create([
                    'description' => $data['description']
                ]);
                if (isset($data['image'])) {
                    $header->addMedia($data['image'])->toMediaCollection('pageHeader');
                }
            }
            DB::commit();

            return $header;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->model->first();
    }

    /**
     * @param $id
     * @return mixed|void
     */
    public function headerDelete($id)
    {
        $header = $this->model->find($id);
        if(isset($header)) {
            $header->clearMediaCollection('pageHeader');
        }
       return $header->delete();

    }

}
