<?php

namespace App\Repositories\Admin\PageInfo;

interface PageInfoRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function infoCreate($data);

    /**
     * @return mixed
     */
    public function getInfo();

    /**
     * @param $id
     * @return mixed
     */
    public function infoDelete($id);
}
