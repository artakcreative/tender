<?php

namespace App\Repositories\Admin\PageInfo;

use App\Models\Profile\PageInfoUs;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\Admin\PageInfo\PageInfoRepositoryInterface;

class PageInfoRepository extends BaseRepository implements PageInfoRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PageInfoUs::class;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function infoCreate($data)
    {
        DB::beginTransaction();
        try {
            $info = $this->model->first();
            if (isset($info)) {
                $info->update([
                    'description' => $data['description'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'more_phone' => $data['more_phone'],
                    'youtube' => $data['youtube'],
                    'facebook' => $data['facebook'],
                    'linkedin' => $data['linkedin'],
                    'full_address' => $data['full_address'],
                    'footer_info' => $data['footer_info'],
                ]);
            } else {
                $info = $this->model->create([
                    'description' => $data['description'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'more_phone' => $data['more_phone'],
                    'youtube' => $data['youtube'],
                    'facebook' => $data['facebook'],
                    'linkedin' => $data['linkedin'],
                    'full_address' => $data['full_address'],
                    'footer_info' => $data['footer_info'],
                ]);
            }
            DB::commit();

            return $info;

        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->model->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function infoDelete($id)
    {
        return  $this->model->find($id)->delete();
    }
}
