<?php

namespace App\Repositories\User\Tariff;

use App\Models\Tariff;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\User\Tariff\TariffRepositoryInterface;

class TariffRepository extends BaseRepository implements TariffRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Tariff::class;
    }

    public function getTariffs()
    {
        return $this->model->get();
    }

    public function getTariffById($id)
    {
        return $this->newQuery()->where('id', $id)->first();
    }
}
