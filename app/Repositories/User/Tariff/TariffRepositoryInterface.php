<?php

namespace App\Repositories\User\Tariff;

use NamTran\LaravelMakeRepositoryService\Repository\RepositoryContract;

interface TariffRepositoryInterface extends RepositoryContract
{
    /**
     * @return mixed
     */
    public function getTariffs();

    /**
     * @param $id
     * @return mixed
     */
    public function getTariffById($id);
}
