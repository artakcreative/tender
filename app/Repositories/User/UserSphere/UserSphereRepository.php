<?php

namespace App\Repositories\User\UserSphere;

use App\Models\CompetitionSphere;
use App\Models\CompetitionSubSphere;
use App\Models\Tariff;
use App\Models\UserSphere;
use App\Models\UserWantTariffAndActiveSelect;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\User\UserSphere\UserSphereRepositoryInterface;

class UserSphereRepository extends BaseRepository implements UserSphereRepositoryInterface
{
    /**
     * @var
     */
    public $activeTime;

    /**
     * @var
     */
    public $tariff;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserSphere::class;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserSpheres($userId)
    {
        return $this->model->where('user_id', $userId)->get()->toArray();
    }

    /**
     * @return mixed|void
     */
    public function tariffs() {
        return Tariff::get()->toArray();
    }

    /**
     * @param $userId
     * @return array|mixed|null
     */
    public function getTariff($userId)
    {
        $tariff = null;
        $info = [];
        $defaultTariff = $this->model->where('user_id', $userId)->first();
        if($defaultTariff) {
            $info = $defaultTariff->tariff->toArray();
        }
        $sphere = $this->model->where('user_id', $userId)
            ->where('payed', UserSphere::PAYED)
            ->orderBy('active', 'desc')->first();
        if($sphere) {
            $active = Carbon::parse($sphere->active);
            $tariff = array_merge($sphere->tariff->toArray(), ['active' => $active->format('d.m.Y')]);
        }

         return collect([$tariff, $info]);
    }


    /**
     * @param $data
     * Ստեղծում է ենթաոլորտ, երբ որ օգտատերը առաջին անգամ է մուտք գործում համակարգ։
     * @return mixed
     */
    public function saveUserFirstSphere($data)
    {
        $defaultTariff = Tariff::first();
        UserWantTariffAndActiveSelect::create([
            'user_id' => $data['user_id'],
            'tariff_id' => $defaultTariff->id
        ]);
        if(isset($data['sphere_id'])) {
            $sphere = CompetitionSphere::find((int) $data['sphere_id']);
            $userSphere = new $this->model();
            $userSphere->user_id = $data['user_id'];
            $userSphere->tariff_id = $defaultTariff->id;
            $sphere->userSpheres()->save($userSphere);
        } else {
            foreach ($data['sub_spheres'] as $value) {
                $sub = CompetitionSubSphere::find((int) $value);
                $userSphere = new $this->model();
                $userSphere->user_id = $data['user_id'];
                $userSphere->tariff_id = $defaultTariff->id;
                $sub->userSpheres()->save($userSphere);
            }
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSelectedSpheres($id)
    {
        $sphereIds = $this->model->where('user_id', $id)
            ->where('sphereable_type', CompetitionSphere::class)->where('payed', 1)
            ->get()->map(function ($item) {
                return $item->sphereable->id;
            });

        $subSphereIds = $this->model->where('user_id', $id)->where('payed', 1)
            ->where('sphereable_type', CompetitionSubSphere::class)
            ->get()->map(function ($item) {
            return $item->sphereable->id;
        });

        return $this->sphereWithSubSphere($id, $sphereIds, $subSphereIds);

    }


    /**
     * @param $id
     * @return array|mixed
     */
    public function getNotPayed($id)
    {
        $sphereIds = $this->model->where('user_id', $id)
            ->where('sphereable_type', CompetitionSphere::class)->where('payed', 0)
            ->get()->map(function ($item) {
                return $item->sphereable->id;
            });

        $subSphereIds = $this->model->where('user_id', $id)->where('payed', 0)
            ->where('sphereable_type', CompetitionSubSphere::class)
            ->get()->map(function ($item) {
                return $item->sphereable->id;
            });

        return $this->sphereWithSubSphere($id, $sphereIds, $subSphereIds);
    }


    /**
     * @param $id
     * @param $sphereIds
     * @param $subSphereIds
     * @return array
     */
    public function sphereWithSubSphere($id, $sphereIds, $subSphereIds)
    {

        $data = CompetitionSubSphere::with(['sphere' => function ($query) {
            $query->select('id', 'name');
        }])->with(['userSpheres' => function($item) use($id){
            $item->select('user_id', 'payed', 'sphereable_id', 'tariff_id', 'active')->with(['tariff' => function($q) {
                $q->select('id', 'price', 'name');
            }])->where('user_id', $id);
        }])->whereIn('id', $subSphereIds)
            ->get()
            ->groupBy('sphere.id')
            ->toArray();

        $data1 = CompetitionSphere::select('id', 'name')->with(['userSpheres' => function($item) use($id){
            $item->select('user_id', 'payed', 'sphereable_id', 'tariff_id', 'active')->with(['tariff' => function($q) {
                $q->select('id', 'price', 'name');
            }])->where('user_id', $id);
        }])->whereIn('id', $sphereIds)
            ->get()
            ->groupBy('id')
            ->toArray();
        if(count($data1) > 0) {
            foreach ($data1 as $key => $value) {
                foreach ($value as $key1 => $item) {
                    $data1[$key][$key1]['sphere'] = [
                        'id' => $item['id'],
                        'name' => $item['name']
                    ];
                    unset( $data1[$key][$key1]['id'], $data1[$key][$key1]['name']);
                }
            }
        }

        return array_merge($data, $data1);
    }


    /**
     * @param $data
     * @return mixed|void
     */
    public function deleteNotPayed($data)
    {
        if(count($data['sub_spheres_ids']) > 0) {
            $this->model->where('user_id', $data['user_id'])
                ->whereIn('sphereable_id', $data['sub_spheres_ids'])
                ->where('sphereable_type', CompetitionSubSphere::class)
                ->delete();
        }else {
            $this->model->where('user_id', $data['user_id'])
                ->where('sphereable_id', $data['sphere_id'])
                ->where('sphereable_type', CompetitionSphere::class)
                ->delete();
        }
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function save($data)
    {
        UserWantTariffAndActiveSelect::where('user_id', (int)$data['user_id'])
            ->update([
                'tariff_id' => (int)$data['tariff_id'],
                'actived' => UserWantTariffAndActiveSelect::BLOCKED
            ],);
        $checkedTariffId = $this->model->where('user_id', (int)$data['user_id'])->first()->tariff->id;
        $info = $this->model->where('user_id', (int)$data['user_id'])
            ->where('payed', UserSphere::PAYED)
            ->whereNotNull('active')
            ->first();

        if($info) {
            $this->activeTime = $info->active;
            $this->tariff = $info->tariff_id;
            $this->model->where('user_id', (int)$data['user_id'])->whereNotNull('active')->delete();
            if(isset($data['spheres_change']) && count($data['spheres_change'])) {
                foreach ($data['spheres_change'] as $value) {
                    $sphere = CompetitionSphere::find((int) $value);
                    $userSphere = new $this->model();
                    $userSphere->user_id = (int)$data['user_id'];
                    $userSphere->active = $this->activeTime;
                    $userSphere->payed = UserSphere::PAYED;
                    $userSphere->tariff_id = $this->tariff;
                    $sphere->userSpheres()->save($userSphere);
                }
            }

            if(isset($data['sub_spheres_change']) && count($data['sub_spheres_change'])) {
                foreach ($data['sub_spheres_change'] as $value) {
                    $subSphere = CompetitionSubSphere::find((int) $value);
                    $userSphere = new $this->model();
                    $userSphere->user_id = (int)$data['user_id'];
                    $userSphere->active = $this->activeTime;
                    $userSphere->payed = UserSphere::PAYED;
                    $userSphere->tariff_id = $this->tariff;
                    $subSphere->userSpheres()->save($userSphere);
                }
            }
        }

        if(isset($data['spheres']) && count($data['spheres'])) {
            foreach ($data['spheres'] as $value) {
                $sphere = CompetitionSphere::find((int) $value);
                $userSphere = new $this->model();
                $userSphere->user_id = (int)$data['user_id'];
                $userSphere->tariff_id = $checkedTariffId;
                $sphere->userSpheres()->save($userSphere);
            }
        }

        if(isset($data['sub_spheres']) && count($data['sub_spheres'])) {
            foreach ($data['sub_spheres'] as $value) {
                $subSphere  = CompetitionSubSphere::find((int) $value);
                $userSphere = new $this->model();
                $userSphere->user_id = (int)$data['user_id'];
                $userSphere->tariff_id = $checkedTariffId;
                $subSphere->userSpheres()->save($userSphere);
            }
        }

        $this->model->where('user_id', (int)$data['user_id'])->update([
            'tariff_id' => (int)$data['tariff_id']
        ]);


    }


    /**
     * @param $id
     * @return bool|mixed
     */
    public function getUserTariffTrueOrFalse($id)
    {
        $sphere =  $this->model->where('user_id', $id)->where('payed', UserSphere::PAYED)->first();
        if($sphere == null) {
            return false;
        }
       return true;
    }


}
