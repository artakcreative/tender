<?php

namespace App\Repositories\User\UserSphere;


interface UserSphereRepositoryInterface
{

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserSpheres($userId);

    /**
     * @return mixed
     */
    public function tariffs();

    /**
     * @param $userId
     * @return mixed
     */
    public function getTariff($userId);

    /**
     * @param $data
     * @return mixed
     */
    public function saveUserFirstSphere($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getSelectedSpheres($id);

    /**
     * @param $userId
     * @return mixed
     */
    public function getNotPayed($userId);

    /**
     * @param $data
     * @return mixed
     */
    public function deleteNotPayed($data);


    /**
     * @param $data
     * @return mixed
     */
    public function save($data);

    /**
     * @param $id
     * @return mixed
     */
    public function getUserTariffTrueOrFalse($id);
}
