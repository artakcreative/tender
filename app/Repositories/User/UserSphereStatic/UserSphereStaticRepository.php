<?php

namespace App\Repositories\User\UserSphereStatic;

use App\Models\CompetitionSphere;
use App\Models\UserSphereStatic;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use NamTran\LaravelMakeRepositoryService\Repository\BaseRepository;
use App\Repositories\User\UserSphereStatic\UserSphereStaticRepositoryInterface;

class UserSphereStaticRepository extends BaseRepository implements UserSphereStaticRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserSphereStatic::class;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createNew($data)
    {
        return $this->model->create($data);
    }


    /**
     * @param $userId
     * @return array
     */
    public function getStatisticDefault($userId)
    {
        $info = [];
        $data = DB::table('user_sphere_statics as uss')->where('uss.user_id', $userId)
            ->join('competition_spheres as cs', 'uss.sphere_id', '=', 'cs.id')
            ->selectRaw('year(uss.created_at) AS year, month(uss.created_at) AS month, cs.name AS sphere_name')
            ->groupBy('uss.id')
            ->get()->groupBy(['month', 'sphere_name'])->toArray();

        if(count($data) > 0) {
            $monthsCount = config('statistic.monthsCount');
            foreach ($data as $key => $value) {
                foreach ($value as $keys => $item) {
                    $info[] = [
                        'name' => $keys,
                        'data' => array_replace($monthsCount, [$key - 1 => count($item)])
                    ];

                }
            }
        }
            return $info;
    }

    /**
     * @param $userId
     * @param $data
     * @return array
     */
    public function getStatisticMount($userId, $data)
    {
        $dateStart = null;
        $dateEnd = null;
        $info = [];
        if(count($data) > 0) {
            $dateStart = Carbon::parse($data['start'])->toDateTimeString();
            $dateEnd = Carbon::parse($data['end'])->toDateTimeString();
        }

        $statics = DB::table('user_sphere_statics as uss')->where('uss.user_id', $userId)
            ->when($dateStart, function ($q) use ($dateStart, $dateEnd ) {
                return $q->whereBetween('uss.created_at', [$dateStart, $dateEnd]);
            })
            ->join('competition_spheres as cs', 'uss.sphere_id', '=', 'cs.id')
            ->selectRaw('year(uss.created_at) AS year, month(uss.created_at) AS month, cs.name AS sphere_name')
            ->groupBy('uss.id')
            ->get()->groupBy(['month', 'sphere_name'])->toArray();

        $type = 'mounts';
        if(count($statics) > 0) {
            $monthsCount = config('statistic.monthsCount');
            foreach ($statics as  $key => $value) {
                foreach ($value as $keys =>$item) {
                    $info[]= [
                        'name' => $keys,
                        'data' => array_replace($monthsCount, [$key-1 => count($item)])
                    ];
                }
            }
        }

        return [$info, $type];
    }


    /**
     * @param $userId
     * @param $data
     * @return array|mixed
     */
    public function getStatisticWeek($userId, $data)
    {
        $dateStart = null;
        $dateEnd = null;
        $info = [];
        if(count($data) > 0) {
            $dateStart = Carbon::parse($data['start'])->toDateTimeString();
            $dateEnd = Carbon::parse($data['end'])->toDateTimeString();
        }


        $statics = DB::table('user_sphere_statics as uss')->where('uss.user_id', $userId)
            ->when($dateStart, function ($q) use ($dateStart, $dateEnd ) {
                return $q->whereBetween('uss.created_at', [$dateStart, $dateEnd]);
            })
            ->join('competition_spheres as cs', 'uss.sphere_id', '=', 'cs.id')
            ->selectRaw('year(uss.created_at) AS year, week(uss.created_at) AS week, cs.name AS sphere_name')
            ->groupBy('uss.id')
            ->get()->groupBy(['week', 'sphere_name'])->toArray();

        $type = 'weeks';
        if(count($statics) > 0) {
            $weeksCount = config('statistic.weeksCount');
            foreach ($statics as  $key => $value) {
                foreach ($value as $keys =>$item) {
                    $info[]= [
                        'name' => $keys,
                        'data' => array_replace($weeksCount, [$key-1 => count($item)])
                    ];

                }
            }
        }
        return [$info, $type];
    }


    /**
     * @param $userId
     * @param $data
     * @return array
     */
    public function getStatisticQuarter($userId, $data)
    {
        $dateStart = null;
        $dateEnd = null;
        $info = [];
        if(count($data) > 0) {
            $dateStart = Carbon::parse($data['start'])->toDateTimeString();
            $dateEnd = Carbon::parse($data['end'])->toDateTimeString();
        }

        $statics = DB::table('user_sphere_statics as uss')->where('uss.user_id', $userId)
            ->when($dateStart, function ($q) use ($dateStart, $dateEnd ) {
                return $q->whereBetween('uss.created_at', [$dateStart, $dateEnd]);
            })
            ->join('competition_spheres as cs', 'uss.sphere_id', '=', 'cs.id')
            ->selectRaw('year(uss.created_at) AS year, quarter(uss.created_at) AS quarters, cs.name AS sphere_name')
            ->groupBy('uss.id')
            ->get()->groupBy(['quarters', 'sphere_name'])->toArray();

        $type = 'quarters';
        if(count($statics) > 0) {
            $quarterlyCount = config('statistic.quarterlyCount');
            foreach ($statics as  $key => $value) {
                foreach ($value as $keys =>$item) {
                    $info[]= [
                        'name' => $keys,
                        'data' => array_replace($quarterlyCount, [$key-1 => count($item)])
                    ];
                }
            }
        }

        return [$info, $type];

    }
}
