<?php

namespace App\Repositories\User\UserSphereStatic;


interface UserSphereStaticRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function createNew($data);

    /**
     * @param $userId
     * @return mixed
     */
    public function getStatisticDefault($userId);

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function getStatisticMount($userId, $data);

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function getStatisticWeek($userId, $data);

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function getStatisticQuarter($userId, $data);

}
