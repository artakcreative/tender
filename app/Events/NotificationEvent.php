<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $id;
    public $subject;
    public $description;
    public $uuid;
    public $linkId;

    /**
     * Create a new event instance.
     * @param $linkId
     * @param $id
     * @param $subject
     * @param $description
     * @param $uuid
     */
    public function __construct($id, $subject, $description, $uuid, $linkId)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->description = $description;
        $this->uuid = $uuid;
        $this->linkId = $linkId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('notification.'. $this->id );
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'NotificationEvent';
    }
}
