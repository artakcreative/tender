<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationUserEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userId;
    public $subject;
    public $description;
    public $linkId;
    public $uuid;
    public $time;
    private $user_name;
    private $user_surname;


    /**
     * GlobalUserNotification constructor.
     * @param $userId
     * @param $subject
     * @param $description
     * @param $linkId
     * @param $time
     * @param $uuid
     */
    public function __construct($userId, $subject, $description, $linkId, $time, $uuid)
    {
        $this->userId = $userId;
        $this->subject = $subject;
        $this->description = $description;
        $this->linkId = $linkId;
        $this->time = $time;
        $this->uuid = $uuid;
;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn()
    {
        return new PrivateChannel('notification-user.'. $this->userId);
    }

    /**
     * @return string
     */
    public function broadcastAs(): string
    {
        return 'NotificationUserEvent';
    }
}
