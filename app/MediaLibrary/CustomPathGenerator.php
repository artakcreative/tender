<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 01.12.2021
 * Time: 10:04
 */
namespace App\MediaLibrary;

use App\Models\User;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator;

class CustomPathGenerator implements PathGenerator
{
    /**
     * @param Media $media
     * @return string
     */
    public function getPath(Media $media): string
    {

        switch ($media->model_type){
            case $media->model_type;
                return strtolower(basename($media->model_type)).DIRECTORY_SEPARATOR.$media->created_at
                        ->format('Y').DIRECTORY_SEPARATOR.$media->created_at
                        ->format('m').DIRECTORY_SEPARATOR.$media->model_id.DIRECTORY_SEPARATOR.$media->id.DIRECTORY_SEPARATOR;
                   break;
            default:
                return $media->id.DIRECTORY_SEPARATOR;
        }
    }

    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media).'/conversions/';
    }

    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media).'/responsive-images/';
    }


}
