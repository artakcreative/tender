<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.12.2021
 * Time: 18:44
 */

namespace App\MediaLibrary;


use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\Support\FileNamer\FileNamer;

class CustomFileName extends FileNamer
{
    /**
     * @param string $fileName
     * @return string
     */
    public function originalFileName(string $fileName): string
    {
        return pathinfo(md5(now()->format('Y m d H:i:s').$fileName), PATHINFO_FILENAME);
    }

    /**
     * @param string $fileName
     * @param Conversion $conversion
     * @return string
     */
    public function conversionFileName(string $fileName, Conversion $conversion): string
    {

    }


    /**
     * @param string $fileName
     * @return string
     */
    public function responsiveFileName(string $fileName): string
    {
        // TODO: Implement responsiveFileName() method.
    }
}
