<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;


    Route::get('/','Profile\ProfileProjectController@index')->name('home');
    Route::get( '/register', function() {
        return redirect('/');
    });
    Route::get( '/login', function(){
        return redirect('/');
    });
    Route::post('/register', [RegisteredUserController::class, 'register'])->middleware('guest');
    Route::post('/login', [AuthenticatedSessionController::class, 'login'])->middleware(['guest','isUserBlocked']);
    Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])->name('verification.verify');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])->name('password.email');
    Route::get('/reset-password/{email}/{token}', [NewPasswordController::class, 'create'])->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])->name('password.update');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])->middleware('auth')->name('logout');









