<?php

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\PasswordResetLinkController;

Broadcast::routes(['middleware' => 'auth:admin']);

Route::namespace('Auth')->middleware('guest:admin')->group(function (){
    Route::get('login','AuthenticatedSessionController@create')->name('login');
    Route::post('login','AuthenticatedSessionController@store')->name('admin-login');
    Route::post('forgot-password',[PasswordResetLinkController::class, 'forgot'])
        ->middleware('guest')
        ->name('admin-forgot');
    Route::get('/reset-password/{email}/{token}', [PasswordResetLinkController::class, 'reset'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
});

Route::post('/logout', 'Auth\AuthenticatedSessionController@destroy')->name('logout');

Route::middleware('admin')->group(function (){
    Route::get('/dashboard','HomeController@index')->name('dashboard');
    Route::prefix('users')->group(function (){
        Route::get('/','UsersController@index')->name('users');
        Route::get('/create','UsersController@create')->name('users.create');
        Route::post('/store','UsersController@store')->name('users.store');
        Route::get('/edit/{id}', 'UsersController@edit')->name('users.edit');
        Route::get('/edit/profile/{id}', 'UsersController@editProfile')->middleware('editUser')->name('users.edit.profile');
        Route::get('/edit/plane/{id}', 'UsersController@editPlane')->name('users.edit.plane')->middleware('setDeadline');
        Route::put('/update/{id}', 'UsersController@update')->name('users.update')->middleware('editUser');
        Route::post('/data/blocked{id}', 'UsersController@dataBlocked')->name('users.data.blocked')->middleware('editUser');
        Route::post('/edit/plane/active/{id}', 'UsersController@activePlane')->name('users.active.plane');
        Route::post('/sign-in/{id}', 'UsersController@SignInByUser')->middleware('editUser')->name('users.sign-in');
//        Route::delete('/delete/{id}', 'UsersController@destroy')->name('users.delete');
        Route::delete('/delete-file/{id}', 'UsersController@destroyFile')->name('users.delete-file');
        Route::middleware('addManager')->group(function(){
            Route::get('/add-manager/{id}', 'UsersController@addManagerView')->name('users.add-manager');
            Route::post('/add-manager', 'UsersController@addManager')->name('users.manager.store');
        });
        Route::put('/block/{id}', 'UsersController@block')->name('users.block')->middleware('canBlockUser');
        Route::put('/unlock/{id}', 'UsersController@unlock')->name('users.unlock')->middleware('canBlockUser');
    });

    Route::prefix('settings')->group(function(){
        Route::get('/personal', 'SettingsController@editPersonalInfo')->name('settings.personal');
        Route::post('/update', 'SettingsController@updatePersonalInfo')->name('settings.update');
        Route::get('/password', 'SettingsController@editPassword')->name('settings.password');
        Route::post('/password/update', 'SettingsController@changePassword')->name('settings.password.update');
        Route::get('/contract', 'SettingsController@contract')->name('settings.add-contract-list');
        Route::post('/add-contract', 'SettingsController@addContract')->name('settings.add-contract');
        Route::get('/add_data_processing_policy', 'DataProcessingPolicy@createDataProcessingPolicy')->name('settings.data_processing_policy.create');
        Route::post('/add_data_processing_policy', 'DataProcessingPolicy@storeDataProcessingPolicy')->name('settings.data_processing_policy.store');
    });

    Route::prefix('managers')->group(function(){
        Route::get('/', 'ManagerController@index')->name('managers');
        Route::get('/create', 'ManagerController@create')->name('managers.create')->middleware('isAdmin');
        Route::post('/store', 'ManagerController@store')->name('managers.store')->middleware('isAdmin');
        Route::get('/edit/{id}', 'ManagerController@edit')->name('managers.edit')->middleware('isAdmin');
        Route::put('/update/{id}', 'ManagerController@update')->name('managers.update')->middleware('isAdmin');
        Route::put('/block/{id}', 'ManagerController@block')->name('managers.block')->middleware('isAdmin');
        Route::put('/unlock/{id}', 'ManagerController@unlock')->name('managers.unlock')->middleware('isAdmin');
    });

    Route::prefix('admins')->middleware('isAdmin')->group(function(){
        Route::get('/', 'AdminController@index')->name('admins');
        Route::get('/create', 'AdminController@create')->name('admins.create');
        Route::post('/store', 'AdminController@store')->name('admins.store');
    });

    Route::prefix('announcement')->group(function(){
        Route::get('/', 'AnnouncementController@index')->name('announcements');
        Route::get('/create', 'AnnouncementController@create')->name('announcements.create');
        Route::get('/edit/{id}', 'AnnouncementController@edit')->middleware('canEditPublishedAnnouncements')->middleware('archiveEdit')->name('announcements.edit');
        Route::post('/store', 'AnnouncementController@store')->name('announcements.store');
        Route::post('/update/{id}', 'AnnouncementController@update')->middleware('canEditPublishedAnnouncements')->name('announcements.update');
        Route::delete('/delete-file/{id}', 'AnnouncementController@destroyFile')->name('announcements.delete-file');
        Route::post('/add-manager/{id}', 'AnnouncementController@addManagerToAnnouncement')->name('announcements.add-manager');
        Route::get('/history/{id}', 'AnnouncementController@history')->name('announcements.history');
        Route::post('/getSubSphere',  'AnnouncementController@getSubSphere');
        Route::get('/saved', 'AnnouncementController@savedAnnouncements')->name('announcements.saved');
        Route::get('/archive', 'AnnouncementController@archivedAnnouncements')->name('announcements.archived');
        Route::get('/search', 'AnnouncementController@searchAnnouncement')->name('announcements.search');
//        Route::get('/search', 'AnnouncementController@searchAnnouncement')->name('announcements.search');
//        Route::post('/publicate/{id}', 'AnnouncementController@publicateAnnouncement')->name('announcements.publicate');

    });

    Route::prefix('competition_settings')->group(function() {
        Route::get('/index/competition_type_and_application_form', 'CompetitionTypeAndApplicationFormController@index')->middleware('isAdmin')->name('competition_type_and_application_form');
        Route::get('/crate/competition_type', 'CompetitionTypeController@create')->middleware('isAdmin')->name('competition_type');
        Route::get('/edit/competition_type/{id}', 'CompetitionTypeController@edit')->middleware('isAdmin')->name('competition_type.edit');
        Route::post('/store/competition_type', 'CompetitionTypeController@store')->middleware('isAdmin')->name('competition_type.store');
        Route::post('/update/competition_type{id}', 'CompetitionTypeController@update')->middleware('isAdmin')->name('competition_type.update');
        Route::get('/crate/application_form', 'CompetitionApplicationFormController@create')->middleware('isAdmin')->name('application_form');
        Route::get('/edit/application_form/{id}', 'CompetitionApplicationFormController@edit')->middleware('isAdmin')->name('application_form.edit');
        Route::post('/store/application_form', 'CompetitionApplicationFormController@store')->middleware('isAdmin')->name('application_form.store');
        Route::post('/update/application_form/{id}', 'CompetitionApplicationFormController@update')->middleware('isAdmin')->name('application_form.update');
//        Route::post('/store/competition_type_and_application_form', 'CompetitionTypeAndApplicationFormController@store')->middleware('isAdmin')->name('competition_type_and_application_form.store');
        Route::get('/tariffs', 'TariffsController@index')->middleware('isAdmin')->name('tariff');
        Route::get('/tariffs/edit/{id}', 'TariffsController@editTariffPrice')->middleware('isAdmin')->name('tariff.edit');
        Route::put('/tariffs/update/{id}', 'TariffsController@updateTariffPrice')->middleware('isAdmin')->name('tariff.update');

//       Route::get('/customer_affiliation/create', 'CompetitionCustomerAffiliationController@create')->middleware('isAdmin')->name('customer_affiliation.create');
//       Route::post('/store/customer_affiliation', 'CompetitionCustomerAffiliationController@store')->middleware('isAdmin')->name('customer_affiliation.store');
       Route::get('/customers', 'CompetitionSettingsController@index')->middleware('canAddSettings')->name('customer');
       Route::get('/customers/create', 'CompetitionCustomerController@create')->middleware('canAddSettings')->name('customer.create');
        Route::post('/customers/store', 'CompetitionCustomerController@store')->middleware('canAddSettings')->name('customer.store');
        Route::get('/edit/customer/{id}', 'CompetitionCustomerController@edit')->middleware('canAddSettings')->name('customer.edit');
        Route::get('/delete/customer/{id}', 'CompetitionCustomerController@delete')->middleware('canAddSettings')->name('customer.delete');
        Route::get('/history/customer', 'CompetitionCustomerController@customerHistory')->name('customer.history');
        Route::put('/update/customer/{id}', 'CompetitionCustomerController@update')->middleware('canAddSettings')->name('customer.update');
        Route::get('/customers/data', 'CompetitionCustomerController@getCustomerData');
        Route::get('/sphere', 'CompetitionSphereController@index')->middleware('canAddSettings')->name('sphere');
        Route::get('/spheres/data', 'CompetitionSphereController@getSphereData');
        Route::get('/sub_spheres/data', 'CompetitionSphereController@getSubSphereData');
        Route::get('/sphere/create', 'CompetitionSphereController@create')->middleware('canAddSettings')->name('sphere.create');
        Route::Post('/store/sphere', 'CompetitionSphereController@store')->middleware('canAddSettings')->name('sphere.store');

        Route::prefix('contact-us')->group(function(){
            Route::get('/', 'ContactUsController@index')->name('contact-us');
            Route::get('/create', 'ContactUsController@create')->name('contact-us.create');
            Route::post('/store', 'ContactUsController@store')->name('contact-us.store');
            Route::get('/edit/{id}', 'ContactUsController@edit')->name('contact-us.edit');
            Route::post('/update/{id}', 'ContactUsController@update')->name('contact-us.update');
        });

    });

    Route::prefix('profile')->middleware('isAdmin')->group(function(){
        Route::prefix('header')->group(function(){
            Route::get('/', 'ProfileController@header')->name('profile.header');
            Route::post('/create', 'ProfileController@headerCreate')->name('profile.header.create');
            Route::post('/delete', 'ProfileController@headerDelete')->name('profile.header.delete');
        });

        Route::prefix('advantage')->group(function(){
            Route::get('/', 'ProfileController@advantage')->name('profile.advantage');
            Route::post('/create', 'ProfileController@advantageCreate')->name('profile.advantage.create');
            Route::get('/edit/{id}', 'ProfileController@advantageEdit')->name('profile.advantage.edit');
            Route::post('/update', 'ProfileController@advantageUpdate')->name('profile.advantage.update');
            Route::post('/delete', 'ProfileController@advantageDelete')->name('profile.advantage.delete');
        });

        Route::prefix('connect')->group(function(){
            Route::get('/', 'ProfileController@connect')->name('profile.connect');
            Route::post('/create', 'ProfileController@connectCreate')->name('profile.connect.create');
            Route::get('/edit/{id}', 'ProfileController@connectEdit')->name('profile.connect.edit');
            Route::post('/update', 'ProfileController@connectUpdate')->name('profile.connect.update');
            Route::post('/delete', 'ProfileController@connectDelete')->name('profile.connect.delete');
        });

        Route::prefix('about')->group(function(){
            Route::get('/', 'ProfileController@about')->name('profile.about');
            Route::post('/create', 'ProfileController@aboutCreate')->name('profile.about.create');
            Route::get('/edit/{id}', 'ProfileController@aboutEdit')->name('profile.about.edit');
            Route::post('/update', 'ProfileController@aboutUpdate')->name('profile.about.update');
            Route::post('/delete', 'ProfileController@aboutDelete')->name('profile.about.delete');
        });

        Route::prefix('material')->group(function(){
            Route::get('/', 'ProfileController@material')->name('profile.material');
            Route::post('/create', 'ProfileController@materialCreate')->name('profile.material.create');
            Route::get('/edit/{id}', 'ProfileController@materialEdit')->name('profile.material.edit');
            Route::post('/update', 'ProfileController@materialUpdate')->name('profile.material.update');
            Route::post('/delete', 'ProfileController@materialDelete')->name('profile.material.delete');
        });

        Route::prefix('info')->group(function(){
            Route::get('/', 'ProfileController@info')->name('profile.info');
            Route::post('/create', 'ProfileController@infoCreate')->name('profile.info.create');
            Route::post('/delete', 'ProfileController@infoDelete')->name('profile.info.delete');
        });

        Route::prefix('description')->group(function(){
            Route::get('/', 'ProfileController@description')->name('profile.description');
            Route::post('/create', 'ProfileController@descriptionCreate')->name('profile.description.create');
            Route::post('/delete', 'ProfileController@descriptionDelete')->name('profile.description.delete');
        });
    });

//    Route::prefix('competition_settings')->middleware('canAddSettings')->group(function(){
//        Route::get('/create', 'CompetitionSettingsController@create')->name('settings');
//        Route::post('/store', 'CompetitionSettingsController@store')->name('competition_settings.store');
//            Route::post('/getSubsphere',  'CompetitionSettingsController@getSubsphere')->name('getSubsphere');
//    });

    Route::prefix('notification')->group(function(){
        Route::get('/read/{id}', 'NotificationController@read')->name('notification.read');
        Route::post('/read/all', 'NotificationController@readAll')->name('notification.readAll');
    });

    Route::prefix('statistics')->group(function(){
        Route::prefix('users')->group(function(){
            Route::get('/', 'StatisticsController@getUsers')->name('statistics');
            Route::get('/data', 'StatisticsController@getUsersData')->name('statistics.user.data');
            Route::get('/statistic', 'StatisticsController@getUserStatistic')->name('statistics.user');
        });
        Route::prefix('managers')->group(function(){
            Route::get('/', 'StatisticsController@getManagers')->name('statistics.managers');
            Route::get('/data', 'StatisticsController@getManagersData')->name('statistics.manager.data');
            Route::get('/statistic', 'StatisticsController@getManagerStatistic')->name('statistics.manager');
        });
        Route::prefix('announcements')->group(function(){
            Route::get('/', 'StatisticsController@getAnnouncements')->name('statistics.announcements');
            Route::get('/statistic', 'StatisticsController@getAnnouncementStatistic')->name('statistics.announcement');
        });

    });

    Route::prefix('help')->group(function(){
        Route::get('/', 'HelpController@index')->name('help');
        Route::group(['middleware' => 'canCreateUpdateDeleteHelp'], function () {
            Route::post('/create', 'HelpController@helpCreate')->name('help.create');
            Route::post('/delete', 'HelpController@helpDelete')->name('help.delete');
        });
    });



});
