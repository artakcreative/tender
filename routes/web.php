<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'Profile\ProfileProjectController@index')->name('home');
Route::get('/register', function () {
    return redirect('/');
});
Route::get('/login', function () {
    return redirect('/');
});

Route::post('/register',  'Auth\RegisteredUserController@register')->middleware('guest');
Route::post('/login',  'Auth\AuthenticatedSessionController@login')->middleware(['guest', 'isUserBlocked'])->name('login');
Route::get('/verify-email/{id}/{hash}',  'Auth\VerifyEmailController@__invoke')->name('verification.verify');
Route::post('/forgot-password',  'Auth\PasswordResetLinkController@store')->name('password.email');
Route::get('/reset-password/{email}/{token}',  'Auth\NewPasswordController@create')->name('password.reset');
Route::post('/reset-password',  'Auth\NewPasswordController@store')->name('password.update');
Route::post('/logout', 'Auth\AuthenticatedSessionController@destroy')->middleware('auth')->name('logout');
Route::get('/data-processing-policy',  'Auth\RegisteredUserController@dataProcessingPolicy')->name('data-processing-policy');
Route::post('/select-announcement-by-sphere', 'Admin\AnnouncementController@selectActiveAndArchivedAnnouncementsBySpheres');
Route::group(['middleware' => ['auth', 'isUserBlocked'], 'as' => 'user.'], function () {
    Route::get('/check/spheres', 'Profile\CompetitionController@index')->name('check.competitions');
    Route::get('/subcategories', 'Profile\CompetitionController@getSubcategories')->name('subcategories');
    Route::post('/save/sphere', 'Profile\CompetitionController@saveSphere')->name('save.sphere');
    Route::middleware('userNotSpheres')->group(function () {
        Route::get('/dashboard', 'Profile\ProfileUserController@index')->name('personal');
        Route::get('/report', 'Profile\ProfileUserController@report')->name('report');
        Route::get('/saved', 'Profile\ProfileUserController@saved')->name('saved');
        Route::get('/report/statistic', 'Profile\ProfileUserController@reportStatistic')->name('report-statistic');

        Route::prefix('notification')->group(function () {
            Route::get('/', 'Profile\Personal\NotificationController@index')->name('notification');
            Route::post('/delete', 'Profile\Personal\NotificationController@delete')->name('notification.delete');
            Route::get('/not-read', 'Profile\Personal\NotificationController@notRead')->name('notification.not-read');
        });

        Route::prefix('tenders')->group(function () {
            Route::get('/', 'Profile\Personal\TendersController@index')->name('tenders');
            Route::post('/', 'Profile\Personal\TendersController@index')->name('tenders');
            Route::post('/details', 'Profile\Personal\TendersController@details')->name('tenders.details');
            Route::get('/details', 'Profile\Personal\TendersController@details')->name('tenders.details');
//            Route::get('/details/{id}', 'Profile\Personal\TendersController@details')->name('tenders.details');
            Route::post('/add-delete-favorite', 'Profile\Personal\TendersController@addDeleteFavoriteTender')->name('tenders.add-delete-favorite');
        });

        Route::prefix('person')->name('person.')->group(function () {
            Route::get('/settings', 'Profile\Personal\PersonSettingsController@index')->name('settings');
            Route::post('/update/password', 'Profile\Personal\PersonSettingsController@passwordUpdate')->name('password.update');
            Route::post('/update/profile', 'Profile\Personal\PersonSettingsController@profileUpdate')->name('profile.update');
            Route::delete('/delete-file/{id}', 'Profile\Personal\PersonSettingsController@destroyFile')->name('delete-file');
        });

        Route::prefix('settings')->group(function () {
            Route::get('/tariff', 'Profile\Personal\SettingsController@getTariff')->name('settings.tariff');
            Route::get('/spheres/selected/payed', 'Profile\Personal\SettingsController@getPayedSelectedSpheres')->name('settings.selected-payed.spheres');
            Route::get('/spheres/selected', 'Profile\Personal\SettingsController@getSelectedSpheres')->name('settings.selected.spheres');
            Route::get('/spheres', 'Profile\Personal\SettingsController@getSpheres')->name('settings.spheres');
            Route::post('/delete/not-payed', 'Profile\Personal\SettingsController@deleteNotPayed')->name('settings.delete.not-payed');
            Route::post('/save', 'Profile\Personal\SettingsController@save')->name('settings.save');
            Route::post('/filter/save', 'Profile\Personal\SettingsController@filterSave')->name('settings.filter.save');
            Route::get('/{map?}', 'Profile\Personal\SettingsController@index')->name('settings');
        });
    });
});




